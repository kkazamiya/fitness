<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class BlackList extends Eloquent
{
    //
    protected $collection = 'blacklists';
    protected $primarykey = "_id";
    protected $dates = ['created_at', 'updated_at'];
}
