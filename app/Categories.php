<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Categories extends Eloquent
{
    //
    use Sluggable;
    protected $collection = 'categories';
    protected $primarykey = "_id";
    protected $dates = ['created_at', 'updated_at'];

    public function sluggable()
    {
        return [
            'category_slug' => [
                'source' => 'category_name'
            ]
        ];
    }
}
