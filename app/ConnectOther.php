<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class ConnectOther extends Eloquent
{
    //
    protected $collection = 'connect_other';
    protected $primarykey = "_id";
    protected $dates = ['created_at', 'updated_at'];


}
