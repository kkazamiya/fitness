<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use App\Posts;
use App\RunTask;

use MongoDB\BSON\ObjectId;

class RunTaskSchedule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runtask:yes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run task schedule';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time = strtotime(date('Y/m/d h:i:s'));
        $schedule = RunTask::all();
        $task = collect($schedule)
            ->where('schedule', '<', $time)
            ->where('entity_type', '=', 'post')
            ->where('status', '=', '0')
            ->pluck('entity_id');
        $post = Posts::whereIn('_id',$task)
            ->update(['status' => '1']);
        $schedule = RunTask::whereIn('_id',$task)
            ->update(['status' => '1']);
    }
}
