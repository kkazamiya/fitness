<?php

namespace App\Console\Commands;

use App\Posts;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class SitemapBlogs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:blogs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sitemap Blogs, Index list_blogs';
    private $type_info = [
        'url_host' => "https://www.fitnessol.com",
        'page_name' => "blogs-",
        'map_index' => "list_blogs"
    ];
    private $url_per_sitemap = 5000;
    private $url_per_txt = 5000;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $blogs = Posts::select('type', 'slug')->get();
        $make_sitemap = App::make('sitemap');
        $counter = 0;
        $sitemapCounter = 0;
        $urls_txt = 0;
        $page_txt = 0;
        $content_sitemap = "";
        try {
            foreach ($blogs as $index => $blog) {
                if ($counter == $this->url_per_sitemap) {
                    // generate new sitemap file
                    $make_sitemap->store('xml', '/map_fitness/' . $this->type_info['page_name'] . $sitemapCounter);
                    // add the file to the sitemaps array
                    $make_sitemap->addSitemap(secure_url('/map_fitness/' . $this->type_info['page_name'] . $sitemapCounter . '.xml'));
                    // reset items array (clear memory)
                    $make_sitemap->model->resetItems();
                    // reset the counter
                    $counter = 0;
                    // count generated sitemap
                    $sitemapCounter++;
                    print ('Create Sitemap XML:' . $this->type_info['page_name'] . $sitemapCounter . '.xml' . PHP_EOL);
                }
                /*Sitemap TXT*/
                if ($blog->type == 'post') {
                    $content_sitemap .= route('blog', ['slug' => $blog['slug']]) . PHP_EOL;
                // } elseif ($blog->type == 'company') {
                //     $content_sitemap .= route('company', ['slug' => $blog->slug]) . PHP_EOL;
                } else {
                    continue;
                }
                $urls_txt++;
                if ($urls_txt % $this->url_per_txt == 0) {
                    /*New Sitemap TXT*/
                    $page_txt++;
                    $urls_txt = 0;
                    $path_txt = '/map_fitness/' . $this->type_info['page_name'] . $page_txt . '.txt';
                    Storage::disk('public')->put($path_txt, "\xEF\xBB\xBF" . $content_sitemap);
                    $content_sitemap = "";
                    print ('Create Sitemap TXT:' . $this->type_info['page_name'] . $page_txt . '.txt' . PHP_EOL);
                }
                /*End Sitemap TXT*/
                if ($blog->type == 'post') {
                    $make_sitemap->add(route('blog', ['slug' => $blog['slug']]), gmdate('Y-m-d\TH:i:s\Z', strtotime('now')), '0.9', 'always');
                // } elseif ($blog->slug == 'company') {
                //     $make_sitemap->add(route('blog', ['slug' => $blog->slug]), gmdate('Y-m-d\TH:i:s\Z', strtotime('now')), '0.9', 'always');
                } else {
                    continue;
                }
                // count number of elements
                $counter++;
            }
            // you need to check for unused items
            if (!empty($make_sitemap->model->getItems())) {
                // generate sitemap with last items
                $make_sitemap->store('xml', '/map_fitness/' . $this->type_info['page_name'] . $sitemapCounter);
                // add sitemap to sitemaps array
                $make_sitemap->addSitemap(secure_url('/map_fitness/' . $this->type_info['page_name'] . $sitemapCounter . '.xml'));
                // reset items array
                $make_sitemap->model->resetItems();
            }
            /*Add To Sitemap TXT index*/
            $last_page = $page_txt + 1;
            $path_txt = '/map_fitness/' . $this->type_info['page_name'] . $last_page . '.txt';
            Storage::disk('public')->put($path_txt, "\xEF\xBB\xBF" . $content_sitemap);
            $content_index = '<?xml version="1.0" encoding="UTF-8"?>
                <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL;
            for ($i = 1; $i <= $last_page; $i++) {
                $content_index .= '<sitemap><loc>' . secure_url('/storage/map_fitness/' . $this->type_info['page_name'] . $i . '.txt') . '</loc></sitemap>' . PHP_EOL;
            }
            Storage::disk('public')->put('/map_fitness/' . $this->type_info['map_index'] . '.xml', "\xEF\xBB\xBF" . $content_index . "</sitemapindex>");
            print ('Create Sitemap INDEX TXT:' . $this->type_info['page_name'] . $last_page . '.txt' . PHP_EOL);
            /*End Sitemap TXT Index*/
            $make_sitemap->store('sitemapindex', '/map_fitness/' . $this->type_info['map_index']);
            if (File::exists(public_path() . '/map_fitness/' . $this->type_info['map_index'] . '.xml')) {
                chmod(public_path() . '/map_fitness/' . $this->type_info['map_index'] . '.xml', 0777);
            }
            $txt_stats['coupon_map'] = ['latest_txt' => $urls_txt, 'latest_page' => $page_txt];
            print_r("Created:" . $this->type_info['map_index'] . "-" . $sitemapCounter . ".xml" . PHP_EOL);
            print ("Save position Blogs: page-" . $sitemapCounter . " count:" . $counter);
        } catch (\Exception $exception) {
            print $exception->getMessage();
            print $exception->getFile();
        }
    }
}
