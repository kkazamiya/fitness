<?php

namespace App\Console\Commands;

use App\Careers;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class SitemapCareers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:careers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sitemap Careers, Index list_careers';
    private $type_info = [
        'url_host' => "https://www.onlinejobe.com",
        'page_name' => "careers-",
        'map_index' => "list_careers"
    ];
    private $url_per_sitemap = 5000;
    private $url_per_txt = 5000;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $datum = Careers::select('slug')->get();
        $make_sitemap = App::make('sitemap');
        $counter = 0;
        $sitemapCounter = 0;
        $urls_txt = 0;
        $page_txt = 0;
        $content_sitemap = "";
        try {
            foreach ($datum as $index => $item) {
                if ($counter == $this->url_per_sitemap) {
                    // generate new sitemap file
                    $make_sitemap->store('xml', '/map_jobs/' . $this->type_info['page_name'] . $sitemapCounter);
                    // add the file to the sitemaps array
                    $make_sitemap->addSitemap(secure_url('/map_jobs/' . $this->type_info['page_name'] . $sitemapCounter . '.xml'));
                    // reset items array (clear memory)
                    $make_sitemap->model->resetItems();
                    // reset the counter
                    $counter = 0;
                    // count generated sitemap
                    $sitemapCounter++;
                    print ('Create Sitemap XML:' . $this->type_info['page_name'] . $sitemapCounter . '.xml' . PHP_EOL);
                }
                /*Sitemap TXT*/
                $content_sitemap .= route('career', ['slug' => $item->slug]) . PHP_EOL;
                $urls_txt++;
                if ($urls_txt % $this->url_per_txt == 0) {
                    /*New Sitemap TXT*/
                    $page_txt++;
                    $urls_txt = 0;
                    $path_txt = '/map_jobs/' . $this->type_info['page_name'] . $page_txt . '.txt';
                    Storage::disk('public')->put($path_txt, "\xEF\xBB\xBF" . $content_sitemap);
                    $content_sitemap = "";
                    print ('Create Sitemap TXT:' . $this->type_info['page_name'] . $page_txt . '.txt' . PHP_EOL);
                }
                /*End Sitemap TXT*/
                $make_sitemap->add(route('career', ['slug' => $item->slug]), gmdate('Y-m-d\TH:i:s\Z', strtotime('now')), '0.9', 'always');
                // count number of elements
                $counter++;
            }
            // you need to check for unused items
            if (!empty($make_sitemap->model->getItems())) {
                // generate sitemap with last items
                $make_sitemap->store('xml', '/map_jobs/' . $this->type_info['page_name'] . $sitemapCounter);
                // add sitemap to sitemaps array
                $make_sitemap->addSitemap(secure_url('/map_jobs/' . $this->type_info['page_name'] . $sitemapCounter . '.xml'));
                // reset items array
                $make_sitemap->model->resetItems();
            }
            /*Add To Sitemap TXT index*/
            $last_page = $page_txt + 1;
            $path_txt = '/map_jobs/' . $this->type_info['page_name'] . $last_page . '.txt';
            Storage::disk('public')->put($path_txt, "\xEF\xBB\xBF" . $content_sitemap);
            $content_index = '<?xml version="1.0" encoding="UTF-8"?>
                <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL;
            for ($i = 1; $i <= $last_page; $i++) {
                $content_index .= '<sitemap><loc>' . secure_url('/storage/map_jobs/' . $this->type_info['page_name'] . $i . '.txt') . '</loc></sitemap>' . PHP_EOL;
            }
            Storage::disk('public')->put('/map_jobs/' . $this->type_info['map_index'] . '.xml', "\xEF\xBB\xBF" . $content_index . "</sitemapindex>");
            print ('Create Sitemap INDEX TXT:' . $this->type_info['page_name'] . $last_page . '.txt' . PHP_EOL);
            /*End Sitemap TXT Index*/
            $make_sitemap->store('sitemapindex', '/map_jobs/' . $this->type_info['map_index']);
            if (File::exists(public_path() . '/map_jobs/' . $this->type_info['map_index'] . '.xml')) {
                chmod(public_path() . '/map_jobs/' . $this->type_info['map_index'] . '.xml', 0777);
            }
            $txt_stats['coupon_map'] = ['latest_txt' => $urls_txt, 'latest_page' => $page_txt];
            print_r("Created:" . $this->type_info['map_index'] . "-" . $sitemapCounter . ".xml" . PHP_EOL);
            print ("Save position Careers: page-" . $sitemapCounter . " count:" . $counter);
        } catch (\Exception $exception) {
            print $exception->getMessage();
            print $exception->getFile();
        }
    }
}
