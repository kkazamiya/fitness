<?php

namespace App\Console\Commands;

use App\Keywords;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class Sitemapv2KW extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:keyword';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sitemap v2 Create TXT - XML Map';

    private $type_info = [
        'keyword' => [
            'url_host' => "https://www.fitnessol.com",
            'page_name' => "keyword-",
            'map_index' => "keyword"]
    ];
    private $url_per_sitemap = 5000;
    private $url_per_txt = 5000;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $keywords = Keywords::select('keyword_slug')->where('tracked', '!=', 0)->get();
        $sitemap_kw = App::make('sitemap');
        $counter = 0;
        $sitemapCounter = 0;
        $urls_txt = 0;
        $page_txt = 0;

        $type_map = $this->type_info['keyword'];
        $content_sitemap = "";
        try {
            foreach ($keywords as $index => $keyword) {
                if ($counter == $this->url_per_sitemap) {
                    // generate new sitemap file
                    $sitemap_kw->store('xml', 'map_fitness/' . $type_map['page_name'] . $sitemapCounter);
                    // add the file to the sitemaps array
                    $sitemap_kw->addSitemap(secure_url('/map_fitness/' . $type_map['page_name'] . $sitemapCounter . '.xml'));
                    // reset items array (clear memory)
                    $sitemap_kw->model->resetItems();
                    // reset the counter
                    $counter = 0;
                    // count generated sitemap
                    $sitemapCounter++;
                    print ('Create Sitemap XML:' . $type_map['page_name'] . $sitemapCounter . '.xml' . PHP_EOL);
                }
                /*Sitemap TXT*/
                $content_sitemap .= route('v_keyword', ['slug' => $keyword['keyword_slug']]) . PHP_EOL;
                $urls_txt++;
                if ($urls_txt % $this->url_per_txt == 0) {
                    /*New Sitemap TXT*/
                    $page_txt++;
                    $urls_txt = 0;
                    $path_txt = '/map_fitness/' . $type_map['page_name'] . $page_txt . '.txt';
                    Storage::disk('public')->put($path_txt, "\xEF\xBB\xBF" . $content_sitemap);
                    $content_sitemap = "";
                    print ('Create Sitemap TXT:' . $type_map['page_name'] . $page_txt . '.txt' . PHP_EOL);
                }
                /*End Sitemap TXT*/
                $sitemap_kw->add(route('v_keyword', ['slug' => $keyword['keyword_slug']]), gmdate('Y-m-d\TH:i:s\Z', strtotime('now')), '0.9', 'always');
                // count number of elements
                $counter++;
            }
            // you need to check for unused items
            if (!empty($sitemap_kw->model->getItems())) {
                // generate sitemap with last items
                $sitemap_kw->store('xml', '/map_fitness/' . $type_map['page_name'] . $sitemapCounter);
                // add sitemap to sitemaps array
                $sitemap_kw->addSitemap(secure_url('/map_fitness/' . $type_map['page_name'] . $sitemapCounter . '.xml'));
                // reset items array
                $sitemap_kw->model->resetItems();
            }
            /*Add To Sitemap TXT index*/
            $last_page = $page_txt + 1;
            $path_txt = '/map_fitness/' . $type_map['page_name'] . $last_page . '.txt';
            Storage::disk('public')->put($path_txt, "\xEF\xBB\xBF" . $content_sitemap);
            $content_index = '<?xml version="1.0" encoding="UTF-8"?>
                <sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL;
            for ($i = 1; $i <= $last_page; $i++) {
                $content_index .= '<sitemap><loc>' . secure_url('/storage/map_fitness/' . $type_map['page_name'] . $i . '.txt') . '</loc></sitemap>' . PHP_EOL;
            }
            Storage::disk('public')->put('/map_fitness/' . $type_map['map_index'] . '.xml', "\xEF\xBB\xBF" . $content_index . "</sitemapindex>");
            print ('Create Sitemap INDEX TXT:' . $type_map['page_name'] . $last_page . '.txt' . PHP_EOL);
            /*End Sitemap TXT Index*/
            $sitemap_kw->store('sitemapindex', '/map_fitness/' . $type_map['map_index']);
            if (File::exists(public_path() . '/map_fitness/' . $type_map['map_index'] . '.xml')) {
                chmod(public_path() . '/map_fitness/' . $type_map['map_index'] . '.xml', 0777);
            }
            $txt_stats['coupon_map'] = ['latest_txt' => $urls_txt, 'latest_page' => $page_txt];
            print_r("Created:" . $type_map['map_index'] . "-" . $sitemapCounter . ".xml" . PHP_EOL);
            print ("Save position keywords: page-" . $sitemapCounter . " count:" . $counter);
        } catch (\Exception $exception) {
            print $exception->getMessage();
            print $exception->getFile();
        }
        return true;
    }
}
