<?php

namespace App\Console;

use App\Console\Commands\Sitemapv2KW;
use App\Console\Commands\SitemapBlogs;
use App\Console\Commands\RunTaskSchedule;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Sitemapv2KW::class,
        Commands\SitemapBlogs::class,
        Commands\RunTaskSchedule::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //Even Days at 03:00AM Create Sitemap /stores
        $log_schedule = public_path('log_schedule_task' . date('Y-m-d') . '.txt');
        $schedule->command("runtask:yes")
            ->cron('*/5 * * * *')
            ->timezone('America/Los_Angeles')
            ->sendOutputTo($log_schedule);
        //Even Days at 03:00AM Create Sitemap /stores
        $log_schedule = public_path('log_schedule_' . date('Y-m-d') . '.txt');
        $schedule->command("sitemap:blogs")
            ->cron('0 2 */2 * *')
            ->timezone('America/Los_Angeles')
            ->sendOutputTo($log_schedule);
        //Even Days at 03:45AM Create Sitemap /stores
        $schedule->command("sitemap:keyword")
            ->cron('0 3 */2 * *')
            ->timezone('America/Los_Angeles')
            ->appendOutputTo($log_schedule)
            ->withoutOverlapping();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
