<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Coupons extends Eloquent
{
    //

    protected $collection = 'coupons';
    protected $dates = ['created_at', 'updated_at'];
    protected $primarykey = "_id";
    protected $guarded = [];
    
    public function stores()
    {
        /*foreign key: khóa ngoại đối chiếu của coupons
        otherKey: khóa ngoại đối chiếu của stores
        */
        return $this->belongsTo('App\Stores', 'forStore', 'slug_store');
    }
}
