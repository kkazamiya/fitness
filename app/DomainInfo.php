<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class DomainInfo extends Eloquent
{
    //
    use Sluggable;
    protected $collection = 'domain_info';
    protected $primarykey = "_id";
    protected $dates = ['created_at', 'updated_at'];

    public function sluggable()
    {
        return [
            'domain_slug' => [
                'source' => 'domain'
            ]
        ];
    }
}
