<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Emails extends Eloquent
{
    protected $collection = 'emails';
    protected $dates = ['created_at', 'updated_at'];
    protected $primarykey = "_id";
    protected $guarded = [];
}
