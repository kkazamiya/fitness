<?php

namespace App\Http\Controllers;

use App\KeywordBlack;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\BlackList;
use Illuminate\Support\Facades\Cache;

class BlacklistController extends Controller
{
    //
    private $cache_sort = 5;
    private $cache_hour = 60;
    private $cache_daily = 24 * 60;
    private $cache_month = 30 * 24 * 60;

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('cpanel.blacklist.index_table');
    }

    protected function show_svs(Request $request)
    {
        $columns = array(
            0 => 'black_domain',
            1 => 'created_at',
            2 => 'options',
        );


        $limit = intval($request->input('length'));
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $hash = md5($limit . $start . $order . $dir . "black_list");

        $totalData = Cache::remember('totalData' . $hash, $this->cache_sort, function () {
            return BlackList::count();
        });

        $totalFiltered = $totalData;

        if (empty($request->input('search.value'))) {
            $posts = Cache::remember('posts' . $hash, $this->cache_sort, function () use ($start, $limit, $order, $dir) {
                return BlackList::offset(intval($start))
                    ->take($limit)
                    ->orderBy($order, $dir)
                    ->get();
            });
        } else {
            $search = $request->input('search.value');
            $hash = md5($limit . $start . $order . $dir . $search . "cat_list");
            $posts = Cache::remember('posts_s' . $hash, $this->cache_sort, function () use ($search, $start, $limit, $order, $dir) {
                return BlackList::where('black_domain', 'LIKE', "%{$search}%")
                    ->offset(intval($start))
                    ->take($limit)
                    ->orderBy($order, $dir)
                    ->get();
            });
            $totalFiltered = Cache::remember('totalFiltered' . $hash, $this->cache_sort, function () use ($search) {
                return BlackList::where('black_domain', 'LIKE', "%{$search}%")
                    ->count();
            });
        }
        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $nestedData['_id'] = $post['_id'];
                if (isset($post['editor'])) {
                    $nestedData['black_domain'] = $post['black_domain'] . "<br/>Removed by:" . $post['editor'];
                } else {
                    $nestedData['black_domain'] = $post['black_domain'] . "<br/>Removed by: System";
                }
                $nestedData['created_at'] = 'Created at: ' . Carbon::parse($post['created_at'])->format('Y-m-d H:m:s') . '<br/>Updated at:' . Carbon::parse($post['updated_at'])->format('Y-m-d H:m:s');
                $nestedData['options'] = '<a href="' . route('cpanel.blacklist.edit', ['id' => $post['_id']]) . '"><span class="label label-primary"><i class="fa fa-edit"></i></span></a>
                <a href="' . route('cpanel.blacklist.destroy', $post['_id']) . '" data-id="' . $post['_id'] . '" data-name="' . $post['black_domain'] . '" data-method="delete" class="jquery-postback"><span class="label label-danger"><i class="fa fa-times-circle"></i></span></a>';

                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
            "hash" => $hash,
        );

        echo json_encode($json_data);
    }

    protected function show_svs_kw(Request $request)
    {
        $columns = array(
            0 => 'keyword',
            1 => 'created_at',
            2 => 'options',
        );


        $limit = intval($request->input('length'));
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $hash = md5($limit . $start . $order . $dir . "black_list");

        $totalData = Cache::remember('totalData' . $hash, $this->cache_sort, function () {
            return KeywordBlack::count();
        });

        $totalFiltered = $totalData;

        if (empty($request->input('search.value'))) {
            $posts = Cache::remember('black_kw' . $hash, $this->cache_sort, function () use ($start, $limit, $order, $dir) {
                return KeywordBlack::offset(intval($start))
                    ->take($limit)
                    ->orderBy($order, $dir)
                    ->get();
            });
        } else {
            $search = $request->input('search.value');
            $hash = md5($limit . $start . $order . $dir . $search . "cat_list");
            $posts = Cache::remember('black_kw' . $hash, $this->cache_sort, function () use ($search, $start, $limit, $order, $dir) {
                return KeywordBlack::where('keyword', $search)
                    ->offset(intval($start))
                    ->take($limit)
                    ->orderBy($order, $dir)
                    ->get();
            });
            $totalFiltered = Cache::remember('totalFiltered_black_kw' . $hash, $this->cache_sort, function () use ($search) {
                return KeywordBlack::where('keyword', $search)
                    ->count();
            });
        }
        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $nestedData['_id'] = $post['_id'];
                if (isset($post['editor'])) {
                    $nestedData['keyword'] = $post['keyword'] . "<br/>Removed by:" . $post['editor'];
                } else {
                    $nestedData['keyword'] = $post['keyword'] . "<br/>Removed by: System";
                }
                $nestedData['created_at'] = 'Created at: ' . Carbon::parse($post['created_at'])->format('Y-m-d H:m:s') . '<br/>Updated at:' . Carbon::parse($post['updated_at'])->format('Y-m-d H:m:s');
                $nestedData['options'] = 'No Support';

                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
            "hash" => $hash,
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        return view('cpanel.blacklist.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $blacklist = $request->input('blacklist');
        $domains = preg_split('/\r\n|[\r\n]/', $blacklist);
        foreach ($domains as $domain) {
            $host = parse_url($domain)['host'];
            $exits = BlackList::where('black_domain', $host)->count();
            if ($exits == 0) {
                $new_keyword = new BlackList;
                $new_keyword->black_domain = $host;
                $new_keyword->save();
            }
        }
        $msg = "Created New Black Domain: " . $blacklist;
        return redirect()->route('cpanel.blacklist.index', ['msg' => $msg]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        //
        $editor = BlackList::find($id);
        return view('cpanel.blacklist.edit', compact(array('editor')));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $editor = BlackList::find($id);
        if ($editor) {
            $editor->black_domain = trim($request->input('black_domain'));
            $editor->save();
        }
        $msg = "Updated";
        return redirect()->route('cpanel.blacklist.index', ['msg' => $msg]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        BlackList::find($id)->delete();
        $msg = "Deleted:" . $id . " And All Result";
        return response($msg);
    }

    protected function searchByName(Request $request)
    {
        $value = trim($request->value);
        $results = BlackList::where('black_domain', 'like', '%' . $value . '%')
            ->limit(15)
            ->get();
        return response()->json($results);
    }
}
