<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CacheController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    protected function _clearCache(Request $request)
    {
        if ($request->cache_option) {
            if ($request->cache_option == 'home') {
                Cache::forget('seo_config_h');
                Cache::forget('get_banner' . md5("branding-home"));
                Cache::forget('categories_top');
                Cache::forget('keyword_top');
                echo "Clear Cache";
                echo "Go Check:<a href='/'> Click </a>";
            }
            if ($request->cache_option == 'more-offers') {
                $hash = md5(trim($request->slug_cache) . "_more_offers");
                Cache::forget('more_offer' . $hash);
                Cache::forget('domain_info_n' . $hash);
                Cache::forget('get_banner' . md5("branding-more"));
                echo "Clear Cache";
                echo "Go Check:<a href='/more-offers/" . trim($request->slug_cache) . "'> Click </a>";
            }
            if ($request->cache_option == 'keyword') {
                $hash = md5(trim($request->slug_cache) . "go_page");
                echo Cache::forget('check_found' . $hash);
                echo Cache::forget('all_data' . $hash);
                echo Cache::forget('coupons' . $hash);
                echo Cache::forget('ads_other' . $hash);
                echo Cache::forget('get_banner' . md5("branding-kw"));
                echo "Clear Cache<br/>";
                echo "Go Check:<a href='/" . trim($request->slug_cache) . "'> Click </a>";
            }
            if ($request->cache_option == 'category') {
                $category_slug = trim(strtolower($request->slug_cache));
                $hash = md5($category_slug);
                echo Cache::forget('category' . $hash);
                echo Cache::forget('stores' . $hash);
                echo "Clear Cache<br/>";
                echo "Go Check:<a href='/category/" . $category_slug . "'> Click </a>";
            }
        } else {
            return view('cpanel.cache_clear');
        }
    }
}
