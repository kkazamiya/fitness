<?php

namespace App\Http\Controllers;

use App\Categories;
use App\KeywordCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CategoriesController extends Controller
{
    private $cache_sort = 5;
    private $cache_hour = 60;
    private $cache_daily = 24 * 60;
    private $cache_month = 30 * 24 * 60;

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cpanel.categories.index_table');
    }

    protected function show_svs(Request $request)
    {
        $columns = array(
            0 => 'category_name',
            1 => 'created_at',
            2 => 'options',
        );


        $limit = intval($request->input('length'));
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $hash = md5($limit . $start . $order . $dir . "cat_list");

        $totalData = Cache::remember('totalData' . $hash, $this->cache_sort, function () {
            return Categories::count();
        });

        $totalFiltered = $totalData;

        if (empty($request->input('search.value'))) {
            $posts = Cache::remember('posts' . $hash, $this->cache_sort, function () use ($start, $limit, $order, $dir) {
                return Categories::offset(intval($start))
                    ->take($limit)
                    ->orderBy($order, $dir)
                    ->get();
            });
        } else {
            $search = $request->input('search.value');
            $hash = md5($limit . $start . $order . $dir . $search . "cat_list");
            $posts = Cache::remember('posts_s' . $hash, $this->cache_sort, function () use ($search, $start, $limit, $order, $dir) {
                return Categories::where('_id', 'LIKE', "%{$search}%")
                    ->orWhere('category_name', 'LIKE', "%{$search}%")
                    ->offset(intval($start))
                    ->take($limit)
                    ->orderBy($order, $dir)
                    ->get();
            });
            $totalFiltered = Cache::remember('totalFiltered' . $hash, $this->cache_sort, function () use ($search) {
                return Categories::where('_id', 'LIKE', "%{$search}%")
                    ->orWhere('category_name', 'LIKE', "%{$search}%")
                    ->count();
            });
        }
        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $nestedData['_id'] = $post['_id'];
                $nestedData['category_name'] = '<a href="/browse/' . $post['category_slug'] . '" target="_blank">' . $post['category_name'] . '</a>';
                $nestedData['created_at'] = 'Created at: ' . Carbon::parse($post['created_at'])->format('Y-m-d H:m:s') . '<br/>Updated at:' . Carbon::parse($post['updated_at'])->format('Y-m-d H:m:s');
                $nestedData['options'] = '<a href="' . route('cpanel.categories.edit', ['id' => $post['_id']]) . '"><span class="label label-primary"><i class="fa fa-edit"></i></span></a>
                <a href="' . route('cpanel.categories.destroy', $post['_id']) . '" data-id="' . $post['_id'] . '" data-name="' . $post['category_name'] . '" data-method="delete" class="jquery-postback"><span class="label label-danger"><i class="fa fa-times-circle"></i></span></a>';

                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
            "hash" => $hash,
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        return view('cpanel.categories.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input('category_name')) {
            $new_category = new Categories;
            $new_category->category_name = trim($request->input('category_name'));
            $new_category->save();
        }
        $msg = "Created New Category: " . $request->input('category_name');
        return redirect()->route('cpanel.categories.index', ['msg' => $msg]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $editor = Categories::find($id);
        return view('cpanel.categories.edit', compact(array('editor')));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editor = Categories::find($id);
        if ($editor) {
            $editor->category_name = trim($request->input('category_name'));
            $editor->save();
        }
        $msg = "Updated Category: " . $editor('category_name');
        return redirect()->route('cpanel.categories.index', ['msg' => $msg]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Categories::find($id)->delete();
        KeywordCategory::where('category_id', $id)->delete();
        $msg = "Deleted Category:" . $id . " And All Result";
        return response($msg);
    }

    protected function searchByName(Request $request)
    {
        $value = trim($request->value);
        $results = Categories::where('category_name', 'like', '%' . $value . '%')
            ->limit(15)
            ->get();
        return response()->json($results);
    }
}
