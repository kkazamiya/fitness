<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use MongoDB\BSON\ObjectId;

class CmsConfigController extends Controller
{
    private $relations = ['url' => 1, 'result' => 2, 'banner' => 3, 'category' => 4, 'coupon' => 5];
    public function __construct()
    {
        //$this->middleware(['auth', 'permission:manage-seo']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $site_configs = DB::collection('website_configs')->orderBy('created_at', 'desc')->first();
        return view('cpanel.site_config.index', compact(array('site_configs')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function store(Request $request)
    {
        $all_req = $request->all();
        unset($all_req['_token']);
        unset($all_req['submit']);
        $data_insert = array_map('trim', $all_req);

        //$list_other = preg_split('/\r\n|[\r\n]/', trim($request->config_urls));
        //$id_others = $this->_addOther($list_other);
        //$data_insert['config_urls'] = implode(",", $list_other);

        if ($request->id_config) {
            unset($data_insert['id_config']);
            $saved = DB::collection('website_configs')->where('_id', trim($request->id_config))->update($data_insert);
        } else {
            $saved = DB::collection('website_configs')->insert($data_insert);
        }
        Cache::forget('site_configs');
        Cache::forget('brand_home');
        Cache::forget('brand_stores');
        return back()->with('success', "Success Save Config");
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
