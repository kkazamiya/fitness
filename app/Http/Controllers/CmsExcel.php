<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Coupons;
use App\Stores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CmsExcel extends Controller
{
    //
    //private $addSiteMap;
    protected $client_ES;
    //private $makePhoto;

    private $auth_logged;

    public function __construct()
    {
        //$this->middleware(['auth', 'permission:edit-store', 'permission:edit-coupon']);
        //$this->addSiteMap = new SitemapController();
        $this->client_ES = new EsController();
        //$this->makePhoto = new MakeLogoController();
    }

    private function _uniqueStr()
    {
        $characters = '1234567890';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 13; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function _addExcel(Request $request)
    {
        $categories = Categories::all()->sortByDesc("category_name");
        //$stores = Store::orderBy('updated_at', 'desc')->get();
        return view('cpanel.stores.add_excel', compact(array('categories')));
    }

    public function _importReview(Request $request)
    {
        return view('cpanel.reviews.add_file');
    }

    protected function _importReviewPost(Request $request)
    {
        $insert = DB::collection('reviews')->insert($request->data_json);
        dd($insert);
    }

    public function _importCouponGet(Request $request)
    {
        return view('cpanel.coupons.add_excel');
    }

    protected function _importCouponPost(Request $request)
    {
        if ($request->coupon_name) {
            $hash_r = md5(strtolower(trim($request->coupon_name) . trim($request->forStore)));
            $exits = Coupons::where('hash_r', $hash_r)->count();
            if ($exits > 0) {
                /*Update*/
                return "<span class='label label-warning'>Coupon Added:" . $request->coupon_code . " of Store " . $request->forStore . "</span>";
            } else {
                $auth_logged = Auth::user()->email;
                $slug = $this->_uniqueStr();
                $new_coupon = new Coupons;
                $new_coupon->coupon_name = trim($request->coupon_name);
                $new_coupon->coupon_excerpt = trim($request->coupon_excerpt);
                $new_coupon->coupon_code = trim($request->coupon_code);
                $new_coupon->code_status = intval($request->code_status);
                $new_coupon->off_sale = intval($request->off_sale);
                $new_coupon->off_type = $request->off_type;
                $new_coupon->code_type = $request->code_type;
                $new_coupon->expired_date = $request->expired_date;
                $new_coupon->forStore = trim($request->forStore);
                $new_coupon->aff_link = $request->aff_link;
                $new_coupon->order_index = 0;
                $new_coupon->coupon_slug = $slug;
                $new_coupon->update_at = strtotime("now");
                $new_coupon->hash_r = $hash_r;
                $new_coupon->created_by = $auth_logged;
                //$new_coupon->created_user = $this->user_name;
                $new_coupon->save();
                Stores::where('slug_store', trim($request->forStore))->increment('coupon_added');
            }
            return "<span class='label label-success'>Create New Coupon:" . $request->coupon_name . "</span>";
        } else {
            return "<span class='label label-danger'>Haven't Coupon</span>";
        }
    }

    protected function _importStores(Request $request)
    {
        if ($request->store_name) {
            $slug = slugify($request->store_name);
            $exits = Stores::where('slug_store', $slug)->first();
            $auth_logged = Auth::user()->email;
            if (!$exits) {
                if ($request->store_excerpt == '') {
                    $store_excerpt = "Get 70% off discount with " . $request->store_name . " coupon codes and " . $request->store_name . " promo codes.";
                } else {
                    $store_excerpt = $request->store_excerpt;
                }
                $new_store = new Stores;
                $new_store->store_name = ucwords(trim($request->store_name));
                $new_store->store_excerpt = $store_excerpt;
                $new_store->store_image = $request->store_image;
                $new_store->rich_content = trim($request->rich_content);
                $new_store->store_aff = $request->store_aff;
                if (is_array($request->store_cat)) {
                    $new_store->store_cat = implode(",", $request->store_cat);
                }
                $new_store->store_domain = $request->store_domain;
                $new_store->view = 0;
                $new_store->coupon_added = 0;
                $new_store->update_at = strtotime("now");
                $new_store->created_by = $auth_logged;
                $new_store->type_store = 'coupons';
                $new_store->save();
                $this->client_ES->_addESDoc('stores', '_doc', $new_store);
                //$this->addSiteMap->_add2SiteMap($new_store, 'coupon_map');
                /*if ($new_store->store_image == null && $new_store->store_domain != null) {
                    $this->makePhoto->_createLogo($new_store, 'stores');
                }*/
                return "<p class='text-success'>Added Stores " . $request->store_name . " Success</p>";
            } else {
                //$exits->store_name = ucwords(trim($request->store_name));
                //$exits->store_image = $request->store_image;
                $exits->store_aff = $request->store_aff;
                $exits->store_domain = $request->store_domain;
                $exits->created_by = $auth_logged;
                $exits->save();
                $this->client_ES->_findAndUpdate('stores', '_doc', $exits);
                return "<p class='text-warning'>Updated Stores " . $request->store_name . " Success</p>";
            }
        } else {
            dd("<p class='text-danger'>No Store </p>");
        }
    }
}
