<?php

namespace App\Http\Controllers;

use App\Keywords;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use MongoDB\BSON\UTCDateTime as MongoUTC;

class DashboardController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        die("You are logged!!!");
        $now = new MongoUTC(strtotime('now') * 1000);
        $start = new MongoUTC(strtotime('-7 days') * 1000);
        $file_path = '/stats/' . date('Y-m-d-H') . '.json';
        $file_exits = Storage::disk('local')->exists($file_path);
        if ($file_exits) {
            $file_data = Storage::get($file_path);
            $datum = (array)json_decode($file_data);
        } else {
            ini_set('memory_limit', '2048M');
            $datum = [];
            $month_stats = Keywords::select('updated_at')->whereBetween('updated_at', [$start, $now])
                ->get()
                ->groupBy(function ($item) {
                    return $item->updated_at->format('d-M-y');
                })->sortKeys();
            foreach ($month_stats as $key => $data) {
                $datum['labels'][] = $key;
                $datum['data_set'][] = count($month_stats[$key]);
            }
            $datum['kw_not_crawled'] = Keywords::where('tracked', 0)->count();
            $datum['kw_crawled'] = Keywords::where('tracked', 1)->count();
            $datum['all_kw'] = Keywords::count();
            $datum['p_kw_crawled'] = intval(($datum['kw_crawled'] / $datum['all_kw']) * 100);
            $datum['p_kw_not_crawled'] = intval(($datum['kw_not_crawled'] / $datum['all_kw']) * 100);
            Storage::put($file_path, json_encode($datum));
        }
        return view('home', compact(['datum']));
    }
}
