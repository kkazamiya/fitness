<?php

namespace App\Http\Controllers;

use App\Emails;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class EmailController extends Controller
{
    private $cache_sort = 5;
    private $client_http, $api_email = 'http://localhost:3002/api-email/send-email';

    public function __construct()
    {
        $this->middleware(['auth']);
        $this->client_http = new Client();
    }

    public function index()
    {
        return view('cpanel.emails.index_table');
    }

    protected function show_svs(Request $request)
    {
        $columns = array(
            0 => 'email',
            1 => 'created_at',
            2 => 'options',
        );


        $limit = intval($request->input('length'));
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $hash = md5($limit . $start . $order . $dir . "list_emails");

        $totalData = Cache::remember('totalData' . $hash, $this->cache_sort, function () {
            return Emails::count();
        });

        $totalFiltered = $totalData;

        if (empty($request->input('search.value'))) {
            $posts = Cache::remember('posts' . $hash, $this->cache_sort, function () use ($start, $limit, $order, $dir) {
                return Emails::offset(intval($start))
                    ->take($limit)
                    ->orderBy($order, $dir)
                    ->get();
            });
        } else {
            $search = $request->input('search.value');
            $hash = md5($limit . $start . $order . $dir . $search);

            $posts = Emails::where('email', 'LIKE', "%{$search}%")
                ->offset(intval($start))
                ->take($limit)
                ->orderBy($order, $dir)
                ->get();
            $totalFiltered = Emails::where('email', 'LIKE', "%{$search}%")
                ->count();
        }
        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $nestedData['_id'] = $post['_id'];
                if ($post['sended'] == 1) {
                    $stt = "<br/><span class='text-success'><i class='fa fa-grin-wink'></i> Sended</span>";
                } else {
                    $stt = "<br/><span class='text-danger'><i class='fa fa-bug'></i>--</span>";
                }
                $nestedData['email'] = $post['email'] . $stt;
                $nestedData['created_at'] = 'Created at: ' . Carbon::parse($post['created_at'])->format('Y-m-d H:m:s') . '<br/>Updated at:' . Carbon::parse($post['updated_at'])->format('Y-m-d H:m:s');
                $nestedData['options'] = '<a href="' . route('cpanel.emails.edit', ['id' => $post['_id']]) . '"><span class="label label-primary"><i class="fa fa-edit"></i></span></a>
                <a href="' . route('cpanel.emails.destroy', $post['_id']) . '" data-id="' . $post['_id'] . '" data-name="' . $post['email'] . '" data-method="delete" class="jquery-postback"><span class="label label-danger"><i class="fa fa-times-circle"></i></span></a>';
                $nestedData['_id'] = $post['_id'];
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
            "hash" => $hash,
        );

        echo json_encode($json_data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        //
        $editor = Emails::find($id);
        return view('cpanel.emails.edit', compact(['editor']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update(Request $request, $id)
    {
        //
        $editor = Emails::find($id);
        $editor->sended = $request->input('sended');
        $editor->save();
        $success = "Updated Email info: " . $id;
        return view('cpanel.emails.index_table', compact(['success']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return string
     */
    public function destroy($id)
    {
        //
        Emails::destroy($id);
        return "Remove Banner:" . $id;
    }

    protected function _sendEmail(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('cpanel.emails.email_send');
        } else {
            $list_email = trim($request->list_email);
            $mailing_list = trim($request->mailing_list);
            $send_from = trim($request->send_from);
            $o_tag = explode(',', trim($request->o_tag));
            $cc = trim($request->cc);
            $bcc = trim($request->bcc);
            $subject = trim($request->subject);
            $template = trim($request->template);
            $text_content = trim($request->text_content);
            $html_content = trim($request->html_content);
            $attachment = parse_url(trim(urldecode($request->attachment)));
            $data_build = ['data_send' => ['o:tracking' => true, 'o:tracking-clicks' => true, 'o:tracking-opens' => true]];
            if ($mailing_list != '') {
                $data_build['mailing_lists'] = $mailing_list;
            } else {
                $a_mails = explode(',', $list_email);
                if (count($a_mails) > 1) {
                    $data_build['mailing_lists'] = $list_email;
                } else {
                    $data_build['an_email'] = $list_email;
                }
            }
            $data_build['data_send']['from'] = $send_from;
            if ($cc != '') {
                $data_build['data_send']['cc'] = $cc;
            }
            if ($bcc != '') {
                $data_build['data_send']['bcc'] = $bcc;
            }
            $data_build['data_send']['subject'] = $subject;
            if ($o_tag != '') {
                $data_build['data_send']['o:tag'] = $o_tag;
            }
            if ($template) {
                $data_build['data_send']['template'] = $template;
            } else {
                if ($html_content) {
                    $data_build['data_send']['html'] = $html_content;
                }
                if ($text_content) {
                    $data_build['data_send']['text'] = $text_content;
                }
            }
            if ($attachment) {
                //$path_info = pathinfo($attachment['path']);
                //$data_build['attachment'] = [['filePath' => public_path($attachment['path']), 'filename' => $path_info['filename'] . '.' . $path_info['extension']]];
            }
            $get_info = $this->client_http->request('POST', $this->api_email, [
                'json' => $data_build
            ]);
            $body = json_decode((string)$get_info->getBody());
            dd($body);
        }
    }
}
