<?php

namespace App\Http\Controllers;

use App\Keywords;
use App\Stores;
use Illuminate\Http\Request;
use Elasticsearch\ClientBuilder;
use Illuminate\Support\Facades\Cache;

class EsController extends Controller
{
    //
    public $client_es;

    public function __construct()
    {
        $this->client_es = ClientBuilder::create()->build();
    }

    public function _esAlive()
    {
        return $this->client_es->ping();
    }

    public function _createPipeline()
    {
        $params = [
            'id' => 'transport_id',
            'body' => [
                'description' => 'pipeline convert _id to _id_mongo',
                'processors' => [
                    [
                        'rename' => [
                            'field' => '_id',
                            'target_field' => "_id_mongo"
                        ]
                    ]
                ]
            ]
        ];
        return $this->client_es->ingest()->putPipeline($params);
    }

    public function _findAndRemoveES($index, $type, $id_mongo)
    {
        $params = [
            'index' => $index,
            'type' => $type,
            'body' => [
                'query' => [
                    'match' => [
                        '_id_mongo' => $id_mongo
                    ]
                ]
            ]
        ];
        $id_ES = $this->client_es->search($params)['hits']['hits'];
        if (count($id_ES) > 0) {
            $_id = $id_ES[0]['_id'];
            $params = [
                'index' => $index,
                'type' => $type,
                'id' => $_id
            ];
            $response = $this->client_es->delete($params);
            return $response;
        }
    }

    public function _findAndUpdate($index, $type, $document, $upsert = false)
    {
        $params = [
            'index' => $index,
            'type' => $type,
            'body' => [
                'query' => [
                    'match' => [
                        '_id_mongo' => $document->_id
                    ]
                ]
            ]
        ];
        $id_ES = $this->client_es->search($params)['hits']['hits'];
        if (count($id_ES) > 0) {
            $_id = $id_ES[0]['_id'];
            unset($document->_id);
            $params = [
                'index' => $index,
                'type' => $type,
                'id' => $_id,
                'body' => ['doc' => $document],
                //'pipeline' => 'transport_id'
            ];

            $response = $this->client_es->update($params);
            return $response;
        } else {
            if ($upsert == true) {
                $this->_addESDoc($index, $type, $document);
            }
        }
    }

    public function _addESDoc($index, $type, $document)
    {
        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $document['_id'],
            'body' => $document,
            'pipeline' => 'transport_id'  // <----- here
        ];
        $response = $this->client_es->index($params);
        return $response;
    }

    public function _searchQueryClient($index, $type, $body)
    {
        $params = [
            'index' => $index,
            'type' => $type,
            'body' => $body
        ];
        return $this->client_es->search($params);
    }

    public function _randomDocuments($limit, $index, $id_rand)
    {
        $hash = md5($limit . $index . $id_rand);
        $params = array();
        $params['index'] = $index;
        $params['body']['size'] = $limit;
        $params['body']['from'] = 0;

        $random = new \stdClass();
        $random->seed = time(); //change this to make randomization consistent, e.g. user id
        if ($index == 'keywords') {
            $params['body']['query']['function_score']['query']['bool']['must'] = array(
                'term' => array(
                    'tracked' => 1,
                )
            );
        }
        $params['body']['query']['function_score']['functions'][]['random_score'] = $random;
        return Cache::remember($hash, 60 * 24, function () use ($params) {
            return Keywords::complexSearch($params);
        });
    }
    public function _randomInLocal($limit, $index, $id_rand)
    {
        $hash = md5($limit . $index . $id_rand);
        $params = array();
        $params['index'] = $index;
        $params['body']['size'] = $limit;
        $params['body']['from'] = 0;

        $random = new \stdClass();
        $random->seed = time(); //change this to make randomization consistent, e.g. user id
        if ($index == 'keywords') {
            $params['body']['query']['function_score']['query']['bool']['must'] = array(
                'term' => array(
                    'tracked' => 1,
                )
            );
        }
        $params['body']['query']['function_score']['functions'][]['random_score'] = $random;
        return Cache::remember($hash, 60 * 24, function () use ($params) {
            return Keywords::complexSearch($params);
        });
    }

}
