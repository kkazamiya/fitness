<?php

namespace App\Http\Controllers;


use App\Emails;
use App\Posts;
use App\Stores;
use App\Http\Controllers\Posts\PostViewController;
use App\Http\Controllers\Services\CommonServices;
use App\KeywordBlack;
use App\Keywords;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class HomeController extends Controller
{
    private $cache_sort = 5;
    private $cache_hour = 60;
    private $cache_daily = 24 * 60;
    private $cache_weekly = 24 * 60 * 7;
    private $cache_month = 24 * 60 * 30;
    private $es;
    private $ScraperController, $client_http;
    private $email_subscribe = 'http://localhost:3002/api-email/send-active';
    private $secret_token = '[fitnessol_mk_mahoa]';
    private $common;
    private $post_services;

    public function __construct()
    {
        $this->es = new EsController();
        $this->client_http = new Client();
        $this->ScraperController = new ScraperController();
        $this->common = new CommonServices();
        $this->post_services = new PostViewController();
    }

    public function _index(Request $request)
    {
        
        $seo_config = $this->common->_getConfig();
         if (array_key_exists('advertise_home', $seo_config) && $seo_config['advertise_home'] != "") {
                $advertise = str_replace(" ", "", $seo_config['advertise_home']);
                $advertise_list = explode(",", $advertise);
                if (is_array($advertise_list)) {
                    $store = Cache::remember('brand_home', $this->cache_daily, function () use ($advertise_list) {
                        return Stores::whereIn('slug_store', $advertise_list)->take(20)->get();
                    });
                }
            } else {

                $store = Cache::remember('brand_home', $this->cache_daily, function () {
                    return Stores::where('store_cat', 'LIKE', '%top%')->orderBy('updated_at', 'desc')->get()->take(20);
                });
            }
        $stores = json_decode($store);
        $result = array_chunk($stores, 2);
        $keywords = $this->common->_randomInLocal(60, 'keywords', 'keywords_home');
        $posts = $this->post_services->_latestPost(8, 'homepage_post');
        $footer = $this->common->_randomInLocal(5, 'footer', 'footer');
        $detect_mobile = new \Mobile_Detect();
        
        $mobile = false;
        if ($detect_mobile->isMobile()) {
            $mobile = true;
        }
        return view('site_jobs.index', compact(['seo_config', 'footer', 'keywords', 'posts','result']));
    }

    public function _isBlack($keyword_slug)
    {
        $black_parks = ["arizona-state-parks", "alamo-lake", "buckskin-mountain", "river-island-state-park", "catalina-state-park", "cattail-cove", "dead-horse-ranch", "rockin-river-ranch", "fool-hollow-lake", "fort-verde", "homolovi-state", "jerome-state", "kartchner-caverns", "lake-havasu", "lost-dutchman", "lyman-lake", "mcfarland-state-historic-park", "oracle-state-park", "patagonia-lake", "picacho-peak", "red-rock-state-park", "riordan-mansion", "dankworth-pond", "san-rafael-state-natural-area", "slide-rock-state-park", "sonoita-creek", "tombstone-courthouse-state-historic-park", "tonto-natural-bridge-state-park", "tubac-presidio", "colorado-river-state-park", "yuma-territorial-prison", "roper-lake", "arizonastateparks", "alamolakestatepark", "buckskinmountainstatepark", "riverislandstatepark", "catalinastatepark", "cattailcovestatepark", "deadhorseranchstatepark", "rockinriverranchstatepark", "foolhollowlakeredcreationarea", "fortverdesthistoricpark", "homolovistatehistoricpark", "jeromestatehistoricpark", "kartchnercavernsstatepark", "lakehavasustatepark", "lostdutchmanstatepark", "lymanlakestatepark", "mcfarlandstatehistoricpark", "oraclestatepark", "patagonialakestatepark", "picachopeakstatepark", "redrockstatepark", "riordanmansionstatehistoricpark", "roperlakedankworthpondstatepark", "sanrafaelstatenaturalarea", "sliderockstatepark", "sonoitacreekstatenaturalarea", "tombstonecourthousestatehistoricpark", "tontonaturalbridgestatepark", "tubacpresidiostatehistoricpark", "coloradoriverstatepark", "yumaterritorialprisonstatehistoricpark", "az-state-parks", "azstateparks"];
        foreach ($black_parks as $black_park) {
            if (stripos($keyword_slug, $black_park) > -1) {
                return true;
            }
        }
        $black_array = Cache::remember('Blacklist_keywords', $this->cache_sort, function () {
            return KeywordBlack::all()->pluck('keyword_slug')->all();
        });
        if (in_array($keyword_slug, $black_array)) {
            return true;
        } else {
            return false;
        }
    }
    
    protected function _autocomplete(Request $request)
    {
        if (!empty($request->q)) {

                
                $q = slugify($request->q);
                
                    $results = Cache::remember(md5($q), $this->cache_sort, function () use ($q) {
                        return Keywords::where([
                            ['keyword_slug', 'LIKE', '%' . $q . '%'],
                            ['tracked',1]
                            ])->take(10)->get();
                    });
                
               
                if ($request->res == 'amp') {
                    return response()->json(['items' => $results]);
                } else {
                    return response()->json($results);
                }
        }
    }

    protected function _savingEmail(Request $request)
    {
        if ($request->email) {

            $email = trim(strtolower($request->email));
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $exits = Emails::where('email', $email)->count();
                if (!$exits) {
                    $exits = new Emails();
                    $exits->email = $email;
                    $exits->sended = 0;
                    $exits->save();
                    $data_build = ['data_send' => ['o:tracking' => true, 'o:tracking-clicks' => true, 'o:tracking-opens' => true]];
                    $data_build['data_send']['from'] = 'Fitnessol <noreply@fitnessol.com>';
                    $data_build['data_send']['to'] = $email;
                    $data_build['data_send']['subject'] = 'Fitnessol.com: Confirm the subscription';
                    $data_build['data_send']['template'] = 'confirm_mail_1';
                    $data_build['data_send']['h:X-Mailgun-Variables'] = '{"email":"' . $email . '","active_code":"' . md5($this->secret_token . $email) . '"}';
                    try {
                        $get_info = $this->client_http->request('POST', $this->email_subscribe, [
                            'json' => $data_build
                        ]);
                        $body = json_decode((string)$get_info->getBody());
                        if ($body->id) {
                            $exits->id_queued = $body->id;
                            $exits->sent = 1;
                            $exits->save();
                        }
                    } catch (\Exception $exception) {

                    }
                }
                Cache::put('email_' . $email, $exits, 60);
                return back(302)->with('success', trim($request->email));
            } else {
                return back(302)->with('error', trim($request->email));
            }
        }
    }

}
