<?php

namespace App\Http\Controllers\Keyword;

use App\Advertisers;
use App\BlackList;
use App\Categories;
use App\ConnectOther;
use App\Http\Controllers\EsController;
use App\Http\Controllers\ScraperController;
use App\KeywordBlack;
use App\Keywords;
use App\Results;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use MongoDB\BSON\UTCDateTime as MongoUTC;

class CmsKeysController extends Controller
{

    private $cache_sort = 5;
    private $cache_hour = 60;
    private $cache_daily = 24 * 60;
    private $cache_month = 30 * 24 * 60;
    private $relations = ['url' => 1, 'result' => 2, 'banner' => 3, 'category' => 4, 'coupon' => 5];
    private $es;
    private $ScraperController;

    public function __construct()
    {
        $this->middleware(['auth']);
        $this->es = new EsController();
        $this->ScraperController = new ScraperController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('cpanel.keywords.index_table');
    }

    protected function show_svs(Request $request)
    {
        $columns = array(
            0 => 'keyword',
            1 => 'keyword_ads',
            2 => 'keyword_cat',
            3 => 'created_at',
            4 => 'options',
        );


        $limit = intval($request->input('length'));
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $hash = md5($limit . $start . $order . $dir . "key_list");

        $totalData = Cache::remember('totalData' . $hash, $this->cache_sort, function () {
            return Keywords::count();
        });

        $totalFiltered = $totalData;

        if (empty($request->input('search.value'))) {
            $posts = Cache::remember('posts' . $hash, $this->cache_sort, function () use ($start, $limit, $order, $dir) {
                return Keywords::offset(intval($start))
                    ->take($limit)
                    ->orderBy($order, $dir)
                    ->get();
            });
        } else {
            $search = $request->input('search.value');
            $hash = md5($limit . $start . $order . $dir . $search);
            $posts = Cache::remember('search'. $hash, $this->cache_sort, function() use ($start, $limit, $order, $dir, $search) {
                return Keywords::where('keyword', 'LIKE', '%' . $search . '%')
                    ->offset(intval($start))
                    ->take($limit)
                    ->orderBy($order, $dir)
                    ->get();
            });
            // $posts = $this->_searchES($search);
            $totalFiltered = count($posts);
        }
        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $nestedData['_id'] = $post['_id'];
                if ($post['tracked'] == 1) {
                    $stt = "<br/><span class='text-success'><i class='fa fa-grin-wink'></i> Crawled</span> - <a title='Clear Cache' class='text-danger' href='/c-panel/clear-cache/keyword/" . $post['keyword_slug'] . "' target='_blank'><i class='fas fa-smoking-ban'></i></a> ";
                } else if ($post['tracked'] == 0) {
                    $stt = "<br/><span class='text-secondary'><i class='fa fa-play-circle'></i> Need Crawl</span>";
                } else {
                    $stt = "<br/><span class='text-danger'><i class='fa fa-bug'></i> Error Crawl</span>";
                }
                $nestedData['keyword'] = '<a href="/' . $post['keyword_slug'] . '" target="_blank">' . $post['keyword'] . '</a>' . $stt;
                $nestedData['keyword_ads'] = $post['keyword_ads'];
                $nestedData['keyword_cat'] = $post['keyword_cat'];
                $nestedData['created_at'] = 'Created at: ' . Carbon::parse($post['created_at'])->format('Y-m-d H:m:s') . '<br/>Updated at:' . Carbon::parse($post['updated_at'])->format('Y-m-d H:m:s');
                $nestedData['options'] = '<a class="jquery-tracking" href="' . route('cpanel.bing_scraper', ['id' => $post['_id'], 'keyword' => $post['keyword'], 'bing_scraper' => true, 'page' => 2]) . '" data-method="post" data-name="' . $post['keyword'] . '"><span class="label label-primary"><i class="fa fa-spider"></i></span></a> <a href="' . route('cpanel.keywords.edit', ['id' => $post['_id']]) . '"><span class="label label-primary"><i class="fa fa-edit"></i></span></a>
                <a href="' . route('cpanel.keywords.destroy', $post['_id']) . '" data-id="' . $post['_id'] . '" data-name="' . $post['keyword'] . '" data-method="delete" class="jquery-postback"><span class="label label-danger"><i class="fa fa-times-circle"></i></span></a>';
                $nestedData['_id'] = $post['_id'];
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
            "hash" => $hash,
        );

        echo json_encode($json_data);
    }

    public function create()
    {
        $categories = Categories::all()->sortByDesc("category_name");
        // $ads = Advertisers::all()->sortByDesc("created_at");
        return view('cpanel.keywords.add', compact(array( 'categories')));
    }


    private function _scrapeOther($other)
    {
        $data = ['type_search' => "get-other", 'uri_request' => $other['full_url'], 'domain' => $other['full_url']];
        $get_info = $this->ScraperController->_nojsScraper('info-domain', $data);
        if ($get_info->code === 1) {
            ConnectOther::where('_id', $other['_id'])->update(['tracked' => 1, 'full_data' => (object)['title' => $get_info->title, 'desc' => $get_info->desc]]);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    private function _addOther($list_other)
    {
        $id_others = [];
        foreach ($list_other as $other) {
            $exits = ConnectOther::where('hash_r', md5($other))->first();
            if (!$exits) {
                $new_other = new ConnectOther;
                $new_other->hash_r = md5($other);
                $new_other->full_url = $other;
                $new_other->tracked = 0;
                $new_other->full_data = "";
                $new_other->type = $this->relations['url'];
                $new_other->save();
                $this->_scrapeOther($new_other);
                array_push($id_others, $new_other['_id']);
            } else {
                array_push($id_others, $exits['_id']);
            }
        }
        return $id_others;
    }

    public function store(Request $request)
    {
        $list_keywords = $request->input('list_keywords');
        $keywords = preg_split('/\r\n|[\r\n]/', $list_keywords);
        $list_ads = $request->input('list_ads');
        $list_other = preg_split('/\r\n|[\r\n]/', $request->connect_other);
        $id_others = $this->_addOther($list_other);
        foreach ($keywords as $keyword) {
            $exits = Keywords::where('hash_r', md5($keyword))->count();
            if ($exits == 0) {
                $new_keyword = new Keywords;
                $new_keyword->keyword = $keyword;
                if (is_array($request->input('list_cat'))) {
                    $new_keyword->keyword_cat = implode(",", $request->input('list_cat'));
                } else {
                    $new_keyword->keyword_cat = false;
                }
                $new_keyword->keyword_ads = $list_ads;
                $new_keyword->keyword_couponupto = trim($request->input(['keyword_couponupto']));
                $new_keyword->connect_other = implode(",", $id_others);
                $new_keyword->view = 0;
                $new_keyword->tracked = 0;
                $new_keyword->hash_r = md5($keyword);
                $new_keyword->save();
                $this->es->_addESDoc('keywords', '_doc', $new_keyword);
            }
        }
        $msg = "Created New Keywords: " . $request->input('list_keywords');
        return redirect()->route('cpanel.keywords.index', ['msg' => $msg]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        //
        $editor = Keywords::find($id);
        $categories = Categories::all()->sortByDesc("category_name");
        // $ads = Advertisers::all()->sortByDesc("created_at");
        return view('cpanel.keywords.edit', compact(array('editor', 'categories')));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $editor = Keywords::find($id);
        $list_other = preg_split('/\r\n|[\r\n]/', $request->connect_other);
        $id_others = $this->_addOther($list_other);
        if ($editor) {
            if (is_array($request->input('list_cat'))) {
                $editor->keyword_cat = implode(",", $request->input('list_cat'));
            } else {
                $editor->keyword_cat = false;
            }
            $editor->keyword_ads = $request->input('list_ads');
            $editor->keyword_couponupto = trim($request->input('keyword_couponupto'));
            $editor->connect_other = implode(",", $id_others);
            $editor->rich_content = $request->input('rich_content');
            $editor->save();
            // $this->es->_findAndUpdate('keywords', '_doc', $editor);
        }
        $msg = "Updated keyword: " . $editor['keyword'];
        return redirect()->route('cpanel.keywords.index', ['msg' => $msg]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        Keywords::find($id)->delete();
        $count_result = Results::where('for_keyword', $id)->delete();
        $this->es->_findAndRemoveES('keywords', '_docs', $id);
        return response()->json("Deleted: " . $id . " and " . $count_result . " results");
    }


    private function _searchES($search_value)
    {
        if (Cache::has('keywords' . md5($search_value))) {
            $ids = Cache::get('keywords' . md5($search_value));
            return $ids;
        } else {
            $params = [
                'bool' => [
                    'should' => [
                        ['match' => ['keyword_slug' => $search_value]],
                        ['fuzzy' => ['keyword' => ['value' => $search_value, 'max_expansions' => '50']]]
                    ]
                ]];
            $posts = Keywords::searchByQuery($params, null, null, 20, null, null);
            $posts->sortBy('_score');
            foreach ($posts as $post) {
                $post['_id'] = $post['_id_mongo'];
            }
            Cache::put('keywords' . md5($search_value), $posts, $this->cache_sort);
            return $posts;
        }
    }

    protected function searchByName(Request $request)
    {
        $value = trim($request->value);
        if ($request->full) {
            $stores = Keywords::searchByQuery(['multi_match' => [
                'query' => $value,
                'fields' => ['keyword_slug', 'keyword'],
                'fuzziness' => 'AUTO'
            ]
            ], null, null, 15);
        } else {
            $stores = Keywords::searchByQuery(['multi_match' => [
                'query' => $value,
                'fields' => ['keyword_slug', 'keyword'],
                'fuzziness' => 'AUTO'
            ]
            ], null, ['keyword_slug', 'keyword'], 15);
        }
        return response()->json($stores);
    }

    protected function _vAddExcel(Request $request)
    {
        return view('cpanel.keywords.add_excel');
    }

    protected function _addTXT(Request $request)
    {
        if ($request->file_url) {
            $file_url = parse_url(trim(urldecode($request->file_url)));
            $pos = intval($request->pos);
            $limit = intval($request->limit);
            $contents = file_get_contents(public_path($file_url['path']));
            $arrays = explode(PHP_EOL, trim($contents));
            $arrays = array_map('trim', array_unique($arrays));
            if (!is_array($arrays) || $pos >= count($arrays)) {
                return response()->json(['code' => 2, 'msg' => 'All Done']);
            }

            $get_pos = array_slice($arrays, $pos, $limit);
            $kws_hash = ['hash_r' => [], 'slug' => []];
            foreach ($get_pos as $index => $per_kw) {
                //$kws_hash['hash_r'][] = md5(str_filter($per_kw));
                $kws_hash['slug'][] = slugify(trim($per_kw));
            }
            //$kw_exits = Keywords::whereIn('hash_r', $kws_hash['hash_r'])->pluck('keyword')->toArray();
            $kw_slug_exits = Keywords::whereIn('keyword_slug', $kws_hash['slug'])->pluck('keyword_slug')->toArray();
            //$test_kw = array_unique(array_merge($kw_exits, $kw_slug_exits));

            $kw_not_exits = array_diff($kws_hash['slug'], $kw_slug_exits);
            //dd($kw_not_exits);
            $datum_insert = [];
            foreach ($kw_not_exits as $keyword_slug) {
                $keyword = str_replace('-', ' ', $keyword_slug);
                $new_kw = [];
                $new_kw['keyword'] = $keyword;
                $new_kw['tracked'] = 0;
                $new_kw['hash_r'] = md5($keyword);
                $new_kw['keyword_slug'] = $keyword_slug;
                $datum_insert[] = $new_kw;
            }
            if (count($datum_insert) > 0) {
                try {
                    $datum_insert = array_values($datum_insert);
                    Keywords::insert($datum_insert);
                } catch (\Exception $e) {
                    echo "Error:" . $e->getMessage();
                    dd($datum_insert);
                }
            }
            $result_msg = ['total' => count($arrays),
                'total_exits' => count($kw_slug_exits),
                'total_to_new' => count($kw_not_exits),
                'pos' => $pos,
                'limit' => $limit];
            return response()->json($result_msg);
        } else {
            return response()->json(['code' => 99, 'msg' => 'Error Params']);
        }
    }

    protected function _addExcel(Request $request)
    {
        if ($request->keyword) {
            $slug = Str::slug(trim($request->keyword), "-");
            $exits = Keywords::where('hash_r', md5(trim($request->keyword)))->count();
            if ($exits == 0) {
                $new_kw = new Keywords;
                $new_kw->keyword = trim($request->keyword);
                $new_kw->keyword_cat = trim($request->category);
                $new_kw->keyword_ads = trim($request->ads);
                $new_kw->keyword_couponupto = trim($request->couponupto);
                $new_kw->connect_other = trim($request->other_urls);
                $new_kw->view = 0;
                $new_kw->tracked = 0;
                $new_kw->hash_r = md5(trim($request->keyword));
                $new_kw->save();
                $this->es->_addESDoc('keywords', '_doc', $new_kw);
                return response()->json(['code' => 1, 'kw' => $new_kw]);
            } else {
                return response()->json(['code' => 2, 'msg' => 'Duplicate Keyword:' . trim($request->keyword)]);
            }
        }
    }

    protected function _vAddCUT(Request $request)
    {
        return view('cpanel.keywords.add_cut');
    }

    protected function _pAddCUT(Request $request)
    {
        if ($request->keyword_contain && $request->url_cut) {
            $kw_contain = strtolower(trim($request->keyword_contain));
            $url_cut = strtolower(trim($request->url_cut));
            $affected = Keywords::where('keyword', 'LIKE', "%" . $kw_contain . "%")->update(['keyword_couponupto' => $url_cut]);
            return view('cpanel.keywords.add_cut', compact(['affected']));
        }
    }

    protected function _removeKw(Request $request)
    {
        if ($request->method() == 'GET') {
            return view('cpanel.keywords.remove_kw');
        } else {
            try {
                $msg_result = [];
                if ($request->keyword_slug != '') {
                    $keyword = trim(strtolower($request->keyword_slug));
                    if ($keyword) {
                        $keyword_info = Keywords::where('keyword_slug', $keyword)->first();
                        if ($keyword_info) {
                            Keywords::where('_id', $keyword_info->_id)->delete();

                            $hash = md5($keyword_info->keyword_slug . "go_page");
                            Cache::forget('check_found' . $hash);

                            $effect_removed = Results::where('for_keyword', $keyword_info->_id)->count();
                            Results::where('for_keyword', $keyword_info->_id)->delete();
                            $black_list = KeywordBlack::where('keyword_slug', $keyword)->count();
                            if ($black_list === 0) {
                                $new_black = new KeywordBlack();
                                $new_black->keyword = $keyword_info->keyword;
                                $new_black->keyword_slug = $keyword;
                                $new_black->editor = Auth::user()->email;
                                $new_black->save();
                            }
                            Cache::forget('Blacklist_keywords');
                            array_push($msg_result, "Removed:" . $keyword_info->keyword . ' and ' . $effect_removed . ' Results And Add To Black List');
                        } else {
                            array_push($msg_result, 'Can not found ' . $keyword . ' keyword');
                        }
                    }
                }
                if ($request->of_domain != '') {
                    $domain = parse_url($request->of_domain);
                    if ($domain['host']) {
                        $hash = md5($domain['host'] . "_more_offers1");
                        Cache::forget('more_offers' . $hash);
                        $count_effect = Results::where('domain', $domain['host'])->count();
                        Results::where('domain', $domain['host'])->delete();
                        $exits = BlackList::where('black_domain', $domain['host'])->count();
                        if ($exits == 0) {
                            $new_keyword = new BlackList;
                            $new_keyword->black_domain = $domain['host'];
                            $new_keyword->editor = Auth::user()->email;
                            $new_keyword->save();
                        }
                        array_push($msg_result, 'Remove results follow domain:' . $domain['host'] . ' with ' . $count_effect . ' results & add to blacklist');
                    }
                }
                echo Auth::user()->email;
                echo '<br/><a href="' . route('cpanel.get-remove-keyword') . '">BACK</a>';
                dd($msg_result);
            } catch (\Exception $exception) {
                dd($exception);
            }
        }
    }

    protected function _exportAllURL()
    {
        return view('cpanel.keywords.export_app');
    }

    protected function _pexportAllURL(Request $request)
    {
        ini_set('memory_limit', '-1');
        if ($request->date_range) {
            $split = explode(" - ", trim($request->date_range));
            $start = new MongoUTC(strtotime($split[0] . " 00:00:00") * 1000);
            $end = new MongoUTC(strtotime($split[1] . " 23:59:59") * 1000);
            $apps = Keywords::select('keyword_slug')->where('tracked', 1)
                ->whereBetween('updated_at', [$start, $end])->get();
            foreach ($apps as $app) {
                echo secure_url("/" . $app['keyword_slug'] . "-download") . "<br/>";
            }
        } else {
            dd("not include date");
        }
    }
}
