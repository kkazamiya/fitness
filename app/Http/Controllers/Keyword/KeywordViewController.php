<?php 

namespace App\Http\Controllers\Keyword;

use App\DomainInfo;
use App\Http\Controllers\Controller;
use App\Http\Controllers\EsController;
use App\Http\Controllers\ScraperController;
use App\Http\Controllers\Services\CommonServices;
use App\Jobs\ProgressKeyword;
use App\Keywords;
use App\Posts;
use App\Results;
use App\Stores;
use App\SpamKeyword;
use App\Tags;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use MongoDB\BSON\ObjectId;

class KeywordViewController extends Controller
{
    private $cache_sort = 5;
    private $cache_hour = 60;
    private $cache_daily = 24 * 60;
    private $cache_weekly = 24 * 60 * 7;
    private $cache_month = 24 * 60 * 30;
    private $old_delay = 1728000; //60*60*24*20 days
    private $limit_results = 20;
    private $replica_target = ['https://www.couponupto.com/search?q=', 'https://www.goodinfohome.com/search-stores?q='];
    private $es;
    private $common;
    private $ScraperController;

    public function __construct()
    {
        $this->es = new EsController();
        $this->common = new CommonServices();
        $this->ScraperController = new ScraperController();
    }

    /**
     * Show ads top results
     *
     * @param array $data
     * ['hash'=>String,'all_data'=>Array,'search_key'=>String,'search_filter'=>String]
     * @return array $all_data;
     */
    private function _addReplica($data)
    {
        $replica_result = Cache::remember('replica_result' . $data['hash'], $this->cache_daily, function () use ($data) {
            if (count($data['all_data']) != 0) {
                $rand_on_data = $data['all_data'][rand(0, count($data['all_data']) - 1)];
                $rand_target = rand(0, 100);
                if ($rand_target <= 35) {
                    $uri_target = $this->replica_target[0];
                } else if ($rand_target <= 55) {
                    $uri_target = $this->replica_target[1];
                } else {
                    return false;
                }
                return [
                    '_id' => rand(100000, 999999),
                    'rank' => 0,
                    'title' => ucwords($data['search_key']) . " Coupons, Promo Codes " . date('m-Y'),
                    'snippet' => $rand_on_data['snippet'],
                    'rel' => 'dofollow',
                    'link' => $uri_target . $data['search_filter'] . "&ref=" . md5($data['search_filter'] . "-taotimstoreketao"),
                    'show_link' => 'https://' . parse_url($uri_target)['host'] . '/' . slugify($data['search_key']),
                    'sale_info' => ['off_percent' => rand(4, 19) * 5, 'off_dollar' => 0, 'sale' => true, 'free_shipping' => false, 'gift_card' => false]
                ];
            } else {
                return false;
            }
        });
        if ($replica_result) {
            $data['all_data'][] = $replica_result;
        }
        $find_coupon = $this->_topCoupon($data['search_filter']);
        if ($find_coupon) {
            $data['all_data'][] = $find_coupon;
        }
        return $data['all_data'];
    }

    private function _queryTag($tag_id, $all_data, $hash)
    {
        /*store_id,external_url,external_replica,external_info*/
        try {
            return Cache::remember('all_data_with_tag' . $hash, $this->cache_daily, function () use ($tag_id, $all_data) {
                if (isset($tag_id)) {
                    $tag = Tags::find($tag_id);
                    if ($tag) {
                        $all_data = collect($all_data);
                        $first_data = $all_data->shift();
                        $all_data = $all_data->all();
                        if (count($first_data) > 0) {
                            if ($tag->tag_type == 'internal_id') {
                                $store_info = Stores::find(new ObjectId($tag->foreign_id));
                                if ($store_info) {
                                    $top_coupon = Coupons::where('forStore', $store_info->slug_store)->get()
                                        ->sortByDesc('order_index')
                                        ->first();
                                    $first_data['link'] = secure_url(route('stores', ['store_slug' => $store_info->slug_store]));
                                    $first_data['_id'] = rand(0, 9999);
                                    $first_data['rel'] = $tag->rel;
                                    $first_data['domain'] = 'www.fitnessol.com';
                                    $first_data['rank'] = 0;
                                    if ($top_coupon) {
                                        $first_data['show_code'] = secure_url(route('stores', ['store_slug' => $store_info->slug_store])) . '?fr=result#' . $top_coupon['coupon_slug'];
                                    } else {
                                        $first_data['show_link'] = secure_url(route('stores', ['store_slug' => $store_info->slug_store]));
                                    }
                                    $all_data[] = $first_data;
                                }
                                return $all_data;
                            } elseif ($tag->tag_type == 'external_url') {
                                if (count($all_data) != 0 && isset($tag->foreign_url)) {
                                    $domain = parse_url($tag->foreign_url)['host'];
                                    $first_data['link'] = $tag->foreign_url;
                                    $first_data['_id'] = rand(0, 9999);
                                    $first_data['rel'] = $tag->rel;
                                    $first_data['show_link'] = $tag->foreign_url;
                                    $first_data['domain'] = $domain;
                                    $first_data['rank'] = 0;
                                }
                                $all_data[] = $first_data;
                                return $all_data;
                            } else {
                                $all_data[] = $first_data;
                                return $all_data;
                            }
                        } else {
                            return $all_data;
                        }
                    } else {
                        return $all_data;
                    }
                } else {
                    return $all_data;
                }
            });
        } catch (\Exception $exception) {
            return $all_data;
        }
    }

    private function _getOtherGlobal($seo_config)
    {
        try {
            $seo_config = (object)$seo_config;
            if (property_exists($seo_config, 'config_urls')) {
                if ($seo_config->config_urls != "") {
                    $array_md5 = explode(",", $seo_config->config_urls);
                    foreach ($array_md5 as $index => $item) {
                        $array_md5[$index] = md5($array_md5[$index]);
                    }
                    $hash_cache = md5($seo_config->config_urls);
                    $other_global = Cache::remember('global_urls' . $hash_cache, $this->cache_daily, function () use ($array_md5) {
                        $results = ConnectOther::where('tracked', 1)->whereIn('hash_r', $array_md5)->get()->toArray();
                        foreach ($results as $index => $other_ads) {
                            $results[$index]['link'] = $results[$index]['full_url'];
                            $results[$index]['title'] = $results[$index]['full_data']['title'];
                            $results[$index]['snippet'] = $results[$index]['full_data']['desc'];
                            $results[$index]['sale_info'] = filter_info($results[$index]['title'], $results[$index]['snippet']);
                            $results[$index]['rank'] = rand(7, 15);
                            $results[$index]['show_link'] = $results[$index]['full_url'];
                            $results[$index]['rel'] = 'dofollow';
                        }
                        return $results;
                    });
                    return $other_global;
                }
            }
            return false;
        } catch (\Exception $exception) {
            return false;
        }
    }

    private function _curlPhp($data, $url_api)
    {
        $payload = json_encode($data);
        $ch = curl_init($url_api);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        // Set HTTP Header for POST request
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($payload))
        );
        // Submit the POST request
        $result = curl_exec($ch);
        // Close cURL session handle
        curl_close($ch);

        return json_decode($result);
    }

    public function _moreOffers(Request $request)
    {
        if ($request->domain) {
            $query = trim($request->domain);
            $currentPage = isset($_GET['page']) ? (int)$_GET['page'] : 1;
            $hash = md5($query . "_more_offers" . $currentPage);
            $all_data = Cache::remember('more_offers' . $hash, $this->cache_hour, function () use ($query) {
                return Results::where('domain', $query)->simplePaginate(20);
            });
            $domain_info = Cache::remember('domain_info_n' . $hash, $this->cache_daily, function () use ($query) {
                return DomainInfo::where('domain', $query)->first();
            });
            if (!$domain_info) {
                $domain_info = Cache::remember('domain_info_n' . $hash, $this->cache_daily, function () use ($query) {
                    $data = ['type_search' => "get-other", 'uri_request' => "https://" . $query, 'domain' => $query];
                    $get_info = $this->ScraperController->_nojsScraper('info-domain', $data);

                    return $get_info;
                });
            }
            //Get Ads From Couponupto
            $search_key = $query . " coupons";
            //Get All Information
            $keyword_top = $this->common->_randomInLocal(16, 'keywords', $domain_info['_id']);
            $seo_config = Cache::remember('site_configs', $this->cache_daily, function () {
                return DB::collection('website_configs')->orderBy('created_at', 'desc')->first();
            });
            if (array_key_exists('advertise_keyword', $seo_config) && $seo_config['advertise_keyword'] != "") {
                $advertise = str_replace(" ", "", $seo_config['advertise_keyword']);
                $advertise_list = explode(",", $advertise);
                if (is_array($advertise_list)) {
                    $brand_top = Cache::remember('brand_keywords', $this->cache_weekly, function () use ($advertise_list) {
                        return Stores::whereIn('slug_store', $advertise_list)->take(20)->get();
                    });
                }
            } else {
                $brand_top = Cache::remember('brand_keywords', $this->cache_weekly, function () {
                    return Stores::where('store_cat', 'LIKE', '%top%')->orderBy('updated_at', 'desc')->get()->take(20);
                });
            }
            $other_global = $this->_getOtherGlobal($seo_config);
            $posts = $this->common->_randomInLocal(4, 'posts', 'posts' . $hash);
            $html_modal = false;
            if (isset($_GET['cid'])) {
                $cid = trim($_GET['cid']);
                $html_modal = Cache::remember('html_modal' . $cid, $this->cache_daily, function () use ($all_data, $cid,$query,$keyword_top) {
                    $result_modal = Results::where('domain', $query)->Where('_id', $cid)->first();
                    if ($result_modal) {
                        return view('site_jobs.email_modal', compact(['result_modal', 'keyword_top','result_modal']))->render();
                    } else {
                        return false;
                    }
                });
            }
            if ($all_data) {
                return view('site_jobs.more_offers', compact(['other_global', 'domain_info', 'seo_config', 'brand_top', 'search_key', 'all_data', 'keyword_top','posts','html_modal']));
            } else {
                return view('site_jobs.more_offers', compact(['other_global', 'domain_info', 'seo_config', 'brand_top', 'search_key', 'keyword_top','posts','html_modal']));
            }
        } else {
            return redirect("/", 301);
        }
    }
    public function _searchKeyword(Request $request)
    {
        
        if (!empty($request->q)) {
            
            
                $query_str = trim($request->q);
                
                $slug = slugify($query_str);
                $hash = md5("keyword_" . $slug);
                $keyword = Cache::remember($hash, $this->cache_hour, function () use ($slug) {
                    return Keywords::where('keyword_slug', $slug)->first();
                });
                           
               
            
            if ($keyword) {                
                if ($keyword->tracked == 1) {

                    return redirect(route('v_keyword', ['slug' => $keyword->keyword_slug]), 301);
                } else {
                    /*Scrape New Data*/
                    $hash = md5("keyword_" . $keyword->keyword_slug);
                    $keyword_scraper = $this->ScraperController->_scraperQuery($keyword->_id, 1);
                    if ($keyword_scraper->code == 1) {
                        Cache::forget($hash);
                        return redirect(route('v_keyword', ['slug' => $keyword->keyword_slug]), 301);
                    } else {
                        /*Crawl Error*/
                        return back(302);
                    }
                }
            } else {
                $keyword_scraper = $this->ScraperController->_scraperQuery($query_str, 0);
                if ($keyword_scraper->code == 1) {
                    $hash = md5("keyword_" . $keyword_scraper->keyword_slug);
                    Cache::forget($hash);
                    return redirect(route('v_keyword', ['slug' => $keyword_scraper->keyword_slug]), 301);
                } else {
                    return back(302);
                }
            }
        } else {

            return redirect('/', 301);
        }
    }

    public function _viewKeyword(Request $request)
    {
        
        if ($request->slug) {
            
            $slug = trim(strtolower($request->slug));
            $hash = md5("keyword_" . $slug);
            $keyword = Cache::remember($hash, $this->cache_hour, function () use ($slug) {
                return Keywords::where('keyword_slug', $slug)->first();
            });
            
            if ($keyword) {
                
                $id_obj = new ObjectId($keyword['_id']);
                $all_data = Cache::remember('all_data' . $hash, $this->cache_daily, function () use ($id_obj) {
                    return Results::where('for_keyword', $id_obj)->get()->toArray();
                });
                
                if (!$all_data || count($all_data) == 0) {
                    $keyword_scraper = $this->ScraperController->_scraperQuery($keyword['_id'], 1);
                    if ($keyword_scraper->code != 1) {
                        abort(500);
                    }
                    Cache::forget('all_data' . $hash);
                    $all_data = Cache::remember('all_data' . $hash, $this->cache_daily, function () use ($id_obj) {
                        return Results::where('for_keyword', $id_obj)->get()->toArray();
                    });
                }
                $count_data = count($all_data);
                if ($count_data > 0) {
                    /*Sort Rank & Created_at*/
                    if ($count_data < $this->limit_results) {
                        $this->limit_results = $count_data;
                    }
                    $all_data = collect($all_data)
                        ->take($this->limit_results)
                        ->unique('link')
                        ->values()
                        ->all();
                }
                $search_key = $keyword['keyword'];
                $search_filter = Cache::remember('search_filter' . $hash, $this->cache_daily, function () use ($search_key, $all_data) {
                    return trim(strtolower(filter_store_name($search_key)));
                });

                $seo_config = $this->common->_getConfig();
               
                $all_data = collect($all_data)->sortBy('rank')->values()->all();

                // Sort result
                if (isset($request->t) && $request->t == 'lastest') {
                    $all_data = collect($all_data)->sortBy('created_at')->values()->all();
                }
                if (isset($request->t) && $request->t == 'today') {
                    $sort = [];
                    foreach ($all_data as $value) {
                       if (((time() - strtotime($all_data[0]['created_at']))/60/60/24)<=1) {
                           $sort[]=$value;
                       }
                    }
                    $all_data = $sort;
                }
                if (isset($request->t) && $request->t == '3days') {
                    $sort = [];
                    foreach ($all_data as $value) {
                       if (((time() - strtotime($all_data[0]['created_at']))/60/60/24)<=3) {
                           $sort[]=$value;
                       }
                    }
                    $all_data = $sort;
                }
                if (isset($request->t) && $request->t == '7days') {
                    $sort = [];
                    foreach ($all_data as $value) {
                       if (((time() - strtotime($all_data[0]['created_at']))/60/60/24)<=7) {
                           $sort[]=$value;
                       }
                    }
                    $all_data = $sort;
                }
                if (isset($request->c) && $request->c == 'az') {
                    $all_data = collect($all_data)->sortBy('title')->values()->all();
                }
                if (isset($request->c) && $request->c == 'za') {
                    $all_data = collect($all_data)->sortByDesc('title')->values()->all();
                }
                if (isset($request->c) && $request->c == 'relevance') {
                    $all_data = collect($all_data)->sortBy('title')->values()->all();
                }
                if (isset($request->f) && $request->f == 'english') {
                }
                if (isset($request->c) && $request->f == 'other') {
                    
                }

                //Get All Information
                $random_kws = $this->common->_randomInLocal(25, 'keywords', $keyword['_id']);
                $trending_search = array_slice($random_kws, 0, 20);
                $related_search = array_slice($random_kws, 10, 15);

                $posts = $this->common->_randomInLocal(4, 'posts', 'posts' . $hash);
                $faqs = $this->common->_randFAQs($hash, $keyword['keyword'], 4);
                $html_modal = false;
                $stores = $this->common->_randomInLocal(12,'stores','store-kw');
                $mobile = false;
                $detect_mobile = new \Mobile_Detect();
                $rate_star = Cache::remember('rate_count' . $hash, $this->cache_month, function () {
                    return ['count_rate' => rand(40, 50), 'avg_rate' => rand(4 * 10, 5 * 10) / 10];
                });
                if (isset($_GET['cid'])) {
                    $cid = trim($_GET['cid']);
                    $html_modal = Cache::remember('html_modal' . $cid, $this->cache_daily, function () use ($all_data, $cid, $related_search) {
                        $result_modal = collect($all_data)->firstWhere('_id', $cid);
                        if ($result_modal) {
                            return view('site_jobs.email_modal', compact(['result_modal', 'related_search']))->render();
                        } else {
                            return false;
                        }
                    });
                }
                if ($detect_mobile->isMobile() || isset($_GET['amp'])) {
                    return view('amp.keyword.keyword', compact(['rate_star',
                        'stores',
                        'html_modal',
                        'mobile',
                        'faqs',
                        'posts',
                        'seo_config',
                        'search_key',
                        'keyword',
                        'all_data',
                        'trending_search',
                        'related_search',
                        'search_filter']));
                } else {
                    return view('site_jobs.keyword', compact(['rate_star',
                        'stores',
                        'html_modal',
                        'mobile',
                        'faqs',
                        'posts',
                        'seo_config',
                        'search_key',
                        'keyword',
                        'all_data',
                        'trending_search',
                        'related_search',
                        'search_filter']));
                }
            } else {
                return redirect('/', 302);
            }
        } else {
            return redirect('/', 302);
        }
    }
    

    protected function _outLink(Request $request)
    {
        if ($request->result_id) {
            $id = trim($request->result_id);
            if ($request->ref == 'domain') {
                $domain_info = DomainInfo::find($id);
                if ($domain_info) {
                    return redirect("http://" . $domain_info['domain'], 302);
                }
            } else {
                $link_go = Results::find($id);
                if ($link_go) {
                    return redirect(urldecode($link_go['link']), 302);
                }
            }
            return redirect(route('home'));
        }
    }
    // add fuction _relatedKeyword
    public function _relatedKeyword(Request $request)
    {
        $method = $request->method();
       
        if ($request->q && $request->ref) {
            $query = trim(strtolower($request->q));
            $ref = $request->ref;
            if ($ref == md5("search" . $query)) {
                if ($method == "GET") {
                    return redirect("/" . $query, 301);
                } else {
                    return response()->json(['code' => 1, 'msg' => secure_url("/" . $query)]);
                }
            }
            $hash = md5($query . "_query_related");
            $verify_ref = Cache::remember('verify_ref' . $hash, $this->cache_hour, function () use ($ref) {
                return Keywords::find(new ObjectId($ref));
            });

            if ($verify_ref) {
                $query_trust = explode(",", $verify_ref['k_related']);
                if (in_array($query, $query_trust)) {
                    $exits = Cache::remember('check_found' . $hash, $this->cache_hour, function () use ($query) {
                        return Keywords::where('keyword_slug', slugify($query))->first();
                    });
                    if ($exits) {
                        $count_results = Results::where('for_keyword', new ObjectId($exits['_id']))->count();
                        if ($exits['tracked'] == 1 && $count_results > 0) {
                            if ($method == "GET") {
                                Cache::forget('all_data' . md5($exits['keyword_slug'] . 'go_page'));
                                return redirect("/" . $exits['keyword_slug'], 301);
                            } else {
                                return response()->json(['code' => 1, 'msg' => secure_url("/" . $exits['keyword_slug'])]);
                            }
                        } else {
                            $keyword_scraper = $this->ScraperController->_scraperQuery($exits['_id'], 1);
                        }
                    } else {
                        /*Not Exits*/
                        $keyword_scraper = $this->ScraperController->_scraperQuery($query, 0);
                    }
                    if ($keyword_scraper->code == 1) {
                        if ($method == "GET") {
                            Cache::forget('all_data' . md5($keyword_scraper->keyword_slug . 'go_page'));
                            return redirect("/" . $keyword_scraper->keyword_slug, 301);
                        } else {
                            return response()->json(['code' => 1, 'msg' => secure_url("/" . $keyword_scraper->keyword_slug)]);
                        }
                    } else {
                        if ($method == "GET") {
                            abort(403, "Error Search: " . $keyword_scraper->code);
                        } else {
                            return response()->json(['code' => 403, 'msg' => "Error Search: " . $keyword_scraper->code]);
                        }
                    }
                } else {
                    if ($method == "GET") {
                        return redirect("/" . $verify_ref['keyword_slug'], 301);
                    } else {
                        return response()->json(['code' => 1, 'msg' => secure_url("/" . $verify_ref['keyword_slug'])]);
                    }
                }
            } else {
                if ($method == "GET") {
                    return redirect("/", 301);
                } else {
                    return response()->json(['code' => 1, 'msg' => secure_url("/")]);
                }
            }
        } else {
            if ($method == "GET") {
                return redirect("/", 301);
            } else {
                return response()->json(['code' => 1, 'msg' => secure_url("/")]);
            }
        }
    }
    // function for route post_faq
    protected function _postFAQs(Request $request)
    {
        if ($request->id_kw && $request->id_q) {
            $id_kw = trim($request->id_kw);
            $id_q = trim($request->id_q);
            $kw = Cache::remember('kw_id_' . $id_kw, $this->cache_hour, function () use ($id_kw) {
                return Keywords::find(new ObjectId($id_kw));
            });
            if ($kw) {
                if (isset($kw['also_ask'])) {
                    /*Valid Ask ID Follow Keyword*/
                    $ask_valid = collect($kw['also_ask'])->firstWhere('_id', $id_q);
                    if ($ask_valid) {
                        /*Find Ask Is Keyword - Exits*/
                        $ask_kw = Cache::remember('kw_id_' . $id_q, $this->cache_daily, function () use ($id_q) {
                            return Keywords::find(new ObjectId($id_q));
                        });
                        if ($ask_kw) {
                            if (isset($ask_kw['tracked']) && $ask_kw['tracked'] > 0) {
                                /*If exits & tracked*/
                                /*md5($keyword_slug . "go_page")*/
                                Cache::remember('check_found' . $ask_kw['keyword_slug'] . 'go_page', $this->cache_daily, function () use ($ask_kw) {
                                    /*Add Cache for Slug*/
                                    return $ask_kw;
                                });
                                return response()->json(['code' => 1, 'msg' => route('v_keyword', ['slug' => $ask_kw['keyword_slug']])]);
                            } else {
                                $ask_scraper = $this->ScraperController->_scraperQuery($id_q, 1);
                            }
                        } else {
                            $ask_scraper = $this->ScraperController->_scraperQuery($ask_valid['question'], 2, $id_q);
                        }
                        if (isset($ask_scraper) && $ask_scraper->code === 1) {
                            Cache::remember('check_found' . $ask_scraper->keyword_slug . 'go_page', $this->cache_daily, function () use ($ask_scraper) {
                                /*Add Cache for Slug*/
                                return Keywords::where('keyword_slug', $ask_scraper->keyword_slug)->first();
                            });
                            return response()->json(['code' => 1, 'msg' => route('v_keyword', ['slug' => $ask_scraper->keyword_slug])]);
                        } else {
                            return response()->json(['code' => 403, 'msg' => "Error Search: " . $ask_scraper->code]);
                        }
                    } else {
                        /*Id ask valid return r*/
                        return response()->json(['code' => 1, 'msg' => route('home', ['q' => $kw->keyword_slug])]);
                    }
                } else {
                    /*Id ask ERROR return r*/
                    return response()->json(['code' => 1, 'msg' => route('v_keyword', ['slug' => $kw->keyword_slug])]);
                }
            } else {
                /*ID KW not found return home*/
                return response()->json(['code' => 1, 'msg' => route('home', ['q' => $kw->keyword_slug])]);
            }
        } else {
            /*Params Error*/
            return response()->json(['code' => 1, 'msg' => route('home')]);
        }
    }

}
