<?php

namespace App\Http\Controllers\Posts;

use App\Categories;
use App\Http\Controllers\Controller;
use App\Posts;
use App\Tags;
use App\RunTask;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Lullabot\AMP\AMP;
use MongoDB\BSON\ObjectId;

class CmsPostController extends Controller
{
    //
    private $cache_time = 1;
    private $amp;

    public function __construct()
    {
        $this->middleware(['auth']);
        $this->amp = new AMP();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cpanel.posts.index_table');
    }
    protected function UpdateOrCreate($entity_id,$entity_type,$schedule)
    {
        $task = RunTask::updateOrCreate(
            ['entity_id'=>new ObjectId($entity_id)],
            ['entity_type'=>$entity_type,
            'schedule' => $schedule,
            'status' => '0',
            ]);
        return $task;
    }
    protected function show_svs(Request $request)
    {
        $columns = array(
            0 => 'type',
            1 => 'img',
            2 => 'title',
            3 => 'created_at',
            4 => 'options',
        );

        $limit = intval($request->input('length'));
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $hash = md5(date("H") . "information" . $limit . $start . $order . $dir);

        $totalData = Posts::count();

        $totalFiltered = $totalData;

        if (empty($request->input('search.value'))) {
            $posts = Posts::offset(intval($start))
            ->take($limit)
            ->orderBy($order, $dir)
            ->get();
        } else {
            $search = $request->input('search.value');
            $hash = md5(date("H") . $limit . $start . $order . $dir . $search);
            $posts = Posts::where('_id', $search)
            ->orWhere('slug', $search)
            ->orWhere('title', 'LIKE', "%{$search}%")
            ->offset(intval($start))
            ->take($limit)
            ->orderBy($order, $dir)
            ->get();
            $totalFiltered = Posts::where('_id', $search)
            ->orWhere('slug', $search)
            ->orWhere('title', 'LIKE', "%{$search}%")
            ->count();
        }
        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                $nestedData['_id'] = $post['_id'];
                if ($post['type'] == 'infor') {
                    $nestedData['type'] = "<span class='label label-primary'>Information</span>";
                    $link_type = "/blog/";
                } elseif ($post['type'] == 'post') {
                    $nestedData['type'] = "<span class='label label-default'>Post</span>";
                    $link_type = "/blog/";
                } elseif ($post['type'] == 'event') {
                    $nestedData['type'] = "<span class='label label-warning'>Event</span>";
                    $link_type = "/event/";
                } elseif ($post['type'] == 'company') {
                    $nestedData['type'] = "<span class='label label-info'>Company</span>";
                    $link_type = "/company/";
                } else {
                    $nestedData['type'] = "<span class='label label-danger'>Page</span>";
                    $link_type = "/blog/";
                }
                if (isset($post['featured_img']) && $post['featured_img'] !== '') {
                    $nestedData['img'] = '<img height="80px" class="lazyload"
                    data-original="' . $post['featured_img'] . '"
                    src="/assets/images/health-loading.png"
                    alt="' . str_title($post['title']) . '"/>';
                } else {
                    $nestedData['img'] = '<img height="80px" class="lazyload"
                    data-original="' . asset('assets/images/health-loading.png') . '"
                    src="/assets/images/health-loading.png"
                    alt="' . str_title($post['title']) . '"/>';
                }
                $nestedData['title'] = '<a href="' . secure_url($link_type . $post['slug']) . '" target="_blank">' . $post['title'] . '</a>';
                if ($post['status'] == '1') {
                    $nestedData['title'] .= '<br/><span class="badge badge-primary">Publish</span>';
                } elseif ($post['status'] == '0') {
                    $nestedData['title'] .= '<br/><span class="badge badge-secondary">Draft</span>';
                }else {
                    $nestedData['title'] .= '<br/><span class="badge badge-secondary">Scheduled</span>';
                }
                $nestedData['created_at'] = 'Created at: ' . Carbon::parse($post['created_at'])->format('Y-m-d H:m:s') . '<br/>Updated at:' . Carbon::parse($post['updated_at'])->format('Y-m-d H:m:s');
                $nestedData['options'] = '<a href="' . route('cpanel.posts.edit', ['id' => $post['_id']]) . '"><span class="label label-primary"><i class="fas fa-pen-square"></i></span></a>
                <a href="' . route('cpanel.posts.destroy', $post['_id']) . '" data-id="' . $post['_id'] . '" data-title="' . $post['title'] . '" data-method="delete" class="jquery-postback"><span class="label label-danger"><i class="fas fa-window-close"></i></span></a>';
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data,
            "hash" => $hash,
        );

        echo json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return array
     */

    protected function _newTags($tags)
    {
        $list_tags = explode(',', trim($tags));
        $last_tag_id = Tags::max('id');
        $tag_ids = [];
        if ($list_tags) {
            foreach ($list_tags as $tag) {
                if ($tag != null || $tag != '') {
                    $new_tag = new Tags;
                    $new_tag->name = $tag;
                    $new_tag->id = $last_tag_id;
                    $new_tag->save();
                    array_push($tag_ids, $new_tag['_id']);
                }
            }
        }
        return $tag_ids;
    }

    public function store(Request $request)
    {
        $all_req = $request->all();
        unset($all_req['_token']);
        unset($all_req['submit']);
        unset($all_req['slug']);
        if ($request->new_tags != '') {
            $tag_ids = $this->_newTags($request->new_tags);
            if (isset($all_req['tags'])) {
                $all_req['tags'] = array_merge($all_req['tags'], $tag_ids);
            } else {
                $all_req['tags'] = $tag_ids;
            }
        }
        if ($all_req['schedule']!= null) {
            $all_req['status'] = '2';
            $all_req['schedule'] = strtotime($all_req['schedule']);
        }
        if ($request->slug != '' || $request->slug != null) {
            $all_req['slug'] = slugify($request->slug);
            $slug_exits = Posts::where('slug', $all_req['slug'])->count();
            if ($slug_exits > 0) {
                Session::flash('error', 'Slug:' . $all_req['slug'] . ' already exists');
                return Redirect::back();
            }
            if($all_req['type'] == 'infor'){
                $all_req['status'] = '1';
            }
        }

        $all_req['id'] = rand(2000, 99999);
        $all_req['content'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $all_req['content']);
        $all_req['excerpt'] = mb_substr(strip_tags($all_req['content']), 0, 160, "utf-8");
        $this->amp->loadHtml($all_req['content']);
        $amp_html = $this->amp->convertToAmpHtml();
        $all_req['amp_html'] = $amp_html;
        $saved = Posts::create($all_req);
        $task = $this->UpdateOrCreate($saved->_id,$saved->type,$saved->schedule);
        Session::flash('message', 'Created:<a href="' . route('blog', ['slug' => $saved->slug]) . '" target="_blank">' . $saved->title . '</a>');
        return Redirect::back();
    }

    public function create(Request $request)
    {
        //
        $categories = Cache::remember('categories', 60, function () {
            return Categories::all();
        });
        $tags = Cache::remember('tags', 60, function () {
            return Tags::all();
        });
        return view('cpanel.posts.add', compact(['categories', 'tags']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $edit = Posts::find($id);
        $categories = Cache::remember('categories', 60, function () {
            return Categories::all();
        });
        $tags = Cache::remember('tags', 60, function () {
            return Tags::all();
        });
        return view('cpanel.posts.edit', compact(['edit', 'categories', 'tags']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $all_req = $request->all();
        unset($all_req['_token']);
        unset($all_req['submit']);
        if ($request->new_tags) {
            $tag_ids = $this->_newTags($request->new_tags);
            $all_req['tags'] = array_merge($all_req['tags'], $tag_ids);
        }
        if($all_req['type'] == 'infor'){
            $all_req['status'] = '1';
        }
        if ($all_req['schedule']!= null) {
            $all_req['status'] = '2';
            $all_req['schedule'] = strtotime($all_req['schedule']);
        }
        $this->amp->loadHtml($all_req['content']);
        $amp_html = $this->amp->convertToAmpHtml();
        $all_req['amp_html'] = $amp_html;
        $all_req['content'] = preg_replace('/(<[^>]+) style=".*?"/i', '$1', $all_req['content']);
        $all_req['excerpt'] = mb_substr(strip_tags($all_req['content']), 0, 160, "utf-8");
        $saved = Posts::where('_id', $id)->update($all_req);
        $task = $this->UpdateOrCreate($id,$all_req['type'],$all_req['schedule']);
        $post_info = Posts::find($id);
        Cache::forget(md5($post_info['slug']) . 'post');
        Session::flash('message', 'Updated: <a href="' . route('blog', ['slug' => $post_info['slug']]) . '">' . $post_info['title'] . '</a>');
        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        //
        Posts::where('_id', $id)->delete();
        return response()->json(['msg' => "DELETE SUCCESS:" . $id]);
    }

    protected function searchByName(Request $request)
    {
        $value = trim($request->value);
        $informs = Posts::where('inform_title', 'like', '%' . $value . '%')
        ->select('inform_title', 'inform_slug')
        ->limit(15)
        ->get();
        return response()->json($informs);
    }
}
