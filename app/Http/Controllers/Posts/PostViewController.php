<?php

namespace App\Http\Controllers\Posts;

use App\Http\Controllers\Services\CommonServices;
use App\Posts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class PostViewController extends Controller
{
    private $cache_daily = 60 * 24;
    private $cache_short = 5;
    private $cache_weekly = 60 * 24 * 7;
    private $common;

    public function __construct()
    {
        $this->common = new CommonServices();
    }

    public function _latestPost($limit, $hash)
    {
        return Cache::remember($hash, $this->cache_daily, function () use ($limit) {
            return Posts::where([
                ['type','=','post'],
                ['status','=','1']
            ])->orderBy('updated_at', 'desc')
                ->take($limit)
                ->get();
        });
    }

    /*Information*/
    protected function _blog(Request $request)
    {
        if ($request->slug && $request->slug != "") {
            $slug = trim(strtolower($request->slug));
            $post = Cache::remember(md5($slug) . 'post', $this->cache_daily, function () use ($slug) {
                return Posts::where('slug', $slug)->first();
            });
        }else{
            $post = null;
        }
        if ($post!=null) {
            $seo_config = $this->common->_getConfig();
                $footer = $this->common->_randomInLocal(5, 'footer', 'footer');
                $search = $this->common->_randomInLocal(10, 'keywords','keywords');
                $latest_post = $this->_latestPost(5, $post['_id']);
               //nextpost
                
                $slug = Posts::where([['type','=','post'],['status','=','1']])->where('created_at', '>', $post->created_at)->first();
                return view('site_jobs.blog', compact(['footer', 'seo_config', 'post', 'latest_post','search','slug']));
            
        } else {
            
            $currentPage = isset($_GET['page']) ? (int)$_GET['page'] : 1;
            $blogs = Cache::remember('list_blogs' . $currentPage, $this->cache_daily, function () {
                return Posts::where([
                ['type','=','post'],
                ['status','=','1']
            ])->simplePaginate(6);
            });
            
            if ($request->ajax()) {
                $view = view('site_jobs.ajax.more_blogs', compact('blogs'))->render();
                echo $view;
            } else {
                $seo_config = $this->common->_getConfig();
                $related_search = $this->common->_randomInLocal(24, 'keywords', 'kw_list_blogs');
                $job_titles = $this->common->_randomInLocal(10, 'careers', 'careers_list_blogs');
                $footer = $this->common->_randomInLocal(5, 'footer', 'footer');
                return view('site_jobs.list_blogs', compact(['blogs', 'footer', 'seo_config', 'job_titles', 'related_search']));
            }
        }
    }
}
