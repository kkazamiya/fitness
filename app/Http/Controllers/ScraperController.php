<?php

namespace App\Http\Controllers;

use App\ConnectOther;
use App\Coupons;
use App\DomainInfo;
use Illuminate\Http\Request;
use App\Keywords;
use App\Results;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use MongoDB\BSON\ObjectId;
use voku\helper\HtmlDomParser;

class ScraperController extends Controller
{
    //
    private $cache_sort = 5;
    private $cache_daily = 60 * 24;
    private $page_scraper = 1;
    private $node_api, $py_api, $nojs_api;
    private $black_kw = ["http://", "https://", "$(", '\"', "({", ".php", ".html", ".aspx", "?url", "http:", "subscribe4less"];

    public function __construct()
    {
        $this->node_api = env('NODE_API');
        $this->py_api = env('PY_API');
        $this->nojs_api = env('NOJS_API');
        //$this->middleware(['auth']);
    }

    protected function _index()
    {
        if (Auth::check()) {
            $count_exits = Keywords::where('tracked', 0)->count();
            $count_tracked = Keywords::where('tracked', 1)->count();
            $count_error = Keywords::where('tracked', '>', 1)->count();
            return view('cpanel.scrapers.run_bot', compact(array('count_exits', 'count_tracked', 'count_error')));
        }
    }

    protected function _getOtherInfo(Request $request)
    {
        if (Auth::check()) {
            $other = ConnectOther::where('tracked', 0)->orderBy('updated_at', 'desc')->first();
            if ($other && $other['full_url'] != "") {
                $data = ['type_search' => "get-other", 'uri_request' => $other['full_url'], 'domain' => $other['full_url']];
                $result_data = $this->_nojsScraper('get-other', $data);
                if ($result_data->code == 1) {
                    $other->tracked = 1;
                    $other->full_data = (object)['title' => $result_data->title, 'desc' => $result_data->desc];
                    $other->save();
                }
                $result_data->other_type = $other['type'];
                $result_data->full_url = $other['full_url'];
            } else {
                $result_data = ['code' => 3, 'msg' => 'STOP'];
            }
            return response()->json($result_data);
        }
    }

    protected function _trackControl(Request $request)
    {
        if (Auth::check()) {
            if ($request->sort_by) {
                switch ($request->sort_by) {
                    case 'new':
                        $keyword = Keywords::where('tracked', 0)->orderBy('updated_at', 'desc')->first();
                        break;
                    case 'old':
                        $keyword = Keywords::where('tracked', 0)->orderBy('updated_at', 'asc')->first();
                        break;
                    case 'view':
                        $keyword = Keywords::where('tracked', 0)->orderBy('view', 'desc')->first();
                        break;
                    default:
                        $keyword = Keywords::where('tracked', 0)->orderBy('updated_at', 'desc')->first();
                        break;
                }
                if ($keyword) {
                    $result_data = $this->_bingNodeScraper($keyword['keyword'], $keyword['_id']);
                    if ($result_data->code == 1) {
                        $k2sitemap = new SiteMapController();
                        $k2sitemap->_key2SiteMap($keyword);
                    }
                    return response()->json($result_data);
                } else {
                    return response()->json(['code' => 3, 'slug' => "", 'msg' => 'Stop']);
                }
            }
        }
    }

    protected function _bingSaveData($data_results, $id)
    {
        $added = 0;
        $keywords_related = [];
        $best_offers = 0;
        foreach ($data_results as $page) {
            foreach ($page->results as $index => $value) {
                $exits = Results::where('hash_r', md5($value->visible_link . $id . $value->rank))->first();
                $sale_info = filter_info($value->title, $value->snippet);
                if (!$exits) {
                    $new_result = new Results;
                    $new_result->link = $value->visible_link;
                    $new_result->title = $value->title;
                    $new_result->snippet = $value->snippet;
                    $new_result->rank = $value->rank;
                    $new_result->hash_r = md5($value->visible_link . $id . $value->rank);
                    $new_result->rating = explode("/", $value->rating)[0];
                    $new_result->for_keyword = $id;
                    $new_result->sale_info = json_encode($sale_info);
                    $new_result->save();
                }
                $added++;
            }

            foreach ($page->related_searches as $i => $v) {
                array_push($keywords_related, $v);
            }
            foreach ($page->best_offers as $i2 => $v2) {
                $hash_r = md5($id . $v2->title . $v2->coupon_code);
                $exits = Coupons::where('hash_r', $hash_r)->count();
                if ($exits == 0) {
                    $new_coupon = new Coupons;
                    $new_coupon->title = $v2->title;
                    $new_coupon->coupon_code = $v2->coupon_code;
                    $new_coupon->hash_r = $hash_r;
                    $new_coupon->for_keyword = $id;
                }
                $best_offers++;
            }
        }
        return ['code' => 1, 'added' => $added, 'best_offers' => $best_offers, 'keyword_related' => array_unique($keywords_related)];
    }


    private function _resultInfo($results)
    {
        $added = 0;
        $keywords_related = [];
        $best_offers = 0;
        foreach ($results as $page) {
            $added = $added + $page->added;
            $best_offers = $best_offers + $page->best_offers;
            foreach ($page->keywords_related as $i => $v) {
                array_push($keywords_related, $v);
            }
        }
        return ['added' => $added, 'k_related' => array_unique($keywords_related), 'best_offers' => $best_offers];
    }

    public function _bingNodeScraper($keyword, $id, $page = 2)
    {
        $bing_params = [
            'cc' => 'US',
            'mkt' => 'en-US',
            'q' => trim($keyword),
            'promote' => "RelatedSearches,SpellSuggestions",
            'safeSearch' => false
        ];
        if ($page == 1) {
            $this->page_scraper = 1;
            $urls_request = "https://bing.com/search?" . http_build_query($bing_params) . "\n";
            $hash = md5(http_build_query($bing_params));
        } elseif ($page == 'more') {
            /*Search Page 2*/
            $bing_params['first'] = 11;
            $urls_request = "https://bing.com/search?" . http_build_query($bing_params) . "\n";
            $hash = md5(http_build_query($bing_params));
        } else {
            $urls_request = [
                "https://bing.com/search?" . http_build_query($bing_params) . "\n",
                "https://bing.com/search?first=11&" . http_build_query($bing_params) . "\n",
            ];
            $hash = md5(json_encode($urls_request));
        }
        if (Cache::has('bing_' . $hash)) {
            return (object)['code' => 2, 'slug' => Str::slug($keyword, "-"), 'msg' => 'Duplicate Thread'];
        }
        if (is_array($urls_request)) {
            //Multi Page
            foreach ($urls_request as $index => $value) {
                $data = ['keyword' => trim($keyword), 'uri_request' => urlencode($value), 'id_keyword' => $id];
                $result = $this->_curlPost($data, $this->node_api);
                if ($result->code != 1) {
                    return (object)['code' => $result->code, 'keyword' => $keyword, 'msg' => "Api Error:" . json_encode($result->msg)];
                }
            }
        } else {
            //One Page
            $data = ['keyword' => trim($keyword), 'uri_request' => urlencode($urls_request), 'id_keyword' => $id];
            $result = $this->_curlPost($data, $this->node_api);
            if ($result->code != 1) {
                return (object)['code' => $result->code, 'keyword' => $keyword, 'msg' => "Api Error:" . $result->msg];
            }
        }

        $kw_info = Keywords::find($id);
        return (object)['code' => 1, 'keyword' => $keyword, 'slug' => $kw_info['keyword_slug'], 'results_info' => $result];
    }

    public function _bingPost(Request $request)
    {
        if (Auth::check()) {
            if ($request->bing_scraper == true) {
                $keyword = trim($request->keyword);
                $id = trim($request->id);
                $result_data = $this->_bingNodeScraper($keyword, $id);
                return response()->json($result_data);
            }
        }
    }

    public function _googleNodeScraper($kw, $id_kw, $id_old = null)
    {
        if ($id_kw == false) {
            if ($id_old != null) {
                $data = ['crawl_type' => 'manual', 'options' => ['q' => $kw], 'find_on' => 'bing', 'id_old' => $id_old];
            } else {
                $data = ['crawl_type' => 'manual', 'options' => ['q' => $kw], 'find_on' => 'bing'];
            }
        } else {
            $data = ['crawl_type' => 'manual', 'options' => ['q' => $kw], 'find_on' => 'bing', 'id_kw' => $id_kw];
        }
        return $this->_curlPost($data, $this->node_api);
    }

    public function _scraperQuery($key_query, $option = 1, $id_old = null)
    {
        /* @option
         * 1: Keyword Exits & Update Results
         * 2: Keyword Not Exits, But Have _Id
         * 0: Keyword Not Exits - Create And Update Results
         */
        if ($option == 0) {
            return $this->_googleNodeScraper($key_query, false, 0);
        } else if ($option == 2) {
            return $this->_googleNodeScraper($key_query, false, $id_old);
        } else {
            $key_info = Keywords::find($key_query);
            return $this->_googleNodeScraper($key_info['keyword'], $key_info['_id'], 0);
        }
    }

    public function _simpledomPHP($url)
    {
        $document = HtmlDomParser::file_get_html($url);
        $title = $document->find('title', 0)->plaintext;
        $desc = $document->find("meta[name=description]", 0)->content;
        return ['title' => $title, 'snippet' => $desc];
    }

    public function _nojsScraper($type, $data_send)
    {
        $result = $this->_curlPost($data_send, $this->nojs_api);
        if (property_exists($result, 'code')) {
            if ($result->code == 1) {
                if ($type == 'info-domain') {
                    $save_domain_info = new DomainInfo;
                    $save_domain_info->domain = $data_send['domain'];
                    if (property_exists($result, 'title')) {
                        $save_domain_info->title = $result->title;
                    } else {
                        //$save_domain_info->title = "Up to 99% OFF Coupon Codes, Promo Codes & Discounts " . $data_send['domain'];
                        $save_domain_info->title = "Best Offers On " . $data_send['domain'];
                    }
                    if (property_exists($result, 'desc')) {
                        $save_domain_info->desc = $result->desc;
                    } else {
                        //$save_domain_info->desc = "Top Coupon Codes, Promo Codes & Discounts - Updated Daily at " . $data_send['domain'] . ". 100% Working Code & All Verified by users. Visit us for more money saving tips!";
                        $save_domain_info->desc = "All the latest and best offers based on reviews and arrangements from users.";
                    }
                    $save_domain_info->save();
                    $result = $save_domain_info;
                }
            } else {
                $save_domain_info = new DomainInfo;
                $save_domain_info->domain = $data_send['domain'];
                $save_domain_info->title = "Best Offers On " . $data_send['domain'];
                $save_domain_info->desc = "All the latest and best offers based on reviews and arrangements from users.";
                
                // dd($save_domain_info);
                $save_domain_info->save();
                $result = $save_domain_info;
            }
        }
        return $result;
    }

    private function _curlGet($url_query)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url_query);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $data = curl_exec($ch);
        curl_close($ch);
        return json_decode($data);
    }

    protected function _curlPost($data, $url_api)
    {
        $payload = json_encode($data);
        $ch = curl_init($url_api);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($payload))
        );
        $result = json_decode(curl_exec($ch));
        curl_close($ch);
        if (is_object($result) && property_exists($result, 'code')) {
            return $result;
        } else {
            $result = (object)['code' => 99, 'msg' => 'Cannot Connnect To API'];
            return $result;
        }
    }

    /*Scrap Multi*/


    protected function _vBotMulti(Request $request)
    {
        if (Auth::check()) {
            $count_exits = Keywords::where('tracked', 0)->count();
            $count_tracked = Keywords::where('tracked', 1)->count();
            $count_error = Keywords::where('tracked', '>', 1)->count();
            return view('cpanel.scrapers.bot_multi', compact(array('count_exits', 'count_tracked', 'count_error')));
        }
    }

    protected function _add2Sitemap(Request $request)
    {
        if ($request->keyword) {
            $info_kw = Keywords::where('_id', $request->keyword['_id'])->first();
            $k2sitemap = new SiteMapController();
            $k2sitemap->_key2SiteMap($info_kw);
            return response()->json($k2sitemap);
        }
    }

    protected function _getKeyword(Request $request)
    {
        if ($request->limit) {
            $limit_get = intval($request->limit);
            if ($limit_get == 1) {
                $list_kw = Keywords::where('tracked', 0)->orderBy('updated_at', 'asc')->first();
            } else {
                $list_kw = Keywords::where('tracked', 0)->orderBy('updated_at', 'asc')->take($limit_get)->get();
            }
            if ($list_kw) {
                foreach ($list_kw as $index => $kw) {
                    foreach ($this->black_kw as $black) {
                        if (strpos(strtolower($kw['keyword']), $black) > -1) {
                            Keywords::findOrFail($kw['_id'])->delete();
                            unset($list_kw[$index]);
                        }
                    }
                }
                return response()->json($list_kw);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
