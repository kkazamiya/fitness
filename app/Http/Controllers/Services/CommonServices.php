<?php

namespace App\Http\Controllers\Services;

use App\Careers;
use App\Cities;
use App\Keywords;
use App\Posts;
use App\Tags;
use App\Stores;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class CommonServices extends Controller
{
    private $cache_daily = 60 * 24;
    private $cache_short = 5;
    private $cache_weekly = 60 * 24 * 7;

    public function _getConfig()
    {
        return Cache::remember('site_configs', $this->cache_daily, function () {
            return DB::collection('website_configs')->orderBy('created_at', 'desc')->first();
        });
    }

    public function _getResultTag($all_data)
    {
        foreach ($all_data as $index => $per_data) {
            $tag_info = [];
            if (isset($all_data[$index]['job_type']) && count($all_data[$index]['job_type']) > 0) {
                foreach ($all_data[$index]['job_type'] as $job_type) {
                    // dd($job_type);
                    $job_info = Cache::remember('tag' . $job_type, $this->cache_daily, function () use ($job_type) {
                        return Tags::where('_id', $job_type)->first()->toArray();
                    });
                    if ($job_info) {
                        array_push($tag_info, $job_info);
                    }
                }
            } elseif (isset($all_data[$index]['experiences']) && count($all_data[$index]['experiences']) > 0) {
                foreach ($all_data[$index]['experiences'] as $job_type) {
                    $job_info = Cache::remember('tag' . $job_type, $this->cache_daily, function () use ($job_type) {
                        return Tags::where('_id', $job_type)->first()->toArray();
                    });
                    if ($job_info) {
                        array_push($tag_info, $job_info);
                    }
                }
            }
            $all_data[$index]['tag_info'] = $tag_info;
        }
        return $all_data;
    }

    public function _randomInLocal($limit, $index, $id_rand)
    {
        $hash = md5($limit . $index . $id_rand);
        if ($index == 'keywords') {            
            $results = Cache::remember($hash, $this->cache_daily, function () use ($limit) {
                $list_kws = collect(Keywords::raw(function ($collection) use ($limit) {
                    return $collection->aggregate([['$sample' => ['size' => $limit * 2]]]);
                }));
                return $list_kws->filter(function ($value, $key) {
                    return $value['tracked'] == 1 || $value['tracked'] == 2;
                })->take($limit)->all();
            });
            
        } elseif ($index == 'posts') {
            $results = Cache::remember($hash, $this->cache_daily, function () use ($limit) {
                return Posts::raw(function ($collection) use ($limit) {
                    return $collection->aggregate([['$match' => ['type' => 'post','status' => '1']], ['$sample' => ['size' => $limit]]]);
                });
            });
        
        } elseif ($index == 'stores') {
            $results = Cache::remember($hash, $this->cache_daily, function () use ($limit) {
                return Stores::raw(function ($collection) use ($limit) {
                    return $collection->aggregate([['$sample' => ['size' => $limit]]]);
                });
            });
        
        }else {
            $results = false;
        }
        return $results;
    }

    public function _randFAQs($hash, $keyword, $limit)
    {
        $faqs = [
            ["How many @ workouts per week?", "OThe frequency of @ workout will depend on your fitness, weight, ... If you want to get the answer’s detail, our experts will directly give you specific advice and the most suitable method for your health."],
            ["How to know when you update the latest @ information?", "As you know, we usually update the latest @ services, which are newly launched on the fitness market. We usually update the latest information for @ every 2 weeks, so please visit our site more than to get the newest @ information."],
            ["What can I do if I want to choose an expert to advise me about @?", "Simply, at Fitnessol, you have a lot of choices to select the best gym expert for yourself. On the Fitnessol site, the exercise experts are a professional coach, they have a lot of experience in the fitness field. You can choose one to ask about @ through the expert's detailed information our site offers."],
            ["Can you refer me to the best @ service?", "Our system will refer to the best results for @ in the priority order at the Fitnessol service tool on the homepage. Therefore, when searching for @ service,  do not ignore on-top results, it is the best choice for @."],
            ["By searching @, how many results do you give me?", "Because we are an online fitness service aggregative site, the database of our site is so varied and numerous. Therefore, you can get dozens of results related to @ in a moment after clicking to find."],
            ["Are there any @ fitness centers near my location?", "All of the @ fitness centers we recommended come from different locations. In order to find a @ fitness center near your living location, you can use the searching tool on our site to type ' @ fitness center + your location'. Our system will give the nearest @ fitness center for you immediately."],
            ["Do you offer any @ service for the elderly?", "Yes. Our @ service is for people knowing the way to care about their health. There are sufficient @ services for both young people and the elderly. With the old people, they can increase maximum resistance and lead a healthier lifestyle than ever before at the Fitnessol site."],
            ["What are the best ways to save money when purchasing your @ service?", "We will give some tips for you to save the most maximum way when purchasing  @ service. Firstly, it is to wait for special occasions such as Halloween Day, Military Day, Black Friday, or other days. Besides, you can also buy a large quantity to get the wholesale price."],
            ["How to contact you when I am disappointed in @ service?", "Surely, customer satisfaction is the top of our goal. Therefore, our customer support team is always willing to make an effort to satisfy users. In case you are disappointed in our service for @, please contact or notify us via email."],
            ["How to get more details about benefits @?", "All our recommendations for @ will present all the main benefits briefly. To know more benefits for @ service, you can click to the link of the @ service on our site."],
            ["I want my site to appear on Fitnessol as a recommendation for the search @?", "We have to make an effort to find a site meeting the search '@', so it will be great if your site is one of them. We always welcome all proposals from you, please send us an email attaching your URL, we will check and work with it. "],
            ["How to suggest Fitnessol a site for the search @?", "Most of our business problems are solved via email. Therefore, if you see any quality site for the search @ and want to suggest to us, you can contact us via email in 'Contact us'."],
            ["Will there be a detailed service description in each recommendation for @?", "The page of @ search result will appear right after you type your search and press 'Enter'. You almost don't have to wait for results. In case, the system cannot give you any recommendation, it may be because our site is under maintenance or there are somethings that need to be fixed."],
            ["Do you give the rating of a service shown on the page of the search results for @?", "There is no specific rating for services shown on the page of the search results for @. However, normally, the best sites for that service will be on the top of the results. Take it as the priority of your choice."],
            ["Where is your data for @ collected from?", "Where we collect data for @? From many of the quality sites, reputable sources that offer the services, products, or anything meeting your demand."],
            ["Do you make sure there is no risk as I click a link attached in '@' search results?", "We are not responsible for any risk you face in the process of using our site to search @, including virus invading, harmful links... Only when you detect wrong information or non-quality sites, you can contact us via email for a fix."],
            ["Can I search for '@' on your site for free?", "Yes, it is completely free. All users of Fitnessol are not required to have any personal information or cost for using our site. Feel free to search for your expected fitness service."],
            ["I see my site shown as a recommendation for the search '@', I want to remove it from the page?", "By giving people recommendations for @, we aim to bring benefits for both customers and service providers. However,  if the appearance of your site on our site makes you unsatisfied, we will remove it from the system right after required."],
            ["How can I contact you for a trade?", "We mainly work with partners via email, so if you want to do a trade, please contact us."]
        ];

        return Cache::remember('faqs' . $hash, $this->cache_weekly, function () use ($keyword, $faqs, $limit) {
            $randoms = array_rand($faqs, $limit);
            $results = [];
            foreach ($randoms as $index => $random) {
                $results[$index][0] = str_replace("@", ucwords($keyword), $faqs[$random][0]);
                $results[$index][1] = str_replace("@", ucwords($keyword), $faqs[$random][1]);
            }
            return $results;
        });
    }
}
