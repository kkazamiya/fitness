<?php

namespace App\Http\Controllers;

use App\Keywords;
use App\SiteMapPos;
use App\Stores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class SiteMapController extends Controller
{
    //
    private $type_info = [
        'url_host' => "https://www.onlinejobe.com/",
        'page_name' => "jobs-",
        'map_index' => "jobs_map"
    ];

    private $url_per_sitemap = 1000;
    private $url_per_txt = 10000;

    public function _sitemapDynamic(Request $request)
    {
        $randoms = Keywords::select('slug', 'tracked')->raw(function ($collection) {
            return $collection->aggregate([['$sample' => ['size' => $this->url_per_sitemap]]]);
        });
        $sitemap_random = App::make('sitemap');
        $sitemap_random->setCache('sitemapdynamic.' . rand(0, 9999), 1);
        // add sitemaps (loc, lastmod (optional))
        $update_day = 0;
        $update_time = gmdate('Y-m-d\TH:i:s\Z', strtotime('now'));
        foreach ($randoms as $index => $random) {
            if ($index != 0 && $index % 250 == 0) {
                $update_day++;
                $update_time = gmdate('Y-m-d\TH:i:s\Z', strtotime('-' . $update_day . ' days'));
            }
            if ($random['tracked'] == 1) {
                $url = route('v_keyword', ['slug' => $random['slug']]);
                $sitemap_random->add($url, $update_time, 0.9, 'always');
            }
        }
        return $sitemap_random->render('xml');
    }

    public function _sitemapDynamicStore(Request $request)
    {
        $type_map = $this->type_info['store'];
        if ($request->type_sitemap) {
            $type_sitemap = trim($request->type_sitemap);
            if ($request->page_sitemap) {
                $randoms = Stores::raw(function ($collection) {
                    return $collection->aggregate([['$sample' => ['size' => $this->url_per_sitemap]]]);
                });
                $sitemap_random = App::make('sitemap');
                $sitemap_random->setCache('sitemapdynamicstore.' . rand(0, 9999), 1);
                // add sitemaps (loc, lastmod (optional))
                $update_day = 0;
                $update_time = gmdate('Y-m-d\TH:i:s\Z', strtotime('now'));
                foreach ($randoms as $index => $random) {
                    if ($index != 0 && $index % 250 == 0) {
                        $update_day++;
                        $update_time = gmdate('Y-m-d\TH:i:s\Z', strtotime('-' . $update_day . ' days'));
                    }
                    $url = $type_map['url_host'] . $random['slug_store'];
                    $sitemap_random->add($url, $update_time, 0.9, 'always');
                }
                return $sitemap_random->render('xml');
            } else {
                /*Index*/
                $count = Cache::remember('stores_count', 60, function () {
                    return Stores::count();
                });
                $page = intval($count / $this->url_per_sitemap) + 1;
                $sitemap_index = App::make('sitemap');
                $sitemap_index->setCache('sitemap_index.' . $type_sitemap, 3600);
                // add sitemaps (loc, lastmod (optional))
                $update_day = 0;
                $update_time = gmdate('Y-m-d\TH:i:s\Z', strtotime('now'));
                for ($i = 0; $i <= $page; $i++) {
                    if ($i != 0 && $i % 200 == 0) {
                        $update_day++;
                        $update_time = gmdate('Y-m-d\TH:i:s\Z', strtotime('-' . $update_day . ' days'));
                    }
                    $sitemap_index->addSitemap(secure_url('sitemap-store/' . $type_sitemap . '/' . $type_map['page_name'] . $i), $update_time, 1, 'always');
                }
                return $sitemap_index->render('sitemapindex');
            }
        }
    }

    private function _addSiteMapXML($urls, $type_map, $page)
    {
        /*New Sitemap With Page*/

        $new_sitemap = App::make('sitemap');
        $new_sitemap->store('xml', '/sitemapv2/' . $type_map['page_name'] . $page);
        $new_sitemap->addSitemap(secure_url('/sitemapv2/' . $type_map['page_name'] . $page . '.xml'));
        $new_sitemap->model->resetItems();

        /*Add New Page XML To SiteMap Index Of Type*/
        $file_sitemap_index = public_path() . '/sitemapv2/' . $type_map['map_index'] . '.xml';
        if (File::exists($file_sitemap_index)) {
            chmod($file_sitemap_index, 0777);
            try {
                $xml_index = simplexml_load_file($file_sitemap_index);
            } catch (\Exception $e) {
                $content_xml = file_get_contents($file_sitemap_index);
                $str = preg_replace('/&(?!;{6})/', '&amp;', $content_xml);
                file_put_contents($file_sitemap_index, $str);
                $xml_index = simplexml_load_file($file_sitemap_index);
            }
            $new_url = $xml_index->addChild('sitemap');
            $new_url->addChild('loc', secure_url('/sitemapv2/' . $type_map['page_name'] . $page . '.xml'));
            $xml_index->saveXML($file_sitemap_index);
        }
        /*End Add To Index*/

        /*Add Urls To Page XML*/
        $file = public_path() . '/sitemapv2/' . $type_map['page_name'] . $page . ".xml";
        if (File::exists($file)) {
            chmod($file, 0777);
            try {
                $xml = simplexml_load_file($file);
            } catch (\Exception $e) {
                /*Fix Error*/
                $content_xml = file_get_contents($file);
                $str = preg_replace('/&(?!;{6})/', '&amp;', $content_xml);
                file_put_contents($file, $str);
                $xml = simplexml_load_file($file);
            }
            foreach ($urls as $url) {
                $new_url = $xml->addChild('url');
                $new_url->addChild('loc', $url);
                $new_url->addChild('lastmod', gmdate('Y-m-d\TH:i:s\Z', strtotime('now')));
                $new_url->addChild('changefreq', 'always');
            }
            $xml->saveXML($file);
        }
        return true;
    }

    public function _key2SiteMap($keywords)
    {
        $type_map = $this->type_info['keyword'];
        $cache_name = 'cache_sitemap_kw';
        $sitemap_keyword = SiteMapPos::where('type', 'keywords')->first();

        if (Cache::has($cache_name)) {
            $urls = Cache::get($cache_name);
            $urls[] = $type_map['url_host'] . $keywords['keyword_slug'];
        } else {
            $urls = [];
            $urls[] = $type_map['url_host'] . $keywords['keyword_slug'];
            Cache::put($cache_name, $urls, 60 * 24 * 7);
        }
        if (count($urls) == $this->url_per_sitemap) {
            $sitemap_keyword['latest_page'] = $sitemap_keyword['latest_page'] + 1;
            $sitemap_keyword['latest_count'] = 0;

            $this->_addSiteMapXML($urls, $type_map, $sitemap_keyword['latest_page']);
            /*Clear Cache & Save Position*/
            Cache::forget($cache_name);
            $sitemap_keyword->save();
        }
        return true;
    }
}
