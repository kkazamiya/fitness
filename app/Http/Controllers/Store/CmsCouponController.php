<?php

namespace App\Http\Controllers\Store;

use App\Stores;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Coupons;
use Illuminate\Support\Facades\Cache;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\UTCDateTime as MongoUTC;
use App\Http\Controllers\Controller;

class CmsCouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $cache_time = 1;
    private $auth_logged;

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $auth_logged = Auth::user()->email;
        return view('cpanel.coupons.index_table');
    }

    public function coupon_table()
    {
        return view('cpanel.coupons.index_table');
    }

    protected function coupons_svs(Request $request)
    {
        $columns = array(
            0 => 'order_index',
            1 => 'code_type',
            2 => 'coupon_name',
            3 => 'coupon_code',
            4 => 'expired_date',
            5 => 'update_at',
            6 => 'options',
        );

        $limit = intval($request->input('length'));
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        $hash = md5($limit . $start . $order . $dir . date("H"));
        $totalData = Cache::remember('totalData' . $hash, $this->cache_time, function () {
            return Coupons::count();
        });
        $totalFiltered = $totalData;

        if (empty($request->input('search.value'))) {
            $posts = Cache::remember('posts' . $hash, $this->cache_time, function () use ($limit, $order, $dir, $start) {
                return Coupons::with('stores')->offset(intval($start))
                    ->take($limit)
                    ->orderBy($order, $dir)
                    ->get();
            });
        } else {
            $search = $request->input('search.value');
            $slug_store = slugify($search);
            $hash = md5($limit . $start . $order . $dir . date("H") . $search);
            $posts = Cache::remember('posts' . $hash, $this->cache_time, function () use ($limit, $order, $dir, $start, $search) {
                return Coupons::with('stores')->where('forStore', 'LIKE', "%{$search}%")
                    ->orWhere('coupon_name', 'LIKE', "%{$search}%")
                    ->orWhere('coupon_code', 'LIKE', "%{$search}%")
                    ->offset(intval($start))
                    ->take($limit)
                    ->orderBy($order, $dir)
                    ->get();
            });

            $totalFiltered = Cache::remember('totalFiltered' . $hash, $this->cache_time, function () use ($search) {
                return Coupons::where('forStore', 'LIKE', "%{$search}%")
                    ->orWhere('coupon_name', 'LIKE', "%{$search}%")
                    ->orWhere('coupon_code', 'LIKE', "%{$search}%")
                    ->count();
            });
        }

        $data = array();
        if (!empty($posts)) {
            foreach ($posts as $post) {
                if ($post['order_index'] > 0) {
                    $nestedData['order_index'] = '<span class="label label-success">Order ' . $post['order_index'] . '</span>';
                } else {
                    $nestedData['order_index'] = '<span class="label label-default">Normal</span>';
                }
                $nestedData['code_type'] = ucwords($post['code_type']);
                $nestedData['coupon_name'] = '<a title="Edit coupon: ' . $post['coupon_name'] . '" href="' . route('cpanel.coupons.edit', ['id' => $post['coupon_slug']]) . '">' . $post['coupon_name'] . ' <i class="fa fa-edit"></i></a> <br/>
					Store: <a class="text-red" href="/stores/' . $post['forStore'] . '" target="_blank"
                           title="Go to ' . $post->stores['store_name'] . '">' . $post->stores['store_name'] . '</a> - Id Coupons:' . $post['_id'];
                $nestedData['coupon_code'] = $post['coupon_code'];
                if ($post['expired_date']) {
                    try {
                        $nestedData['expired_date'] = date('Y-m-d', intval($post['expired_date']));
                    } catch (\Exception $e) {
                        $nestedData['expired_date'] = $post['expired_date'];
                    }
                } else {
                    $nestedData['expired_date'] = '<span class="label label-default">not_expired</span>';
                }
                $nestedData['updated_at'] = 'Created at:' . Carbon::parse($post['created_at'])->format('d-m-Y H:m:s') . '<br/>Updated at:' . Carbon::parse($post['updated_at'])->format('d-m-Y H:m:s') . '<br/><small>by:' . $post['created_by'] . '</small>';
                $nestedData['options'] = '<a href="' . route('cpanel.coupons.destroy', $post['coupon_slug']) . '" data-id="' . $post['coupon_slug'] . '" data-name="' . $post['coupon_name'] . '" data-method="delete" class="jquery-postback"><span class="label label-danger"><i class="fa fa-remove"></i></span></a>';
                $nestedData['_id'] = $post['_id'];
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data" => $data
        );

        echo json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    private function _uniqueStr()
    {
        $characters = '1234567890';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 13; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function create()
    {
        //
        return view('cpanel.coupons.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //
        $auth_logged = Auth::user()->email;
        //if (Auth::user()->hasPermissionTo('add-coupon')) {
        $slug = $this->_uniqueStr();
        if ($request->input('expired_date') == '') {
            $expired_date = false;
        } else {
            $expired_date = strtotime($request->input('expired_date') . " 23:59:59");
        }
        DB::collection('coupons')->insert(
            [
                'coupon_name' => $request->input('coupon_name'),
                'coupon_excerpt' => $request->input('coupon_excerpt'),
                'coupon_code' => $request->input('coupon_code'),
                'code_status' => intval($request->input('code_status')),
                'off_sale' => intval($request->input('off_sale')),
                'off_type' => $request->input('off_type'),
                'code_type' => $request->input('code_type'),
                'expired_date' => $expired_date,
                'forStore' => $request->input('forStore'),
                'aff_link' => $request->input('aff_link'),
                'order_index' => intval($request->input('order_index')),
                'coupon_slug' => $slug,
                'created_at' => new MongoUTC(strtotime('now') * 1000),
                'created_by' => $auth_logged,
                'update_at' => strtotime("now"),
            ]
        );
        Stores::where('slug_store', trim($request->input('forStore')))->increment('coupon_added');
        //DB::collection('stores')->where('slug_store', $request->input('forStore'))->increment('coupon_added');
        $msg = "Created coupon";
        return redirect()->route('cpanel.coupons.index', ['msg' => $msg]);
        //}
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        //
        $coupon_edit = Coupons::where('coupon_slug', $id)->first();
        return view('cpanel.coupons.edit', compact(array('coupon_edit')));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        //
        $auth_logged = Auth::user()->email;
        if ($request->input('expired_date') == '') {
            $expired_date = false;
        } else {
            $expired_date = strtotime($request->input('expired_date') . " 23:59:59");
        }
        Coupons::where('coupon_slug', $id)
            ->update([
                'coupon_name' => $request->input('coupon_name'),
                'coupon_excerpt' => $request->input('coupon_excerpt'),
                'coupon_code' => $request->input('coupon_code'),
                'code_status' => intval($request->input('code_status')),
                'off_sale' => intval($request->input('off_sale')),
                'off_type' => $request->input('off_type'),
                'code_type' => $request->input('code_type'),
                'expired_date' => $expired_date,
                'order_index' => intval($request->input('order_index')),
                'forStore' => $request->input('forStore'),
                'aff_link' => $request->input('aff_link'),
                'update_at' => strtotime("now"),
                'created_by' => $auth_logged,
            ]);
        $msg = "Update coupon:" . $request->input('coupon_name');
        return redirect()->route('cpanel.coupons.index', ['msg' => $msg]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $info_coupon = Coupons::where('coupon_slug', $id)->first();
        Stores::where('slug_store', trim($info_coupon['forStore']))->decrement('coupon_added');
        Coupons::find($id)->delete();
        $msg = "Delete coupon: " . $info_coupon['coupon_name'];
        return response()->json(['msg' => $msg]);
    }

    public function _fixCoupon(Request $request)
    {
        $listCoupons = Coupons::with('stores')->orderBy('update_at', 'desc')->limit(2000)->get();
        foreach ($listCoupons as $coupon) {
            if (strlen($coupon['expired_date']) > 3) {
                $expired_date = str_replace("/", "-", $coupon['expired_date']);
                DB::collection('coupons')->where('_id', $coupon['_id'])
                    ->update([
                        'expired_date' => $expired_date
                    ]);
            }
            if ($coupon->stores == null) {
                DB::collection('coupons')->where('_id', $coupon['_id'])->delete();
                echo "Remove Coupon:" . $coupon['coupon_name'];
            }
        }
        $remove_null = Coupons::where("coupon_name", "Coupon Name")->count();
        Coupons::where("coupon_name", "Coupon Name")->delete();
        echo "Remove: Null Store " . $remove_null;
        dd($listCoupons);
    }
}
