<?php

namespace App\Http\Controllers\Store;

use App\Http\Controllers\EsController;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Stores;
use App\Categories;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use MongoDB\BSON\UTCDateTime as MongoUTC;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class CmsStoreController extends Controller
{
    private $cache_time = 1;
    private $es;

    public function __construct()
    {
        $this->middleware(['auth']);
        $this->es = new EsController();
        // $this->addSiteMap = new SitemapController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('cpanel.stores.index_table');
    }
    public function add()
    {
        return view('cpanel.stores.add_excel');
    }
    public function export()
    {
        return view('cpanel.stores.export_excel');
    }

    protected function svs_store(Request $request)
    { 
        $columns = array(
            0 => 'store_image',
            1 => 'store_name',
            2 => 'coupon_added',
            3 => 'created_at',
            4 => 'options',
        );
        try {

            $limit = intval($request->input('length'));
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
            $hash = md5($limit . $start . $order . $dir);
            if ($_GET['filter_with'] != "null") {
                $filter_with = trim(strtolower($_GET['filter_with']));
            } else {
                $filter_with = 'all';
            }

            if ($filter_with == 'all') {
                $totalData = Cache::remember('totalDataall' . $hash, $this->cache_time, function () {
                    return Stores::count();
                });

            } else {
                $totalData = Cache::remember('totalData' . $hash, $this->cache_time, function () use ($filter_with) {
                    return Stores::where('net_type', $filter_with)->count();
                });
            }

            $totalFiltered = $totalData;
            
            if (empty($request->input('search.value'))) {
                if ($filter_with == 'all') {
                    $posts = Cache::remember('postsall' . $hash, $this->cache_time, function () use ($start, $limit, $order, $dir) {
                        return Stores::offset(intval($start))
                        ->take($limit)
                        ->orderBy($order, $dir)
                        ->get();
                    });
                } else {
                    $posts = Cache::remember('posts' . $hash, $this->cache_time, function () use ($start, $limit, $order, $dir, $filter_with) {
                        return Stores::where('net_type', $filter_with)->offset(intval($start))
                        ->take($limit)
                        ->orderBy($order, $dir)
                        ->get();
                    });
                }
            } else {
                $search = $request->input('search.value');
                $hash = md5($limit . $start . $order . $dir . $search);
                if ($filter_with == 'all') {
                    $posts = $this->_searchES(strtolower(trim($search)));
                    $totalFiltered = count($posts);
                } else {
                    $posts = Cache::remember('posts' . $hash, $this->cache_time, function () use ($start, $limit, $order, $dir, $filter_with, $search) {
                        return Stores::where('net_type', $filter_with)
                        ->where('slug_store', 'LIKE', '%' . slugify($search) . '%')
                        ->offset(intval($start))
                        ->take($limit)
                        ->orderBy($order, $dir)
                        ->get();
                    });
                    $totalFiltered = count($posts);
                }
            }
            $data = array();
            // dd($posts);s
            if (!empty($posts)) {
                foreach ($posts as $post) {
                    $type_store = "/store/";
                    $active_stt = '';
                    if ($post['active_stt'] == 1) {
                        $active_stt = 'checked';
                    }
                    $active_html = '<br/><div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input active_stt" data-id="' . $post['_id'] . '" name="active_stt" id="' . $post['_id'] . '" ' . $active_stt . '>
                    <label class="custom-control-label" for="' . $post['_id'] . '">Active Store</label>
                    </div>';
                    $nestedData['_id'] = $post['_id'];
                    $nestedData['store_image'] = '<a href="' . $post['store_domain'] . '" target="_blank">
                    <img height="80px" class="lazyload"
                    data-original="' . img_sync($post['store_image']) . '"
                    src="/files/5d64e1fea22d142284003812/img_loading.svg"
                    alt="' . str_title($post['store_name']) . '"/>
                    </a>';

                    $nestedData['store_name'] = $post['store_name'] . ' -
                    <a title="Go Aff Link" href="' . $post['store_aff'] . '" target="_blank"><i class="fa fa-money-bill"></i></a>
                    -
                    <a title="View on front-end" href="' . $type_store . $post['slug_store'] . '" target="_blank"><i class="fa fa-external-link-alt"></i></a><br/><a title="Clear Cache" class="text-danger" href="/c-panel/cache-clear/store/' . $post['slug_store'] . '" target="_blank"><i class="fa fa-eraser"></i></a>
                    - <a title="Take capture replace image thumb" class="text-primary" href="/c-panel/take-capture/' . $post['_id'] . '/stores" target="_blank"><i class="fa fa-camera"></i></a>';
                    $nestedData['store_name'] .= $active_html;
                    $nestedData['coupon_added'] = $post['coupon_added'] . '<br/><a title="Sync Info" href="/c-panel/sync-one" data-id-store="' . $post['_id'] . '" data-store="' . $post['store_name'] . '" data-from_collection="store" data-method="post" class="jquery-postback"><span class="label label-primary"><i class="fa fa-spinner"></i></span></a>';
                    $nestedData['created_at'] = 'Created at:' . Carbon::parse($post['created_at'])->format('d-m-Y H:m:s') . '<br/>Updated at:' . Carbon::parse($post['updated_at'])->format('d-m-Y H:m:s') . '<br/><small>by:' . $post['created_by'] . '</small>';
                    $nestedData['options'] = '<a title="Edit: ' . $post['store_name'] . '" href="' . route('cpanel.stores.edit', ['id' => $post['_id']]) . '"><span class="label label-primary"><i class="fa fa-edit"></i></span></a>
                    <a title="Remove: ' . $post['store_name'] . '" href="' . route('cpanel.stores.destroy', $post['_id']) . '" data-id="' . $post['_id'] . '" data-name="' . $post['store_name'] . '" data-method="delete" class="jquery-postback"><span class="label label-danger"><i class="fa fa-window-close"></i></span></a>';
                    $nestedData['_id'] = $post['_id'];
                    $data[] = $nestedData;
                }
            }

            $json_data = array(
                "draw" => intval($request->input('draw')),
                "recordsTotal" => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data" => $data,
                "hash" => $hash,
            );

            echo json_encode($json_data);
        } catch (\Exception $e) {
            echo $e;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {

        $categories = Categories::all()->sortByDesc("category_name");
        return view('cpanel.stores.add', compact(array( 'categories')));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $slug = Str::slug(trim($request->input('store_name')));
        $exits = Stores::where('slug_store', $slug)->count();
        if ($exits == 0) {
            if ($request->input('store_excerpt') == '') {
                $store_excerpt = "Get 70% off discount with " . $request->input('store_name') . " coupon codes and " . $request->input('store_name') . " promo codes.";
            } else {
                $store_excerpt = $request->input('store_excerpt');
            }
            $new_store = new Stores;
            $new_store->store_name = ucwords(trim($request->input('store_name')));
            $new_store->store_excerpt = $store_excerpt;
            $new_store->store_image = $request->input('store_image');
            $new_store->rich_content = trim($request->input('rich_content'));
            $new_store->store_aff = $request->input('store_aff');
            if (is_array($request->input('store_cat'))) {
                $new_store->store_cat = implode(",", $request->input('store_cat'));
            }
            $new_store->store_domain = $request->input('store_domain');
            $new_store->connect_id = $request->input('connect_id');
            $new_store->view = 0;
            $new_store->coupon_added = 0;
            $new_store->kw_related = $request->input('kw_related');
            $new_store->update_at = strtotime("now");
            if ($request->active_stt == 'on') {
                $new_store->active_stt = 1;
            } else {
                $new_store->active_stt = 0;
            }
            $new_store->save();
            /*Add to ES*/
            // $this->es->_addESDoc('stores', '_doc', $new_store);
            $msg = "Created new store: " . $request->input('store_name');
            return back()->with('success', $msg);
        } else {
            $msg = "Duplicate: " . $request->input('store_name');
            return back()->with('warning', $msg);
        }
    }
    protected function _addTXT(Request $request)
    {
        if ($request->file_url) {
            $file_url = parse_url(trim(urldecode($request->file_url)));
            $pos = intval($request->pos);
            $limit = intval($request->limit);
            $contents = file_get_contents(public_path($file_url['path']));
            $arrays = explode(PHP_EOL, trim($contents));
            $arrays = array_map('trim', array_unique($arrays));
            if (!is_array($arrays) || $pos >= count($arrays)) {
                return response()->json(['code' => 2, 'msg' => 'All Done']);
            }

            $get_pos = array_slice($arrays, $pos, $limit);
            $kws_hash = ['hash_r' => [], 'slug' => []];
            foreach ($get_pos as $index => $per_kw) {
                //$kws_hash['hash_r'][] = md5(str_filter($per_kw));
                $kws_hash['slug'][] = slugify(trim($per_kw));
            }
            //$kw_exits = Keywords::whereIn('hash_r', $kws_hash['hash_r'])->pluck('keyword')->toArray();
            $kw_slug_exits = Stores::whereIn('slug_store', $kws_hash['slug'])->pluck('slug')->toArray();
            //$test_kw = array_unique(array_merge($kw_exits, $kw_slug_exits));

            $kw_not_exits = array_diff($kws_hash['slug'], $kw_slug_exits);
            //dd($kw_not_exits);
            $datum_insert = [];
            foreach ($kw_not_exits as $new_store) {
                $new_store->store_name = ucwords(trim($request->input('store_name')));
                $new_store->store_excerpt = $store_excerpt;
                $new_store->store_image = $request->input('store_image');
                $new_store->rich_content = trim($request->input('rich_content'));
                $new_store->store_aff = $request->input('store_aff');
                if (is_array($request->input('store_cat'))) {
                    $new_store->store_cat = implode(",", $request->input('store_cat'));
                }
                $new_store->store_domain = $request->input('store_domain');
                $new_store->connect_id = $request->input('connect_id');
                $new_store->view = 0;
                $new_store->coupon_added = 0;
                $new_store->kw_related = $request->input('kw_related');
                $new_store->update_at = strtotime("now");
                if ($request->active_stt == 'on') {
                    $new_store->active_stt = 1;
                } else {
                    $new_store->active_stt = 0;
                }
            }
            if (count($datum_insert) > 0) {
                try {
                    $datum_insert = array_values($datum_insert);
                    Stores::insert($datum_insert);
                } catch (\Exception $e) {
                    echo "Error:" . $e->getMessage();
                    dd($datum_insert);
                }
            }
            $result_msg = ['total' => count($arrays),
            'total_exits' => count($kw_slug_exits),
            'total_to_new' => count($kw_not_exits),
            'pos' => $pos,
            'limit' => $limit];
            return response()->json($result_msg);
        } else {
            return response()->json(['code' => 99, 'msg' => 'Error Params']);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        //
        $store_edit = Stores::find($id);
        $categories = Categories::orderBy('categories_name', 'desc')->get();

        return view('cpanel.stores.edit', compact(array('store_edit', 'categories')));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        //
        if ($request->input('store_excerpt') == '') {
            $store_excerpt = "Get 70% off discount with " . $request->input('store_name') . " coupon codes and " . $request->input('store_name') . " promo codes.";
        } else {
            $store_excerpt = $request->input('store_excerpt');
        }
        $store_edit = Stores::find($id);
        $change_slug = false;
        if ($store_edit['store_name'] != ucwords($request->input('store_name'))) {
            $slug = Str::slug(trim($request->input('store_name')));
            $change_slug = true;
        }

        $store_edit->store_name = ucwords(trim($request->input('store_name')));
        $store_edit->store_excerpt = $store_excerpt;
        $store_edit->store_aff = $request->input('store_aff');
        $store_edit->store_domain = $request->input('store_domain');
        $store_edit->store_image = $request->input('store_image');
        $store_edit->rich_content = trim($request->input('rich_content'));
        $store_edit->connect_id = trim($request->input('connect_id'));
        $store_edit->update_at = strtotime("now");
        if ($change_slug) {
            $store_edit->slug_store = $slug;
        }
        if (is_array($request->input('store_cat'))) {
            $store_edit->store_cat = implode(",", $request->input('store_cat'));
        }
        if ($request->active_stt == 'on') {
            $store_edit->active_stt = 1;
        } else {
            $store_edit->active_stt = 0;
        }
        $store_edit->save();
        // $this->es->_findAndUpdate('stores', '_doc', $store_edit);
        $msg = "Updated store: " . $request->input('store_name');
        return back()->with('success', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        //
        Stores::find($id)->delete();
        $msg = "Delete store";
        $this->es->_findAndRemoveES('stores', '_doc', $id);
        return response()->json(['msg' => $msg]);
    }

    protected function _updateStt(Request $request)
    {
        if ($request->id) {
            $update = Stores::where('_id', trim($request->id))->update(['active_stt' => intval($request->active_stt)]);
            if ($update) {
                return response()->json(['msg' => 'Update Success:' . $request->id]);
            } else {
                return response()->json(['msg' => 'Update Fail:' . $request->id]);
            }
        } else {
            return response()->json(['msg' => 'Params Error']);
        }
    }

    private function _searchES($search_value)
    {
        if (Cache::has('stores' . md5($search_value))) {
            $ids = Cache::get('stores' . md5($search_value));
            return $ids;
        } else {
            $params = [
                'bool' => [
                    'should' => [
                        ['match' => ['store_name' => $search_value]],
                        ['match' => ['store_domain' => $search_value]],
                        ['fuzzy' => ['slug_store' => ['value' => $search_value, 'max_expansions' => '50']]]
                    ]
                ]];
                dd($params);
                $posts = Stores::searchByQuery($params, null, null, 20, null, null);
                $posts->sortBy('_score');
                foreach ($posts as $post) {
                    $post['_id'] = $post['_id_mongo'];
                }
                Cache::put('stores' . md5($search_value), $posts, $this->cache_time);
                return $posts;
            }
        }

        protected function searchByName(Request $request)
        {
            $value = trim($request->value);
            if ($request->full) {
                $stores = Stores::searchByQuery(['multi_match' => [
                    'query' => $value,
                    'fields' => ['store_name', 'slug_store', 'store_domain'],
                    'fuzziness' => 'AUTO'
                ]
            ], null, null, 15);
            } else {
                $stores = Stores::searchByQuery(['multi_match' => [
                    'query' => $value,
                    'fields' => ['store_name', 'slug_store', 'store_domain'],
                    'fuzziness' => 'AUTO'
                ]
            ], null, ['store_name', 'slug_store'], 15);
            }
            return response()->json($stores);
        }

        public function _fixStore(Request $request)
        {
            $listStore = DB::collection('stores')->orderBy('updated_at', 'desc')->limit(100)->get();
            dd($listStore);
        }

        protected function _exportDulicate()
        {
            $store_dupe = Stores::where('slug_store', 'like', '%-1')->get();
            foreach ($store_dupe as $store) {
                echo $store->store_name . "<br/>";
            }
            die();
        }
        protected function _pexportAllURL(Request $request)
        {
            $spreadsheet = new Spreadsheet();
            $Excel_writer = new Xlsx($spreadsheet);

            $spreadsheet->setActiveSheetIndex(0);
            $activeSheet = $spreadsheet->getActiveSheet();

            $activeSheet->setCellValue('A1', '_id');
            $activeSheet->setCellValue('B1', 'slug_store');
            $activeSheet->setCellValue('C1', 'store_name');
            $activeSheet->setCellValue('D1', 'store_aff');
            $activeSheet->setCellValue('E1', 'created_at');
            $activeSheet->setCellValue('F1', 'update_at');
            $activeSheet->setCellValue('G1', 'coupon_added');
            $activeSheet->setCellValue('H1', 'view');
            $activeSheet->setCellValue('I1', 'store_cat');
            $activeSheet->setCellValue('J1', 'store_image');
            $activeSheet->setCellValue('K1', 'tore_excerpt');
            $activeSheet->setCellValue('L1', 'rich_content');
            $activeSheet->setCellValue('M1', 'store_domain');
            $activeSheet->setCellValue('N1', 'updated_at');
            $activeSheet->setCellValue('O1', 'created_by');
            $activeSheet->setCellValue('P1', 'connect_id');
            $activeSheet->setCellValue('Q1', 'type_store');
            $activeSheet->setCellValue('R1', 'active_stt');
            $activeSheet->setCellValue('S1', 'net_type');

            if ($request->date_range) {
                $split = explode(" - ", trim($request->date_range));
                $start = new MongoUTC(strtotime($split[0] . " 00:00:00") * 1000);
                $end = new MongoUTC(strtotime($split[1] . " 23:59:59") * 1000);
                $apps = Stores::whereBetween('updated_at', [$start, $end])->get();
                // dd(count($apps));
                for($i=2; $i < count($apps)+2 ; $i++) { 

                    $activeSheet->setCellValue('A1'.$i, strval($apps[$i-2]['_id']));
                    $activeSheet->setCellValue('B1'.$i, strval($apps[$i-2]['slug_store']));
                    $activeSheet->setCellValue('C1'.$i, strval($apps[$i-2]['store_name']));
                    $activeSheet->setCellValue('D1'.$i, strval($apps[$i-2]['store_aff']));
                    $activeSheet->setCellValue('E1'.$i, strval($apps[$i-2]['created_at']));
                    $activeSheet->setCellValue('F1'.$i, strval($apps[$i-2]['update_at']));
                    $activeSheet->setCellValue('G1'.$i, strval($apps[$i-2]['coupon_added']));
                    $activeSheet->setCellValue('H1'.$i, strval($apps[$i-2]['view']));
                    $activeSheet->setCellValue('I1'.$i, strval($apps[$i-2]['store_cat']));
                    $activeSheet->setCellValue('J1'.$i, strval($apps[$i-2]['store_image']));
                    $activeSheet->setCellValue('K1'.$i, strval($apps[$i-2]['tore_excerpt']));
                    $activeSheet->setCellValue('L1'.$i, strval($apps[$i-2]['rich_content']));
                    $activeSheet->setCellValue('M1'.$i, strval($apps[$i-2]['store_domain']));
                    $activeSheet->setCellValue('N1'.$i, strval($apps[$i-2]['updated_at']));
                    $activeSheet->setCellValue('O1'.$i, strval($apps[$i-2]['created_by']));
                    $activeSheet->setCellValue('P1'.$i, strval($apps[$i-2]['connect_id']));
                    $activeSheet->setCellValue('Q1'.$i, strval($apps[$i-2]['type_store']));
                    $activeSheet->setCellValue('R1'.$i, strval($apps[$i-2]['active_stt']));
                    $activeSheet->setCellValue('S1'.$i, strval($apps[$i-2]['net_type']));
                    
                }
                $filename = "stores_data_" . date('Ymd') . ".xls";
 
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='. $filename);
header('Cache-Control: max-age=0');
$Excel_writer->save('php://output');

            } else {
                dd("not include date");
            }
         // $filename = "stores_data_" . date('Ymd') . ".xls";

        // header("Content-Disposition: attachment; filename=\"$filename\"");
        // header("Content-Type: application/vnd.ms-excel");
        // ini_set('memory_limit', '-1');
        }

    }
