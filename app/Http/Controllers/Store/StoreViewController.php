<?php

namespace App\Http\Controllers\Store;

use App\Advertisers;
use App\Coupons;
use App\Http\Controllers\EsController;
use App\Http\Controllers\Services\CommonServices;
use App\Posts;
use App\Rates;
use App\Reviews;
use App\Stores;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Mobile_Detect;

class StoreViewController extends Controller
{
    private $cache_sort = 5;
    private $cache_hour = 60;
    private $cache_daily = 24 * 60;
    private $cache_weekly = 24 * 60 * 7;
    private $cache_month = 24 * 60 * 30;
    private $es;

    public function __construct()
    {
        $this->es = new EsController();
        $this->common = new CommonServices();
    }

    private function _updateCoupon($id, $update)
    {
        Coupons::where('_id', $id)->update($update);
    }

    public function stores(Request $request)
    {
        $store_slug = trim($request->slug);

        $hash = md5($store_slug);
        $store_info = Cache::remember('store_info' . $hash, $this->cache_hour, function () use ($store_slug) {
            return Stores::where('slug_store', $store_slug)->first();
        });
        // dd($store_info);
        if ($store_info) {
            if ($store_info['active_stt'] == 0) {
                return redirect()->route('home');
            }
            $seo_config = Cache::remember('site_configs', $this->cache_daily, function () {
                return DB::collection('website_configs')->orderBy('created_at', 'desc')->first();
            });
            if (array_key_exists('advertise_stores', $seo_config) && $seo_config['advertise_stores'] != "") {
                $advertise = str_replace(" ", "", $seo_config['advertise_stores']);
                $advertise_list = explode(",", $advertise);
                if (is_array($advertise_list)) {
                    $brand_top = Cache::remember('brand_stores', $this->cache_daily, function () use ($advertise_list) {
                        return Stores::whereIn('slug_store', $advertise_list)->take(20)->get();
                    });
                }
            } else {

                $brand_top = Cache::remember('brand_stores', $this->cache_daily, function () {
                    return Stores::where('store_cat', 'LIKE', '%top%')->orderBy('updated_at', 'desc')->get()->take(20);
                });
            }

            $all_coupons = Cache::remember('all_coupons' . $hash, $this->cache_hour, function () use ($store_slug) {
                return Coupons::where('forStore', $store_slug)
                    ->get()->sortByDesc('order_index')->values()->all();
            });
            // dd($all_coupons);
            $count_coupon = count($all_coupons);
            $verified = 0;
            $seo_config['top_off'] = 0;
            $about_merchant = ['codes' => 0, 'deals' => 0];
            $expired_coupons = [];
            foreach ($all_coupons as $index => $coupon) {
                if ($coupon['code_status'] == 2) {
                    $verified++;
                }
                if ($coupon['code_type'] == 'code') {
                    $about_merchant['codes']++;
                } else {
                    $about_merchant['deals']++;
                }
                if ($coupon['off_type'] == "percent" && $coupon['off_sale'] > $seo_config['top_off']) {
                    $seo_config['top_off'] = $coupon['off_sale'];
                }
                if (!isset($coupon['coupon_excerpt']) || $coupon['coupon_excerpt'] === '') {
                    if ($coupon['off_type'] == "percent" && $coupon['off_sale'] != 0) {
                        $xx = $coupon['off_sale'] . '%';
                    } elseif ($coupon['off_type'] == "discount" && $coupon['off_sale'] != 0) {
                        $xx = '$' . $coupon['off_sale'];
                    } else {
                        $xx = '50%';
                    }
                    $all_coupons[$index]['coupon_excerpt'] = new_coupon_excerpt($xx, $store_info['store_name']);
                    $this->_updateCoupon($coupon['_id'], ['coupon_excerpt' => $all_coupons[$index]['coupon_excerpt']]);
                }
                if ($coupon['expired_date'] != '') {
                    $expired = strtotime('now') - intval($coupon['expired_date']);
                    if ($expired > 0) {
                        /*Expired Coupon*/
                        $expired_coupons[] = $coupon;
                        unset($all_coupons[$index]);
                    }
                }
            }
            $all_coupons = array_values($all_coupons);

            $reviews = Cache::remember('reviews' . $hash, $this->cache_hour, function () use ($store_info) {
                return Reviews::where('review_store', $store_info['slug_store'])->orderBy('review_date', 'desc')->get();
            });
            $rate_star = Cache::remember('rate_count' . $hash, $this->cache_daily, function () {
                return ['count_rate' => rand(40, 50), 'avg_rate' => rand(4 * 10, 5 * 10) / 10];
            });
            $posts = Cache::remember('posts' . $hash, $this->cache_sort, function () use ($store_info) {
                return Posts::where('forStore', $store_info['slug_store'])->orderBy('created_at', 'desc')->get();
            });
            $random_stores = $this->common->_randomInLocal(12, 'stores', $store_info['_id']);
            // dd($random_stores);
            $random_keywords = $this->common->_randomInLocal(12, 'keywords', $store_info['_id']);

            if ($count_coupon < 10) {
                $random_coupons = $this->_randomCoupons($store_info['_id']);
            }else{
                $random_coupons = [];
            }

            // $banners = $this->_getBannerStatic('store');
            $all_blogs = Cache::remember('all_blogs', $this->cache_daily, function () {
                return collect(Posts::where('type', 'post')->get());
            });
            if (count($all_blogs) > 5) {
                $rand_blogs = Cache::remember('blogs_' . $hash, $this->cache_daily, function () use ($all_blogs) {
                    return $all_blogs->random(5)->all();
                });
            } else {
                $rand_blogs = $all_blogs;
            }
            $html_modal = false;
            // dd($all_coupons);
            if (isset($_GET['cid'])) {
                if (isset($_GET['r']) && $_GET['r'] == 'e') {
                    $result_modal = collect($expired_coupons)->firstWhere('coupon_slug', trim($_GET['cid']));
                } else {
                    $result_modal = collect($all_coupons)->firstWhere('coupon_slug', trim($_GET['cid']));
                }
                
                if ($result_modal) {
                    $html_modal = view('site_jobs.stores.modal', compact(['result_modal', 'random_stores', 'store_info']))->render();
                }
            }
            $type_out = "";
            $detect_mobile = new Mobile_Detect();
            if ($detect_mobile->isMobile()) {
                return view('site_jobs.stores.stores_mobile', compact(['html_modal', 'rand_blogs', 'type_out', 'brand_top', 'rate_star', 'about_merchant', 'all_coupons', 'expired_coupons', 'store_info', 'count_coupon', 'verified', 'seo_config', 'reviews', 'posts', 'random_stores', 'random_keywords', 'random_coupons']));
            } else {
                return view('site_jobs.stores.stores', compact(['html_modal', 'rand_blogs', 'brand_top',  'rate_star', 'about_merchant', 'all_coupons', 'expired_coupons', 'store_info', 'count_coupon', 'verified', 'seo_config', 'reviews', 'posts', 'random_stores', 'random_keywords', 'random_coupons']));
            }
        } else {
            return redirect()->route('home');
        }
    }

    protected function _vote(Request $request)
    {
        $agent_ip = $request->ip();
        $exits = Rates::where('store', trim($request->website))->where('ip', $agent_ip)->count();
        if ($exits == 0) {
            if ($request->website != '') {
                $rate = new Rates;
                $rate->store = trim($request->website);
                if ($request->rating > 5) {
                    $request->rating = 5;
                }
                if ($request->rating < 3) {
                    $request->rating = 3;
                }
                $rate->rating = trim($request->rating);
                $rate->ip_agent = $agent_ip;
                if ($request->comment) {
                    $rate->comment = trim($request->comment);
                }
                $rate->save();
            }
        }

        return "Thanks";
    }

    // private function _getBannerStatic($banner_type)
    // {
    //     if ($banner_type == "home") {
    //         $hash = md5("banner-home");
    //         $all_banner = Cache::remember('get_banner' . $hash, $this->cache_daily, function () {
    //             return Advertisers::where('banner_pos', "LIKE", "H%")->get()->toArray();
    //         });

    //         return $all_banner;
    //     }
    //     if ($banner_type == "store") {
    //         $hash = md5("banner-store");
    //         $all_banner = Cache::remember('get_banner' . $hash, $this->cache_daily, function () {
    //             return Advertisers::where('banner_pos', "LIKE", "S%")->get()->toArray();
    //         });

    //         return $all_banner;
    //     }
    //     if ($banner_type == "branding-home") {
    //         $hash = md5("branding-home");
    //         $all_banner = Cache::remember('get_banner' . $hash, $this->cache_daily, function () {
    //             return Advertisers::where('banner_pos', "BH")->orderBy('order_pos', 'desc')->get();
    //         });

    //         return $all_banner;
    //     }
    //     if ($banner_type == "branding-kw") {
    //         $hash = md5("brand-kw");
    //         $all_banner = Cache::remember('get_banner' . $hash, $this->cache_daily, function () {
    //             return Advertisers::where('banner_pos', "BK")->orderBy('order_pos', 'desc')->take(45)->get();
    //         });

    //         return $all_banner;
    //     }
    //     if ($banner_type == "branding-more") {
    //         $hash = md5("branding-more");
    //         $all_banner = Cache::remember('get_banner' . $hash, $this->cache_daily, function () {
    //             return Advertisers::where('banner_pos', "BM")->orderBy('order_pos', 'desc')->take(45)->get();
    //         });

    //         return $all_banner;
    //     }
    // }

    protected function _redirectAff(Request $request)
    {
        $coupon_id = trim($request->coupon_id);

        $coupon_info = Cache::remember('redirect-' . $coupon_id, $this->cache_hour, function () use ($coupon_id) {
            return Coupons::where('coupon_slug', $coupon_id)->first();
        });
        if ($coupon_info->aff_link != '') {
            return redirect(str_replace("https//", "", $coupon_info->aff_link), 302);
        }
        $get_aff_store = Cache::remember('redirecta-' . $coupon_id, $this->cache_hour, function () use ($coupon_info) {
            return Stores::where('slug_store', $coupon_info['forStore'])->first();
        });
        if ($get_aff_store->store_aff != '') {
            return redirect(str_replace("https//", "", $get_aff_store['store_aff']), 302);
        } else {
            return redirect(str_replace("https//", "", $get_aff_store['store_domain']), 302);
        }
    }

    public function _findStore(Request $request)
    {
        if ($request->q) {
            $value_search = trim($request->q);
            $hash = md5($value_search . "typeahead.store");
            $suggest = Cache::remember('typeahead' . $hash, $this->cache_hour, function () use ($value_search) {
                return Stores::select('store_name', 'slug_store')->where('store_name', 'LIKE', '%' . $value_search . '%')->take(50)->get();
            });

            return response()->json($suggest);
        }
    }

    private function _randomCoupons($store_id)
    {
        if (Cache::has('random_coupons' . md5($store_id))) {
            return Cache::get('random_coupons' . md5($store_id));
        } else {
            $coupons = Coupons::raw(function ($collection) {
                return $collection->aggregate([['$match' => ['expired_date' => '']], ['$sample' => ['size' => 10]]]);
            });
            foreach ($coupons as $index => $coupon) {
                $store_info = Stores::where('slug_store', $coupon['forStore'])->first();
                $coupons[$index]['store_info'] = $store_info;
            }
            Cache::put('random_coupons' . md5($store_id), $coupons, $this->cache_weekly);
            return $coupons;
        }
    }

   
}
