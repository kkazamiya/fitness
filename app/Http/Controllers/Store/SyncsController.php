<?php

namespace App\Http\Controllers\Store;

use App\Categories;
use App\Coupons;
use App\Http\Controllers\EsController;
use App\Stores;
use App\SyncsModel;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use MongoDB\BSON\ObjectId;

class SyncsController extends Controller
{
    private $api_couponupto = 'https://www.couponupto.com/api/v1-get-info';
    //private $api_couponupto = 'http://127.0.0.1:8001/api/v1-get-info';
    private $secret = 'howtogetinfo_lol';
    private $cache_time = 5;
    private $client_http, $es;

    public function __construct()
    {
        $this->middleware(['auth']);
        $this->client_http = new Client();
        $this->es = new EsController();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        //
        return view('cpanel.syncs.index_table');
    }

    protected function _SyncOne(Request $request)
    {
        if ($request->id) {
            $id = trim($request->id);
            $from = trim($request->from_collection) ?? false;
            if ($from == 'store') {
                $info_store = Stores::where('_id', $id)->first()->toArray();
                if ($info_store) {
                    $params = [
                        'hash_md5' => md5(json_encode($info_store['_id']) . $this->secret),
                        'datum' => $info_store,
                        'type' => 'one-store'
                    ];
                } else {
                    $params = 'store id not found:' . $id;
                }
            } else {
                $info_sync = SyncsModel::where('_id', $id)->first()->toArray();
                if ($info_sync) {
                    $params = [
                        'hash_md5' => md5(json_encode($info_sync['_id']) . $this->secret),
                        'datum' => $info_sync,
                        'type' => 'one-sync'
                    ];
                } else {
                    $params = 'sync id not found:' . $id;
                }
            }
            if (!is_array($params)) {
                return response()->json(['code' => 98, 'msg' => $params]);
            }
            /*Send Post API*/
            $get_info = $this->client_http->request('POST', $this->api_couponupto, [
                'form_params' => $params
            ]);
            $body = json_decode((string)$get_info->getBody());
            if ($body->code == 1) {
                $res_data = $body->res;
                $coupons = $res_data->coupons;
                unset($res_data->coupon_added);
                unset($res_data->coupons);
                unset($res_data->rich_content);
                unset($res_data->store_aff);
                $orNewStore = Stores::updateOrCreate(
                    ['slug_store' => $res_data->slug_store]
                    , (array)$res_data
                );
                // $this->es->_findAndUpdate('stores', '_doc', $orNewStore, true);
                // dd($coupons);
                foreach ($coupons as $index_2 => $coupon) {
                    if (isset($coupon->hash_r) && $coupon->hash_r != null) {
                    $change_info = changeInfoCoupon($coupon, $res_data);
                    $coupon->coupon_name = $change_info['title'];
                    $coupon->coupon_excerpt = $change_info['desc'];
                    Coupons::firstOrCreate(
                        ['coupon_slug' => $coupon->coupon_slug],
                        (array)$coupon
                    );  
                    }else{
                        $coupon->hash_r = md5(json_encode($coupon->_id) . $this->secret);
                        $change_info = changeInfoCoupon($coupon, $res_data);
                        $coupon->coupon_name = $change_info['title'];
                        $coupon->coupon_excerpt = $change_info['desc'];
                        try {
                            Coupons::firstOrCreate(
                                ['coupon_slug' => $coupon->coupon_slug],
                                (array)$coupon
                            );
                        } catch (\Exception $exception) {
                        }
                    }
                }
                return response()->json([$res_data->store_name, count($coupons) . ' coupons']);
            } else {
                return response()->json($body);
            }
        } else {
            return response()->json('Params Error');
        }
    }

    protected function _gstartSync(Request $request)
    {
        return view('cpanel.syncs.sync_start');
    }

    protected function _startSync(Request $request)
    {
        $limit = 50;
        $tracked = 0;
        $active_stt = 0;
        $store_aff = false;
        if ($request->store_aff == true) {
            $store_aff = true;
        }
        if ($request->active_stt == true) {
            $active_stt = 1;
        }
        $total = SyncsModel::where('tracked', $tracked)->count();
        $total_page = intval($total / $limit);
        for ($i = 0; $i <= $total_page; $i++) {
            $skip = $i * $limit;
            $datum = collect(SyncsModel::select('connect_id', 'type_source')
                ->where('tracked', $tracked)
                ->orderBy('updated_at', 'asc')
                ->skip($skip)
                ->take($limit)->get());
            //print_r("===================Starting Page:" . $i . "/" . $total_page . PHP_EOL);
            $ids_datum = [];
            $group_by_source = $datum->groupBy('type_source')->toArray();
            if (count($group_by_source) == 0) {
                dd("===============All DONE=================");
            }
            foreach ($group_by_source as $key => $items) {
                $connect_ids = [];
                $ids = [];
                foreach ($items as $item) {
                    $connect_ids[] = trim($item['connect_id']);
                    $ids[] = trim($item['_id']);
                }
                $group_by_source[$key] = ['connect_ids' => $connect_ids, 'ids' => $ids];
            }
            $rand_key = rand(0, 9999);
            $params = [
                'hash_md5' => md5($rand_key . $this->secret),
                'datum' => $group_by_source,
                'rand_id' => $rand_key
            ];
            $get_info = $this->client_http->request('POST', $this->api_couponupto, [
                'form_params' => $params
            ]);
            $body = json_decode((string)$get_info->getBody());
            if ($body->code == 1) {
                $res_data = $body->res;
                $results_msg = [];
                foreach ($res_data as $key => $stores) {
                    foreach ($stores as $index => $store) {
                        if (property_exists($store, 'coupons')) {
                            $coupons = $store->coupons;
                            unset($store->coupons);
                            unset($store->rich_content);
                            if (!$store_aff) {
                                unset($store->store_aff);
                            }
                            $store->connect_id = $store->_id;
                            $store->active_stt = $active_stt;
                            if (property_exists($store, 'slug_store')) {
                                array_push($ids_datum, $store->connect_id);
                                $orNewStore = Stores::updateOrCreate(
                                    ['slug_store' => $store->slug_store]
                                    , (array)$store
                                );
                                // $this->es->_findAndUpdate('stores', '_doc', $orNewStore, true);
                                $coupons = collect($coupons);
                                $pluck_coupon_slug = $coupons->pluck('coupon_slug')->all();
                                $pluck_exits = Coupons::whereIn('coupon_slug', $pluck_coupon_slug)->pluck('coupon_slug')->toArray();
                                $diff_result = array_diff($pluck_coupon_slug, $pluck_exits);
                                foreach ($coupons->toArray() as $index_2 => $coupon) {
                                    // dd($coupon);
                                    if (isset($coupon->hash_r) && $coupon->hash_r != null) {
                                        if (in_array($coupon->coupon_slug, $diff_result)) {
                                            $change_info = changeInfoCoupon((object)$coupon, (object)$store);
                                            $coupon->coupon_name = $change_info['title'];
                                            $coupon->coupon_excerpt = $change_info['desc'];
                                            try {
                                                Coupons::insert((array)$coupon);
                                            } catch (\Exception $exception) {
                                            }
                                        }
                                    } else {
                                        $coupon->hash_r = md5(json_encode($coupon->_id) . $this->secret);
                                        if (in_array($coupon->coupon_slug, $diff_result)) {
                                            $change_info = changeInfoCoupon((object)$coupon, (object)$store);
                                            $coupon->coupon_name = $change_info['title'];
                                            $coupon->coupon_excerpt = $change_info['desc'];
                                            try {
                                                Coupons::insert((array)$coupon);
                                            } catch (\Exception $exception) {
                                            }
                                        }
                                    }
                                }
                                array_push($results_msg, ['stores' => $store->store_name, 'coupon' => count($coupons)]);
                            }
                        }
                    }
                }
                SyncsModel::whereIn('connect_id', $ids_datum)->update(['tracked' => 1]);
                dd($results_msg);
            } else {
                dd($body);
            }
        }
    }

    protected function stores_svs(Request $request)
    {
        $columns = array(
            0 => 'store_info',
            1 => 'connect_id',
            2 => 'created_at',
            3 => 'options',
        );
        try {
            $limit = intval($request->input('length'));
            $start = $request->input('start');
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
            $hash = md5(date("H") . $limit . $start . $order . $dir);

            $totalData = Cache::remember('totalData' . $hash, $this->cache_time, function () {
                return SyncsModel::count();
            });

            $totalFiltered = $totalData;

            if (empty($request->input('search.value'))) {
                $posts = Cache::remember('posts' . $hash, $this->cache_time, function () use (
                    $start,
                    $limit,
                    $order,
                    $dir
                ) {
                    return SyncsModel::offset(intval($start))
                        ->take($limit)
                        ->orderBy($order, $dir)
                        ->get();
                });
            } else {
                $search = $request->input('search.value');
                $hash = md5(date("H") . $limit . $start . $order . $dir . $search);
                $posts = Cache::remember('posts_s' . $hash, $this->cache_time, function () use (
                    $search,
                    $start,
                    $limit,
                    $order,
                    $dir
                ) {
                    return SyncsModel::where('slug', $search)
                        ->orWhere('connect_id', $search)
                        ->offset(intval($start))
                        ->take($limit)
                        ->orderBy($order, $dir)
                        ->get();
                });
                $totalFiltered = Cache::remember('totalFiltered' . $hash, $this->cache_time, function () use ($search) {
                    return SyncsModel::where('slug', $search)
                        ->orWhere('connect_id', $search)
                        ->count();
                });
            }
            $data = array();
            if (!empty($posts)) {
                foreach ($posts as $post) {
                    $nestedData['_id'] = $post['_id'];
                    $nestedData['connect_id'] = '<a href="' . route('cpanel.syncs.edit', $post['_id']) . '">' . $post['connect_id'] . ' <i class="fa fa-edit"></i></a><br/>/' . $post['type_source'];
                    if ($post['tracked'] == 0) {
                        $nestedData['store_info'] = '<i class="fa fa-sync"></i> Untrack';
                    } else {
                        $nestedData['store_info'] = '<i class="fa fa-check-circle"></i> Tracked';
                    }
                    $nestedData['created_at'] = 'Created at: ' . Carbon::parse($post['created_at'])->format('Y-m-d H:m:s') . '<br/>Updated at:' . Carbon::parse($post['updated_at'])->format('Y-m-d H:m:s');
                    $nestedData['options'] = '<a href="' . route('cpanel.syncs.destroy', $post['_id']) . '" data-id-store="' . $post['_id'] . '" data-store="' . $post['connect_id'] . '" data-method="delete" class="jquery-postback"><span class="label label-danger"><i class="fa fa-times-circle"></i></span></a>';
                    $data[] = $nestedData;

                }
            }

            $json_data = array(
                "draw" => intval($request->input('draw')),
                "recordsTotal" => intval($totalData),
                "recordsFiltered" => intval($totalFiltered),
                "data" => $data,
                "hash" => $hash,
            );

            echo json_encode($json_data);
        } catch (\Exception $e) {
            echo $e;
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        //
        $categories = Categories::all()->sortByDesc("category_name");
        return view('cpanel.syncs.add', compact(array('categories')));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if (!empty($request->list_ids)) {
            $list_ids = explode(PHP_EOL, trim($request->list_ids));
            $ids = array_map('trim', array_unique($list_ids));
            $ids_exits = SyncsModel::whereIn('connect_id', $ids)->pluck('connect_id')->toArray();
            $ids_not_exits = array_diff($ids, $ids_exits);
            $type = $request->type_source;
            $datum_insert = [];
            foreach ($ids_not_exits as $id) {
                $new_sync = [];
                $new_sync['connect_id'] = trim($id);
                $new_sync['type_source'] = $type;
                $new_sync['slug'] = '';
                $new_sync['tracked'] = 0;
                $datum_insert[] = $new_sync;
            }
            if (count($datum_insert) > 0) {
                SyncsModel::insert($datum_insert);
            }
            $results_msg = ['total_ids' => count($ids), 'total_exits' => $ids_exits, 'total_added' => $datum_insert];
            echo "<a href='/c-panel/sync/create'>Back</a>";
            dd($results_msg);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $edit = SyncsModel::find($id);
        $categories = Categories::all()->sortByDesc("category_name");
        if ($edit) {
            return view('cpanel.syncs.edit', compact(['edit', 'categories']));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $edit = SyncsModel::find($id);
        $edit->connect_id = trim($request->connect_id);
        $edit->type_source = trim($request->type_source);
        $edit->tracked = 0;
        $edit->save();
        return redirect()->route('cpanel.sync.index', ['msg' => 'Update ' . $request->connect_id . ' stores sync with type:' . $request->type_source]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        SyncsModel::find($id)->delete();
        return response()->json('Success Delete:' . $id);
    }
}
