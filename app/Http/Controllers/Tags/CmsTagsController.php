<?php

namespace App\Http\Controllers\Tags;

use App\Http\Controllers\Controller;
use App\Tags;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class CmsTagsController extends Controller
{
    private $cache_short = 5;
    private $cache_hourly = 60;
    private $cache_monthly = 60 * 24 * 30;

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $columns = array(
                0 => 'tag_type',
                1 => 'tag_name',
                2 => 'created_at',
                3 => 'options',
            );
            try {
                $limit = intval($request->input('length'));
                $start = $request->input('start');
                $order = $columns[$request->input('order.0.column')];
                $dir = $request->input('order.0.dir');
                $hash = md5($limit . $start . $order . $dir);

                $totalData = Cache::remember('totalData' . $hash, $this->cache_short, function () {
                    return Tags::count();
                });

                $totalFiltered = $totalData;

                if (empty($request->input('search.value'))) {
                    $posts = Cache::remember('tags' . $hash, $this->cache_short, function () use ($start, $limit, $order, $dir) {
                        return Tags::offset(intval($start))
                            ->take($limit)
                            ->orderBy($order, $dir)
                            ->get();
                    });
                } else {
                    $search = $request->input('search.value');
                    $hash = md5($limit . $start . $order . $dir . $search . "cat_list");
                    $posts = Cache::remember('tags' . $hash, $this->cache_short, function () use ($search, $start, $limit, $order, $dir) {
                        return Tags::where('tag_slug', 'LIKE', "%" . slugify($search) . "%")
                            ->offset(intval($start))
                            ->take($limit)
                            ->orderBy($order, $dir)
                            ->get();
                    });
                    $totalFiltered = Cache::remember('totalFiltered' . $hash, $this->cache_short, function () use ($search) {
                        return Tags::where('tag_slug', 'LIKE', "%" . slugify($search) . "%")
                            ->count();
                    });
                }
                $data = array();
                if (!empty($posts)) {
                    foreach ($posts as $post) {
                        $nestedData['tag_type'] = $post['tag_type'];
                        $nestedData['tag_name'] = $post['tag_name'] . '<small><br/>Id Tag: ' . $post['_id'] . '</small>';
                        $nestedData['created_at'] = 'Created at:' . Carbon::parse($post['created_at'])->format('d-m-Y H:m:s') . '<br/>Updated at:' . Carbon::parse($post['updated_at'])->format('d-m-Y H:m:s') . '<br/>';
                        $nestedData['options'] = '<a title="Edit: ' . $post['tag_name'] . '" href="' . route('cpanel.tags.edit', ['id' => $post['_id']]) . '"><span class="label label-primary"><i class="fa fa-edit"></i></span></a>
                        <a title="Remove: ' . $post['tag_name'] . '" href="' . route('cpanel.tags.destroy', $post['_id']) . '" data-id="' . $post['_id'] . '" data-name="' . $post['tag_name'] . '" data-method="delete" class="jquery-postback"><span class="label label-danger"><i class="fa fa-window-close"></i></span></a>';
                        $nestedData['_id'] = $post['_id'];
                        $data[] = $nestedData;
                    }
                }

                $json_data = array(
                    "draw" => intval($request->input('draw')),
                    "recordsTotal" => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data" => $data,
                    "hash" => $hash,
                );
                echo json_encode($json_data);
            } catch (\Exception $e) {
                dd($e);
            }
        } else {
            return view('cpanel.tags.index_table');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('cpanel.tags.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if ($request->tag_type == '') {
            return back(302)->withErrors(['Tag Type Null']);
        } else {
            try {
                $slug = slugify(trim($request->tag_name));
                $exits = Tags::where('tag_slug', $slug)->count();
                if ($exits > 0) {
                    return back(302)->withErrors(['Tag Name Exits']);
                } else {
                    $new_tag = new Tags();
                    $new_tag->tag_name = trim($request->tag_name);
                    $new_tag->tag_type = trim($request->tag_type);
                    $new_tag->save();
                }
                return redirect(route('cpanel.tags.index'), 302);
            } catch (\Exception $e) {
                dd($e);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        if ($id) {
            $editor = Tags::find($id);
            if ($editor) {
                return view('cpanel.tags.edit', compact(['editor']));
            } else {
                return back(302)->with('ID not found');
            }
        } else {
            return back(302)->with('Id not found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        if ($id) {
            $update_tag = Tags::find($id);
            $update_tag->tag_name = trim($request->tag_name);
            $update_tag->tag_type = trim($request->tag_type);
            $update_tag->save();
            return back(302)->with('Updated success');
        } else {
            return back(302)->with('Update fail');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if ($id) {
            Tags::where('_id', $id)->delete();
            return response()->json(['code' => 1, 'msg' => 'Deleted Success']);
        } else {
            return response()->json(['code' => 99, 'msg' => 'Fail']);
        }
    }
}
