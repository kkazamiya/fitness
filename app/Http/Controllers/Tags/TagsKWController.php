<?php

namespace App\Http\Controllers\Tags;

use App\Http\Controllers\Controller;
use App\Jobs\ProcessMarkTags;
use App\Keywords;
use App\TagsKW;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class TagsKWController extends Controller
{
    private $cache_short = 5;
    private $cache_hourly = 60;
    private $cache_monthly = 60 * 24 * 30;

    public function __construct()
    {
        $this->middleware(['auth']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $columns = array(
                0 => 'tag_type',
                1 => 'for_keyword',
                2 => 'created_at',
                3 => 'options',
            );
            try {
                $limit = intval($request->input('length'));
                $start = $request->input('start');
                $order = $columns[$request->input('order.0.column')];
                $dir = $request->input('order.0.dir');
                $hash = md5($limit . $start . $order . $dir);

                $totalData = Cache::remember('totalData' . $hash, $this->cache_short, function () {
                    return TagsKW::count();
                });

                $totalFiltered = $totalData;

                if (empty($request->input('search.value'))) {
                    $posts = Cache::remember('tags_kw' . $hash, $this->cache_short, function () use ($start, $limit, $order, $dir) {
                        return TagsKW::offset(intval($start))
                            ->take($limit)
                            ->orderBy($order, $dir)
                            ->get();
                    });
                } else {
                    $search = $request->input('search.value');
                    $hash = md5($limit . $start . $order . $dir . $search . "cat_list");
                    $posts = Cache::remember('tags_kw' . $hash, $this->cache_short, function () use ($search, $start, $limit, $order, $dir) {
                        return TagsKW::where('for_keyword', 'LIKE', "%{$search}%")
                            ->offset(intval($start))
                            ->take($limit)
                            ->orderBy($order, $dir)
                            ->get();
                    });
                    $totalFiltered = Cache::remember('totalFiltered' . $hash, $this->cache_short, function () use ($search) {
                        return TagsKW::where('for_keyword', 'LIKE', "%{$search}%")
                            ->count();
                    });
                }
                $data = array();
                if (!empty($posts)) {
                    foreach ($posts as $post) {
                        $nestedData['tag_type'] = $post['tag_type'] . '<br/>' . $post['tag_name'];
                        $nestedData['for_keyword'] = $post['for_keyword'] . '<small><br/>Id Tag: ' . $post['_id'] . '<br/>Effect: ' . $post['effected'] . '</small>';
                        $nestedData['created_at'] = 'Created at:' . Carbon::parse($post['created_at'])->format('d-m-Y H:m:s') . '<br/>Updated at:' . Carbon::parse($post['updated_at'])->format('d-m-Y H:m:s') . '<br/>';
                        $nestedData['options'] = '<a title="Edit: ' . $post['tag_name'] . '" href="' . route('cpanel.tags_kw.edit', ['id' => $post['_id']]) . '"><span class="label label-primary"><i class="fa fa-edit"></i></span></a>
                        <a title="Remove: ' . $post['tag_name'] . '" href="' . route('cpanel.tags_kw.destroy', $post['_id']) . '" data-id="' . $post['_id'] . '" data-name="' . $post['tag_name'] . '" data-method="delete" class="jquery-postback"><span class="label label-danger"><i class="fa fa-window-close"></i></span></a>';
                        $nestedData['_id'] = $post['_id'];
                        $data[] = $nestedData;
                    }
                }

                $json_data = array(
                    "draw" => intval($request->input('draw')),
                    "recordsTotal" => intval($totalData),
                    "recordsFiltered" => intval($totalFiltered),
                    "data" => $data,
                    "hash" => $hash,
                );
                echo json_encode($json_data);
            } catch (\Exception $e) {
                dd($e);
            }
        } else {
            return view('cpanel.tags_kw.index_table');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('cpanel.tags_kw.add');
    }

    protected function _checkEffect(Request $request)
    {
        if ($request->for_keyword) {
            $keyword_slug = str_replace('*', '%', trim(strtolower($request->for_keyword)));
            $keyword_slug = str_replace(' ', '-', $keyword_slug);
            $effect = Cache::remember('effect' . $keyword_slug, $this->cache_short, function () use ($keyword_slug) {
                return Keywords::where('keyword_slug', 'LIKE', $keyword_slug)->count();
            });
            $request->session()->flash('effect' . slugify($request->for_keyword), $effect);
            return $effect;
        } else {
            return 0;
        }
    }

    private function _keywordTrust($keyword_slug)
    {
        $filter_str = preg_replace('/[^A-Za-z0-9\-]/', '', $keyword_slug);
        $filter_str = str_replace('-', '', $filter_str);
        if (strlen($filter_str) == 3) {
            return trim(str_replace('*', ' *', $keyword_slug));
        } elseif (strlen($filter_str) < 3) {
            return false;
        } else {
            return trim($keyword_slug);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if ($request->tag_type == '') {
            return back(302)->withErrors(['Tag Type Null']);
        } else {
            try {
                $hash_r = md5(trim(strtolower($request->tag_type . $request->tag_name)));
                $kw_trust = $this->_keywordTrust($request->for_keyword);
                if ($kw_trust === false) {
                    return back(302)->withErrors(['Keyword slug illegal']);
                }
                $exits = TagsKW::where('hash_r', $hash_r)->count();
                if ($exits > 0) {
                    return back(302)->withErrors(['Tag Name Exits']);
                }
                $new_tag = new TagsKW();
                $new_tag->tag_name = trim($request->tag_name);
                $new_tag->hash_r = $hash_r;
                $new_tag->tag_type = trim($request->tag_type);
                $new_tag->for_keyword = $kw_trust;
                $new_tag->foreign_id = trim($request->foreign_id);
                $new_tag->foreign_url = trim($request->foreign_url);
                $new_tag->rel = trim($request->rel);
                if (session('effect' . slugify($request->for_keyword))) {
                    $new_tag->effected = session('effect' . slugify($request->for_keyword));
                }
                $new_tag->save();
                /*Send Job Queue Mark Tag*/
                $job_addTag = (new ProcessMarkTags($new_tag));
                $this->dispatch($job_addTag);
                return redirect(route('cpanel.tags_kw.index'), 302);
            } catch (\Exception $e) {
                dd($e);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        if ($id) {
            $editor = TagsKW::find($id);
            if ($editor) {
                return view('cpanel.tags_kw.edit', compact(['editor']));
            } else {
                return back(302)->with('ID not found');
            }
        } else {
            return back(302)->with('Id not found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        if ($id) {
            $update_tag = TagsKW::find($id);
            $update_tag->for_keyword = trim($request->for_keyword);
            $update_tag->tag_type = trim($request->tag_type);
            $update_tag->foreign_id = trim($request->foreign_id);
            $update_tag->foreign_url = trim($request->foreign_url);
            $update_tag->rel = trim($request->rel);
            if (session('effect' . slugify($request->for_keyword))) {
                $update_tag->effected = session('effect' . slugify($request->for_keyword));
            }
            $update_tag->save();
            /*Send Job Queue Mark Tag*/
            $job_addTag = (new ProcessMarkTags($update_tag));
            $this->dispatch($job_addTag);
            return back(302)->with('Updated success');
        } else {
            return back(302)->with('Update fail');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        if ($id) {
            TagsKW::where('_id', $id)->delete();
            return response()->json(['code' => 1, 'msg' => 'Deleted Success']);
        } else {
            return response()->json(['code' => 99, 'msg' => 'Fail']);
        }
    }
}
