<?php

namespace App\Http\Controllers\Tags;

use App\Careers;
use App\Cities;
use App\Http\Controllers\Services\CommonServices;
use App\Results;
use App\Tags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use MongoDB\BSON\ObjectId;

class ViewTagsController extends Controller
{
    private $cache_short = 5;
    private $cache_hourly = 60;
    private $cache_daily = 60 * 24;
    private $cache_monthly = 60 * 24 * 30;
    private $common;

    public function __construct()
    {
        $this->common = new CommonServices();
    }

    protected function _newJobs(Request $request)
    {
        $currentPage = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $all_data = Cache::remember('all_data' . $currentPage, $this->cache_daily, function () {
            return Results::orderBy('_id', 'desc')->simplePaginate(20);
        });
        if ($request->ajax()) {
            $view = view('site_jobs.ajax.more_result', compact('all_data'))->render();
            return $view;
        } else {
            $tag = Cache::remember('tag_all_data', $this->cache_daily, function () {
                return ['_id' => rand(0, 9999), 'name' => 'New', 'slug' => 'new'];
            });
            $all_data = $this->common->_getResultTag($all_data);
            $seo_config = $this->common->_getConfig();
            $random_kws = $this->common->_randomInLocal(25, 'keywords', $tag['_id']);
            $trending_search = array_slice($random_kws, 0, 9);
            $related_search = array_slice($random_kws, 10, 15);

            $job_titles = $this->common->_randomInLocal(10, 'careers', 'careers' . $tag['_id']);
            $footer = $this->common->_randomInLocal(5, 'footer', 'footer');
            $mobile = false;
            $detect_mobile = new \Mobile_Detect();
            $rate_star = Cache::remember('rate_count' . $tag['_id'], $this->cache_monthly, function () {
                return ['count_rate' => rand(40, 50), 'avg_rate' => rand(4 * 10, 5 * 10) / 10];
            });
            return view('site_jobs.tag', compact([
                'footer',
                'rate_star',
                'mobile',
                'job_titles',
                'seo_config',
                'tag',
                // 'k_related',
                'all_data',
                'trending_search',
                'related_search',
            ]));
        }
    }

    protected function _showTag(Request $request)
    {
        $request_uri = explode('/', $request->getRequestUri());
        $currentPage = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        if (isset($request->slug)) {
            $slug = trim(strtolower($request->slug));

            if ($request_uri[1] == 'job-type' || $request_uri[1] == 'experience') {
                $tag = Cache::remember($request_uri[1] . $slug, $this->cache_daily, function () use ($slug) {
                    return Tags::where('tag_slug', $slug)->first();
                });
            } elseif ($request_uri[1] == 'career') {
                $tag = Cache::remember('career' . $slug, $this->cache_daily, function () use ($slug) {
                    return Careers::where('slug', $slug)->first();
                });
            } elseif ($request_uri[1] == 'job-in') {
                $tag = Cache::remember('job-in' . $slug, $this->cache_daily, function () use ($slug) {
                    return Cities::where('slug', $slug)->first();
                });
            } else {
                return redirect('/', 301);
            }
            if ($tag) {
                $all_data = Cache::remember('data_for_tag' . $tag['_id'] . $currentPage, $this->cache_daily, function () use ($tag, $request_uri) {
                    if ($request_uri[1] == 'career') {
                        return Results::where('career_tags', 'all', [$tag['_id']])->simplePaginate(20);
                    } elseif ($request_uri[1] == 'job-in') {
                        return Results::where('city_tag', new ObjectId($tag['_id']))->simplePaginate(20);
                    } else {
                        if ($tag['tag_type'] == 'job_type') {
                            return Results::where('job_type', 'all', [new ObjectId($tag['_id'])])->simplePaginate(20);
                        } elseif ($tag['tag_type'] == 'experience') {
                            return Results::where('experiences', 'all', [new ObjectId($tag['_id'])])->simplePaginate(20);
                        } else {
                            return false;
                        }
                    }
                });
                if ($all_data) {
                    $all_data = $this->common->_getResultTag($all_data);
                    if ($request->ajax()) {
                        $view = view('site_jobs.ajax.more_result', compact('all_data'))->render();
                        return $view;
                    } else {
                        $seo_config = $this->common->_getConfig();
                        $random_kws = $this->common->_randomInLocal(25, 'keywords', $tag['_id']);
                        $trending_search = array_slice($random_kws, 0, 9);
                        $related_search = array_slice($random_kws, 10, 15);

                        $job_titles = $this->common->_randomInLocal(10, 'careers', 'careers' . $tag['_id']);
                        $footer = $this->common->_randomInLocal(5, 'footer', 'footer');
                        $mobile = false;
                        $detect_mobile = new \Mobile_Detect();
                        $rate_star = Cache::remember('rate_count' . $tag['_id'], $this->cache_monthly, function () {
                            return ['count_rate' => rand(40, 50), 'avg_rate' => rand(4 * 10, 5 * 10) / 10];
                        });
                        return view('site_jobs.tag', compact([
                            'footer',
                            'rate_star',
                            'mobile',
                            'job_titles',
                            'seo_config',
                            'tag',
                            // 'k_related',
                            'all_data',
                            'trending_search',
                            'related_search',
                        ]));
                    }
                } else {
                    return redirect(route('home'), 301);
                }
            }
        } else {
            if ($request_uri[1] == 'career') {
                $all_career = Cache::remember('all_career' . $currentPage, $this->cache_daily, function () {
                    return Careers::orderBy('slug', 'desc')->simplePaginate(50);
                });
                $seo_config = $this->common->_getConfig();
                $mobile = false;
                $detect_mobile = new \Mobile_Detect();
                return view('site_jobs.careers', compact([
                    'all_career',
                ]));
            } else {
                return redirect('/', 301);
            }
        }
    }
}
