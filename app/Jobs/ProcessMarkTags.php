<?php

namespace App\Jobs;

use App\Http\Controllers\ScraperController;
use App\Keywords;
use App\TagsKW;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use MongoDB\BSON\ObjectId;

class ProcessMarkTags implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $tag;
    public $tries = 2;
    public $timeout = 120;
    private $ScraperController;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(TagsKW $tag)
    {
        //
        $this->tag = $tag;
        $this->ScraperController = new ScraperController();
    }

    public function retryUntil()
    {
        return now()->addSeconds(60);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->tag) {
            /*Begin Start Mark Keyword With Tags*/
            $keyword_slug = str_replace('*', '%', trim(strtolower($this->tag->for_keyword)));
            $keyword_slug = str_replace(' ', '-', trim($keyword_slug));
            Keywords::where('keyword_slug', 'LIKE', $keyword_slug)->update(['tags_kw' => new ObjectId($this->tag->_id)]);
        }
    }
}
