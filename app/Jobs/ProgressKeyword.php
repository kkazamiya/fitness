<?php

namespace App\Jobs;

use App\Keywords;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProgressKeyword implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $keyword;
    public $tries = 2;
    public $timeout = 120;
    private $old_delay = 1728000; //60*60*24*20 days

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Keywords $keyword)
    {
        $this->keyword = $keyword;
    }

    public function retryUntil()
    {
        return now()->addSeconds(60);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->keyword) {
            if ($this->keyword['tracked'] == 1) {
                $time_now = strtotime('now');
                $lastest_update = strtotime(Carbon::parse($this->keyword['updated_at'])->format('Y-m-d H:m:s'));
                if (($time_now - $lastest_update) > $this->old_delay) {
                    Keywords::where('_id', $this->keyword['_id'])->update(['tracked' => 2]);
                }
            }
        }
    }
}
