<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class KeywordBlack extends Eloquent
{
    protected $collection = 'keyword_black';
    protected $primarykey = "_id";
    protected $dates = ['created_at', 'updated_at'];
}
