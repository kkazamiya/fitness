<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class KeywordCategory extends Eloquent
{
    //
    protected $collection = 'keyword_category';
    protected $primarykey = "_id";
    protected $dates = ['created_at', 'updated_at'];
}
