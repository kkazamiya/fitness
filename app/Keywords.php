<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Elasticquent\ElasticquentTrait;

class Keywords extends Eloquent
{
    //
    use Sluggable;
    protected $collection = 'keywords';
    protected $primarykey = "_id";
    protected $dates = ['created_at', 'updated_at'];

    public function sluggable()
    {
        return [
            'keyword_slug' => [
                'source' => 'keyword'
            ]
        ];
    }

    public function results()
    {
        return $this->hasMany('App\Results', '_id', 'for_keyword');
    }

    /*EloElastic*/
    use ElasticquentTrait;

    function getIndexName()
    {
        return 'keywords';
    }

    protected $indexSettings = [
        'number_of_shards' => 5,
        'number_of_replicas' => null,
        'analysis' => [
            'filter' => [
                'english_stop' => [
                    'type' => 'stop',
                    'stopwords' => '_english_'
                ],
                'english_keywords' => [
                    'type' => 'keyword_marker',
                    'keywords' => ['example']
                ],
                'english_stemmer' => [
                    'type' => 'stemmer',
                    'language' => 'english'
                ],
                'english_possessive_stemmer' => [
                    'type' => 'stemmer',
                    'language' => 'possessive_english'
                ]
            ],
            'analyzer' => [
                'autocomplete' => [
                    'tokenizer' => 'autocomplete',
                    'filter' => [
                        'english_possessive_stemmer',
                        'lowercase',
                        'english_stop',
                        'english_keywords',
                        'english_stemmer'
                    ]
                ],
                'autocomplete_search' => [
                    'tokenizer' => 'lowercase'
                ]
            ],
            'tokenizer' => [
                'autocomplete' => [
                    'type' => 'edge_ngram',
                    'min_gram' => 2,
                    'max_gram' => 20,
                    'token_chars' => [
                        'letter'
                    ]
                ]
            ]
        ]
    ];
    protected $mappingProperties = array(
        'keyword_slug' => [
            'type' => 'text',
            'analyzer' => 'keyword',
        ],
        'keyword' => [
            'type' => 'text',
            'analyzer' => 'autocomplete',
            'search_analyzer' => 'autocomplete_search'
        ],
    );
}
