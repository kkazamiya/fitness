<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Posts extends Eloquent
{
    //
    use Sluggable;
    protected $collection = 'posts';
    protected $primarykey = "_id";
    protected $dates = ['created_at', 'updated_at'];
    protected $guarded = [];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
}
