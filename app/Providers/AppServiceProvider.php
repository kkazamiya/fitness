<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Posts;
use App\Http\Controllers\Posts\PostViewController;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function __construct()
    {
        $this->post_services = new PostViewController();
    }
    public function boot()
    {
        
        View::composer('*',function($view){
            $view->with('latest_posts',$this->post_services->_latestPost(4, 'latest_post'));
        });
    }
}
