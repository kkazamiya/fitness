<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Results extends Eloquent
{
    //
    protected $collection = 'results';
    protected $dates = ['created_at', 'updated_at'];
    protected $primarykey = "_id";


    public function keyword()
    {
        /*foreign key: khóa ngoại đối chiếu của coupons
        otherKey: khóa ngoại đối chiếu của stores
        */
        return $this->belongsTo('App\Keywords', 'for_keyword', '_id');
    }
}
