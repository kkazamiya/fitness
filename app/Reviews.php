<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class Reviews extends Model
{
    //
    protected $collection = 'reviews';
    protected $primarykey = "_id";
    protected $dates = ['created_at', 'updated_at'];

    public function stores(){
        return $this->belongsTo('App\Stores', 'review_store', 'slug_store');
    }
}
