<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class RunTask extends Eloquent
{
    protected $collection = 'schedule';
    protected $primarykey = "_id";
    protected $fillable = ['entity_id','entity_type','schedule','status'];
}
