<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SpamKeyword extends Eloquent
{
    //
    protected $collection = 'spam_keyword';
    protected $primarykey = "_id";
    protected $dates = ['created_at', 'updated_at'];
    protected $index = ['keyword'];
}
