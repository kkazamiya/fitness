<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Elasticquent\ElasticquentTrait;
use Spatie\Feed\Feedable;
use Spatie\Feed\FeedItem;

class Stores extends Eloquent implements Feedable
{
    //
    use Sluggable;

    protected $collection = 'stores';
    protected $dates = ['created_at', 'updated_at'];
    protected $primaryKey = '_id';
    protected $guarded = [];

    public function sluggable()
    {
        return [
            'slug_store' => [
                'source' => 'store_name'
            ]
        ];
    }

    public function coupons()
    {
        return $this->hasMany('App\Coupons', 'slug_store', 'forStore');
    }

    public function reviews()
    {
        return $this->hasMany('App\Reviews', 'slug_store', 'review_store');
    }

    public function rates()
    {
        return $this->hasMany('App\Rates', 'slug_store', 'rate_store');
    }

    public function posts()
    {
        return $this->hasMany('App\Posts', 'slug_store', 'forStore');
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();
        $array['id'] = $array['_id'];
        unset($array['_id']);

        return $array;
    }

    /*EloElastic*/
    use ElasticquentTrait;

    function getIndexName()
    {
        return 'stores';
    }

    protected $indexSettings = [
        'number_of_shards' => 5,
        'number_of_replicas' => null,
        'analysis' => [
            'filter' => [
                'english_stop' => [
                    'type' => 'stop',
                    'stopwords' => '_english_'
                ],
                'english_keywords' => [
                    'type' => 'keyword_marker',
                    'keywords' => ['example']
                ],
                'english_stemmer' => [
                    'type' => 'stemmer',
                    'language' => 'english'
                ],
                'english_possessive_stemmer' => [
                    'type' => 'stemmer',
                    'language' => 'possessive_english'
                ]
            ],
            'analyzer' => [
                'autocomplete' => [
                    'tokenizer' => 'autocomplete',
                    'filter' => [
                        'english_possessive_stemmer',
                        'lowercase',
                        'english_stop',
                        'english_keywords',
                        'english_stemmer'
                    ]
                ],
                'autocomplete_search' => [
                    'tokenizer' => 'lowercase'
                ]
            ],
            'tokenizer' => [
                'autocomplete' => [
                    'type' => 'edge_ngram',
                    'min_gram' => 2,
                    'max_gram' => 20,
                    'token_chars' => [
                        'letter'
                    ]
                ]
            ]
        ]
    ];
    protected $mappingProperties = array(
        'slug_store' => [
            'type' => 'text',
            'analyzer' => 'keyword',
        ],
        'store_name' => [
            'type' => 'text',
            'analyzer' => 'autocomplete',
            'search_analyzer' => 'autocomplete_search'
        ],
    );

    public static function getFeedItems()
    {
        $stores = Stores::raw(function ($collection) {
            return $collection->aggregate([['$sample' => ['size' => 100]]]);
        });
        foreach ($stores as $index => $store) {
            $first_coupon = Coupons::where('forStore', $store['slug_store'])->orderBy('order_index', 'desc')->first();
            if ($first_coupon) {
                $stores[$index]['coupon'] = $first_coupon;
            } else {
                unset($stores[$index]);
            }
        }
        return $stores;
    }

    /**
     * @inheritDoc
     */
    public function toFeedItem()
    {
        // TODO: Implement toFeedItem() method.
        return FeedItem::create([
            'id' => route('stores', ['store_slug' => $this->slug_store]),
            'title' => $this->coupon->coupon_name,
            'summary' => $this->store_excerpt || 'Get coupons for ' . $this->store_name . ' up to 80%',
            'updated' => $this->updated_at,
            'link' => route('stores', ['store_slug' => $this->slug_store]),
            'author' => 'fitnessol.com',
        ]);
    }
    
}
