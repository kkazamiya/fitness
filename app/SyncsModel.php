<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SyncsModel extends Eloquent
{
    protected $collection = 'syncs_collection';
    protected $dates = ['created_at', 'updated_at'];
    protected $primaryKey = '_id';
}
