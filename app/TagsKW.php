<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class TagsKW extends Eloquent
{
    use Sluggable;

    protected $collection = 'tags_kw';
    protected $dates = ['created_at', 'updated_at'];
    protected $primaryKey = '_id';
    protected $guarded = [];
    public $type_id = ['tag' => 0, 'store_id' => 1, 'store_url' => 2, 'ref_info' => 3, 'ref_url' => 4];

    public function sluggable()
    {
        return [
            'tag_slug' => [
                'source' => 'tag_name'
            ]
        ];
    }
}
