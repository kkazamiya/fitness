<?php
/**
 * Created by PhpStorm.
 * User: nm
 * Date: 11/6/2018
 * Time: 7:03 PM
 */

if (!function_exists('rm_all_link')) {
    function rm_all_link($str)
    {
        return preg_replace('#<a.*?>(.*?)</a>#i', '\1', $str);
    }
}

if (!function_exists('filter_string')) {
    function filter_string($str)
    {
        $str = str_replace("\n", "", $str);
        $str = str_replace("\t", "", $str);
        $str = str_replace("&#13;", "", $str);
        $str = preg_replace("/&#?[a-z0-9]+;/i", "", $str);
        return $str;
    }
}

if (!function_exists('get_string_between')) {
    function get_string_between($string, $start, $end)
    {
        $string = ' ' . $string;
        $ini = strpos($string, $start);
        if ($ini == 0) return '';
        $ini += strlen($start);
        $len = strpos($string, $end, $ini) - $ini;
        return substr($string, $ini, $len);
    }
}
if (!function_exists('_uniqueStr')) {
    function _uniqueStr($str)
    {
        return md5($str . strtotime('now'));
    }
}
if (!function_exists('str_filter')) {
    function str_filter($text, $replace_with = '')
    {
        $text = str_replace("'", "", $text);
        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        // trim
        $text = trim($text, ' ');
        // remove duplicate -
        $text = preg_replace('~ +~', $replace_with, $text);
        // lowercase
        $text = strtolower($text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }
}
setlocale(LC_ALL, 'en_US.UTF8');
if (!function_exists('trim_uft8')) {
    function trim_uft8($text, $lowcase = false, $trust = false)
    {
        //$text = iconv('UTF-8', 'ASCII//TRANSLIT', $text);
        $text = trim($text);
        if ($lowcase) {
            $text = strtolower($text);
        }
        $black_kw = ['legalzoom', 'legal zoom', 'legal-zoom', 'legal-zoom-discount'];
        foreach ($black_kw as $black) {
            $pos = strpos($text, $black);
            if ($pos) {
                return false;
            }
        }
        if ($trust) {
            $kw_trusts = ["offer", "saving", "code", "coupon", "coupons", "deal", "deals", "discount", "promo code", "promo", "promotion", "promotional", "free shipping", "printable", "cashback", "% ", "$", "off ", "voucher", "sale"];
            /*test index text*/
            $split_kws = explode(' ', $text);
            $include_text = true;
            foreach ($split_kws as $per_char) {
                if (in_array($per_char, $kw_trusts)) {
                    $include_text = true;
                    break;
                } else {
                    $include_text = false;
                }
            }
            if (!$include_text) {
                $text = $text . ' coupon';
            }
        }
        return $text;
    }
}
if (!function_exists('slugify')) {
    function slugify($text, $replace_with = '-')
    {
        $black_list = '/ptc|paid to click|adsense|click ads/';
        $text = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $text), '-'));
        setlocale(LC_ALL, 'en_US.UTF8');
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', $replace_with, $text);
        // transliterate
        $text = iconv('UTF-8', 'ASCII//TRANSLIT', $text);
        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);
        // trim
        $text = trim($text, '-');
        // remove duplicate -
        $text = preg_replace('~-+~', $replace_with, $text);
        // lowercase
        $text = strtolower($text);
        $text = preg_replace($black_list, '', $text);
        if (empty($text)) {
            return 'n-a';
        }
        return $text;
    }
}
if (!function_exists('str_title')) {
    function str_title($str)
    {
        //$str = str_replace(" discount", "$", $str);
        $str = str_replace(" EUR", "€", $str);
        $str = str_replace(" GBP", "£", $str);
        $str = str_replace(" JPY", "¥", $str);
        $str = str_replace(" RUB", "руб", $str);
        $str = str_replace(" percent", "% off", $str);
        $str = str_replace(" 0%", " 50%", $str);
        $str = str_replace(" 0$", " 50%", $str);
        $str = str_replace("%$", "% discount", $str);
        if (substr($str, 0, 2) == "0%") {
            $str = str_replace("0%", "50%", $str);
        }
        return ucwords($str);
    }
}
if (!function_exists('rand_str')) {
    function rand_str($len)
    {
        $characters = 'ABCDEFGHIKJLMNOPQRSVTUXYZ1234567890!@#$%^&*()abcdefghikjlmnopqrsvtuxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $len; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
if (!function_exists('isValidTimeStamp')) {
    function isValidTimeStamp($timestamp)
    {
        return ((string)(int)$timestamp === $timestamp)
            && ($timestamp <= PHP_INT_MAX)
            && ($timestamp >= ~PHP_INT_MAX);
    }
}
if (!function_exists('store_seo')) {
    function seo_optimize($web_config, $type, $main_info, $additional_info = 'Enjoy')
    {
        $now_month = date('M', time());
        $now_year = date('Y', time());
        $str = $web_config[$type];
        if ($str) {
            $str = str_replace("%month%", $now_month, $str);
            $str = str_replace("%year%", $now_year, $str);
            if (isset($main_info['top_off']) && $main_info['top_off'] > 0) {
                $str = str_replace("%top_off%", $main_info['top_off'] . "%", $str);
            } else {
                $str = str_replace("%top_off%", 'Up to 50%', $str);
            }
            if (isset($main_info['store_name'])) {
                $str = str_replace("%store_name%", $main_info['store_name'], $str);
            }
            $str = str_replace("%top_coupon%", $additional_info, $str);
            $str = str_replace("%top_snippet%", $additional_info, $str);
        } else {
            $str = 'Best Deals and Coupons Updated Every Day - ' . $now_month . '/' . $now_year;
        }
        $str = str_replace(" 0% Off", " 50% Off", $str);
        return $str;
    }
}
if (!function_exists('seo_config')) {
    function seo_config($type)
    {
        $now_month = date('M', time());
        $now_year = date('Y', time());
        if ($type == "title") {
            $str = "Best Deals and Coupons Updated Every Day";
        } elseif ($type == "excerpt") {
            $str = "Search Best Coupon Codes " . $now_month . "-" . $now_year;
        } else {
            $str = "Search Best Coupon Codes " . $now_month . "-" . $now_year;
        }
        return $str;
    }
}
if (!function_exists('imgALT_Nofollow')) {
    function imgALT_Nofollow($content, $alt_img = 'coupons up to')
    {
        $dom = new DOMDocument('1.0');
        @$dom->loadHTML($content);
        $anchors = $dom->getElementsByTagName('img');
        foreach ($anchors as $element) {
            $element->setAttribute('alt', $alt_img . ' ' . rand(0, 99));
            $element->setAttribute('style', 'max-width:100%;');
            
        }
        
        $a_hrefs = $dom->getElementsByTagName('a');
        foreach ($a_hrefs as $a_href) {
            $relation = $a_href->getAttribute('rel');
            if (!$relation) {
                $a_href->setAttribute('rel', 'nofollow');
            }
        }
        $content = $dom->saveHTML();
        return $content;
    }
}
if (!function_exists('unique_by_property')) {
    function unique_by_property($object, $unique_property)
    {
        $unique = [];
        $new_array = [];
        foreach ($object as $value) {
            if (!in_array($value[$unique_property], $unique)) {
                array_push($unique, $value[$unique_property]);
                array_push($new_array, $value);
            }
        }
        return array_values($new_array);
    }
}
if (!function_exists('extend_info')) {
    function extend_info($all_data)
    {
        $result = ['average' => 0, 'coupons' => rand(10, 25), 'deals' => rand(5, 10), 'best_coupon' => 0];
        $sum = 0;
        foreach ($all_data as $datum) {
            // dd($datum);
            $sum = $sum + $datum['sale_info']['off_percent'];
            if ($datum['sale_info']['off_percent'] > $result['best_coupon']) {
                $result['best_coupon'] = $datum['sale_info']['off_percent'];
            }
        }
        if ($sum > 0) {
            $result['average'] = intval($sum / count($all_data));
        }
        if ($result['average'] < 50) {
            $result['average'] = 50;
        }
        if ($result['best_coupon'] < $result['average']) {
            $result['best_coupon'] = 50;
        }
        return $result;
    }
}
if (!function_exists('rand_howto')) {
    function rand_howto($kw)
    {
        $hows = [
            ['title' => 'How many |NAME| results are available?', 'desc' => "According to Fitnessol's tracking system, there are currently |count_coupons| |NAME| results. These deal offers are from many sources, selected by our smart and comprehensive system on coupon code, discounts, and deals."],
            ['title' => 'Tips to save money with |NAME| offer', 'desc' => "You can hunt for discount codes on many events such as Flash Sale, Occasion like Halloween, Back to School, Christmas, Back Friday, Cyber Monday,…which you can get the best discounts. If you buy regularly at a store, do not hesitate to contact us, Fitnessol will support you with an exclusive discount code."],
            ['title' => 'How do I know which website will take me to when I click to Get Link Coupon on |NAME| Searching?', 'desc' => "When you click to the button Get Link Coupon, the raw link will appear and you will know what website you will visit to get the discounts."],
            ['title' => 'How do I know what is the best Coupon for me on |NAME|?', 'desc' => "Fitnessol algorism arranges the best results on the top of the list when you type |NAME| to the box. You can find the best Coupons, discounts, deals, promote codes by clicking to the top results."],
            ['title' => 'How can I keep track of information on |NAME|?', 'desc' => "The best way to update deals from an online store is to visit their homepage regularly. In parallel, you should also refer to the websites of Coupon, Deals,... and you should not ignore Fitnessol, which is a huge database of discount and coupon codes. Fitnessol always updates the latest coupon codes periodically, which ensures that you always have the latest ones..."],
            ['title' => 'How can I know whether |NAME| result are verified or not?', 'desc' => "According to Fitnessol's tracking system, |NAME| searching currently have |count_coupons| available results. Coupons with verified labels are working for most. To make certain, you just need to copy the code and apply it to any products that are on sale."],
            ['title' => 'What steps can I do to apply |NAME| offers?', 'desc' => "To apply a |NAME| coupon, all you have to do is to copy the related code from Fitnessol to your clipboard and apply it while checking out.<br/>Note: Some results of |NAME| only suit for specific products, so make sure all the items in your cart qualify before submitting your order."],
            ['title' => 'What tips are there to save money when shopping online with |NAME|?', 'desc' => "Like most of online stores, |NAME| also offers customers coupon codes. So, the best tip to save money when shopping online is to hunt for coupon codes of the store that you want to buy the product. Fitnessol can strongly support you because we aggregated lots of coupon codes from all sources from Website Store, Coupon sites,..."],
            ['title' => 'How do I filter the result of |NAME| on Fitnessol?', 'desc' => "What you need to do is click to the options ($ Off, % Off, Free Shipping, Gift Card,…) on Filter By and you can easily arrange your results."],
            ['title' => 'Why do I have to wait for a bit when searching |NAME|?', 'desc' => "The reason is there are many |NAME| results we have discovered especially updated the new coupons and this process will take a while to present the best result for your searching. This often takes 0.25 seconds with normal search and around 1 sec for a difficult search."],
            ['title' => 'How can I submit a |NAME| result to Fitnessol?', 'desc' => "We're very happy to have coupon code submitted by customers. Also, we will reward someone who usually submits coupons to us. We verify the coupons before sharing them on the site.<br/>For submission, you can reach out Contact@Fitnessol.com"],
            ['title' => 'How many coupon codes can be used for each order when I search for |NAME|?', 'desc' => "There are usually 1 to 3 discount codes for one product. However, in each order, customers can only use one coupon code. Therefore, when using the coupon code, try to select the best code with the highest discount."],
            ['title' => 'Where to find the relevant results of |NAME|?', 'desc' => "Right below the |NAME|, Fitnessol shows all the related result of |NAME|, then you can easily go for. Along with that, at the bottom of the page, you can find the section Recently Searched which is great to see what you have searched."],
            ['title' => 'How do I use |NAME| offer?', 'desc' => "After you find out all |NAME| results you wish, you will have many options to find the best saving by clicking to the button Get Link Coupon or more offers of the Store on the right to see all the related Coupon, Promote & Discount Code."],
            ['title' => 'If all the results of |NAME| are not working with me, what should I do?', 'desc' => "Almost your searching will be available on Fitnessol in general. The only reason you can not find the offer you are seeking is they don’t exist, or the store doesn’t have it. You just simply reach out to our support team at Contact@Fitnessol.com and we will do our best help you."],
        ];
        $randoms = array_rand($hows, 4);
        $results = [];
        foreach ($randoms as $index => $random) {
            $results[$index]['title'] = str_replace("|NAME|", ucwords($kw), $hows[$random]['title']);
            $results[$index]['desc'] = str_replace("|NAME|", ucwords($kw), $hows[$random]['desc']);
            $results[$index]['desc'] = str_replace("|count_coupons|", rand(15, 25), $results[$index]['desc']);
        }
        return $results;
    }
}

if (!function_exists('filter_kw')) {
    function filter_kw($str, $domain)
    {
        try {
            $a_filter = [' - ', ' & ', ': ', ' | ', ',', 'coupon', "promotional", "claim", "codes", "renewals", "renewal", "coupon code", "couponing", "deal ", "deals", "discount", "discount code", "promo code", "promo", "promotion", "promotion code", "promotional", "promotional code", "free shipping", "free shipping code", "free", "code", "printable", "printable coupon", "cashback", "voucher", "voucher code", " sale ", "sale off", "on sale", " off", "sign up", "new customer", "buy one get one", "buy 1 get 1", " top", "active", "rewards", "entire ", " order ", " offer", "  for ", " on ", " of ", " best", "http", "www", "https", "black friday", "cyber monday", "christmas", "new year", "thanksgving", "labor day", "valentine", "4th of july", "mother's day", "father's day", "memorial day", "2019", "2018", "2017", "2016", "2015", "2014", "2013", "2012", "retailmenot", "groupon", "reddit", "twitter", "facebook", "youtube", "linkedin", "couponbird", "krazy coupon lady", "couponsplusdeals", "myshopify", "bigcartel", "blogspot", "january ", "february ", "march ", "april ", "may ", "june ", "july ", "august ", "september ", "october ", "november ", "december ", " gift"];
            $str = trim(strtolower($str));
            foreach ($a_filter as $filter_char) {
                if (strpos($str, $filter_char)) {
                    $a_str = explode($filter_char, $str);
                    return $a_str[0];
                    break;
                }
            }
            $filter_domain = "~\.(za|yt|xyz|xxx|wf|website|vc|uz|uy|us|uk|ug|ua|tw|tv|travel|tr|top|to|tn|tl|th|tf|tel|tech|tc|sx|su|store|st|space|solutions|so|sn|sm|sk|site|si|sh|sg|se|sc|sb|rw|ru|rs|ro|re|qa|pw|pt|pro|pr|pm|pl|pf|pe|org|online|om|nz|nyc|nu|no|nl|ng|nf|news|net|nc|mx|mu|ms|mobi|mn|mk|mg|me|md|ma|ly|lv|lu|lt|live|link|life|li|lc|la|kz|ky|kr|ki|kg|ke|jp|jobs|je|it|is|ir|iq|io|info|in|im|il|ie|id|hu|ht|hr|hn|hk|gy|gs|gl|gi|gg|gd|fr|fo|fm|fi|eu|es|ee|do|dm|dk|de|cz|cx|cr|com|co|cn|club|cloud|cl|ci|ch|cc|cat|ca|bz|by|bw|br|bo|bn|bj|biz|bi|bg|be|ax|aw|au|at|asia|as|ar|am|ag|af|aero|ae|ac)\b~i";
            $filter_str = preg_replace($filter_domain, '', strtolower($domain));
            return str_replace('www', '', $filter_str);
        } catch (Exception $e) {
            return 'ebay sale';
        }
    }
}
if (!function_exists('filter_info')) {
    function filter_store_name($str)
    {
        try {
            /*$url_first = $all_data[0]['link'];
            $parse_url = parse_url($url_first)['path'];
            $a_url = explode('/', $parse_url);
            $last_path = end($a_url);
            if ($last_path && !is_numeric($last_path)) {
                $str = $last_path;
            }*/
            $filter_domain = "~\.(za|yt|xyz|xxx|wf|website|vc|uz|uy|us|uk|ug|ua|tw|tv|travel|tr|top|to|tn|tl|th|tf|tel|tech|tc|sx|su|store|st|space|solutions|so|sn|sm|sk|site|si|sh|sg|se|sc|sb|rw|ru|rs|ro|re|qa|pw|pt|pro|pr|pm|pl|pf|pe|org|online|om|nz|nyc|nu|no|nl|ng|nf|news|net|nc|mx|mu|ms|mobi|mn|mk|mg|me|md|ma|ly|lv|lu|lt|live|link|life|li|lc|la|kz|ky|kr|ki|kg|ke|jp|jobs|je|it|is|ir|iq|io|info|in|im|il|ie|id|hu|ht|hr|hn|hk|gy|gs|gl|gi|gg|gd|fr|fo|fm|fi|eu|es|ee|do|dm|dk|de|cz|cx|cr|com|co|cn|club|cloud|cl|ci|ch|cc|cat|ca|bz|by|bw|br|bo|bn|bj|biz|bi|bg|be|ax|aw|au|at|asia|as|ar|am|ag|af|aero|ae|ac)\b~i";
            $filter_char = ["coupon", "promotional", "claim", "codes", "renewals", "renewal", "coupon code", "couponing", "deal ", "deals", "discount", "discount code", "promo code", "promo", "promotion", "promotion code", "promotional", "promotional code", "free shipping", "free shipping code", "free", "code", "printable", "printable coupon", "cashback", "voucher", "voucher code", " sale ", "sale off", "on sale", " off", "sign up", "new customer", "buy one get one", "buy 1 get 1", " top", "active", "rewards", "entire ", " order ", " offer", "  for ", " on ", " of ", " best", "http", "www", "https", "black friday", "cyber monday", "christmas", "new year", "thanksgving", "labor day", "valentine", "4th of july", "mother's day", "father's day", "memorial day", "2019", "2018", "2017", "2016", "2015", "2014", "2013", "2012", "retailmenot", "groupon", "reddit", "twitter", "facebook", "youtube", "linkedin", "couponbird", "krazy coupon lady", "couponsplusdeals", "myshopify", "bigcartel", "blogspot", "january ", "february ", "march ", "april ", "may ", "june ", "july ", "august ", "september ", "october ", "november ", "december ", " gift"];
            $filter_str = str_replace(' ', '-', $str);
            $filter_str = preg_replace($filter_domain, '', strtolower($filter_str));
            $filter_str = preg_replace('/[^A-Za-z0-9\-]/', '', $filter_str);
            $filter_str = str_replace('-', ' ', $filter_str);
            $filter_str = str_replace($filter_char, ' ', $filter_str);
            $filter_str = preg_replace('~ +~', ' ', $filter_str);
            $spilit = preg_split("/[\s,]+/", $filter_str);
            if (empty($filter_str)) {
                return false;
            }
            if (is_array($spilit) && count($spilit) > 1) {
                return $spilit[0] . " " . $spilit[1];
            } else {
                return $filter_str;
            }
        } catch (Exception $e) {
            return false;
        }
    }
}
if (!function_exists('filter_info')) {
    function filter_info($str, $str2)
    {
        $data_result = ['off_percent' => 0, 'off_dollar' => 0, 'free_shipping' => false, 'gift_card' => false, 'sale' => true];
        $array = explode(" ", strtolower($str));
        $array2 = explode(" ", strtolower($str2));
        $new_array = array_merge($array, $array2);
        foreach ($new_array as $value) {
            if (strpos($value, "%") > -1) {
                $number = intval(str_replace("%", "", $value));
                if ($number > $data_result['off_percent']) {
                    $data_result['off_percent'] = $number;
                    $data_result['sale'] = false;
                }
            }
            if (strpos($value, "$") > -1) {
                $number = intval(str_replace("$", "", $value));
                if ($number > $data_result['off_dollar']) {
                    $data_result['off_dollar'] = $number;
                    $data_result['sale'] = false;
                }
            }
            if (strpos($value, "free shipping") > -1) {
                $data_result['free_shipping'] = true;
                $data_result['sale'] = false;
            }
            if (strpos($value, "freeshipping") > -1) {
                $data_result['free_shipping'] = true;
                $data_result['sale'] = false;
            }
            if (strpos($value, "gift card") > -1) {
                $data_result['gift_card'] = true;
                $data_result['sale'] = false;
            }
        }
        return $data_result;
    }
}
if (!function_exists('img_sync')) {
    function img_sync($img_url)
    {
        if (filter_var($img_url, FILTER_VALIDATE_URL)) {
            //Remote IMG URL
            return $img_url;
        } else {
            if (strpos($img_url, 'couponupto-loading') > -1) {
                return "https://www.fitnessol.com/files/5d64e1fea22d142284003812/fitnessol-loading.svg";
            } else {
                return "https://www.couponupto.com" . $img_url;
            }
        }
    }
}
if (!function_exists('img_showThumbs')) {
    function img_showThumbs($img_url)
    {
        if (strpos($img_url, 'https:') || strpos($img_url, 'http:')) {
            //Remote IMG URL
            return $img_url;
        } else {
            if (strripos($img_url, 'photos') > 0) {
                $img_thumb = explode('/', $img_url);
                $directory = dirname($img_url);
                $thumbnail = $directory . '/thumbs/' . end($img_thumb);
                return "https://www.couponupto.com" . $thumbnail;
            } else {
                return "https://www.couponupto.com" . $img_url;
            }
        }
    }
}
if (!function_exists('isUrl')) {
    function isUrl($url, $domain = false)
    {
        if (filter_var($url, FILTER_VALIDATE_URL)) {
            /*$url is a valid URL*/
            return $url;
        } else {
            if ($domain) {
                return $domain . $url;
            } else {
                return $url;
            }
        }
    }
}

if (!function_exists('changeInfoCoupon')) {
    function changeInfoCoupon($coupon, $store)
    {
        $title_1 = ["Get UP TO XX OFF", "Get UP TO XX OFF with STORENAME coupon code", "Get UP TO XX OFF with STORENAME promo code", "XX Off STORENAME Coupon & Promo Code", "XX OFF with STORENAME Coupon", "Save XX w/ BEST Deals, Coupon Codes", "Save XX Off Sitewide", "Take an Extra XX Off With STORENAME Coupon", "Take an Extra XX Off At STORENAME", "Take an Extra XX Off With STORENAME Promo Code", "Take XX Off Sitewide At STORENAME", "Save XX Off Entire Purchase", "Up To XX Off Select Items", "Apply STORENAME Coupon & Get XX Off", "Apply STORENAME Coupon & Save XX Off", "Save Up To XX Off This Holiday", "XX Off Your First Order At STORENAME", "XX Off Your First Order with STORENAME Coupon Code", "XX Off Your First Order with STORENAME Promo Code", "XX Off Your STORENAME First Order", "Up To XX Off STORENAME Deals of The Week", "Enjoy XX Off with STORENAME Coupon Code", "Enjoy XX Off with STORENAME Promo Code", "Enjoy XX Off at STORENAME", "Extra XX OFF at STORENAME with code", "Grab Up to XX Off with STORENAME Coupon Code", "Grab Up to XX Off with STORENAME Promo Code", "Grab Up to XX Off at STORENAME", "Flat XX Off On Sitewide Purchase", "Get XX OFF Online Orders STORENAME Promo Code", "Get XX OFF Online Orders STORENAME Coupon Code", "Get XX OFF Online Orders at STORENAME", "In-Store: Get XX OFF Entire Purchase", "Take XX Off Orders With STORENAME Coupon Code", "Save An Additional XX Off When You Apply STORENAME Coupon", "Save An Additional XX Off When You Apply STORENAME Promo Code", "Save An Additional XX Off At STORENAME", "STORENAME Coupon: Get Upto XX OFF", "Get XX Off Everything At STORENAME", "STORENAME Coupon Code - Save XX OFF", "XX Off STORENAME Promo Code", "Free XX OFF Coupon", "STORENAME Coupon: Huge Savings XX OFF", "STORENAME coupon", "XX Off STORENAME Coupon", "STORENAME coupon, promo code & sale", "STORENAME Coupon: Up to XX Off w/ Promo Codes", "STORENAME Coupon: XX Off & Free Shipping", "Best Deal: XX Off STORENAME Coupon Code", "Save XX w/ STORENAME Coupon", "HOT STORENAME offers", "Save An Extra XX Off On Orders $100", "Get An Extra XX Off On Your Order", "Take XX Off Plus FREE Shipping for Storewide", "Extra XX Off Your Order With $100 Or More", "Get Savings Up To XX Off On Your Order ", "Save XX Off Sitewide + Free Shipping", "XX Off at STORENAME", "New Customers: XX Off Your Order", "Save NOW: XX OFF with STORENAME Coupon Code", "Save NOW: XX OFF with STORENAME Promo Code", "Save NOW: XX OFF at STORENAME", "Use Coupon Code And Receive XX Off At STORENAME", "Flash Sale Now! Get Up to XX Off with Free Shipping", "XX Off All Your Purcharse at STORENAME (Site-Wide)", "Extra XX Off Your Next Order at STORENAME (Site-Wide)", "Save XX Off on All Regular Priced Item Purchase at STORENAME", "Up to XX Off Clearance at STORENAME", "Get Extra XX Off Your Next Order at STORENAME", "Save XX On All Orders At STORENAME", "Get Over XX Discount At STORENAME", "XX Off Any Order at STORENAME", "XX Off Entire Order at STORENAME", "XX Off Your Purchase Coupon Code for STORENAME", "Holiday Sale! XX OFF SITEWIDE", "Up to XX Off Plus Free Shipping", "Up to XX Off STORENAME's Best Sellers with Coupon Code", "Up to XX Off STORENAME's Best Sellers with Promo Code", "XX Off STORENAME 's Best Sellers", "XX Off Sitewide Sale at STORENAME", "XX Off Sitewide Sale | STORENAME Coupon Code", "XX Off Sitewide Sale | STORENAME Promo Code", "STORENAME Coupon Code: XX Off all products", "STORENAME Promo Code: XX Off all products", "STORENAME Discount Code: XX Off all products", "Today XX Off STORENAME Coupon", "STORENAME Coupon Code {XX discount}", "Get XX off and free shipping on all orders from STORENAME", "XX Off all New arrivals with code", "Final SALE 48 Hours XX Off at STORENAME", "Up to XX Off Clearance with STORENAME Coupon", "Save XX Off One Select Regular-Priced Item", "Save XX Off Now", "Get Code & Save XX Off Now", "Apply Code & Save XX Off Now", "XX Off All Orders with Code", "XX Off Any Orders with Coupon Code", "XX Off with STORENAME Coupon Code", "XX Off with STORENAME Promo Code", "XX Off with STORENAME Discount Code", "Save XX Off with STORENAME Coupon Code", "Save XX Off with STORENAME Promo Code", "Save XX Off with STORENAME Discount Code", "Get XX OFF at STORENAME with code", "Apply STORENAME Coupon Code at checkout to save XX OFF", "Apply STORENAME Promo Code at checkout to save XX OFF", "XX Off Today", "Get Code & Save XX OFF before checkout", "Get Code & Save XX OFF at checkout", "XX Off Coupon Code at STORENAME", "XX Off Promo Code at STORENAME", "XX Off Discount Code at STORENAME", "Special XX OFF with STORENAME Coupon Code", "Special XX OFF with STORENAME Promo Code", "Special XX OFF with STORENAME Discount Code", "Special XX OFF at STORENAME with code", "SAVE BIG with XX OFF", "Exclusive Deal: XX off EVERYTHING", "Save XX OFF", "Get XX OFF", "Enjoy XX OFF", "Extra XX OFF", "Buy Now and Get XX OFF", "Today Only: XX OFF ", "Take An Extra XX OFF", "XX Off Your Order", "XX OFF All Orders", "Upto XX OFF with code", "Use Code and Get XX OFF", "Apply Code and Get XX OFF", "Use Code and Save XX OFF", "Apply Code and Save XX OFF", "Get XX Off On Any Order", "Save XX Off On Any Order", "Take XX OFF STORENAME", "Save XX Off Your Purchase", "Take Up to XX Off Your Purchase", "Get XX Off Your Purchase", "XX OFF STORENAME", "Receive XX Off On Any Order", "Take XX Off On Any Order Sitewide", "Save XX Off On Any Order (Site-wide)", "Take XX Off On Your Order", "Take XX Off On Any Order", "XX Off Your Order", "XX OFF for new customers", "XX Off Your First Purchase", "Get XX Discount On Your Order", "Take XX Discount On Your Purchase", "Get An Extra XX Off Your Purchase", "Save XX Off First Order at STORENAME", "Save XX Off On Select Products", "Save XX Off Entire Order", "All Sale Items Up to XX Off", "XX Off Any Purchase", "Take XX Off Any Order $100+ Sitewide!", "Take XX Off Your Next Purchase", "Save XX Off Your Next Purchase", "UP TO XX OFF", "Grab Up to XX Off", "XX Off Entire Purchase", "XX Off Select Items", "Final Sale XX OFF", "XX OFF", "Flash Sale XX OFF", "Hot Offer: XX OFF", "Last Chance XX OFF", "Upto XX OFF STORENAME", "Grab Up to XX Off STORENAME Coupon", "Special XX OFF", "Take XX Off On Your Order at STORENAME", "Take An Extra XX OFF at STORENAME", "XX Off Select Items at STORENAME", "XX Off Your Order with STORENAME Coupon Code", "XX Off All Orders with STORENAME Coupon Code", "XX Off Any Order with STORENAME Coupon Code", "Last Chance XX OFF at STORENAME", "XX Off Entire Purchase at STORENAME", "All Sale XX Off", "Sitewide XX OFF", "XX Off Your First Purchase at STORENAME", "XX Off First Order at STORENAME", "Save XX OFF at STORENAME", "Get XX OFF at STORENAME", "UP TO XX OFF with STORENAME Coupon Code", "Extra XX OFF at STORENAME", "Special XX OFF at STORENAME", "XX Off All Order with STORENAME Coupon Code", "Get XX OFF with STORENAME Coupon Code", "XX Off Your Order with STORENAME Promo Code", "Extra XX OFF with STORENAME Coupon Code", "XX Off Everything", "Huge Savings XX OFF", "Clearance Sale XX OF", "Big Sale XX OFF", "Enjoy XX OFF Sitewide", "XX Off Coupon Code", "XX Off Code", "Exclusive XX Off", "Free XX OFF", "XX OFF storewide using coupon code", "Additional XX off"];
        $title_2 = ["Save Now!", "Don't Miss It!", "Fast & Easy", "Just Shop & Save", "Checkout Easy Savings", "Quantity Limited!", "Hurry Up!", "Limited Time!", "{FREE}", "Today Only!", "Best Discount Today!", "New & Verified", "Active Now", "Apply Now", "Take action now!", "Best Deal of the day", "Don't wait anymore!", "Sale for today only.", "Grab this awesome deal", "Enjoy attractive discounts!", "FINAL HOURS!", "Great deals ending soon", "Ends Soon!", "It's shopping time!", "Be quick to shop!", "Shop and Save Now!", "2019 Verified", "Updated Daily", "100% Active", "100% Working", "Enjoy savings", "Checkout Now!", "100% Verified", "2020 New", "Best Choice!", "{Exclusive}", "Last Chance to save!", "Ends Tonight!", "Shop Now!", "Buy Now!", "Woohoo!!", "Great!!", "HUGE Sale!", "Hot Deal!", "Last Hours!", "Quick!!", "HURRY NOW!", "SURPRISE!"];
        $des_1 = ["Use STORENAME Coupon Code to save XX OFF", "Apply STORENAME Coupon Code to save XX OFF", "Check out STORENAME with our coupon", "Enter promo code at checkout to get XX OFF", "Enter coupon code at checkout to get XX OFF", "Use this code to get XX Off at STORENAME", "Use this code to save XX Off at STORENAME", "Redeem this XX off deal at STORENAME", "Use this offer at STORENAME", "Check out STORENAME to enjoy instant savings", "Use code to get XX discount at STORENAME", "Apply this code to get XX OFF", "Receive XX off at STORENAME", "STORENAME at discounted prices", "STORENAME at best prices", "Enter the code at STORENAME to receive XX Off ", "Cannot be combined with other offers", "Shop at STORENAME", "Click Get Code & Save XX OFF", "Copy Code & Save XX OFF", "Apply promo code at checkout at STORENAME", "Save XX with this offer", "Get Code & Apply at checkout to get XX OFF", "Simply copy code & Enter at checkout to get XX OFF", "Simply copy code & Checkout STORENAME", "Simply use code & Make a purchase at STORENAME", "Use code at STORENAME to receive XX Off", "Use code at STORENAME to save XX Off", "Use code at STORENAME to get XX Off", "Checkout at STORENAME", "Make your purchase at STORENAME", "Treat yourself to XX Off at STORENAME", "Redeem this XX Off STORENAME Coupon", "Enter the code at checkout to get XX off ", "To get a XX discount enter the code at checkout", "Copy code and apply at checkout at STORENAME", "Use coupon code at checkout to take XX off at STORENAME", "Use this code to redeem this XX off deal at STORENAME", "Check out STORENAME to get a XX discount", "Simply copy code and apply at checkout", "Simply copy STORENAME code and apply at checkout", "Copy STORENAME code and apply at checkout", "Find great products at STORENAME", "See What's On Sale at STORENAME", "Best Deals Of The Day", "Special For STORENAME's customers", "Get XX off your favorite products", "Best deals now available", "Hot Deal for you at STORENAME", "Receive discounts of up to XX OFF", "Here's a special sale for you.", "Just enter the coupon code", "EVERYTHING Is Included!"];
        $des_2 = ["Makes you feel like shopping.", "Do not miss this amazing opportunity!", "Shop now and save instantly", "Shop now and save now, don't wait anymore.", "Take a closer look now!", "Shop and save with us!", "Amazing offers ending soon!", "Grab before someone else does.", "Spend less, save more!", "Find the joy of saving while shopping!", "Don't wait anymore!", "Amazing savings you won't find anywhere!", "Now is the time to enjoy fantastic deals.", "Shop now and save immediately!", "We serve to help you save.", "The more you order, the more you save.", "A perfect price for what you love.", "Take action now!", "Find the joy of saving while shopping!", "Enjoy your great journey on online shopping.", "Get your coupon before they expire!", "Best Deal of the day", "Start saving money on your online purchases", "Get Code and Save Now", "There's More Than One Way To Save", "There's no better online promo code for you than ours", "Don't miss this deal or you will regret it.", "Grab your savings today", "Look no further than here for the most amazing deals.", "Save big during this seasonal sale", "Add them to your cart now.", "It's now or never.", "Be budget savvy with this great offer", "Experience major savings with this great deal", "Sale for limited time only.", "Get huge dicounts for you today", "New and amazing items for a limited time.", "Big savings while they last!", "Rediscover a great shopping tradition.", "Today marks the final day to save!", "Time to get your shopping on.", "Discover incredible discounts", "You will only find the best deals here!", "Affordable and highly recommended by users, make your purchase today", "Add to your cart and check out", "Don't hesitate any longer, the time to make your purchase is now", "Don't wait to snatch up your savings.", "Everyday, we offer enticing deals that you can't miss.", "Buy now, instead of regret later.", "Take this great opportunity to save", "Your bargain is waiting at the check-out.", "Right now is the best time to buy and claim it as your own", "Great chance to save money with this offer", "Spend much less on your favorite items", "Prices vary daily, so take action now.", "Be the first to know, first to shop, and first to save!", "Sale for today only.", "Check out now before this deal expires", "Check merchant site for more information.", "Instant savings when you purchase today", "For a limited time only.", "Get the deal to save money. Make them yours now!", "Be budget savvy with this great offer", "Sale ends soon! Buy it before it's too late.", "Stack coupons for maximum savings.", "Bargains at these amazingly low prices won't last long!", "Extraordinary savings, only today.", "Remember to check out so you can keep the discounts!", "Amazing deals like this don't appear everyday", "Remember, you have only a few hours left to redeem your offer!", "Seasonal sale for an extended time only.", "It is currently at its best price ever.", "Discounted price as marked.", "Remember that good deals are hard to come by!", "For the ultimate shopping experience, look no further.", "Don't miss out. We know you don't want to!", "We only help you find the best bargains.", "Order yours now and take advantage of this deal!", "Amazing sale, great prices.", "No time is better than right now.", "Grab these must-have items now.", "You never have to pay full price on your favorite items ever again!", "It's always worth checking, after all.", "Save money with promo codes guaranteed to work", "Don't want to miss the opportunity to save even more money.", "Make sure that you take that opportunity to save.", "We offer you the chance to buy your products at the best possible rates", "Simply copy code and apply at checkout step and the price will be discounted directly to your price", "We find all the best deals daily and handpick every coupon code", "Your search for great deals and coupon savings ends here", "Take advantage of these promotions and order online now.", "It's the greatest offer you'll ever get, but it expires when you take your last breath.", "Grab this awesome deal", "Enjoy attractive discounts!", "Your gateway to a great shopping experience.", "When is the best time? Now!", "Best sellers will disappear soon if you don't grab them!", "Shop for what you want and we will help you close the deal.", "Save big during this seasonal sale", "Sale ends soon, so take advantage of these great savings now!", "Check out these awesome products just waiting for you in store.", "The hottest items are selling out, so hurry! Check out the recommended deals", "Here's your exclusive coupon code", "Pick Up All Those Awesome Goodies You've Been Wanting", "Do Not Miss Out On This Much Awesome While Supplies Last!", "Its a crazy deal and youd be crazy to let it slide", "Last chance to use this coupon. Once they're gone, they're gone!", "Don't miss your chance to be lucky users!", "Redeem it to get that exclusive discount just for you.", "Big savings for what you love", "Trust me, you can't miss these hot deals", "FINAL HOURS! Great deals ending soon", "It's shopping time! Shop and save", "Time for big savings when you shop", "Pick your favorite items and save with our coupon code", "Last chance to shop and save", "Here is the right place you can find amazing savings", "Save money on things you want", "Find the best discount and save!", "Grab the freshest coupon codes, get your awesome discounts", "Start saving money on your online purchases with our coupon", "Look for codes that match what you want", "Most Popular Newest", "We've got promo codes & sales to help you score savings", "You can avail these amazing coupon and deal ", "Every item is discounted to the lowest price possible", "Amazing Promotional Codes", "Come on and take this one!", "The easiest way is to get lower prices on any orders", "Just Shop & Save", "Checkout Easy Savings.", "Visit now and get your coupons before they expire!", "In-Store Promotions & Deals", "Ready to check out?", "Great new way to enjoy additional savings", "All codes guaranteed to work.", "Get money off your purchases", "It's easy to save money when you shop", "Check out with coupon and start saving right now", "Shop now with our coupon codes for instant discounts", "If you want to maximize your savings we suggest you get one!", "Enter the code at checkout.", "Take advantage of this savings opportunity", "Fantastic discounted price using this code at the checkout.", "Great discounted price using this voucher code at the checkout.", "Use this voucher code at the checkout and get a fabulous discount on your purchase", "Now you can get wonderful savings on your purchases", "Enjoy incredible discounts on all your favorite items", "Always ahead so you can get amazing deals!", "You won't find this deal elsewhere!", "For limited time only, this great deals to help you save.", "Your place to shop and discover amazing deals.", "Beat the crowd and buy now.", "Great stores. Great choices.", "Affordable and highly recommended by users, make your purchase today.", "Groundbreaking sale with never before seen prices.", "Don't miss this chance to save money ", "Don't wait to snatch up your savings. Grab them while you can!", "Be the first to know, first to shop, and first to save!", "Click on this great deal and activate an amazing discount.", "Make your purchase now and save big", "You will not miss the best discount ever.", "Be quick to shop!", "Just a step away from one of the best shopping experiences of your life.", "Everyday, we offer enticing deals that you can't miss.", "Take the great chance today!", "Extraordinary savings, only for a limited time.", "Get attractive discounts while you can", "Absolutely. Positively. Perfect.", "Right now is the best time to buy and claim it as your own.", "Use this wonderful voucher code and get amazing savings on your purchase.", "Surround yourself with a world of happiness once you check out.", "Check out the big, never-before-seen sale.", "Act now while offer lasts.", "Expect the unexpected.", "Discover these exceptionally good deals today", "Remember to finish your transaction!", "Act immediately before the sale ends.", "Our customers love good bargains and we know you do too.", "Hot specials don't last forever.", "These deals won't last, so make the purchase today.", "It is for a limited time, so be quick.", "Get at a discounted price using this code.", "Guaranteed to make your heart beat with these deals.", "Last chance to grab must-have style at even better prices", "Hurry before the deals are gone.", "Exclusive offers only for you.", "Click through to shop.", "Be the first to get it!", "Hurry up and enjoy it before it expires", "Hope you enjoy it on time", "Here is your chance", "Hurry! Happy Hour Deals Are Ending", "You know you want to buy it!", "What are you waiting for?", "There's Still Time", "This is a great time", "Don't say we didn't warn you", "Savings continue!", "The sale lives on!"];
        $shuffer = [12, 1, 12, 12, 12, 12];
        $shuffer_des = [12, 12, 12, 12, 1, 2];

        $rTitle_1 = $title_1[rand(0, count($title_1) - 1)];
        $rTitle_2 = $title_2[rand(0, count($title_2) - 1)];
        $rDes_1 = $des_1[rand(0, count($des_1) - 1)];
        $rDes_2 = $des_2[rand(0, count($des_2) - 1)];
        if ($coupon->off_type == 'percent') {
            $xx = $coupon->off_sale . "%";
        } else {
            if ($coupon->off_sale == 0) {
                $coupon->off_sale = rand(5, 30);
            }
            $xx = "$" . $coupon->off_sale;
        }
        $title_1 = str_replace("XX", $xx, str_replace("STORENAME", $store->store_name, $rTitle_1));
        $shuffer = rand(0, count($shuffer) - 1);
        if ($shuffer == 1) {
            $title = $title_1 . " .";
        } else {
            $title = $title_1 . ". " . $rTitle_2;
        }
        $des_1 = str_replace("XX", $xx, str_replace("STORENAME", $store->store_name, $rDes_1));
        $shuffer_des = rand(0, count($shuffer_des) - 1);
        if ($shuffer_des == 1) {
            $desc = $des_1;
        } elseif ($shuffer_des == 2) {
            $desc = $rDes_2;
        } else {
            $desc = $des_1 . ". " . $rDes_2;
        }
        return ['title' => $title, 'desc' => $desc];
    }
}

if (!function_exists('_getRandomKw')) {
    function getRandomKw($type, $limit, $store_id, $cache_time = 10080)
    {
        $hash = md5($type . $limit . $store_id);
        if ($type == 'kw') {
            $results = \Illuminate\Support\Facades\Cache::remember($hash, $cache_time, function () use ($limit) {
                return \App\Keywords::raw(function ($collection) use ($limit) {
                    return $collection->aggregate([['$match' => ['tracked' => 1]], ['$sample' => ['size' => $limit]]]);
                });
            });
            return $results;
        }
    }
}

if (!function_exists('getRandomStores')) {
    function getRandomStores($type, $limit, $store_id)
    {
        $hash = md5($type . $limit . $store_id);
        if ($type == 'store' || $type == 'store-popular') {
            $results = \Illuminate\Support\Facades\Cache::remember($hash, 10080, function () use ($limit) {
                return \App\Stores::raw(function ($collection) use ($limit) {
                    return $collection->aggregate([['$sample' => ['size' => $limit]]]);
                });
            });
            return $results;
        }
    }
}

if (!function_exists('is_non_english')) {
    function is_non_english($str)
    {
        if (strlen($str) != strlen(utf8_decode($str))) {
            return true;
        } else {
            return false;
        }
    }
}
if (!function_exists('new_coupon_excerpt')) {
    function new_coupon_excerpt($store_name, $xx)
    {
        $random = ["Coupon code is only applicable to regular priced items. Anything that has a marked-down price is on sale and doesn't apply.", "Simply use the code at checkout (but keep in mind that you can’t combine this discount with other discounts or coupons).", "Unless you enjoy paying full price, right now is the perfect time to stock up on all your favorite STORENAME products!", "Enter coupon code at checkout for _XX_ Off all products. Offer cannot be combined with any other coupons", "Simply Copy Code And Apply At Checkout Step And The Price Will Be Discounted Directly To Your Price", "Be sure to take advantage of additional store discounts to save up to _XX_ on your favorites today. ", "Use the following coupon code and get _XX_ Off on your total order amount. Valid for one time only!", "Here's a _XX_ off coupon for you and your loved ones to stock up on CBD and enjoy huge savings", "Use coupon code at checkout to receive _XX_ OFF your entire purchase of STORENAME products!", "Use This Voucher Code At The Checkout And Get A Fabulous Discount On Your Purchase", "Add Coupons To Your Card And Apply Them To Your In-Store Purchase Or Online Order.", "Here's a special coupon only for you! Enjoy STORENAME at a special price NOW!", "The Hottest Items Are Selling Out, So Hurry! Check Out The Recommended Deals", "Get Code And Save Now. Special offers for newly subscribed customers only.", "This particular offer allows customers to save _XX_ off STORE NAME products ", "Use This Wonderful Voucher Code And Get Amazing Savings On Your Purchase.", "Just A Step Away From One Of The Best Shopping Experiences Of Your Life.", "We Offer You The Chance To Buy Your Products At The Best Possible Rates", "Affordable And Highly Recommended By Users, Make Your Purchase Today.", "You Never Have To Pay Full Price On Your Favorite Items Ever Again!", "Don't Hesitate Any Longer, The Time To Make Your Purchase Is Now", "We Find All The Best Deals Daily And Handpick Every Coupon Code", "Check Out These Awesome Products Just Waiting For You In Store.", "Great Discounted Price Using This Voucher Code At The Checkout.", "Surround Yourself With A World Of Happiness Once You Check Out.", "Remember, You Have Only A Few Hours Left To Redeem Your Offer!", "Don't Wait To Snatch Up Your Savings. Grab Them While You Can!", "Sale Ends Soon, So Take Advantage Of These Great Savings Now!", "If You Want To Maximize Your Savings We Suggest You Get One!", "You Will Not Want To Miss This Amazing Instant Savings Deals", "Don't Want To Miss The Opportunity To Save Even More Money.", "Start Saving Money On Your Online Purchases With Our Coupon", "Fantastic Discounted Price Using This Code At The Checkout.", "Right Now Is The Best Time To Buy And Claim It As Your Own.", "Grab The Freshest Coupon Codes, Get Your Awesome Discounts", "Click On This Great Deal And Activate An Amazing Discount.", "Do Not Miss Out On This Much Awesome While Supplies Last!", "For Limited Time Only, This Great Deals To Help You Save.", "Last Chance To Grab Must-Have Style At Even Better Prices", "Your Search For Great Deals And Coupon Savings Ends Here", "Take Advantage Of These Promotions And Order Online Now.", "Best Sellers Will Disappear Soon If You Don't Grab Them!", "Our Customers Love Good Bargains And We Know You Do Too.", "Be The First To Know, First To Shop, And First To Save!", "Bargains At These Amazingly Low Prices Won't Last Long!", "We'Ve Got Promo Codes & Sales To Help You Score Savings", "Be The First To Know, First To Shop, And First To Save!", "For The Ultimate Shopping Experience, Look No Further.", "Redeem It To Get That Exclusive Discount Just For You.", "Pick Your Favorite Items And Save With Our Coupon Code", "Everyday, We Offer Enticing Deals That You Can't Miss.", "There's No Better Online Promo Code For You Than Ours", "Look No Further Than Here For The Most Amazing Deals.", "Pick Up All Those Awesome Goodies You'Ve Been Wanting", "Every Item Is Discounted To The Lowest Price Possible", "Enjoy Incredible Discounts On All Your Favorite Items", "Huge Savings Available When You Use Storename Coupons", "Remember To Check Out So You Can Keep The Discounts!", "Here Is The Right Place You Can Find Amazing Savings", "The Easiest Way Is To Get Lower Prices On Any Orders", "Shop Now With Our Coupon Codes For Instant Discounts", "Guaranteed To Make Your Heart Beat With These Deals.", "Instantly Apply The Best Codes Directly To Your Cart", "Now You Can Get Wonderful Savings On Your Purchases", "These Deals Won't Last, So Make The Purchase Today.", "Visit Now And Get Your Coupons Before They Expire!", "Groundbreaking Sale With Never Before Seen Prices.", "Make Sure That You Take That Opportunity To Save.", "Get The Deal To Save Money. Make Them Yours Now!", "Order Yours Now And Take Advantage Of This Deal!", "Check Out With Coupon And Start Saving Right Now", "Extraordinary Savings, Only For A Limited Time.", "Save Money With Promo Codes Guaranteed To Work", "Your Place To Shop And Discover Amazing Deals.", "Experience Major Savings With This Great Deal", "Amazing Deals Like This Don't Appear Everyday", "Remember That Good Deals Are Hard To Come By!", "Discover These Exceptionally Good Deals Today", "Get The Best Deals When You Shop On Storename", "Enjoy Your Great Journey On Online Shopping.", "Sale Ends Soon! Buy It Before It's Too Late.", "Your Gateway To A Great Shopping Experience.", "You Can Avail These Amazing Coupon And Deal ", "Start Saving Money On Your Online Purchases", "Don't Miss This Deal Or You Will Regret It.", "Shop Now And Save Now, Don't Wait Anymore.", "Great Chance To Save Money With This Offer", "Take Advantage Of This Savings Opportunity", "Always Ahead So You Can Get Amazing Deals!", "Check Out The Big, Never-Before-Seen Sale.", "Get At A Discounted Price Using This Code.", "Now Is The Time To Enjoy Fantastic Deals.", "New And Amazing Items For A Limited Time.", "Check Merchant Site For More Information.", "Don't Miss Your Chance To Be Lucky Users!", "Great New Way To Enjoy Additional Savings", "You Will Not Miss The Best Discount Ever.", "Save Money On Thousands Of Items You Love", "Amazing Savings You Won't Find Anywhere!", "Seasonal Sale For An Extended Time Only.", "We Only Help You Find The Best Bargains.", "Trust Me, You Can't Miss These Hot Deals", "You Will Only Find The Best Deals Here!", "Instant Savings When You Purchase Today", "It Is Currently At Its Best Price Ever.", "Look For Codes That Match What You Want", "Find The Joy Of Saving While Shopping!", "The More You Order, The More You Save.", "Find The Joy Of Saving While Shopping!", "Spend Much Less On Your Favorite Items", "Prices Vary Daily, So Take Action Now.", "Check Out Now Before This Deal Expires", "Get Attractive Discounts While You Can", "It Is For A Limited Time, So Be Quick.", "Be Budget Savvy With This Great Offer", "Don't Wait To Snatch Up Your Savings.", "Be Budget Savvy With This Great Offer", "It's Easy To Save Money When You Shop", "Don't Miss This Chance To Save Money ", "Act Immediately Before The Sale Ends.", "Save Even More With Storename Coupons", "Final Hours! Great Deals Ending Soon", "Remember To Finish Your Transaction!", "Get Your Instant Online Savings Now.", "Get Your Coupon Before They Expire!", "Take This Great Opportunity To Save", "You Won't Find This Deal Elsewhere!", "Make Your Purchase Now And Save Big", "A Perfect Price For What You Love.", "Save Big During This Seasonal Sale", "Stack Coupons For Maximum Savings.", "Extraordinary Savings, Only Today.", "Save Big During This Seasonal Sale", "Time For Big Savings When You Shop", "Buy Now, Instead Of Regret Later.", "No Time Is Better Than Right Now.", "Here's Your Exclusive Coupon Code", "It's Shopping Time! Shop And Save", "Find The Best Discount And Save!", "Absolutely. Positively. Perfect.", "Hot Specials Don't Last Forever.", "Hurry Before The Deals Are Gone.", "Special Buy Savings. Today Only!", "Get Huge Dicounts For You Today", "Grab These Must-Have Items Now.", "Now Available For Online Orders", "Save Effortlessly When You Shop", "This offer is for only 24 hours", "Grab Before Someone Else Does.", "Shop Now And Save Immediately!", "Add To Your Cart And Check Out", "Last Chance To Use This Coupon", "Exclusive Offers Only For You.", "Time To Get Your Shopping On.", "Discover Incredible Discounts", "Big Savings For What You Love", "Save Money On Things You Want", "All Codes Guaranteed To Work.", "Take Advantage Of Our Coupons", "Big Savings While They Last!", "Last Chance To Shop And Save", "Get Money Off Your Purchases", "Great Stores. Great Choices.", "Take The Great Chance Today!", "Shop Now And Save Instantly", "Get Code And Save Instantly", "Amazing Offers Ending Soon!", "Sale For Limited Time Only.", "Discounted Price As Marked.", "Amazing Sale, Great Prices.", "Enjoy Attractive Discounts!", "In-Store Promotions & Deals", "Enter The Code At Checkout.", "Beat The Crowd And Buy Now.", "Get Our Low Price Guarantee", "We Serve To Help You Save.", "Add Them To Your Cart Now.", "Come On And Take This One!", "Act Now While Offer Lasts.", "Amazing Promotional Codes", "For First Time Customers!", "For A Limited Time Only.", "Grab Your Savings Today", "Place Order And Rejoice", "That's massive savings!", "Shop And Save With Us!", "Spend Less, Save More!", "Grab This Awesome Deal", "Checkout Easy Savings.", "Expect The Unexpected.", "Click Through To Shop.", "Best Deal Of The Day", "Sale For Today Only.", "Don't Wait Anymore!", "Most Popular Newest", "Ready To Check Out?", "It's Now Or Never.", "Be Quick To Shop!", "Take Action Now!", "Just Shop & Save"];
        $new_excerpt = $random[rand(0, count($random) - 1)];
        $new_excerpt = str_replace('_XX_', $xx, $new_excerpt);
        $new_excerpt = str_replace('STORENAME', $store_name, $new_excerpt);
        return $new_excerpt;
    }
}
if (!function_exists('is_mobile')) {
    function is_mobile()
    {
        $detect_mobile = new Mobile_Detect();
        if ($detect_mobile->isMobile()) {
            return true;
        } else {
            return false;
        }
    }
}
if (!function_exists('strip_amp')) {
    function strip_amp($snippet)
    {
        $snippet = preg_replace("/<img[^>]+\>/i", '', $snippet);
        $snippet = preg_replace("/<?php[^>]+\>/i", '', $snippet);
        $snippet = preg_replace("/<CODE[^>]+\>/i", '', $snippet);
        $snippet = preg_replace('/<h1[^>]*>([\s\S]*?)<\/h1[^>]*>/', '', $snippet);
        $snippet = preg_replace('/<Data[^>]*>([\s\S]*?)<\/Data[^>]*>/', '', $snippet);
        $snippet = preg_replace('/<iframe[^>]*>([\s\S]*?)<\/iframe[^>]*>/', '', $snippet);
        return $snippet;
    }
}
if (!function_exists('currency_format')) {
    function currency_format($number)
    {

        return number_format(intval($number));
    }
}
