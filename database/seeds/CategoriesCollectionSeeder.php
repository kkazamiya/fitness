<?php

use Illuminate\Database\Seeder;
use App\Categories;
use App\Keywords;

class CategoriesCollectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $list_categories = ["Accessories", "Adult", "Air Conditioners", "Amusement & Water Park", "Anniversary Gifts", "Art and Craft Supplies", "Automobiles and Accesssories", "Baby Diapers & Wipes", "Baby Toys", "Backpacks", "Bags", "Bean Bags", "Beauty", "Beauty Services", "Belts", "Beverages", "Bike Accessories", "Books", "Books and Media", "Boys Footwear", "Bus", "Cab", "Cake", "Camera Accessories", "Camera Lens", "Cameras", "Car Care", "Car Rentals", "Car Service and Repair", "Car Wheels and Tyres", "Cashback", "Chocolate", "Chocolate Cakes", "City Taxi", "Clothing", "Computer Accessories", "Computers", "Contact Lenses", "Data Card Recharge", "Digital Cameras", "Domains", "Domestic Flights", "Dry Fruits", "DTH Recharge", "Education", "Electronics", "Entertainment", "Event Tickets", "External Hard Disks", "Eyewear", "Fashion", "Fashion Accessories", "Fast Food", "Flight", "Food", "Food Delivery", "Footwear", "Furniture", "Games", "Gift Cards", "Gifts and Flowers", "Groceries", "Hair Dryers", "Handbags", "Headphones", "Health", "Healthcare & Sports/Fitness", "Holiday Packages", "Home and Garden", "Home And Groceries", "Home and Kitchen", "Home Appliances", "Home Cleaning Services", "Hosting", "Hotel", "International Flights", "Jewelry", "Keyboard and Mouse", "Kids", "Kids Lifestyle", "Kitchen Appliances", "Laptop Accessories", "Laptop Batteries", "Laptops", "LED Bulbs and Lamps", "Lingerie", "Local", "Luggage Bags", "Magazines", "Matrimony", "Medicines", "Memory Cards", "Men Sandals", "Men Sports Shoes", "Mens", "Men's Flip Flops", "Men's Footwear", "Men's Lifestyle", "Men's Shirts", "Men's T-shirts", "Men's Underwear", "Mens watches", "Miscellaneous", "Mobile", "Mobile Accessories", "Mobile Recharge", "Movie Tickets", "Musical Instruments", "Pen Drives", "Personalized Gifts", "Pet Food", "Pets", "Pizza", "Postpaid Bill Payments", "Power Tools", "Printers and Scanners", "Protein Supplements", "Recharge", "Refrigerators", "Reseller Hosting", "Restaurants", "Rummy", "Safety Gloves", "Safety Products", "Services", "Shoe Racks", "Site Wide", "Snacks", "Software and Antivirus", "Speakers", "Sporting Goods", "Study Tables", "Sweets", "Tablets", "Tea", "Television", "Tickets", "Train Tickets", "Travel", "Travel VISA", "Trimmers", "Utility Bill Payments", "VPS Hosting", "Wallets", "Washing Machines", "Watches", "Web Hosting", "Womens", "Women's Footwear", "Women's Kurtis", "Women's Lehengas", "Women's Lifestyle", "Women's Loungewear", "Women's Sarees"];
        foreach ($list_categories as $value) {
            $new_category = new Categories;
            $new_category->category_name = $value;
            $new_category->save();
            $new_keyword = new Keywords;
            $new_keyword->keyword = $value . " Coupons";
            $new_keyword->keyword_cat = $new_category['_id'];
            $new_keyword->keyword_ads = '';
            $new_keyword->keyword_couponupto = '';
            $new_keyword->view = 0;
            $new_keyword->tracked = 0;
            $new_keyword->hash_r = md5($value . " Coupons");
            $new_keyword->save();
        }
        print ("Seed " . count($list_categories) . " Categories");
    }
}
