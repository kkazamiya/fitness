$(document).ready(function() {
    
    onResize();
    loadSliderPartner();
    var screenWidth = window.innerWidth;
    window.addEventListener("resize", onResize);
    function onResize() {
        var screenWidth = window.innerWidth;
        if (screenWidth > 667 ) {
            $('.navbar-light').addClass('p-0');
            $('#sidebar-right').addClass('p-0');
            $('.col_blogs').addClass('pr-0 pl-2');
        } else {
            $('.navbar-light').removeClass('p-0');
            $('#sidebar-right').removeClass('p-0');
            $('.col_blogs').removeClass('pr-0 pl-2');
            $('#box__find').removeClass('row');
            $('#box__find').addClass('pb-3');
        }
    }
    
    $(window).scroll(function() {
        if (document.body.scrollTop > 600 || document.documentElement.scrollTop > 600) {
            $('.gototop').css({ 'opacity': '1' });
        } else {
            $('.gototop').css({ 'opacity': '0' });
        }
    });

    $('.gototop').click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: window.pageYOffset = 0
        }, 2000);
        return false;
    });

    function loadSliderPartner() {
        var screenWidth = window.innerWidth;
        $('#apps-slider').owlCarousel({
            loop:true,
            margin:10,
            autoplay:true,
           
            autoplayHoverPause:true,
            nav: screenWidth < 667 ? false : true,
            dots: false,
            navContainerClass: screenWidth < 667 ? "d-none" : "owl-nav",
            navText: ["<i class='fa fa-angle-left fa-3x text-white'></i>","<i class='fa fa-angle-right fa-3x text-white'></i>"],
            responsive:{
                0:{
                    items:4,
                    loop: true
                },
                600:{
                    items:6,
                    loop: true
                },
                1000:{
                    items:8,
                    loop: true
                }
            }
        });
    }
});

