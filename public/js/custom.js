$(document).ready(function () {
    const urlParams = new URLSearchParams(window.location.search);
    $("img.lazyload").lazyload();
    $("iframe").css("width", "100%");
    var _token = $('meta[name="csrf-token"]').attr('content');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': _token
        }
    });
    onResize();
    loadSliderPartner();
    var screenWidth = window.innerWidth;
    window.addEventListener("resize", onResize);
    function onResize() {
        var screenWidth = window.innerWidth;
        if (screenWidth > 667 ) {
            $('.navbar-light').addClass('p-0');
            $('#sidebar-right').addClass('p-0');
            $('.col_blogs').addClass('pr-0 pl-2');
        } else {
            $('.navbar-light').removeClass('p-0');
            $('#sidebar-right').removeClass('p-0');
            $('.col_blogs').removeClass('pr-0 pl-2');
            $('#box__find').removeClass('row');
            $('#box__find').addClass('pb-3');
        }
    }
    /*if (typeof $.cookie('no_thanks') === "undefined") {
        $('#modal').modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    }
    $("#modal").on("hidden.bs.modal", function () {
        $.cookie('no_thanks', 'true', {expires: 7, path: '/'});
    });*/

    function build_modal(code_id) {
        let store_name = $("#" + code_id).data('store_name');
        let store_domain = $("#" + code_id).data('store_domain');
        let store_image = $("#" + code_id).data('store_image');
        if (typeof store_image === "undefined") {
            store_image = '/files/5d64e1fea22d142284003812/fitnessol-loading.svg';
        }
        let store_slug = $("#" + code_id).data('store_slug');
        let code_coupon = $("#code-" + code_id).text();
        let type_store = find_store_type();
        let coupon_name = $("#" + code_id).data('code-name');
        $("#modal_logo").attr('src', store_image).attr('alt', store_name);
        $("#modal_title").attr('alt', store_name).text(coupon_name);
        $("#store_domain").attr('href', "/out/" + code_id).text(store_domain);
        if ($("#" + code_id).data('code_type') == 'code') {
            $("#modal_input").val(code_coupon);
            $("#copy_code").data('clipboard-text', code_coupon);
            $("#modal_show_code").show();
            $("#modal_show_deal").hide();
        } else {
            $("#go_deal").attr("href", "/out/" + code_id);
            $("#modal_show_code").hide();
            $("#modal_show_deal").show();
        }
        $("#link-store").attr("href", "/" + type_store + "/" + store_slug).text("More " + store_name + " Coupons >>");

        $('#modal-key-coupon').modal('show');
        setTimeout(function () {
            $('html, body').animate({
                scrollTop: $("#" + code_id).offset().top
            }, 'slow');
        }, 500);
    }

    function find_store_type() {
        if (window.location.pathname !== '/') {
            if (window.location.pathname.split("/")[1] === 'event') {
                return "stores";
            } else {
                return window.location.pathname.split("/")[1];
            }
        } else {
            return "stores";
        }
    }

    if (window.location.hash) {
        var code_hash_tag = $(location).attr('hash').substr(1);
        $(document).on('click', 'body *', function () {
            const ref_fr = urlParams.get('fr');
            if (ref_fr === 'result') {
                window.open(window.location.href.split('?')[0] + '#' + code_hash_tag, '_blank');
                window.open('/out/' + code_hash_tag, '_self');
            }
        });
        build_modal(code_hash_tag);
        /*if (ref_fr === 'result') {
            window.open(window.location.href.split('?')[0] + '#' + code_hash_tag, '_blank');
            window.open('/out/' + code_hash_tag, '_self');
        } else {
            build_modal(code_hash_tag);
        }*/
    }

   $(".a_code").click(function () {
                let ex_link = $(this).attr('href');
                let aff_link = $(this).data('aff');
                window.open(ex_link, '_blank');
                window.open(aff_link, '_self');
                return false;
            });

    // Clipboard (Click and Copy)
    let clipboard = new ClipboardJS('#copy_code');
    clipboard.on('success', function (e) {
        $("#copy_code").attr('title', 'Copied').tooltip('show');
    });
    clipboard.on('error', function (e) {
        $("#copy_code").attr('title', 'Try Again').tooltip('show');
    });

    /*TypeAhead*/
    let engine1 = new Bloodhound({
        remote: {
            url: '/autocomplete?q=%QUERY%',
            wildcard: '%QUERY%'
        },
        datumTokenizer: Bloodhound.tokenizers.whitespace('keyword'),
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });

    setTimeout(function () {
        let with_input = $(".type-search").width();
        console.log(with_input);
        if (with_input > 0) {
            $(".type-search > .tt-dataset").css({'width': with_input});
        }
    }, 2000);
    $(".type-search").typeahead({
        hint: true,
        highlight: true,
        minLength: 2,
        display: true

    }, [ 
        {
            source: engine1.ttAdapter(),
            name: '',
            limit: 50,
            displayKey: 'keyword',
            templates: {
                empty: [
                    ''
                ],
                header: [
                    ''
                ],
                suggestion: function (data) {
                    return '<li class="list-group-item px-2 py-1 no-border text-left text-truncate search-hint">' +
                        '<a href="/' + data.keyword_slug + '">' + data.keyword.replace(/"|\\/gi, '') + '</a>' +
                        '</li>';
                }
            }
        },
    ]);

    window.onscroll = function () {
        scrollFunction()
    };

    function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            $("#btn-top").show();
        } else {
            $("#btn-top").hide();
        }
    }

    $("#btn-top").click(function () {
        $('html,body').animate({scrollTop: 0}, 'slow');
    });


    $("#read-more").click(function () {
        let totalHeight = 22;
        $ps = $("#full-text").find("small");
        $ps.each(function () {
            totalHeight += $(this).outerHeight();
        });
        $("#full-text").css({"height": $("#full-text").height(), "max-height": 9999}).animate({"height": totalHeight});
        $(this).hide();
        return false;
    });
    $("#read-more-expired").click(function () {
        let totalHeight = 22;
        $ps = $("#full-text").find(".coupon");
        $ps.each(function () {
            totalHeight += $(this).outerHeight();
        });
        $("#full-text").css({"height": $("#full-text").height(), "max-height": 9999}).animate({"height": totalHeight});
        $(this).hide();
        return false;
    });

    let img_partner = $("#img_partner").data('obj-img');

    function show_partner($this) {
        let size = $this.data('size'), split_size = size.split("x"), w = split_size[0], h = split_size[1],
            pos = $this.data('pos');
        img_partner.sort(function (a, b) {
            return 0.5 - Math.random()
        });
        $.each(img_partner, function (i, v) {
            if (v.banner_pos === pos && v.banner_size === size) {
                $this.html("<div class='p2'><a href='" + v.banner_link + "' target='_blank' rel='nofollow'><img src='" + v.banner_image + "' width='" + w + "' height='" + h + "' alt='" + v.banner_alt + "'/></a></div>");
                return false;
            }
        });
        setTimeout(function () {
            show_partner($this)
        }, Math.floor((Math.random() * 10) + 5) * 1000);
    }

    $(".img_partner").each(function () {
        show_partner($(this));
    });
    $(".click-show").click(function () {
        let id_show = $(this).data("id-link") + "-link";
        $("#" + id_show).removeClass("d-none");
    });
    $("#load-more-result").click(function () {
        let id_keyword = $(this).data("keyword");
        $(this).attr("disabled", true);
        $(this).html("<i class='fa fa-spinner fa-spin'></i>");
        $.post({
            url: "/load-more?keyword=" + id_keyword
        }).done(function (data) {
            $(this).hide();
            if (data.code === 1) {
                location.reload();
            }
        });
    });
    

    $(".view-job").click(function (e) {
        if ($(this).hasClass('view-job')) {
            e.preventDefault();
        }
        let host_link = $(this).data("link"), id_link = $(this).data("id");
        let a_href = '<a class="text-primary" href="/v-result/' + id_link + '" target="_blank" rel="nofollow">' + host_link + '</a>';
        $('#' + id_link).html(a_href).show("fast");
        $(this).removeClass('text-white')
            .removeClass('view-job')
            .attr('href', '/v-result/' + id_link)
            .attr('target', '_blank');
    });
    // if (typeof $('.infinite-scroll') !== "undefined" && typeof $('ul.pagination') !== "undefined") {
    //     $('ul.pagination').hide();
    //     $(function () {
    //         $('.load_more').click(function (e) {
    //             e.preventDefault();
    //             let next_link = $('ul.pagination').find('a[rel=next]').attr('href');
    //             $('ul.pagination').remove();
    //             $('.when-loading').show();
    //             alert(next_link);
    //             $.get(next_link, function (data) {
    //                 alert(data);
    //                 $('.infinite-scroll').append(data);
    //                 $('.when-loading').hide();
    //                 $('ul.pagination').hide();
    //             });
    //         });
    //     });
    // }
    
  

    $("#dismissWithoutDisable").click(function () {
        $.cookie('no_thanks' + window.location.pathname, 'true', {expires: 1, path: window.location.pathname});
        location.reload();
    });

    $("#input-email").keyup(function () {
        if ($("#input-email").val().length > 8) {
            $("#submit-email").addClass('btn-info').removeClass('btn-outline-info').removeAttr("disabled");
        } else {
            $("#submit-email").addClass('btn-outline-info').removeClass('btn-info').attr("disabled", true);
        }
    });
    $("#donate-click").click(function () {
        $('#donate-click').html('<p class="alert alert-success text-center">We accept donations via Paypal: <b>couponxoo@gmail.com</b> or Direct Transfer</p>')
    });
    function loadSliderPartner() {
        var screenWidth = window.innerWidth;
        $('#apps-slider').owlCarousel({
            loop:true,
            margin:10,
            autoplay:true,
           
            autoplayHoverPause:true,
            nav: screenWidth < 667 ? false : true,
            dots: false,
            navContainerClass: screenWidth < 667 ? "d-none" : "owl-nav",
            navText: ["<i class='fa fa-angle-left fa-3x text-white'></i>","<i class='fa fa-angle-right fa-3x text-white'></i>"],
            responsive:{
                0:{
                    items:4,
                    loop: true
                },
                600:{
                    items:6,
                    loop: true
                },
                1000:{
                    items:8,
                    loop: true
                }
            }
        });
    }
    $(window).scroll(function() {
        if (document.body.scrollTop > 600 || document.documentElement.scrollTop > 600) {
            $('.gototop').css({ 'opacity': '1' });
        } else {
            $('.gototop').css({ 'opacity': '0' });
        }
    });

    $('.gototop').click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: window.pageYOffset = 0
        }, 2000);
        return false;
    });
});
