<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="@yield('description')">
    <title>Fitnessol - @yield('title')</title>
    <link rel="stylesheet" href="{{url('/assets')}}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('/assets')}}/css/font-awesome.min.css">
   <link rel="canonical" href="@yield('canonical')">
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('assets/logo/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('assets/logo/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/logo/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/logo/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets/logo/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('assets/logo/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('assets/logo/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('assets/logo/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/logo/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('assets/logo/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/logo/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('assets/logo/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/logo/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('assets/logo/manifest.json')}}">
    <link rel="stylesheet" href="{{url('/assets')}}/css/responsive.css">
    <link rel="stylesheet" href="{{url('/assets')}}/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{url('/assets')}}/css/owl.theme.default.min.css">
 <link rel="stylesheet" href="{{url('/assets')}}/css/style.css">

    <script src="{{url('/assets')}}/js/jquery-3.4.1.min.js"></script>

    <script src="{{url('/js')}}/typeahead.bundle.min.js"></script>
</head>
<body class="option">
@yield('navbar')
@yield('main_content')
<footer id="footer" class="pt-5 px-3">
        <div class="container">
            <div class="row justify-content-center">
                
                <div class="col-md-4">
                    <h5 class="text-uppercase">INFORMATION</h5>
                    <p class=" font-14">Fitnessol’s existence is to bring people great exercising experience and help you get a healthier body in the way you want. At Fitnessol, all fitness services, fitness centers, fitness equipment, workout tips are available for everyone. Find everything related to fitness on our site.</p>
                </div>
                <div class="col-md-2">
                    <h5 class="text-uppercase">MENU</h5>
                    <ul class="list-inline pl-0 menu">
                        <li class="font-14 li-item mb-2">
                            <a href="{{route('blog','about-us')}}">About Us</a>
                        </li>
                        <li class="font-14 li-item mb-2">
                            <a href="{{route('blog','contact-us')}}">Contact Us</a>
                        </li>
                        <li class="font-14 li-item mb-2">
                            <a href="{{route('blog')}}">New blogs</a>
                        </li>
                        <li class="font-14 li-item mb-2">
                            <a href="{{route('blog','privacy-policy')}}">Privacy Policy</a>
                        </li>
                    </ul>
                </div>
                
                <div class="col-md-3">
                    <h5 class="text-uppercase">RECENT POSTS</h5>
                    <ul class="list-inline pl-0">
                        @foreach($latest_posts as $value)
                        <li class="font-14 li-item mb-2">
                            <a href="{{route('blog',['slug'=>$value->slug])}}" class="d-block text-truncate">{{ucwords($value->title)}}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>
                
                
            </div>
        </div>
      
    </footer>
 <div class="box_copyright text-center mt-3 p3">© 2020 Copyright: <a href="">All rights reserved.</a></div>
 <a href="#header" class="d-inline-block effect gototop p-2 text-white px-3"><i class="fa fa-angle-double-up"></i></a>
    
</body>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f228731cea988ad"></script>
<script src="{{url('/assets')}}/js/bootstrap.min.js"></script>
<script src="{{url('/assets')}}/js/owl.carousel.min.js"></script>
<script src="{{url('/assets')}}/js/custom.js"></script>
<script src="{{url('/js')}}/custom.js"></script>

<script src="{{url('/js')}}/jquery.lazyload.min.js"></script>
<script src="{{url('/js')}}/plugins.min.js"></script>
 
@yield('js')
</html>