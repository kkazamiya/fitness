<form class="mb-2" action="/search" method="get" target="_top">
    <input type="hidden" name="ts" value="amp"/>
    <div class="row" id="box__find">
        <div class="col-12 pr-0">
            <amp-autocomplete
                sizes="(max-width: 320px) 310px, 92vw"
                filter="none"
                min-characters="2"
                highlight-user-entry
                src="/autocomplete?ts=what&_token={{csrf_token()}}&res=json"
                [src]="'/autocomplete?ts=what&res=json&_token={{csrf_token()}}&q=' + (query || '')">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                                      <span class="input-group-text bg-white font-weight-bold">
                                        <i class="fa fa-search"></i>
                                    </span>
                        </div>
                        <input type="text" name="q" id="type-what" autocomplete="off"
                               class="form-control border-left-0 input_what"
                               on="input-debounced:AMP.setState({query: event.value})"
                               placeholder="Search for jobs, companies, skills"/>
                    </div>
                </div>
                <template type="amp-mustache">
                    <p class="text-truncate mb-1 p-0">@{{name}}</p>
                </template>
            </amp-autocomplete>
        </div>
        <div class="col-12 pr-0">
            <amp-autocomplete
                sizes="(max-width: 320px) 310px, 92vw"
                filter="none"
                min-characters="2"
                highlight-user-entry
                src="/autocomplete?ts=where&_token={{csrf_token()}}&res=amp"
                [src]="'/autocomplete?ts=where&res=amp&_token={{csrf_token()}}&q=' + (query || '')">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-prepend">
                                      <span class="input-group-text bg-white font-weight-bold">
                                        <i class="fa fa-location-arrow"></i>
                                    </span>
                        </div>
                        <input type="text" name="l" id="type-where" autocomplete="off"
                               class="form-control border-left-0 input_what"
                               on="input-debounced:AMP.setState({query: event.value})"
                               placeholder="Search In Location"/>
                    </div>
                </div>
                <template type="amp-mustache">
                    <p class="text-truncate mb-1 p-0">@{{city_full}}</p>
                </template>
            </amp-autocomplete>
        </div>
        <div class="col-12">
            <div class="d-flex btn__find">
                <button typeof="submit"
                        class="align-items-center btn btn-warning d-flex h-100 justify-content-center p-0 w-100">
                    Find jobs
                </button>
            </div>
        </div>
    </div>
    @csrf
</form>
