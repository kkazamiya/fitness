<header class="navbar-light bg-navy">
    <div class="container">
        <div class="row p-2 d-flex justify-content-between">
            <a class="navbar-brand" href="/">
                <amp-img class="img-fluid" src="{{asset('assets/images/onlinejobe-logo.png')}}" width="100" height="31"
                         alt="scholarshipy.com"></amp-img>
            </a>
            <button class="navbar-toggler" type="button" on="tap:sidebar-navigation.toggle">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </div>
</header>
<aside id="target-element-navigation"></aside>
<amp-sidebar id="sidebar-navigation" class="mobile-sidebar p-3" layout="nodisplay" side="right">
    <button class="btn bg-transparent text-white" on="tap:sidebar-navigation.close">X</button>
    <nav toolbar="(min-width: 784px)" toolbar-target="target-element-navigation">
        <ul class="navbar-nav d-flex w-100">
            <li class="active align-items-center d-flex nav-item">
                <i class="fa fa-search mr-2"></i><a class="nav-link p-0 font-weight-bold" href="/">Find Jobs</a>
            </li>
            <li class="align-items-center d-flex nav-item">
            </li>
            <li class="align-items-center d-flex nav-item">
                <a class="nav-link p-0 font-weight-bold" href="{{route('blog')}}">Career Advices</a>
            </li>
        </ul>
    </nav>
</amp-sidebar>
