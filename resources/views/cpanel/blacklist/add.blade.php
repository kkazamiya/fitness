@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Add Blacklist</h1>
@stop

@section('content')
    <div class="col-lg-offset-2 col-md-8">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'cpanel.blacklist.store']) !!}
            {!! Form::token(); !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="blacklist">List Domain</label>
                    <textarea class="form-control" id="blacklist" name="blacklist" rows="5"
                              placeholder="Per Line Per Domain..."></textarea>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-block">Create</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('js')
@endsection
