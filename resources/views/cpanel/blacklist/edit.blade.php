@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Edit: {{$editor['black_domain']}}</h1>
@stop

@section('content')
    <div class="col-lg-offset-2 col-md-8">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ route('cpanel.blacklist.update',['id' =>  $editor['_id'] ]) }}" method="post">
                {!! Form::token(); !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="blacklist">Category Name</label>
                        <input type="text" class="form-control" name="blacklist" id="blacklist"
                               placeholder="Domain Name" value="{{$editor['black_domain']}}"/>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-warning btn-block">Update</button>
                </div>
            </form>
        </div>
    </div>

@stop
@section('js')
@endsection
