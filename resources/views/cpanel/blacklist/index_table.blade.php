@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><i class="fa fa-ban"></i> List Black Domain</h1>
    <ol class="breadcrumb">
        <li><a href="/c-panel"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#" class="active">BlackList</a></li>
    </ol>
    <br/>
    <a class="btn btn-success" href="{{route('cpanel.blacklist.create')}}">
        <i class=" fa fa-plus-circle"></i>
        Add Blacklist
    </a>
    <a class="btn btn-danger" href="{{route('cpanel.get-remove-keyword')}}">
        Remove Keyword & Domain
    </a>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('content')
    @isset ($msg)
        <div class="alert alert-success">
            <p>{{ $msg }}</p>
        </div>
    @endisset
    <div class="row">
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="data-table">
                    <thead>
                    <tr>
                        <th>Domain</th>
                        <th>Date</th>
                        <th>-</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover" id="data-black-kw">
                    <thead>
                    <tr>
                        <th>Keyword</th>
                        <th>Date</th>
                        <th>-</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@stop
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"
            integrity="sha256-rXnOfjTRp4iAm7hTAxEz3irkXzwZrElV2uRsdJAYjC4=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function () {
            $('#data-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{ route('cpanel.svs_blacklist') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{csrf_token()}}"}
                },
                "columns": [
                    {"data": "black_domain"},
                    {"data": "created_at"},
                    {"data": "options"}
                ]
            });
            $('#data-black-kw').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{ route('cpanel.svs_keyword_black') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{csrf_token()}}"}
                },
                "columns": [
                    {"data": "keyword"},
                    {"data": "created_at"},
                    {"data": "options"}
                ]
            });
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('click', 'a.jquery-postback', function (e) {
            e.preventDefault(); // does not go through with the link.

            var $this = $(this), name = $(this).data('name'), id = $(this).data('id');
            $.post({
                type: $this.data('method'),
                url: $this.attr('href')
            }).done(function (data) {
                alert('Success remove: ' + name);
                $("#" + id).hide();
            });
        });
    </script>
@endsection
