@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Cache Controller</h1>
@stop

@section('content')
    <div class="col-md-8">
        <div class="box box-primary">
            <div class="box-body">
                <a href="/c-panel/clear-cache/home">Clear Cache Home Page</a><br/>
                <a href="/c-panel/clear-cache/more-offers/">Clear Cache More Offer</a><br/>
                <a href="/c-panel/clear-cache/keyword/">Clear Cache Keyword</a><br/>
            </div>
        </div>
    </div>
@stop
@section('js')
@endsection
