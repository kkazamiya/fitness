@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Add New Categories</h1>
@stop

@section('content')
    <div class="col-lg-offset-2 col-md-8">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'cpanel.categories.store']) !!}
            {!! Form::token(); !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="category_name">Category Name</label>
                    <input type="text" class="form-control" name="category_name" id="category_name"
                           placeholder="Category Name"/>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-block">Create</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('js')
@endsection
