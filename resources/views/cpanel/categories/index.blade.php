@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><i class="fa fa-shopping-cart"></i> List Store</h1>
    <ol class="breadcrumb">
        <li><a href="/cms"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#" class="active">Stores</a></li>
    </ol>
    <br/>
    <a class="btn btn-success" href="{{route('cms.stores.create')}}">
        <i class=" fa fa-plus-circle"></i>
        Create New Store
    </a>
    <a class="btn btn-danger" href="{{route('cms.stores.add_excel')}}">
        <i class=" fa fa-plus-circle"></i>
        Add Stores By Excel File
    </a>
    <a class="btn btn-warning" href="{{route('cms.stores.export_excel')}}">
        <i class=" fa fa-plus-circle"></i>
        Export Stores By Excel 2003
    </a>
@stop

@section('content')
    @isset ($msg)
        <div class="alert alert-success">
            <p>{{ $msg }}</p>
        </div>
    @endisset
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover" id="data-table">
            <thead>
            <tr>
                <th>Store Image</th>
                <th>Store Name</th>
                <th>Add/Update Date</th>
                <th>Created At</th>
                <th>-</th>
            </tr>
            </thead>
            <tbody>
            @foreach($stores as $store)
                <tr id="{{$store['_id']}}">
                    <td>
                        <a href="{{$store['store_domain']}}" target="_blank">
                            <img height="80px" class="lazyload"
                                 data-original="{{$store['store_image']}}"
                                 src="/files/5d64e1fea22d142284003812/fitnessol-loading.svg"
                                 alt="{{str_title($store['store_name'])}}"/>
                        </a>
                    </td>
                    <td>
                        {{$store['store_name']}} -
                        <a href="{{$store['store_aff']}}" target="_blank"><i class="fa fa-money"></i></a>
                         -
                        <a href="/coupons/{{$store['slug_store']}}" target="_blank"><i class="fa fa-external-link"></i></a>
                    </td>
                    <td>{{$store['coupon_added']}}</td>
                    <td>Created at: {{Carbon\Carbon::parse($store['created_at'])->format('Y-m-d H:m:s')}}<br/>
                    Updated at: {{Carbon\Carbon::parse($store['updated_at'])->format('Y-m-d H:m:s')}}
					</td>
                    <td>
                        <a href="{{route('cms.stores.edit',['id' =>  $store['_id'] ])}}"><span class="label label-primary"><i class="fa fa-edit"></i></span></a>
                        <a href="{{ route('cms.stores.destroy', $store['_id']) }}" data-id-store="{{$store['_id']}}" data-store="{{$store['name']}}" data-method="delete" class="jquery-postback"><span class="label label-danger"><i class="fa fa-remove"></i></span></a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@stop
@section('js')
    <script src="{{asset('/js/jquery.lazyload.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('#data-table').DataTable({
                "drawCallback": function() {
                    $("img.lazyload").lazyload();
                }
            });
            $("img.lazyload").lazyload({
                threshold: 200
            });
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('click', 'a.jquery-postback', function (e) {
            e.preventDefault(); // does not go through with the link.

            var $this = $(this);
            var name_store = $(this).data('store');
            var id_store = $(this).data('id-store');
            console.log($(this).data('store'));
            $.post({
                type: $this.data('method'),
                url: $this.attr('href')
            }).done(function (data) {
                alert('Success remove: ' + name_store);
                $("#" + id_store).hide();
            });
        });
    </script>
@endsection
