@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><i class="fa fa-shopping-cart"></i> List Keywords</h1>
    <ol class="breadcrumb">
        <li><a href="/c-panel"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#" class="active">Categories</a></li>
    </ol>
    <br/>
    <a class="btn btn-success" href="{{route('cpanel.categories.create')}}">
        <i class=" fa fa-plus-circle"></i>
        Create New Category
    </a>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('content')
    @isset ($msg)
        <div class="alert alert-success">
            <p>{{ $msg }}</p>
        </div>
    @endisset
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover" id="data-table">
            <thead>
            <tr>
                <th>Category</th>
                <th>Date</th>
                <th>-</th>
            </tr>
            </thead>
        </table>
    </div>
@stop
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"
            integrity="sha256-rXnOfjTRp4iAm7hTAxEz3irkXzwZrElV2uRsdJAYjC4=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function () {
            $('#data-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{ route('cpanel.svs_categories') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{csrf_token()}}"}
                },
                "columns": [
                    {"data": "category_name"},
                    {"data": "created_at"},
                    {"data": "options"}
                ],
                "drawCallback": function () {
                    $("img.lazyload").lazyload();
                }
            });
            $("img.lazyload").lazyload({
                threshold: 200
            });
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('click', 'a.jquery-postback', function (e) {
            e.preventDefault(); // does not go through with the link.

            var $this = $(this), name = $(this).data('name'), id = $(this).data('id');
            $.post({
                type: $this.data('method'),
                url: $this.attr('href')
            }).done(function (data) {
                alert('Success remove: ' + name);
                $("#" + id).hide();
            });
        });
    </script>
@endsection
