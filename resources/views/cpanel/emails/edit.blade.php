@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Edit: {{$editor['email']}}</h1>
@stop

@section('content')
    <div class="col-md-offset-2 col-md-8">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ route('cpanel.emails.update',['id' =>  $editor['_id'] ]) }}" method="post">
                {!! Form::token(); !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" name="email" id="email"
                               value="{{$editor['email']}}"
                               placeholder="Enter Coupon Name Ex:30% Off Branded Gifts" disabled>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
@stop
