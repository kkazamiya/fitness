@extends('adminlte::page')
@section('title', 'AdminLTE')
@section('content_header')
    <h1>Send Email: Mailgun</h1>
@stop

@section('content')
    <div class="col-12">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form action="/c-panel/email-send" method="POST" class="needs-validation" id="form-email" novalidate>
                @csrf
                <div class="box-body" id="main_content">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="list_email">Email Or List Emails</label>
                            <input type="text" class="form-control" id="list_email" name="list_email" value=""
                                   placeholder="Ex: contact@fitnessol.com, admin@website.com, president@america.com"/>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="mailing_list">Mailing list</label>
                            <input type="email" class="form-control" id="mailing_list" name="mailing_list" value=""
                                   placeholder="Type Name List"/>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="cc">cc</label>
                            <input type="email" id="cc" name="cc" class="form-control" value="contact@fitnessol.com"
                                   placeholder="CC"/>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="bcc">bcc</label>
                            <input type="email" id="bcc" name="bcc" class="form-control" value=""
                                   placeholder="bcc"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-group">
                            <label for="send_from">From</label>
                            <input type="text" id="send_from" name="send_from" class="form-control"
                                   value="fitnessol <contact@fitnessol.com>"
                                   placeholder="Ex: XOO Partner <partner@mail.fitnessol.com>" required/>
                        </div>
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input type="text" id="subject" name="subject" class="form-control" value=""
                                   placeholder="Subject Email" required/>
                        </div>
                        <div class="form-group">
                            <label for="o_tag">Tags</label>
                            <input type="text" id="o_tag" name="o_tag" class="form-control" value=""
                                   placeholder="Ex: Coupons, Deals, Promos"/>
                        </div>
                        <div class="form-group">
                            <label for="template">Template</label>
                            <input type="text" id="template" name="template" class="form-control" value=""
                                   placeholder="Template Name"/>
                        </div>
                        <div class="form-group">
                            <label for="text_content">Or TEXT</label>
                            <textarea class="form-control" id="text_content" name="text_content" rows="5"
                                      placeholder="Enter ..."></textarea>
                        </div>
                        <div class="form-group">
                            <label for="html_content">Or HTML</label>
                            <textarea class="form-control" id="html_content" name="html_content" rows="5"
                                      placeholder="Enter ..."></textarea>
                        </div>
                        <div class="form-group">
                            <label for="attachment">Attachment</label>
                            <input type="text" id="attachment" name="attachment" class="form-control" value=""
                                   placeholder="Uploaded file link in FileManager"/>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-block" id="send_submit">Send Email</button>
                    <div id="wait-send"></div>
                    <button class="btn btn-danger" id="cancel-submit" style="display: none">Cancel</button>
                </div>
            </form>
        </div>
    </div>
@stop
@section('js')
    <script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
    <script>
        let delay_submit;
        $('#cancel-submit').click(function () {
            clearInterval(delay_submit);
            $('#cancel-submit').hide();
            $('#alert-msg').hide();
        })
        $('#form-email').submit(function (event) {
            var form = this;
            $('#cancel-submit').show();
            event.preventDefault();
            var counter = 30;
            delay_submit = setInterval(function () {
                $('#wait-send').html('<p class="alert alert-danger" id="alert-msg">Please check all content before sending, In ' + counter + ' seconds the mail will automatically send</p>')
                counter--;
                if (counter === 0) {
                    form.submit();
                }
            }, 1000);
        });
        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Get the forms we want to add validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
        var domain = "/c-panel/file-manager";
        $('#lfm').filemanager('image', {prefix: domain});
        var options = {
            filebrowserImageBrowseUrl: '/c-panel/file-manager?type=Images',
            filebrowserImageUploadUrl: '/c-panel/file-manager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/c-panel/file-manager?type=Files',
            filebrowserUploadUrl: '/c-panel/file-manager/upload?type=Files&_token='
        };

        $(document).ready(function () {
            CKEDITOR.replace('html_content', options);
        });
    </script>
@endsection
