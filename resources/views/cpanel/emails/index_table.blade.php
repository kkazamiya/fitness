@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><i class="fa fa-tag"></i> List Coupon</h1>
    <ol class="breadcrumb">
        <li><a href="/cms"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#" class="active">Emails</a></li>
    </ol>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('content')
    @isset ($success)
        <div class="alert alert-success">
            <p>{{ $success }}</p>
        </div>
    @endisset
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover" id="data-table">
            <thead>
            <tr>
                <th>Email</th>
                <th>Create & Update</th>
                <th>-</th>
            </tr>
            </thead>
        </table>
    </div>
@stop
@section('js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#data-table').DataTable({
                "processing": true,
                "serverSide": true,
                "rowId": '_id',
                "ajax": {
                    "url": "{{ route('cpanel.emails.show_table') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":  {_token: "{{csrf_token()}}"}
                },
                "columns": [
                    {"data": "email"},
                    {"data": "created_at"},
                    {"data": "options"}
                ],
                "drawCallback": function () {
                    //$("img.lazyload").lazyload();
                }
            });
        });

        $(document).on('click', 'a.jquery-postback', function (e) {
            e.preventDefault(); // does not go through with the link.
            let $this = $(this);
            let _name = $this.data('name');
            let _id = $this.data('id');
            let r = confirm("Are you sure to delete: " + _name);
            if (r === true) {
                $.post({
                    type: $this.data('method'),
                    url: $this.attr('href')
                }).done(function (data) {
                    alert('Success remove: ' + _name);
                    $("#" + _id).hide();
                });
            }

        });
    </script>
@endsection
