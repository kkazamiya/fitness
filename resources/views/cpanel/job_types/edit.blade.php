@extends('adminlte::page')

@section('title', 'Add Information')

@section('content_header')
    <h1>Edit: {!! $edit['title'] !!}</h1>
    @if (Session::has('message'))
        <div class="alert alert-success">{!! Session::get('message') !!}</div>
    @endif
@stop
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-multiselect.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/css/autosave.min.css')}}"/>
<style>
    .multiselect-item {
        overflow: hidden;
        max-width: 350px;
    }
</style>
@section('content')
    <div class="row">
        <div class="box box-primary">
            <div class="box-body">
                <form action="{{ route('cpanel.posts.update',['id' =>  $edit['_id'] ]) }}" method="post">
                    {!! Form::token(); !!}
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" name="title" id="title"
                                   value="{!! $edit['title'] !!}"
                                   placeholder="Input Title Information... With Page Input Page Name">
                        </div>
                        <div class="form-group">
                            <label for="excerpt">Excerpt (limit 160 char)</label>
                            <input type="text" class="form-control" name="excerpt" id="excerpt"
                                   value="{!! $edit['excerpt'] !!}" placeholder="Excerpt">
                        </div>
                        <div class="form-group">
                            <label for="go_affiliate">Direct Go Affiliate</label>
                            <input type="text" class="form-control" name="go_affiliate" id="go_affiliate"
                                   value="{{$edit['go_affiliate']}}" placeholder="Input URL to Affiliate">
                        </div>
                        <div class="form-group">
                            <label for="content">Rich Content</label>
                            <textarea class="form-control" id="content" name="content" rows="5"
                                      placeholder="Enter ...">
                                {!! $edit['content'] !!}
                            </textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="radio-inline"><input type="radio" name="status" value="publish"
                                                           @if($edit['status'] ==='publish') checked @endif>Publish</label>
                        <label class="radio-inline"><input type="radio" name="status" value="draft"
                                                           @if($edit['status'] ==='draft') checked @endif>Draft</label>
                        <div class="form-group">
                            <label for="type">Type</label>
                            <select class="form-control" name="type" id="type">
                                <option value="post" @if($edit['type'] ==='post') checked @endif>/Post/</option>
                                <option value="infor" @if($edit['type'] ==='infor') checked @endif>/Information/
                                </option>
                                <option value="event" @if($edit['type'] ==='event') checked @endif>/Event/</option>
                                <option value="ads" @if($edit['type'] ==='ads') checked @endif>/Link Ads/</option>
                                <option value="page" @if($edit['type'] ==='page') checked @endif>--Page--</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="slug">Slug</label>
                            <input name="slug" id="slug" class="form-control" type="text" value="{{$edit['slug']}}"
                                   disabled/>
                        </div>
                        <div class="form-group">
                            <label for="categories">Categories</label>
                            <select id="categories" class="form-control" name="categories[]" multiple="multiple">
                                @foreach($categories as $category)
                                    @if(is_array($edit['categories']) && $edit['categories'] != null)
                                        <option value="{{$category['_id']}}"
                                                @if(in_array($category['_id'], $edit['categories'])) selected @endif>{!! $category['name'] !!}</option>
                                    @else
                                        <option value="{{$category['_id']}}">{!! $category['name'] !!}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="thumbnail">Thumbnail</label>
                            <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Browse
                     </a>
                    </span>
                                <input id="thumbnail" class="form-control" type="text" name="featured_img"/>
                            </div>
                            <img id="holder" width="100%"/>
                        </div>
                        <div class="form-group">
                            <label for="tags">Tags</label>
                            <select id="tags" class="form-control" name="tags[]" multiple="multiple">
                                @foreach($tags as $tag)
                                    <option value="{{$tag['_id']}}"
                                            @if(is_array($edit['categories']) && in_array($category['_id'], $edit['categories'])) selected @endif>{!! $tag['name'] !!}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="new_tags">Extend new tags</label>
                            <input id="new_tags" name="new_tags" class="form-control" value=""
                                   placeholder="Ex: Best buy, Buy one..."/>
                        </div>
                        <button type="submit" class="btn btn-block btn-primary">Update</button>
                    </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

@stop
@section('js')
    <script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-multiselect.js')}}"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
    <script>
        $('#categories, #tags').multiselect({
            buttonWidth: '100%',
            maxHeight: 300,
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true
        });
        var domain = "/c-panel/file-manager";
        $('#lfm').filemanager('image', {prefix: domain});
        var options = {
            filebrowserImageBrowseUrl: '/c-panel/file-manager?type=Images',
            filebrowserImageUploadUrl: '/c-panel/file-manager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/c-panel/file-manager?type=Files',
            filebrowserUploadUrl: '/c-panel/file-manager/upload?type=Files&_token=',
        };

        $(document).ready(function () {
            CKEDITOR.replace('content', options);
        });
    </script>
@endsection
