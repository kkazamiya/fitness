@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Add New Keyword</h1>
@stop

@section('content')
    <div class="col-lg-offset-2 col-md-8">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'cpanel.keywords.store']) !!}
            {!! Form::token(); !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="list_keywords">List Keywords</label>
                    <textarea class="form-control" id="list_keywords" name="list_keywords" rows="5"
                              placeholder="Per Line Per Keyword..."></textarea>
                </div>
                <div class="form-group">
                    <label for="keyword_ads">Ads Will Show</label>
                    <input type="text" class="form-control" name="keyword_ads" id="keyword_ads"
                           placeholder="Ads List Name Ex: ads-1, ads-2">
                </div>
                <div class="form-group">
                    <label for="keyword_couponupto">Connect Store Upto</label>
                    <input type="text" class="form-control" name="keyword_couponupto" id="keyword_couponupto"
                           placeholder="udemy, goddady...">
                </div>
                <div class="form-group">
                    <label for="connect_other">Connect Other (Get Title & Description)</label>
                    <textarea class="form-control" id="connect_other" name="connect_other" rows="5"
                              placeholder="Per Line Per URL..."></textarea>
                </div>
                <div class="form-group">
                    <label>Of Category</label>
                    <select multiple="" class="form-control" name="list_cat[]">
                        @foreach($categories as $category)
                            <option value="{{$category['_id']}}">{{$category['name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-block">Create</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('js')
@endsection
