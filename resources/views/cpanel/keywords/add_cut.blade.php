@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Add Connect Upto By Store</h1>
@stop
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content')
    @isset($affected)
        <p class="label label-success">Success affected: {{$affected}} rows</p>
    @endisset
    <div class="col-md-6 col-lg-offset-3">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'cpanel.add_connect', 'enctype' =>'multipart/form-data']) !!}
            {!! Form::token(); !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="url_cut">Slug Store In CouponUpto</label>
                    <input class="form-control" type="text" name="url_cut" id="url_cut">
                </div>
                <div class="form-group">
                    <label for="keyword_contain">Keyword Contain</label>
                    <input class="form-control" type="text" name="keyword_contain" id="keyword_contain">
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <input class="btn btn-primary" type="submit" name="submit" value="Do Connect">
            </div>
            {!! Form::close() !!}
        </div>
        <div class="box box-success" style="max-height: 400px; overflow: auto;">
            <ul class="list-group-item" id="result_msg"></ul>
        </div>
    </div>
@stop
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.15.1/xlsx.core.min.js"
            integrity="sha256-NEmjetUCzF/mJX6Ztvx6h+WG2j+A8LX85axUiZWWMK4=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function () {
            console.log();
            var oFileIn;

            $(function () {
                oFileIn = document.getElementById('file_stores');
                if (oFileIn.addEventListener) {
                    oFileIn.addEventListener('change', filePicked, false);
                }
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            function saveStores(keyword_add) {
                $.post("/c-panel/add-kw-excel", keyword_add).done(function (data) {
                    if (data.code === 1) {
                        $("#result_msg").prepend("<li class='text-success'>" + keyword_add + "</li>");
                    } else {
                        $("#result_msg").prepend("<li class='text-danger'>" + data.msg + "</li>");
                    }
                });
            }

            function filePicked(oEvent) {
                // Get The File From The Input
                var oFile = oEvent.target.files[0];
                var sFilename = oFile.name;
                // Create A File Reader HTML5
                var reader = new FileReader();
                var delay_s = 0;
                // Ready The Event For When A File Gets Selected
                reader.onload = function (e) {
                    var data = e.target.result;
                    var cfb = XLS.CFB.read(data, {type: 'binary'});
                    var wb = XLS.parse_xlscfb(cfb);
                    // Loop Over Each Sheet
                    wb.SheetNames.forEach(function (sheetName) {
                        // Obtain The Current Row As CSV
                        var sCSV = XLS.utils.make_csv(wb.Sheets[sheetName]);
                        var data = XLS.utils.sheet_to_json(wb.Sheets[sheetName], {header: 1});
                        $.each(data, function (indexR, valueR) {
                            if (indexR > 0) {
                                if (typeof data[indexR][0] !== "undefined") {
                                    var keyword_add = {};
                                    keyword_add.keyword = data[indexR][0];
                                    keyword_add.ads = data[indexR][1];
                                    keyword_add.couponupto = data[indexR][2] || "";
                                    keyword_add.other_urls = data[indexR][3] || "";
                                    keyword_add.category = data[indexR][4] || "";
                                    delay_s = delay_s + 50;
                                    setTimeout(function () {
                                        saveStores(keyword_add);
                                    }, delay_s);
                                }
                            }
                        });
                    });
                };

                // Tell JS To Start Reading The File.. You could delay this if desired
                reader.readAsBinaryString(oFile);
            }
        });
    </script>
@endsection
