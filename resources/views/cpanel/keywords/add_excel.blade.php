@extends('adminlte::page')
 
@section('title', 'AdminLTE')

@section('content_header')
    <h1>Add Keyword By Excel File Or TXT</h1>
@stop
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content')
    <p>
        Total:<span class="label label-info" id="total-on-file"></span>
        Success:<span class="label label-success" id="count-success"></span>
        Fail:<span class="label label-danger" id="count-fail"></span>
    </p>
    <div class="col-md-6">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'cpanel.add_kw_excel', 'enctype' =>'multipart/form-data']) !!}
            {!! Form::token(); !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="file_stores">File Excel Upload</label>
                    <input class="form-control" type="file" name="file_stores" id="file_stores">
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer"></div>
            {!! Form::close() !!}
        </div>
        <div class="box box-success" style="max-height: 400px; overflow: auto;">
            <ul class="list-group-item" id="result_msg"></ul>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-danger">
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <div class="form-group">
                    <label for="file_stores">Url TXT Keywords</label>
                    <input class="form-control" type="text" name="keywords_url" id="keywords_url" required="true">
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button class="btn btn-block btn-danger" id="start">Start</button>
            </div>
        </div>
        <div class="box box-success" style="max-height: 400px; overflow: auto;">
            <ul class="list-group-item" id="result_txt"></ul>
        </div>
    </div>
@stop
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.15.1/xlsx.core.min.js"
            integrity="sha256-NEmjetUCzF/mJX6Ztvx6h+WG2j+A8LX85axUiZWWMK4=" crossorigin="anonymous"></script>
    <script>
        let getLocation = function (href) {
            let l = document.createElement("a");
            l.href = href;
            return l;
        };

        function validURL(str) {
            let pattern = new RegExp('^(https?:\\/\\/)?' + // protocol
                '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
                '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
                '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
                '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
                '(\\#[-a-z\\d_]*)?$', 'i'); // fragment locator
            return !!pattern.test(str);
        }

        let count_s = 0, count_f = 0;

        function add_keywords(data_send) {
            $.post("/c-panel/add-excel", data_send).done(function (data) {
                if (data.code === 99) {
                    console.log("Error:");
                    $("#count-fail").text(count_f++);
                    $("#result_txt").prepend("<li class='text-danger'>" + data.msg + "</li>");
                } else if (data.code === 2) {
                    $("#result_txt").prepend("All DONE =================================");
                } else {
                    let html = "<p class='bg-dark'>Total keywords on file:" + data.total + "<br/>";
                    html += "From line:" + data.pos + " to line " + (data.pos + data.limit) + "<br/>";
                    html += "Exits:" + data.total_exits + "<br/>";
                    html += "New:" + data.total_to_new + "<br/></p>";
                    $("#result_txt").prepend(html);
                    if (!isNaN(data.total_to_new) && !isNaN(data.total_exits)) {
                        count_s = count_s + parseInt(data.total_to_new);
                        count_f = count_f + parseInt(data.total_exits);
                    }
                    $("#count-fail").text(count_f);
                    $("#count-success").text(count_s);
                    $("#total-on-file").text(data.total);
                    data_send.pos = data_send.pos + data_send.limit;
                    add_keywords(data_send);
                }
            });
        }

        $(document).ready(function () {
            let this_url = new URL(window.location.href);
            $("#start").click(function () {
                let keywords_url = $("#keywords_url").val();
                let parseURL = new URL(keywords_url);
                console.log(parseURL);
                if (parseURL.hostname === this_url.hostname && parseURL.pathname.indexOf('keywords') > -1) {
                    add_keywords({file_url: encodeURIComponent(keywords_url), pos: 0, limit: 200});
                } else {
                    $("#result_txt").prepend("<li class='text-danger'>" + keywords_url + " is not Trust URL</li>");
                }
            });
            let oFileIn;
            $(function () {
                oFileIn = document.getElementById('file_stores');
                if (oFileIn.addEventListener) {
                    oFileIn.addEventListener('change', filePicked, false);
                }
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            function saveStores(keyword_add) {
                $.post("/c-panel/add-excel", keyword_add).done(function (data) {
                    if (data.code === 1) {
                        $("#count-success").text(count_s++);
                        $("#result_msg").prepend("<li class='text-success'>" + keyword_add.keyword + "</li>");
                    } else {
                        console.log("Error:");
                        console.log(data);
                        $("#count-fail").text(count_f++);
                        $("#result_msg").prepend("<li class='text-danger'>" + data.msg + "</li>");
                    }
                });
            }

            function filePicked(oEvent) {
                // Get The File From The Input
                let oFile = oEvent.target.files[0];
                let sFilename = oFile.name;
                // Create A File Reader HTML5
                let reader = new FileReader();
                let delay_s = 0;
                // Ready The Event For When A File Gets Selected
                reader.onload = function (e) {
                    let data = e.target.result;
                    console.log(data);
                    let cfb = XLS.CFB.read(data, {type: 'binary'});
                    let wb = XLS.parse_xlscfb(cfb);
                    // Loop Over Each Sheet
                    wb.SheetNames.forEach(function (sheetName) {
                        // Obtain The Current Row As CSV
                        let sCSV = XLS.utils.make_csv(wb.Sheets[sheetName]);
                        let data = XLS.utils.sheet_to_json(wb.Sheets[sheetName], {header: 1});
                        $.each(data, function (indexR, valueR) {
                            if (indexR > 0) {
                                if (typeof data[indexR][0] !== "undefined") {
                                    let keyword_add = {};
                                    keyword_add.keyword = data[indexR][0];
                                    keyword_add.ads = data[indexR][1];
                                    keyword_add.couponupto = data[indexR][2] || "";
                                    keyword_add.other_urls = data[indexR][3] || "";
                                    keyword_add.category = data[indexR][4] || "";
                                    delay_s = delay_s + 100;
                                    setTimeout(function () {
                                        saveStores(keyword_add);
                                    }, delay_s);
                                }
                            }
                        });

                    });
                };
                // Tell JS To Start Reading The File.. You could delay this if desired
                reader.readAsBinaryString(oFile);
            }
        });
    </script>
@endsection
