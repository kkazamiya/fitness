@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Add Keyword By Excel File</h1>
@stop
<meta name="csrf-token" content="{{ csrf_token() }}">
@section('content')
    <div class="col-md-6 col-lg-offset-3">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'cpanel.upload_txt', 'enctype' =>'multipart/form-data']) !!}
            {!! Form::token(); !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="file_stores">File TXT Upload</label>
                    <input type="file" name="file_txt" id="file_txt" required="true">
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer"></div>
            {!! Form::close() !!}
        </div>
        <div class="box box-success" style="max-height: 400px; overflow: auto;">
            <p>
                Success:<span class="label label-success" id="count-success"></span>
                Fail:<span class="label label-danger" id="count-fail"></span>
            </p>
            <ul class="list-group-item" id="result_msg"></ul>
        </div>
    </div>
@stop
