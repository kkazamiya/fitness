@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Edit: {{$editor['keyword']}}</h1>
@stop

@section('content')
    <div class="col-lg-offset-2 col-md-8">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ route('cpanel.keywords.update',['id' =>  $editor['_id'] ]) }}" method="post">
                {!! Form::token(); !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="keyword">Keyword</label>
                        <input type="text" class="form-control" name="keyword" id="keyword"
                               value="{{$editor['keyword']}}" disabled/>
                    </div>
                    <div class="form-group">
                        <label for="keyword_ads">Ads Will Show</label>
                        <input type="text" class="form-control" name="keyword_ads" id="keyword_ads"
                               value="{{$editor['keyword_ads']}}"/>
                    </div>
                    <div class="form-group">
                        <label for="keyword_couponupto">Connect Store Upto</label>
                        <input type="text" class="form-control" name="keyword_couponupto" id="keyword_couponupto"
                               placeholder="udemy, goddady..." value="{{$editor['keyword_couponupto']}}">
                    </div>
                    <div class="form-group">
                        <label for="connect_other">Connect Other (Get Title & Description)</label>
                        <textarea class="form-control" id="connect_other" name="connect_other" rows="5"
                                  placeholder="Per Line Per URL..."></textarea>
                    </div>
                    <div class="form-group">
                        <label>Of Category</label>
                        <select multiple="" class="form-control" name="list_cat[]">
                            @foreach($categories as $category)
                                @if(in_array($category['_id'], explode(",",$editor['keyword_cat'])))
                                    <option value="{{$category['_id']}}" selected>
                                        {{$category['category_name']}}
                                    </option>';
                                @else
                                    <option value="{{$category['_id']}}">
                                        {{$category['category_name']}}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Rich content</label>
                        <textarea name="rich_content" id="rich_content" class="form-control" rows="5">@if(isset($editor['rich_content'])&& $editor['rich_content'] != ''){!! $editor['rich_content'] !!} @endif</textarea>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-warning btn-block">Update</button>
                </div>
            </form>
        </div>
    </div>

@stop
@section('js')
<script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
<script>
    var options = {
            filebrowserImageBrowseUrl: '/c-panel/file-manager?type=Images',
            filebrowserImageUploadUrl: '/c-panel/file-manager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/c-panel/file-manager?type=Files',
            filebrowserUploadUrl: '/c-panel/file-manager/upload?type=Files&_token=',
        };
    $(document).ready(function () {
        CKEDITOR.replace('rich_content', options);
    });
</script>
@endsection
