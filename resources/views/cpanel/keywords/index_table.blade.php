@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><i class="fa fa-shopping-cart"></i> List Keywords</h1>
    <ol class="breadcrumb">
        <li><a href="/c-panel"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#" class="active">Stores</a></li>
    </ol>
    <br/>
    <a class="btn btn-success" href="{{route('cpanel.keywords.create')}}">
        <i class=" fa fa-plus-circle"></i>
        Add List Keywords
    </a>
    <a class="btn btn-danger" href="/c-panel/add-kw-excel">
        <i class=" fa fa-plus-circle"></i>
        Add By Excel 2003
    </a>
    <a class="btn btn-warning" href="{{route('cpanel.run_bot_keywords')}}">
        <i class=" fa fa-spider"></i>
        Run Spider
    </a>
    <a class="btn btn-warning" href="{{route('cpanel.bot_multi')}}">
        <i class=" fa fa-spider"></i>
        Multi Threads Spider
    </a>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('content')
    @isset ($msg)
        <div class="alert alert-success">
            <p>{{ $msg }}</p>
        </div>
    @endisset
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover" id="data-table">
            <thead>
            <tr>
                <th>Keyword</th>
                <th>List Ads</th>
                <th>Categories</th>
                <th>Date</th>
                <th>-</th>
            </tr>
            </thead>
        </table>
    </div>
@stop
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.min.js"
            integrity="sha256-rXnOfjTRp4iAm7hTAxEz3irkXzwZrElV2uRsdJAYjC4=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function () {
            $('#data-table').DataTable({
                "processing": true,
                "serverSide": true,
                "rowId": '_id',
                "ajax": {
                    "url": "{{ route('cpanel.svs_keywords') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{csrf_token()}}"}
                },
                "columns": [
                    {"data": "keyword"},
                    {"data": "keyword_ads"},
                    {"data": "keyword_cat"},
                    {"data": "created_at"},
                    {"data": "options"}
                ],
                "drawCallback": function () {
                    $("img.lazyload").lazyload();
                }
            });
            $("img.lazyload").lazyload({
                threshold: 200
            });
        });
      
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(document).on('click', 'a.jquery-postback', function (e) {
            e.preventDefault(); // does not go through with the link.

            var $this = $(this), name = $(this).data('name'), id = $(this).data('id');
            $.post({
                type: $this.data('method'),
                url: $this.attr('href')
            }).done(function (data) {
                alert('Success remove: ' + name);
                $("#" + id).hide();
            });
        });
        $(document).on('click', 'a.jquery-tracking', function (e) {
            e.preventDefault(); // does not go through with the link.
            $(this).html("<span class='label label-danger'><i class='fa fa-yin-yang fa-spin'></i></span>");
            var $this = $(this), name = $(this).data('name'), id = $(this).data('id');
            $.post({
                type: $this.data('method'),
                url: $this.attr('href')
            }).done(function (data) {
                $this.html("<span class='label label-success'><i class='fa fa-check-circle'></i></span>");
                if (data.code === 1) {
                    alert('Success Scraper Bing\nKeyword: ' + data.keyword
                        + "\nResults:" + data.results_info.added
                        + "\nBest Offers:" + data.results_info.best_offers
                        + "\nKeywords Related:" + JSON.stringify(data.results_info.k_related)
                    );
                } else {
                    alert('Error: ' + data.msg);
                }
            });
        });
    </script>
@endsection
