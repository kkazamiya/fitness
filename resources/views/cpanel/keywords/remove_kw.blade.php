@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Delete Keywords</h1>
@stop
@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
@endsection
@section('content')
    <div class="col-md-6 col-lg-offset-3">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'cpanel.post-remove-keyword', 'enctype' =>'multipart/form-data']) !!}
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label for="keyword_slug">Keyword Slug: Remove This Keyword & All Results Of This</label>
                    <input name="keyword_slug" value="" id="keyword_slug" class="form-control"
                           placeholder="Type Keyword Slug"/>
                </div>
                <div class="form-group">
                    <label for="of_domain">Domain: Remove All Results Of Domain On Data</label>
                    <input name="of_domain" value="" id="of_domain" class="form-control"
                           placeholder="Type Domain Or Link"/>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Remove This</button>
            </div>
            {!! Form::close() !!}
        </div>
        <div class="box box-success">
            <ul class="list-group-item" id="result_msg"></ul>
        </div>
    </div>
@stop
@section('js')
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <script>
        $(document).ready(function () {
            //Date range picker
            //$('#reservation').daterangepicker();
            //Date range picker with time picker
            $('#daterange-btn').daterangepicker(
                {
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                    $("#date_range").val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
            );
            /*
            */
        });
    </script>
@endsection
