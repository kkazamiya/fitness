@extends('adminlte::page')

@section('title', 'Add Information')
@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/dateTimePicker/dist/css/jquery.datetimepicker.min.css') }}">
@endsection

@section('content_header')
    <h1>Add Post</h1>
    @if (Session::has('message'))
        <div class="alert alert-success">{!! Session::get('message') !!}</div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger">{!! Session::get('error') !!}</div>
    @endif
@stop
<link rel="stylesheet" href="{{asset('assets/css/bootstrap-multiselect.css')}}"/>
<link rel="stylesheet" href="{{asset('assets/css/autosave.min.css')}}"/>
<style>
    .multiselect-item {
        overflow: hidden;
        max-width: 350px;
    }
</style>
@section('content')
    <div class="row">
        <div class="box box-primary">
            <div class="box-body">
                {!! Form::open(['route' => 'cpanel.posts.store']) !!}
                {!! Form::token(); !!}
                <div class="col-md-8">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title" id="title"
                               placeholder="Input Title Information... With Page Input Page Name" onkeyup="ChangeToSlug()">
                    </div>
                    <div class="form-group">
                        <label for="excerpt">Excerpt (limit 160 char)</label>
                        <input type="text" class="form-control" name="excerpt" id="excerpt"
                               placeholder="Excerpt">
                    </div>
                    <div class="form-group">
                        <label for="go_aff">Direct Go Affiliate</label>
                        <input type="text" class="form-control" name="go_aff" id="go_aff"
                               placeholder="Input URL to Affiliate">
                    </div>
                    <div class="form-group">
                        <label for="content">Rich Content</label>
                        <textarea class="form-control" id="content" name="content" rows="5"
                                  placeholder="Enter ..."></textarea>
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="radio-inline"><input type="radio" name="status" value="1" checked onclick="hideDateTime()">Publish</label>
                    <label class="radio-inline"><input type="radio" name="status" value="0" onclick="hideDateTime()">Draft</label>
                    <label class="radio-inline"><input type="radio" name="status" value="2" onclick="showDateTime()">Schedule</label>
                    <div class="form-group" id="datetime" style="display: none">
                        <label for="schedule">Schedule post</label>
                        <p class="font-italic small">Set Time America/Los_Angeles .</p>
                        <p class="font-italic small">Now <?php
                            date_default_timezone_set('America/Los_Angeles');
                            $now = time();
                            echo now();
                            ?> .</p>
                        <input id="datetimepicker" type="text" name="schedule" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="type">Type</label>
                        <select class="form-control" name="type" id="type">
                            <option value="post" selected>/Post/</option>
                            <option value="infor">/Information/</option>
                            <option value="event">/Event/</option>
                            <option value="company">/Company/</option>
                            <option value="page">--Page--</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="slug">Slug</label>
                        <input name="slug" id="slug" class="form-control" type="text" value="" placeholder=""/>
                    </div>
                    <div class="form-group">
                        <label for="categories">Categories</label>
                        <select id="categories" class="form-control" name="categories[]" multiple="multiple">
                            @foreach($categories as $category)
                                <option value="{{$category['_id']}}">{!! $category['name'] !!}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="thumbnail">Thumbnail</label>
                        <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Browse
                     </a>
                    </span>
                            <input id="thumbnail" class="form-control" type="text" name="featured_img"/>
                        </div>
                        <img id="holder" width="100%"/>
                    </div>
                    <div class="form-group">
                        <label for="tags">Tags</label>
                        <select id="tags" class="form-control" name="tags[]" multiple="multiple">
                            @foreach($tags as $tag)
                                <option value="{{$tag['_id']}}">{!! $tag['name'] !!}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="new_tags">Extend new tags</label>
                        <input id="new_tags" name="new_tags" class="form-control" value=""
                               placeholder="Ex: Best buy, Buy one..."/>
                    </div>
                    <button type="submit" class="btn btn-block btn-primary">Create</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

@stop
@section('js')

    <script src="{{asset('vendor/adminlte/vendor/dateTimePicker/dist/js/jquery.datetimepicker.full.js')}}"></script>
    <script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap-multiselect.js')}}"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
    <script>
        $('#categories, #tags').multiselect({
            buttonWidth: '100%',
            maxHeight: 300,
            includeSelectAllOption: true,
            enableCaseInsensitiveFiltering: true
        });
        var domain = "/c-panel/file-manager";
        $('#lfm').filemanager('image', {prefix: domain});
        var options = {
            filebrowserImageBrowseUrl: '/c-panel/file-manager?type=Images',
            filebrowserImageUploadUrl: '/c-panel/file-manager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/c-panel/file-manager?type=Files',
            filebrowserUploadUrl: '/c-panel/file-manager/upload?type=Files&_token=',
        };

        $(document).ready(function () {
            CKEDITOR.replace('content', options);
        });
        function showDateTime(){
            document.getElementById('datetime').style.display ='block';
        }
        function hideDateTime(){
            document.getElementById('datetime').style.display ='none';
        }
        function ChangeToSlug()
          {
            var title, slug;
         
            //Lấy text từ thẻ input title 
            title = document.getElementById("title").value;
         
            //Đổi chữ hoa thành chữ thường
            slug = title.toLowerCase();
         
            //Đổi ký tự có dấu thành không dấu
            slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
            slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
            slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
            slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
            slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
            slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
            slug = slug.replace(/đ/gi, 'd');
            //Xóa các ký tự đặt biệt
            slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
            //Đổi khoảng trắng thành ký tự gạch ngang
            slug = slug.replace(/ /gi, "-");
            //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
            //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
            slug = slug.replace(/\-\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-\-/gi, '-');
            slug = slug.replace(/\-\-\-/gi, '-');
            slug = slug.replace(/\-\-/gi, '-');
            //Xóa các ký tự gạch ngang ở đầu và cuối
            slug = '@' + slug + '@';
            slug = slug.replace(/\@\-|\-\@|\@/gi, '');
            //In slug ra textbox có id “slug”
            document.getElementById('slug').value = slug;
          }
        jQuery('#datetimepicker').datetimepicker();
    </script>

@endsection
