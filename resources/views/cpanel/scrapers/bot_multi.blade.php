@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><i class="fa fa-shopping-cart"></i> List Keywords</h1>
    <ol class="breadcrumb">
        <li><a href="/c-panel"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#" class="active">Stores</a></li>
    </ol>
    <br/>
    <a class="btn btn-success" href="{{route('cpanel.keywords.create')}}">
        <i class=" fa fa-plus-circle"></i>
        Add List Keywords
    </a>
    <a class="btn btn-success" href="{{route('cpanel.run_bot_keywords')}}">
        <i class=" fa fa-plus-circle"></i>
        Run Bot
    </a>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="node-api" content="{{ env('NODE_API') }}">
@stop

@section('content')
    @isset ($msg)
        <div class="alert alert-success">
            <p>{{ $msg }}</p>
        </div>
    @endisset
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="n_thread">Threads</label>
                        <input class="form-control" value="10" step="5" max="50" id="n_thread" name="n_thread"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="search_on">Search On</label>
                        <select id="search_on" name="search_on" class="form-control">
                            <option value="google" selected>Google</option>
                            <option value="bing">Bing (SLOW)</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="time_delay">Time Delays</label>
                        <input class="form-control" value="2" step="1" max="10" id="time_delay" name="time_delay"/>
                    </div>
                </div>
                <div class="btn-group btn-group-justified">
                    <div class="btn-group">
                        <button type="button" class="btn btn-info run-bot" data-sort="new">Run Bot
                        </button>
                    </div>
                </div>
                <p class="text-center">
                    Keywords Exits: <span class="label label-info" id="exits">{{$count_exits}}</span>
                    Keywords Tracked: <span class="label label-success" id="tracked">{{$count_tracked}}</span>
                    Keywords Error: <span class="label label-danger" id="error">{{$count_error}}</span>
                    Bot Running Follow: <span class="label label-default" id="bot_sort">--</span>
                </p>
                <div class="progress" id="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar"
                         aria-valuenow="100"
                         aria-valuemin="0" aria-valuemax="100" style="width: 100%" id="progress-text">...Running...<i
                                class='fas fa-sync fa-spin'></i>
                    </div>
                </div>
                <div class="form-group">
                    <div id="msg_result" class="p-2"
                         style="padding: 10px; height: 400px; overflow: auto; background: #1b1e21; color: #00a65a"></div>
                </div>
            </div>
            <!-- /.box-body -->
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('js')
    <script>
        serialize_param = function (obj) {
            let str = [];
            for (let p in obj)
                if (obj.hasOwnProperty(p)) {
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                }
            return str.join("&");
        };
        const unique = (value, index, self) => {
            return self.indexOf(value) === index
        };
        _resultInfo = async function (results) {
            let result_info = {added: 0, k_related: [], best_offers: 0};
            $.each(results, async function (v) {
                result_info.added = result_info.added + v.added;
                result_info.best_offers = result_info.best_offers + v.best_offers;
                result_info.k_related.concat(v.keywords_related);
            });
            result_info.k_related = await result_info.k_related.filter(unique);
            return result_info;
        };
        $(document).ready(function () {

            $("#progress").hide();
            let threads, time_delay, search_on, runned = 1, sort_by, kw_list_count, delay_send = 0, max_wait,
                token_csrf = $('meta[name="csrf-token"]').attr('content');
            let url_api = $('meta[name="node-api"]').attr('content');

            function _getKeyword(limit) {
                search_on = $("#search_on").val();
                time_delay = $("#time_delay").val();
                $("#progress").show();
                $("#progress-text").html("...Running...<i class='fas fa-sync fa-spin'></i>");
                $("#bot_sort").text(sort_by.upperCase);
                clearTimeout(max_wait);
                $.post("/c-panel/get-keyword", {_token: token_csrf, limit: limit}).done(function (data) {
                    if(threads !== data.length){
                        runned = (threads - data.length) + 1;
                    }
                    max_wait = setTimeout(function () {
                        count_time('google', 5);
                    }, 5000 * threads);
                    delay_send = 0;
                    $.each(data, function (i, v) {
                        if (search_on === 'google') {
                            delay_send = delay_send + 500;
                            setTimeout(function () {
                                _crawlGoogle(v);
                            }, delay_send);
                        } else {
                            _crawlBing(v);
                        }
                    });
                }).fail(function() {
                    /*fail rs*/
                    count_time('google', 5);
                });
            }

            function _add2sitemap(keyword) {
                $.post("/c-panel/add-2-sitemap", {_token: token_csrf, keyword: keyword}).done(function (data) {
                    console.log(data);
                });
            }

            function _crawlGoogle(per_kw) {
                if (isNaN(runned)) {
                    runned = 1;
                }
                let google_params = {
                    q: per_kw.keyword,
                    num: 20,
                    lr: "lang_en",
                    cr: "US",
                    hl: "en",
                };
                let url_request = "https://www.google.com/search?" + serialize_param(google_params);
                $.ajax({
                    beforeSend: function(request) {
                        request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                    },
                    url: url_api,
                    type: 'post',
                    dataType: 'json',
                    timeout: 4000 * threads,//Per threads max wait 10s
                    contentType: 'application/json',
                    data: JSON.stringify({
                        uri_request: encodeURIComponent(url_request),
                        keyword: per_kw.keyword,
                        id_keyword: per_kw._id,
                        find_on: 'google',
                    }),
                    error: function (xhr, status, err) {
                        runned++;
                        console.log("Runned:" + runned + " - Limit:" + threads);
                        if (runned >= threads) {
                            runned = 1;
                            console.log("Next...");
                            count_time('google', 15);
                        }
                        $("#msg_result").prepend(
                            "<p class='label label-danger'>CODE:" + xhr.status + " - MSG: " + xhr.statusText + " - Keyword:" + per_kw.keyword + "</p><br/>");
                        $("#error").text(error++);
                        //return true;
                    },
                    success: function (data) {
                        runned++;
                        console.log("Runned:" + runned + " - Limit:" + threads);
                        if (runned >= threads) {
                            runned = 1;
                            console.log("Next...");
                            count_time('google', time_delay);
                        }
                        $("#exits").text(exits--);
                        if (data.code === 1) {
                            $("#msg_result").prepend("<fieldset>\n" +
                                "  <legend class='text-primary'>Keyword:" + per_kw.keyword + "</legend>\n" +
                                "  Total Result: " + data.added + " - Best Offers:" + data.best_offers + "<br>\n" +
                                "  Keywords Related: " + data.keywords_related.join(",") + "<br/>\n" +
                                "  Go View: <a href='/" + per_kw.keyword_slug + "' target='_blank'>Go View</a>\n" +
                                " </fieldset><br/>");
                            $("#tracked").text(tracked++);
                            _add2sitemap(per_kw);
                        } else if (data.code === 2) {
                            $("#msg_result").prepend("<p class='label label-warning'> Track Duplicate - <a href='/" + per_kw.keyword_slug + "' target='_blank'>View</a></p><br/>");
                            $("#tracked").text(tracked++);
                        } else if (data.code === 3) {
                            $("#msg_result").prepend("<p class='label label-warning'> STOP...</p><br/>");
                            $("#progress-text").html("...STOP...");
                        } else {
                            $("#msg_result").prepend(
                                "====================================================<br/>" +
                                "<p class='label label-danger'>CODE:" + data.code + " - Keyword:" + per_kw.keyword + " - Track Error - Msg:" + JSON.stringify(data) + "</p><br/>" +
                                "====================================================<br/>");
                            $("#error").text(error++);
                            $("#progress").hide();
                        }

                    }
                });
            }

            function _crawlBing(per_kw) {
                let bing_params = {
                    cc: 'US',
                    freshness: 'month', //Day, Week
                    mkt: 'en-US',
                    count: 20,
                    offset: 0, //Page
                    q: per_kw.keyword,
                    promote: "RelatedSearches,SpellSuggestions",
                    safeSearch: false
                };
                let url_request = "https://bing.com/search?" + serialize_param(bing_params);
                $.ajax({
                    url: url_api,
                    type: 'post',
                    dataType: 'json',
                    timeout: 10000 * threads,//Per threads max wait 10s
                    contentType: 'application/json',
                    data: JSON.stringify({
                        uri_request: encodeURIComponent(url_request),
                        keyword: per_kw.keyword,
                        id_keyword: per_kw._id,
                    }),
                    error: function () {
                        runned++;
                        if (runned > threads) {
                            console.log("Next...");
                            count_time('bing', 5);
                        }
                        $("#msg_result").prepend(
                            "====================================================<br/>" +
                            "<p class='label label-danger'>Keyword:" + per_kw.keyword + " - Track Error - Msg: API SLOW OR 403</p><br/>" +
                            "====================================================<br/>");
                        $("#error").text(error++);
                        return true;
                    },
                    success: function (data) {
                        runned++;
                        if (runned > threads) {
                            console.log("Next...");
                            count_time('bing', 5);
                        }
                        $("#exits").text(exits--);
                        if (data.code === 1) {
                            $("#msg_result").prepend("<fieldset>\n" +
                                "  <legend class='text-primary'>Keyword:" + per_kw.keyword + "</legend>\n" +
                                "  Total Result: " + data.added + " - Best Offers:" + data.best_offers + "<br>\n" +
                                "  Keywords Related: " + data.keywords_related.join(",") + "<br/>\n" +
                                "  Go View: <a href='/" + per_kw.keyword_slug + "' target='_blank'>Go View</a>\n" +
                                " </fieldset><br/>");
                            $("#tracked").text(tracked++);
                            _add2sitemap(per_kw);
                        } else if (data.code === 2) {
                            $("#msg_result").prepend("<p class='label label-warning'> Track Duplicate - <a href='/" + per_kw.keyword_slug + "' target='_blank'>View</a></p><br/>");
                            $("#tracked").text(tracked++);
                        } else if (data.code === 3) {
                            $("#msg_result").prepend("<p class='label label-warning'> STOP...</p><br/>");
                            $("#progress-text").html("...STOP...");
                        } else {
                            $("#msg_result").prepend(
                                "====================================================<br/>" +
                                "<p class='label label-danger'>Keyword:" + per_kw.keyword + " - Track Error - Msg:" + data.msg + "</p><br/>" +
                                "====================================================<br/>");
                            $("#error").text(error++);
                            $("#progress").hide();
                        }
                    }
                });
            }

            function get_info_other() {
                $("#progress").show();
                $("#progress-text").html("...Running...<i class='fas fa-sync fa-spin'></i>");
                $("#bot_sort").text(sort_by.upperCase);
                $.post("/c-panel/other-get-info", {sort_by: sort_by}).done(function (data) {
                    $("#exits").text(exits--);
                    if (data.code === 1) {
                        $("#msg_result").prepend("<p>Other:" + data.full_url + "</p>br/>");
                        count_time('other', 2);
                        $("#tracked").text(tracked++);
                    } else if (data.code === 2) {
                        $("#msg_result").prepend("<p class='label label-warning'> Track Duplicate - <a href='/" + data.slug + "' target='_blank'>View</a></p><br/>");
                        count_time('other', 2);
                        $("#tracked").text(tracked++);
                    } else if (data.code === 3) {
                        $("#progress-text").html("...STOP...");
                        $("#msg_result").prepend("<p class='label label-warning'> STOP...</p><br/>");
                    } else {
                        $("#msg_result").prepend(
                            "====================================================<br/>" +
                            "<p class='label label-danger'>CODE:" + data.code + " Other:" + data.full_url + " - Track Error - Msg:" + data.msg + "</p><br/>" +
                            "====================================================<br/>");
                        $("#error").text(error++);
                        $("#progress").hide();
                        count_time('other', 2);
                    }
                });
            }

            function count_time(type = 'keyword', delay = 2) {
                if (isNaN(runned)) {
                    runned = 1;
                }
                let interval = setInterval(function () {
                    delay--;
                    $("#progress-text").text("Wait on: " + delay + "s to continue...");
                    if (delay === 0) {
                        if (type === 'other') {
                            get_info_other();
                        } else {
                            _getKeyword(threads);
                        }
                        clearInterval(interval);
                    }
                }, 1000);
            }

            let exits = parseInt($("#exits").text());
            let tracked = parseInt($("#tracked").text());
            let error = parseInt($("#error").text());

            $(".run-bot").click(function () {
                sort_by = $(this).data('sort');

                if (sort_by === 'other') {
                    get_info_other();
                } else {
                    threads = $("#n_thread").val();
                    _getKeyword(threads);
                }
            });

            $(document).on('click', 'a.jquery-postback', function (e) {
                e.preventDefault(); // does not go through with the link.

                let $this = $(this), name = $(this).data('name'), id = $(this).data('id');
                $.post({
                    type: $this.data('method'),
                    url: $this.attr('href')
                }).done(function (data) {
                    alert('Success remove: ' + name);
                    $("#" + id).hide();
                });
            });
        });
    </script>
@endsection
