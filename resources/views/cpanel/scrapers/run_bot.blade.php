@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><i class="fa fa-shopping-cart"></i> List Keywords</h1>
    <ol class="breadcrumb">
        <li><a href="/c-panel"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#" class="active">Stores</a></li>
    </ol>
    <br/>
    <a class="btn btn-success" href="{{route('cpanel.keywords.create')}}">
        <i class=" fa fa-plus-circle"></i>
        Add List Keywords
    </a>
    <a class="btn btn-success" href="{{route('cpanel.run_bot_keywords')}}">
        <i class=" fa fa-plus-circle"></i>
        Run Bot
    </a>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('content')
    @isset ($msg)
        <div class="alert alert-success">
            <p>{{ $msg }}</p>
        </div>
    @endisset
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <div class="btn-group btn-group-justified">
                    <div class="btn-group">
                        <button type="button" class="btn btn-info run-bot" data-sort="new">Run Bot - New First
                        </button>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-warning run-bot" data-sort="old">Run Bot - Old
                            First
                        </button>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary run-bot" data-sort="view">Run Bot - View
                            High
                        </button>
                    </div>
                    <div class="btn-group">
                        <button type="button" class="btn btn-danger run-bot" data-sort="other">Run Bot Other
                        </button>
                    </div>
                </div>
                <p class="text-center">
                    Keywords Exits: <span class="label label-info" id="exits">{{$count_exits}}</span>
                    Keywords Tracked: <span class="label label-success" id="tracked">{{$count_tracked}}</span>
                    Keywords Error: <span class="label label-danger" id="error">{{$count_error}}</span>
                    Bot Running Follow: <span class="label label-default" id="bot_sort">--</span>
                </p>
                <div class="progress" id="progress">
                    <div class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar"
                         aria-valuenow="100"
                         aria-valuemin="0" aria-valuemax="100" style="width: 100%" id="progress-text">...Running...<i
                                class='fas fa-sync fa-spin'></i>
                    </div>
                </div>
                <div class="form-group">
                    <div id="msg_result" class="p-2"
                         style="padding: 10px; height: 400px; overflow: auto; background: #1b1e21; color: #00a65a"></div>
                </div>
            </div>
            <!-- /.box-body -->
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('js')
    <script>
        serialize_param = function (obj) {
            var str = [];
            for (var p in obj)
                if (obj.hasOwnProperty(p)) {
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                }
            return str.join("&");
        };
        const unique = (value, index, self) => {
            return self.indexOf(value) === index
        };
        _resultInfo = async function (results) {
            let result_info = {added: 0, k_related: [], best_offers: 0};
            $.each(results, async function (v) {
                result_info.added = result_info.added + v.added;
                result_info.best_offers = result_info.best_offers + v.best_offers;
                result_info.k_related.concat(v.keywords_related);
            });
            result_info.k_related = await result_info.k_related.filter(unique);
            return result_info;
        };
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#progress").hide();

            function tracking(sort_by, step = 0) {
                $("#progress").show();
                $("#progress-text").html("...Running...<i class='fas fa-sync fa-spin'></i>");
                $("#bot_sort").text(sort_by.upperCase);
                $.post("/c-panel/track-control", {sort_by: sort_by}).done(function (data) {
                    $("#exits").text(exits--);
                    if (data.code === 1) {
                        $("#msg_result").prepend("<fieldset>\n" +
                            "  <legend class='text-info'>" + data.keyword + "</legend>\n" +
                            "  Total Result: " + data.results_info.added + " - Best Offers:" + data.results_info.best_offers + "<br>\n" +
                            "  Keywords Related: " + data.results_info.keywords_related + "<br/>\n" +
                            "  Go View: <a href='/" + data.slug + "' target='_blank'>Go View</a>\n" +
                            " </fieldset><br/>");
                        count_time(sort_by);
                        $("#tracked").text(tracked++);
                    } else if (data.code === 2) {
                        $("#msg_result").prepend("<p class='label label-warning'> Track Duplicate - <a href='/" + data.slug + "' target='_blank'>View</a></p><br/>");
                        count_time(sort_by);
                        $("#tracked").text(tracked++);
                    } else if (data.code === 3) {
                        $("#msg_result").prepend("<p class='label label-warning'> STOP...</p><br/>");
                        $("#progress-text").html("...STOP...");
                    } else {
                        $("#msg_result").prepend(
                            "====================================================<br/>" +
                            "<p class='label label-danger'>Keyword:" + data.keyword + " - Track Error - Msg:" + data.msg + "</p><br/>" +
                            "====================================================<br/>");
                        $("#error").text(error++);
                        $("#progress").hide();
                        count_time(sort_by);
                    }
                });
            }

            function get_info_other(sort_by, step = 0) {
                $("#progress").show();
                $("#progress-text").html("...Running...<i class='fas fa-sync fa-spin'></i>");
                $("#bot_sort").text(sort_by.upperCase);
                $.post("/c-panel/other-get-info", {sort_by: sort_by}).done(function (data) {
                    $("#exits").text(exits--);
                    if (data.code === 1) {
                        $("#msg_result").prepend("<p>Other:" + data.full_url + "</p>br/>");
                        count_time(sort_by, 'other');
                        $("#tracked").text(tracked++);
                    } else if (data.code === 2) {
                        $("#msg_result").prepend("<p class='label label-warning'> Track Duplicate - <a href='/" + data.slug + "' target='_blank'>View</a></p><br/>");
                        count_time(sort_by, 'other');
                        $("#tracked").text(tracked++);
                    } else if (data.code === 3) {
                        $("#progress-text").html("...STOP...");
                        $("#msg_result").prepend("<p class='label label-warning'> STOP...</p><br/>");
                    } else {
                        $("#msg_result").prepend(
                            "====================================================<br/>" +
                            "<p class='label label-danger'>Other:" + data.full_url + " - Track Error - Msg:" + data.msg + "</p><br/>" +
                            "====================================================<br/>");
                        $("#error").text(error++);
                        $("#progress").hide();
                        count_time(sort_by, 'other');
                    }
                });
            }

            function count_time(sort_by, type = 'keyword') {
                var counter = 2;
                var interval = setInterval(function () {
                    counter--;
                    $("#progress-text").text("Wait on: " + counter + "s to continue...");
                    if (counter === 0) {
                        if (type === 'other') {
                            get_info_other(sort_by);
                        } else {
                            tracking(sort_by);
                        }
                        clearInterval(interval);
                    }
                }, 1000);
            }

            var exits = parseInt($("#exits").text());
            var tracked = parseInt($("#tracked").text());
            var error = parseInt($("#error").text());

            $(".run-bot").click(function () {
                var sort_by = $(this).data('sort');
                if (sort_by === 'other') {
                    get_info_other(sort_by);
                } else {
                    tracking(sort_by);
                }
            });

            $(document).on('click', 'a.jquery-postback', function (e) {
                e.preventDefault(); // does not go through with the link.

                var $this = $(this), name = $(this).data('name'), id = $(this).data('id');
                $.post({
                    type: $this.data('method'),
                    url: $this.attr('href')
                }).done(function (data) {
                    alert('Success remove: ' + name);
                    $("#" + id).hide();
                });
            });
        });
    </script>
@endsection
