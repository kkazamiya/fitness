@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Add New Config</h1>
@stop

@section('content')
    <div class="col-md-6 col-lg-offset-3">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'cpanel.config-site.store']) !!}
            {!! Form::token(); !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="config_name">Config Name</label>
                    <input type="text" class="form-control" name="config_name" id="config_name"
                           placeholder="Enter Name Example: Site Black friday">
                </div>
                <p>%store_name%, %code_type%, %off_sale%, %off_type%, %date%, %month%, %year%</p>
                <div class="form-group">
                    <label for="config_title">Title Config</label>
                    <input type="text" class="form-control" name="config_title" id="config_title"
                           placeholder="Enter Title">
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
        @stop
        @section('js')

@endsection
