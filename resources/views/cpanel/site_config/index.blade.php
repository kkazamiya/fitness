@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Config Site</h1>
    <ol class="breadcrumb">
        <li><a href="/cms"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#" class="active">Configs</a></li>
    </ol>
    <br/>
@stop

@section('content')
    @isset ($msg)
        <div class="alert alert-success">
            <p>{{ $msg }}</p>
        </div>
    @endisset
    {!! Form::open(['route' => 'cpanel.config-site.store']) !!}
    {!! Form::token(); !!}
    @isset($site_configs['_id'])
        <input type="hidden" value="{{$site_configs['_id']}}" name="id_config"/>
    @endisset
    <div class="col-md-6">
        <div class="box box-primary">
            <h3 class="box-title text-center">For Home Page</h3>
            <div class="box-body">
                <div class="form-group">
                    <label for="config_title">Title Home Page</label>
                    <input type="text" class="form-control" name="title_home" id="title_home"
                           placeholder="Enter Title"
                           value="@isset($site_configs['title_home']) {{$site_configs['title_home']}} @endisset">
                </div>
                <div class="form-group">
                    <label for="excerpt_home">Excerpt Home</label>
                    <input type="text" class="form-control" name="excerpt_home" id="excerpt_home"
                           placeholder="Enter Excerpt"
                           value="@isset($site_configs['excerpt_home']) {{$site_configs['excerpt_home']}} @endisset"/>
                </div>
                <div class="form-group">
                    <label for="advertise_home">Advertise Home</label>
                    <input type="text" class="form-control" name="advertise_home" id="advertise_home"
                           placeholder="Ex:astroved,beddinginn-com"
                           value="@isset($site_configs['advertise_home']){{$site_configs['advertise_home']}}@endisset"/>
                </div>
                <div class="form-group">
                    <label for="navigation">Categories Navigational</label>
                    <input type="text" class="form-control" name="navigation" id="navigation"
                           placeholder="slug|name||slug_2|name_2"
                           value="@isset($site_configs['navigation']){{$site_configs['navigation']}}@endisset"/>
                </div>
                <div class="form-group">
                    <label for="config_image">Config Image Social <span class="text-danger">* Only home</span>
                    </label>
                    <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Choose
                     </a>
                    </span>
                        <input id="thumbnail" class="form-control" type="text" name="social_image"
                               value="@isset($site_configs['social_image']) {{$site_configs['social_image']}} @endisset">
                    </div>
                    <img id="holder" style="margin-top:15px;max-height:100px;"
                         @isset($site_configs['social_image']) src="{{$site_configs['social_image']}}"
                         @endisset alt="Social image">
                </div>
                <div class="form-group">
                    <label for="config_urls">URLs Connect All Page</label>
                    <textarea rows="5" class="form-control" name="config_urls" id="config_urls"
                              placeholder="Per Line Per Url">
                        @isset($site_configs['config_urls']){{str_replace(",","\n",trim($site_configs['config_urls']))}}@endisset
                    </textarea>
                </div>
                <div class="form-group">
                    <label for="entity_socials">Entity Configs</label>
                    <textarea class="form-control" rows="5"
                              placeholder="Ex: fab fa-facebook|https://www.facebook.com/onlinejobedotcom/||fab fa-facebook|https://www.facebook.com/onlinejobedotcom/"
                              id="entity_socials" name="entity_socials">
                         @isset($site_configs['entity_socials']){{$site_configs['entity_socials']}}@endisset
                    </textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <h3 class="box-title text-center">For Stores & Keyword</h3>
            <div class="box-body">
                <p>Addition: %store_name%, %top_off%, %date%, %month%, %year%</p>
                <div class="form-group">
                    <label for="title_stores">Title Stores</label>
                    <input type="text" class="form-control" name="title_stores" id="title_stores"
                           placeholder="Enter Title Stores"
                           value="@isset($site_configs['title_stores']) {{$site_configs['title_stores']}} @endisset">
                </div>
                <div class="form-group">
                    <p>Addition: %store_name%, %top_coupon%, %date%, %month%, %year%</p>
                    <label for="excerpt_stores">Excerpt Stores </label>
                    <input type="text" class="form-control" name="excerpt_stores" id="excerpt_stores"
                           placeholder="Enter Excerpt Stores"
                           value="@isset($site_configs['excerpt_stores']) {{$site_configs['excerpt_stores']}} @endisset">
                </div>
                <p>Addition: %keyword%, %date%, %month%, %year%</p>
                <div class="form-group">
                    <label for="title_keyword">Title Keywords</label>
                    <input type="text" class="form-control" name="title_keyword" id="title_keyword"
                           placeholder="Enter Title Keyword"
                           value="@isset($site_configs['title_keyword']) {{$site_configs['title_keyword']}} @endisset">
                </div>
                <div class="form-group">
                    <label for="excerpt_keyword">Excerpt Keywords </label>
                    <p>Addition: %keyword%, %top_snippet%, %date%, %month%, %year%</p>
                    <input type="text" class="form-control" name="excerpt_keyword" id="excerpt_keyword"
                           placeholder="Enter Excerpt Keywords"
                           value="@isset($site_configs['excerpt_keyword']) {{$site_configs['excerpt_keyword']}} @endisset">
                </div>
                <div class="form-group">
                    <label for="advertise_stores">Advertise Stores</label>
                    <input type="text" class="form-control" name="advertise_stores" id="advertise_stores"
                           placeholder="Ex:astroved,beddinginn-com"
                           value="@isset($site_configs['advertise_stores']){{$site_configs['advertise_stores']}}@endisset">
                </div>
                <div class="form-group">
                    <label for="advertise_keyword">Advertise Keyword</label>
                    <input type="text" class="form-control" name="advertise_keyword" id="advertise_keyword"
                           placeholder="Ex:astroved,beddinginn-com"
                           value="@isset($site_configs['advertise_keyword']){{$site_configs['advertise_keyword']}}@endisset">
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12">
        <input type="submit" name="submit" class="btn btn-block btn-primary" value="Create and Update"/>
    </div>
    {!! Form::close() !!}
@stop
@section('js')
    <script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
    <script>
        var domain = "/c-panel/file-manager";
        $('#lfm').filemanager('image', {prefix: domain});
        var options = {
            filebrowserImageBrowseUrl: '/c-panel/file-manager?type=Images',
            filebrowserImageUploadUrl: '/c-panel/file-manager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/c-panel/file-manager?type=Files',
            filebrowserUploadUrl: '/c-panel/file-manager/upload?type=Files&_token='
        };
    </script>
@endsection
