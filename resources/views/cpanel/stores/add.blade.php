@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Add New Store Brand</h1>
@stop

@section('content')
    <div class="col-md-8">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'cpanel.stores.store']) !!}
            {!! Form::token(); !!}
            <div class="box-body">
                <div class="custom-control custom-switch">
                    <input type="checkbox" class="custom-control-input" name="active_stt" id="active_stt">
                    <label class="custom-control-label" for="active_stt">Active Store</label>
                </div>
                <div class="form-group">
                    <label for="store_name">Connect ID</label>
                    <input type="text" class="form-control" name="connect_id" id="connect_id"
                           placeholder="Id from UPTO" value="">
                </div>
                <div class="form-group">
                    <label for="store_name">Store Name</label>
                    <input type="text" class="form-control" name="store_name" id="store_name"
                           placeholder="Enter Store Name Example:Adidas">
                </div>
                <div class="form-group">
                    <label for="store_domain">Store Domain</label>
                    <input type="text" class="form-control" name="store_domain" id="store_domain"
                           placeholder="Enter Store Domain Example:Adidas.com">
                </div>
                <div class="form-group">
                    <label for="store_excerpt">Store Excerpt</label>
                    <input type="text" class="form-control" name="store_excerpt" id="store_excerpt"
                           placeholder="Store Excerpt">
                </div>
                <div class="form-group">
                    <label>Of Category</label>
                    <select multiple="" class="form-control" name="store_cat[]">
                        @foreach($categories as $category)
                            <option value="{{$category['slug']}}">{{$category['name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="store_image">Store Thumbnail</label>
                    <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Choose
                     </a>
                    </span>
                        <input id="thumbnail" class="form-control" type="text" name="store_image">
                    </div>
                    <img id="holder" style="margin-top:15px;max-height:100px;">
                </div>
                <div class="form-group">
                    <label for="store_aff">Store Link Affiliate</label>
                    <input type="text" class="form-control" name="store_aff" id="store_aff"
                           placeholder="Store Aff">
                </div>
                <div class="form-group">
                    <label>Rich Content</label>
                    <textarea class="form-control" id="rich_content" name="rich_content" rows="5"
                              placeholder="Enter ..."></textarea>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    

@stop
@section('js')
    <script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
    <script>
        var domain = "/c-panel/file-manager";
        $('#lfm').filemanager('image', {prefix: domain});
        var options = {
            filebrowserImageBrowseUrl: '/c-panel/file-manager?type=Images',
            filebrowserImageUploadUrl: '/c-panel/file-manager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/c-panel/file-manager?type=Files',
            filebrowserUploadUrl: '/c-panel/file-manager/upload?type=Files&_token='
        };

        $(document).ready(function () {
            CKEDITOR.replace('rich_content', options);
        });
    </script>
@endsection
