@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Edit Store: {{$store_edit['store_name']}}</h1>
@stop

@section('content')
    <div class="col-md-8">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ route('cpanel.stores.update',['id' =>  $store_edit['_id'] ]) }}" method="post">
                {!! Form::token(); !!}
                <div class="box-body">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input" name="active_stt" id="active_stt"
                               @if($store_edit['active_stt'] == 1) checked @endif>
                        <label class="custom-control-label" for="active_stt">Active Store</label>
                    </div>
                    <div class="form-group">
                        <label for="store_name">Connect ID</label>
                        <input type="text" class="form-control" name="connect_id" id="connect_id"
                               placeholder="Id from UPTO" value="{{$store_edit['connect_id']}}">
                    </div>
                    <div class="form-group">
                        <label for="store_name">Store Name</label>
                        <input type="text" class="form-control" name="store_name" id="store_name"
                               placeholder="Enter Store Name Example:Adidas" value="{{$store_edit['store_name']}}">
                    </div>
                    <div class="form-group">
                        <label for="store_domain">Store Domain</label>
                        <input type="text" class="form-control" name="store_domain" id="store_domain"
                               placeholder="Enter Store domain Example:Adidas.com"
                               value="@isset($store_edit['store_domain']){{$store_edit['store_domain']}}@endisset">
                    </div>
                    <div class="form-group">
                        <label for="store_excerpt">Store Excerpt</label>
                        <input type="text" class="form-control" name="store_excerpt" id="store_excerpt"
                               placeholder="Store Excerpt" value="{{$store_edit['store_excerpt']}}">
                    </div>

                    <div class="form-group">
                        <label>Of Category</label>
                        <select multiple="" class="form-control" name="store_cat[]">
                            @foreach($categories as $category)
                                @if(in_array($category['category_slug'], explode(",",$store_edit['store_cat'])))
                                    <option value="{{$category['category_slug']}}" selected>
                                        {{$category['category_name']}}
                                    </option>';
                                @else
                                    <option value="{{$category['category_slug']}}">
                                        {{$category['category_name']}}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="store_image">Store Thumbnail</label>
                        <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Choose
                     </a>
                    </span>
                            <input id="thumbnail" class="form-control" type="text" name="store_image"
                                   value="{{$store_edit['store_image']}}">
                        </div>
                        <img id="holder" style="margin-top:15px;max-height:100px;" src="{{$store_edit['store_image']}}">
                    </div>
                    <div class="form-group">
                        <label for="store_aff">Store Link Affiliate</label>
                        <input type="text" class="form-control" name="store_aff" id="store_aff"
                               value="{{$store_edit['store_aff']}}"
                               placeholder="Store Aff">
                    </div>
                    <div class="form-group">
                        <label>Rich Content</label>
                        <textarea class="form-control" id="rich_content" name="rich_content" rows="5"
                                  placeholder="Enter ...">{!! $store_edit['rich_content'] !!}</textarea>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-header with-border">

            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
    <script>
        var domain = "/c-panel/file-manager";
        $('#lfm').filemanager('image', {prefix: domain});
        var options = {
            filebrowserImageBrowseUrl: '/c-panel/file-manager?type=Images',
            filebrowserImageUploadUrl: '/c-panel/file-manager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/c-panel/file-manager?type=Files',
            filebrowserUploadUrl: '/c-panel/file-manager/upload?type=Files&_token='
        };

        $(document).ready(function () {
            CKEDITOR.replace('rich_content', options);
        });
    </script>
@endsection
