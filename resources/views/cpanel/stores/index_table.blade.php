@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><i class="fa fa-shopping-cart"></i> List Store</h1>
    <ol class="breadcrumb">
        <li><a href="/c-panel"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#" class="active">Stores</a></li>
    </ol>
    <br/>
    <a class="btn btn-success" href="{{route('cpanel.stores.create')}}">
        <i class=" fa fa-plus-circle"></i>
        Create New Store
    </a>
    <a class="btn btn-danger" href="{{route('cpanel.stores.add_excel')}}">
        <i class=" fa fa-plus-circle"></i>
        Add Stores By Excel File
    </a>
    <a class="btn btn-warning" href="{{route('cpanel.stores.export_excel')}}">
        <i class=" fa fa-plus-circle"></i>
        Export Stores By Excel 2003
    </a>
    <a class="btn btn-primary" href="/c-panel/net-api">
        <i class=" fa fa-plus-circle"></i>
        Sync Stores & Coupons From NET
    </a>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
@stop

@section('content')
    @isset ($msg)
        <div class="alert alert-success">
            <p>{{ $msg }}</p>
        </div>
    @endisset
    <div class="table-responsive">
        <div class="form-group">
            <label class="radio-inline"><input type="radio" name="filter_with" value="all">All Stores</label>
            <label class="radio-inline"><input type="radio" name="filter_with" value="sharesale">Net ShareSale</label>
        </div>
        <table class="table table-bordered table-striped table-hover" id="data-table">
            <thead>
            <tr>
                <th>Store Image</th>
                <th>Store Name</th>
                <th>Coupons added</th>
                <th>Created At</th>
                <th>-</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection
@section('js')
    <script src="{{asset('/js/jquery.lazyload.min.js')}}"></script>
    <script>
        const urlParams = new URLSearchParams(window.location.search);
        let filter_with = urlParams.get('filter_with');
        if (typeof filter_with === "undefined") {
            filter_with = $('input[type=radio][name=filter_with]').val();
        }
        $('input[type=radio][name=filter_with]').change(function () {
            filter_with = this.value;
            window.open('http://' + window.location.hostname + window.location.pathname + '?filter_with=' + filter_with, '_self');
        });
        
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
           
            $('#data-table').DataTable({
                "processing": true,
                "serverSide": true,
                "columnDefs": [
                    {"orderable": false, "targets": [0, 1, 2, 4]}
                ],
                "ajax": {
                    "url": "{{ route('cpanel.all_stores') }}?filter_with=" + filter_with,
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{csrf_token()}}"}
                },
                "columns": [
                    {"data": "store_image"},
                    {"data": "store_name"},
                    {"data": "coupon_added"},
                    {"data": "created_at"},
                    {"data": "options"}
                ],
                "drawCallback": function () {
                    $("img.lazyload").lazyload();
                }
            });
            $("img.lazyload").lazyload({
                threshold: 200
            });
        });
        $('.active_stt').change(function () {
            alert('changed');
            let params = {};
            params.id = $(this).data('id');
            if ($(this).is(':checked')) {
                params.active_stt = 1;
            } else {
                params.active_stt = 0;
            }
            $.post({
                type: 'POST',
                url: '/c-panel/update-store-stt',
                data: params
            }).done(function (data) {
                alert(JSON.stringify(data));
            });
        })

        $(document).on('click', 'a.jquery-postback', function (e) {
            e.preventDefault(); // does not go through with the link.
            let $this = $(this),
                from_collection = $(this).data('from_collection'),
                id_store = $(this).data('id-store'),
                name_store = $(this).data('store');
            if (typeof from_collection !== "undefined") {
                $this.find('i').addClass('fa-spin');
                $.post({
                    type: $this.data('method'),
                    url: $this.attr('href'),
                    data: {id: id_store, from_collection: from_collection}
                }).done(function (data) {
                    $this.find('i').removeClass('fa-spin');
                    alert(JSON.stringify(data));
                });
            } else {
                console.log($(this).data('store'));
                $.post({
                    type: $this.data('method'),
                    url: $this.attr('href')
                }).done(function (data) {
                    alert('Success remove: ' + name_store);
                    $("#" + id_store).hide();
                });
            }
        });
    </script>
@endsection
