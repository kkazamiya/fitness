@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Add New Syncs</h1>
@stop

@section('content')
    <div class="col-md-8">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'cpanel.syncs.store']) !!}
            {!! Form::token(); !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="store_name">Type Source</label>
                    <select class="form-control" name="type_source" id="type_source">
                        <option value="coupons" selected>/Coupons</option>
                        <option value="deals">/Deals</option>
                        <option value="promos">/Promos</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Of Category</label>
                    <select multiple="" class="form-control" name="store_cat[]">
                        @foreach($categories as $category)
                            <option value="{{$category['slug']}}">{{$category['name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label>List Ids Connect</label>
                    <textarea class="form-control" id="list_ids" name="list_ids" rows="5"
                              placeholder="Per Id per row..."></textarea>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-header with-border">

            </div>
        </div>
    </div>
@stop
