@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Edit Sync: {{$edit['store_name']}}</h1>
@stop

@section('content')
    <div class="col-md-8">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ route('cpanel.syncs.update',['id' =>  $edit['_id'] ]) }}" method="post">
                {!! Form::token(); !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="store_name">Connect ID</label>
                        <input type="text" class="form-control" name="connect_id" id="connect_id"
                               value="{{$edit['connect_id']}}">
                    </div>
                    <div class="form-group">
                        <label for="store_name">Type Source</label>
                        <select class="form-control" name="type_source" id="type_source">
                            <option value="coupons" @if ($edit['type_source'] == 'coupons')selected @endif>/Coupons
                            </option>
                            <option value="deals" @if ($edit['type_source'] == 'deals')selected @endif>/Deals</option>
                            <option value="promos" @if ($edit['type_source'] == 'promos')selected @endif>/Promos</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Of Category</label>
                        <select multiple="" class="form-control" name="store_cat[]">
                            @foreach($categories as $category)
                                @if(in_array($category['category_slug'], explode(",",$edit['store_cat'])))
                                    <option value="{{$category['slug']}}" selected>
                                        {{$category['name']}}
                                    </option>';
                                @else
                                    <option value="{{$category['slug']}}">
                                        {{$category['name']}}
                                    </option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-header with-border">

            </div>
        </div>
    </div>

@stop
@section('js')
    <script src="{{asset('/vendor/laravel-filemanager/js/lfm.js')}}"></script>
    <script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
    <script>
        var domain = "/c-panel/file-manager";
        $('#lfm').filemanager('image', {prefix: domain});
        var options = {
            filebrowserImageBrowseUrl: '/c-panel/file-manager?type=Images',
            filebrowserImageUploadUrl: '/c-panel/file-manager/upload?type=Images&_token=',
            filebrowserBrowseUrl: '/c-panel/file-manager?type=Files',
            filebrowserUploadUrl: '/c-panel/file-manager/upload?type=Files&_token='
        };

        $(document).ready(function () {
            CKEDITOR.replace('rich_content', options);
        });
    </script>
@endsection
