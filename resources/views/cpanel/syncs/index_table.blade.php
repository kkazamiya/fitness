@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><i class="fa fa-shopping-cart"></i> List Sync</h1>
    <ol class="breadcrumb">
        <li><a href="/c-panel"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#" class="active">Stores</a></li>
    </ol>
    <br/>
    <a class="btn btn-success" href="{{route('cpanel.syncs.create')}}">
        <i class=" fa fa-plus-circle"></i>
        Create New Sync
    </a>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
@stop

@section('content')
    @isset ($msg)
        <div class="alert alert-success">
            <p>{{ $msg }}</p>
        </div>
    @endisset
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover" id="data-table">
            <thead>
            <tr>
                <th>Store Info</th>
                <th>Sync Config</th>
                <th>Add/Update Date</th>
                <th>-</th>
            </tr>
            </thead>
        </table>
    </div>
@stop
@section('js')
    <script src="{{asset('/js/jquery.lazyload.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#data-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{ route('cpanel.all_sync') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{csrf_token()}}"}
                },
                "columns": [
                    {"data": "store_info"},
                    {"data": "connect_id"},
                    {"data": "created_at"},
                    {"data": "options"}
                ],
                "drawCallback": function () {
                    $("img.lazyload").lazyload();
                }
            });
            $("img.lazyload").lazyload({
                threshold: 200
            });
        });

        $(document).on('click', 'a.jquery-postback', function (e) {
            e.preventDefault(); // does not go through with the link.

            var $this = $(this);
            var name_store = $(this).data('store');
            var id_store = $(this).data('id-store');
            console.log($(this).data('store'));
            $.post({
                type: $this.data('method'),
                url: $this.attr('href')
            }).done(function (data) {
                alert('Success remove: ' + name_store);
                $("#" + id_store).hide();
            });
        });
    </script>
@endsection
