@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Start Sync</h1>
@stop

@section('content')
    <div class="col-md-8">
        <p class="text-center">
            <label class="checkbox-inline"><input type="checkbox" value="store_aff">Sync Link Aff</label>
            <label class="checkbox-inline"><input type="checkbox" value="active_stt">Active Stores</label>
        </p>
        <div class="form-group">
            <input type="button" class="btn btn-block btn-primary" id="start-sync" value="Start Sync"/>
        </div>
        <div class="progress" style="display: none" id="running_stt">
            <div class="progress-bar progress-bar-striped active" role="progressbar"
                 aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
                I'm Running Man, Please wait for moment...
            </div>
        </div>
        <div class="box box-primary">
            <ol id="result_msg" reversed></ol>
        </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            function start(data_send) {
                $("#running_stt").show();
                $.post({
                    type: 'POST',
                    url: '/c-panel/sync-start',
                    data: data_send,
                }).done(function (data) {
                    if (typeof data.code === "undefined" || data.code !== 0) {
                        $("#result_msg").prepend("<li>" + data + "</li>");
                        start(data_send);
                    } else {
                        $("#running_stt").hide();
                        $("#result_msg").prepend("<li>" + JSON.stringify(data) + "</li>");
                    }
                });
            }

            let data_send = {};
            $("input[type=checkbox]").change(function () {
                if ($(this).is(':checked')) {
                    data_send[$(this).val()] = true;
                } else {
                    delete data_send[$(this).val()];
                }
                console.log(data_send);
            });
            $("#start-sync").click(function () {
                start(data_send);
            });
        });
    </script>
@endsection

