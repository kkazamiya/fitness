@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Add New Tags</h1>
@stop

@section('content')
    @if($errors->any())
        <p class="alert alert-error">{{$errors->first()}}</p>
    @endif
    <div class="col-12">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'cpanel.tags.store']) !!}
            {!! Form::token(); !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="tag_type">Tag Type</label>
                    <select class="form-control" id="tag_type" name="tag_type">
                        <option value="job_type" selected>Job Type</option>
                        <option value="experience">Experience</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="tag_name">Tag Name</label>
                    <input type="text" class="form-control" name="tag_name" id="tag_name"
                           placeholder="include tag name"/>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('js')
@endsection
