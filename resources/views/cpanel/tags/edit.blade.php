@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Edit Tag: {{$editor['tag_name']}}</h1>
@stop
@section('content')
    @if($errors->any())
        <p class="alert alert-error">{{$errors->first()}}</p>
    @endif
    <div class="col-12">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ route('cpanel.tags.update',['id' =>  $editor['_id'] ]) }}" method="post">
                {!! Form::token(); !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="tag_type">Tag Type</label>
                        <select class="form-control" id="tag_type" name="tag_type">
                            <option value="job_type" @if($editor['tag_type'] == 'job_type') selected @endif>Job Type
                            </option>
                            <option value="experience" @if($editor['tag_type'] == 'experience') selected @endif>
                                Experience
                            </option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tag_name">Tag Name</label>
                        <input type="text" class="form-control" name="tag_name" id="tag_name"
                               value="{{$editor['tag_name']}}"
                               placeholder="include tag name" disabled/>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
@stop
