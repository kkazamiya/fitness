@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1><i class="fa fa-tags"></i> List Tags</h1>
    <ol class="breadcrumb">
        <li><a href="/c-panel"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#" class="active">Tags</a></li>
    </ol>
    <br/>
    <a class="btn btn-success" href="{{route('cpanel.tags.create')}}">
        <i class=" fa fa-plus-circle"></i>
        Create New Tags
    </a>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
@stop

@section('content')
    @isset ($msg)
        <div class="alert alert-success">
            <p>{{ $msg }}</p>
        </div>
    @endisset
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover" id="data-table">
            <thead>
            <tr>
                <th>Tag Type</th>
                <th>Tag Name</th>
                <th>Created At</th>
                <th>-</th>
            </tr>
            </thead>
        </table>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('#data-table').DataTable({
                "processing": true,
                "serverSide": true,
                "rowId": '_id',
                "ajax": {
                    "url": "{{ route('cpanel.tags.table') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data": {_token: "{{csrf_token()}}"}
                },
                "columns": [
                    {"data": "tag_type"},
                    {"data": "tag_name"},
                    {"data": "created_at"},
                    {"data": "options"}
                ]
            });
        });
        $(document).on('click', 'a.jquery-postback', function (e) {
            e.preventDefault(); // does not go through with the link.
            let id = $(this).data('id'), name = $(this).data('id');
            $this = $(this);
            $.post({
                type: $this.data('method'),
                url: $this.attr('href')
            }).done(function (data) {
                alert('Success remove: ' + name);
                $("#" + id).hide();
            });
        });
    </script>
@endsection
