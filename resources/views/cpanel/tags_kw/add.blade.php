@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Add New Tags</h1>
@stop

@section('content')
    @if($errors->any())
        <p class="alert alert-error">{{$errors->first()}}</p>
    @endif
    <div class="col-12">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'cpanel.tags_kw.store']) !!}
            {!! Form::token(); !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="tag_type">Tag Type</label>
                    <select class="form-control" id="tag_type" name="tag_type">
                        <option value="" selected>--Select--</option>
                        <option value="internal_id">Store Internal By ID</option>
                        <option value="external_url">External URL (Replica Top 1 Info)</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="tag_name">Tag Name</label>
                    <input type="text" class="form-control" name="tag_name" id="tag_name"
                           placeholder="include tag name"/>
                </div>
                <div class="form-group">
                    <label for="for_keyword">Will Add For Keyword: (keyword_slug, *keyword_slug,
                        *keyword_slug*,keyword_slug*)</label>
                    <input type="text" class="form-control" name="for_keyword" id="for_keyword"/>
                </div>
                <div class="form-group">
                    <label for="foreign_id">Store ID</label>
                    <input type="text" class="form-control" name="foreign_id" id="foreign_id"
                           placeholder="Include if tag type: Store Internal"/>
                </div>
                <div class="form-group">
                    <label for="foreign_url">External URL</label>
                    <input type="text" class="form-control" name="foreign_url" id="foreign_url"
                           placeholder="Include if tag type: External URL"/>
                </div>
                <div class="form-group">
                    <label for="rel">Referral</label>
                    <select class="form-control" id="rel" name="rel">
                        <option value="nofollow" selected>No Follow</option>
                        <option value="dofollow">Do Follow</option>
                        <option value="nofollow noreferrer">Will Show Direct Source</option>
                    </select>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Create</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('js')
    <script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
    <script>
        $(document).ready(function () {
            CKEDITOR.replace('snippet');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $('#for_keyword').blur(function () {
                if ($('#for_keyword').val().length < 4) {
                    alert('Keyword slug:' + $('#for_keyword').val() + ' illegal');
                    $(':button[type="submit"]').prop('disabled', true);
                } else {
                    $.post('/c-panel/tags_kw-effect', {
                        _token: $('input[name="_token"]').val(),
                        for_keyword: $('#for_keyword').val()
                    }).done(function (data) {
                        if (data === 0) {
                            $(':button[type="submit"]').prop('disabled', true);
                            alert('Keyword will effect to ' + data + ' keywords');
                        } else {
                            $(':button[type="submit"]').prop('disabled', false);
                            alert('Keyword will effect to ' + data + ' keywords');
                        }

                    });
                }
            });
        });
    </script>
@endsection
