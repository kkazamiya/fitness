@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Add Stores By Excel File</h1>
@stop

@section('content')
    <div class="col-md-6 col-lg-offset-3">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['route' => 'cpanel.stores.add_excel_post', 'enctype' =>'multipart/form-data']) !!}
            @csrf
            <div class="box-body">
                <div class="form-group">
                    <label for="file_stores">File Upload</label>
                    <input type="file" name="file_stores" id="file_stores">
                </div>
            </div>
            <!-- /.box-body -->
            {!! Form::close() !!}
        </div>
        <div class="box box-success">
            <ul class="list-group-item" id="result_msg"></ul>
        </div>
    </div>
@stop
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.15.4/xlsx.full.min.js"></script>
    <script>
        $(document).ready(function () {
            let oFileIn;
            $(function () {
                oFileIn = document.getElementById('file_stores');
                if (oFileIn.addEventListener) {
                    oFileIn.addEventListener('change', filePicked, false);
                }
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });

            function saveStores(store_data) {
                $.post("/c-panel/add_excel/stores", store_data).done(function (data) {
                    $("#result_msg").prepend(data);
                });
            }

            function filePicked(oEvent) {
                // Get The File From The Input
                let oFile = oEvent.target.files[0];
                let sFilename = oFile.name;
                // Create A File Reader HTML5
                let reader = new FileReader();

                // Ready The Event For When A File Gets Selected
                reader.onload = function (e) {
                    let data = e.target.result;
                    let cfb = XLS.CFB.read(data, {type: 'binary'});
                    let wb = XLS.parse_xlscfb(cfb);
                    let time_delay = 0;
                    // Loop Over Each Sheet
                    wb.SheetNames.forEach(function (sheetName) {
                        // Obtain The Current Row As CSV
                        let sCSV = XLS.utils.make_csv(wb.Sheets[sheetName]);
                        let data = XLS.utils.sheet_to_json(wb.Sheets[sheetName], {header: 1});
                        $.each(data, function (indexR, valueR) {
                            if (typeof data[indexR][0] !== "undefined" && valueR.length > 1) {
                                console.log(valueR);
                                let store_data = {};
                                let timeNow = Math.round(new Date().getTime() / 1000);
                                store_data.store_name = data[indexR][0];
                                store_data.store_aff = data[indexR][2];
                                store_data.created_at = timeNow;
                                store_data.update_at = timeNow;
                                store_data.coupon_added = 0;
                                store_data.view = 0;
                                if (typeof data[indexR][4] !== 'undefined') {
                                    store_data.store_cat = data[indexR][4];
                                } else {
                                    store_data.store_cat = 'home-page';
                                }
                                store_data.store_image = data[indexR][3];
                                store_data.store_excerpt = null;
                                store_data.rich_content = null;
                                store_data.store_domain = data[indexR][1];
                                time_delay = time_delay + 100;
                                setTimeout(function () {
                                    saveStores(store_data);
                                }, time_delay);
                            }
                        });
                    });
                };

                // Tell JS To Start Reading The File.. You could delay this if desired
                reader.readAsBinaryString(oFile);
            }
        });
    </script>
@endsection
