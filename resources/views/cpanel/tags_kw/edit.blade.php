@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Edit Tag: {{$editor['tag_name']}}</h1>
@stop
@section('content')
    @if($errors->any())
        <p class="alert alert-error">{{$errors->first()}}</p>
    @endif
    <div class="col-12">
        <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form action="{{ route('cpanel.tags_kw.update',['id' =>  $editor['_id'] ]) }}" method="post">
                {!! Form::token(); !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="tag_type">Tag Type</label>
                        <select class="form-control" id="tag_type" name="tag_type">
                            <option value="">--Select--</option>
                            <option value="internal_id" @if($editor['tag_type'] == 'internal_id') selected @endif>Store Internal By ID</option>
                            <option value="external_url" @if($editor['tag_type'] == 'external_url') selected @endif>External URL (Replica Top 1 Info)</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="tag_name">Tag Name</label>
                        <input type="text" class="form-control" name="tag_name" id="tag_name"
                               value="{{$editor['tag_name']}}"
                               placeholder="include tag name" disabled/>
                    </div>
                    <div class="form-group">
                        <label for="for_keyword">Keyword Slug: (keyword_slug, *keyword_slug,
                            *keyword_slug*,keyword_slug*)</label>
                        <input type="text" class="form-control" name="for_keyword" id="for_keyword"
                               value="{{$editor['for_keyword']}}"/>
                    </div>
                    <div class="form-group">
                        <label for="for_id">Store ID</label>
                        <input type="text" class="form-control" name="foreign_id" id="foreign_id"
                               value="{{$editor['foreign_id']}}"
                               placeholder="Include if tag type: Store Inline By ID"/>
                    </div>
                    <div class="form-group">
                        <label for="for_url">External URL</label>
                        <input type="text" class="form-control" name="foreign_url" id="foreign_url"
                               value="{{$editor['foreign_url']}}"
                               placeholder="Include if tag type: By URL"/>
                    </div>
                    <div class="form-group">
                        <label for="rel">Referral</label>
                        <select class="form-control" id="rel" name="rel">
                            <option value="nofollow" @if($editor['rel'] == 'nofollow') selected @endif>No Follow</option>
                            <option value="dofollow" @if($editor['rel'] == 'dofollow') selected @endif>Do Follow</option>
                            <option value="nofollow noreferrer" @if($editor['rel'] == 'nofollow noreferrer') selected @endif>Will Show Direct Source</option>
                        </select>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
@stop
@section('js')
    <script src="https://cdn.ckeditor.com/4.12.1/full/ckeditor.js"></script>
    <script>
        $(document).ready(function () {
            CKEDITOR.replace('snippet');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                }
            });
            $('#for_keyword').blur(function () {
                if ($('#for_keyword').val().length < 4) {
                    alert('Keyword slug:' + $('#for_keyword').val() + ' illegal');
                    $(':button[type="submit"]').prop('disabled', true);
                } else {
                    $.post('/c-panel/tags_kw-effect', {
                        _token: $('input[name="_token"]').val(),
                        for_keyword: $('#for_keyword').val()
                    }).done(function (data) {
                        if (data === 0) {
                            $(':button[type="submit"]').prop('disabled', true);
                            alert('Keyword will effect to ' + data + ' keywords');
                        } else {
                            $(':button[type="submit"]').prop('disabled', false);
                            alert('Keyword will effect to ' + data + ' keywords');
                        }

                    });
                }
            });
        });
    </script>
@endsection
