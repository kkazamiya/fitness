@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')
    @if($datum)
        <div class="row">
            <div id="chart_datum" data-labels="{{json_encode($datum['labels'])}}"
                 data-values="{{json_encode($datum['data_set'])}}"></div>
            <div class="col-md-9">
                <div id="chart-container">
                    <canvas id="graphCanvas"></canvas>
                </div>
            </div>
            <div class="col-md-3">
                <div class="info-box bg-green">
                    <span class="info-box-icon"><i class="ion ion-ios-heart-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Keyword Crawled</span>
                        <span class="info-box-number">{{$datum['kw_crawled']}}</span>
                        <div class="progress">
                            <div class="progress-bar" style="width: {{$datum['p_kw_crawled']}}%"></div>
                        </div>
                        <span class="progress-description">
                    {{$datum['p_kw_crawled']}} Keywords Total
                  </span>
                    </div>
                </div>
                <div class="info-box bg-red">
                    <span class="info-box-icon"><i class="ion ion-ios-cloud-download-outline"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Keyword Not Crawled</span>
                        <span class="info-box-number">{{$datum['kw_not_crawled']}}</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: {{$datum['p_kw_not_crawled']}}%"></div>
                        </div>
                        <span class="progress-description">
                    {{$datum['p_kw_not_crawled']}} Keywords Total
                  </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </div>
        </div>
    @endif
@stop
@section('js')
    <script>
        let ctx = document.getElementById('graphCanvas').getContext('2d');
        let labels = $('#chart_datum').data('labels');
        let values = $('#chart_datum').data('values');
        let myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: labels,
                datasets: [{
                    label: '#Crawled',
                    data: values,
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    </script>
@endsection
