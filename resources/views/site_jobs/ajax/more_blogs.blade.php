
@foreach($blogs as $value)
<div class="col-md-4 pb-2">
    <div class="card border-0 border-radius-0 mb-4">
        <div class="card-media blog-home-img">
            <a href="#">
                <img src="{{$value->featured_img}}" alt="" class="card-img-top border-radius-0" >
            </a>
        </div>
        <div class="card-body px-0 pb-0">
            <div class="d-flex blogs_title border-bottom">
                <div class="align-items-center d-flex flex-2 flex-column justify-content-center pb-3">
                    <span class="d-block">{{$value->created_at->format('d')}}</span>
                    <span class="d-block">{{$value->created_at->format('M')}}</span>
                </div>
                <div class="flex-8 pl-3">
                    <a href="{{route('blog',['slug'=>$value['slug']])}}" class="text-uppercase card-title">{{$value->title}}</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
{{$blogs->links()}}
