<div class="infinite-scroll">
    @if(isset($all_data) && count($all_data) > 0)
        @foreach($all_data as $item)
            <div class="card mb-2">
                <div class="card-body pt-2">
                    <h3>{{$item['title']}}</h3>
                    @if($item['salary'] != '0')
                        <p class="mb-0">
                            <span class="badge-outline-success mr-2">{{$item['salary']}}</span>
                            @if($item['salary_hourly'] != 0)
                            <span class="badge-outline-success mr-2">${{currency_format($item['salary_hourly'])}} per hour</span>@endif
                            @if($item['salary_daily'] != 0)
                            <span class="badge-outline-success mr-2">${{currency_format($item['salary_hourly'])}} daily</span>@endif
                            @if($item['salary_monthly'] != 0)
                            <span class="badge-outline-success mr-2">${{currency_format($item['salary_monthly'])}} monthly</span>@endif
                            @if($item['salary_yearly'] != 0)
                            <span class="badge-outline-success mr-2">${{currency_format($item['salary_yearly'])}} yearly</span>@endif
                        </p>
                    @endif
                    <p class="mb-0" id="{{$item['_id']}}" style="display: none"></p>
                    <p class="text-justify text-snippet mb-2 line-4">
                        {!! $item['snippet'] !!}
                    </p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="d-flex flex-auto align-items-baseline">
                            @if(isset($item['tag_info']) && count($item['tag_info']) > 0)
                                @foreach($item['tag_info'] as $tag_info)
                                    <a class="badge badge-info mr-2" href="{{route('job_type', ['slug'=>$tag_info['tag_slug']])}}">
                                        {{ucwords($tag_info['tag_name'])}}
                                    </a>
                                @endforeach
                            @endif
                        </div>
                        <div class="d-flex flex-auto align-items-center">
                            <div class="d-flex flex-auto justify-content-end"></div>
                            <div class="d-flex ml-5 justify-content-end">
                                <a href="#" data-id="{{$item['_id']}}"
                                   data-link="{{parse_url($item['link'])['host']}}"
                                   class="view-job btn btn-warning px-md-5 b-radius-0 text-white font-weight-bold">View
                                    Job</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        {{ $all_data->links() }}
    @endif
</div>
