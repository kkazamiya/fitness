@extends('site_jobs.layouts.job')
@section('title')
{{ucwords($post->title)}}
@endsection
@section('description', '')
@section('image', '')
@section('navbar')
@include('site_jobs.parts.navbar')
@endsection
@section('main_content')

<div class="devide"></div>
<section id="result_content" class="pt-3 pb-4 bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-8 breadcrumbs">
                <div class="content">
                    <ol class="bg-transparent breadcrumb breadcrumb_list p-0 pb-3 pt-3 mb-0">
                        <li class="breadcrumb_item mr-2">
                            <a href="{{route('home')}}">Home&nbsp;<i class="fa fa-angle-right ml-2"></i></a>
                        </li>
                        <li class="breadcrumb_item">
                            <span class="current_item">
                                {{ucwords($post->title)}}
                            </span>
                        </li>
                    </ol>
                </div>
                <h1 class="font-weight-bold">{{ucwords($post->title)}}</h1>
                    <div class="entry-meta">

                    </div>
                    <div class="line_share mb-3">
                        <div class="addthis_inline_share_toolbox_8h0w"></div>
                    </div>
            </div>

            
            <div class="col-md-8 pr-0 blog-content">

                <div class="content__blogs pr-2 border-right">
                    
                    <div class="post-body text-justify">
                        <img src="{{$post->featured_img}}" alt="" class="pb-5">
                        {!! imgALT_Nofollow($post['content']) !!}
                    </div>
                    
                </div>
                @if($slug!=null)
                <div class="next__post">
                        <div class="p-2 text-right">
                            <a href="{{route('blog',['slug'=>$slug->slug])}}" class="btn btn-primary">Next post</a>
                        </div>
                       </div> 
                @endif
            </div>
            <div class="col-md-4 p-0" id="sidebar-right">
                <div class="card pb-2 mb-3 border-0">
                    <div class="card-body related-blog pl-2 pr-0 pb-3 pt-0">
                        <div class="card pt-0">
                            <div class="card-header bg-white border-0 pb-0">
                                <h4 class="header-title row mb-0 border-bottom pb-2">Related Articles</h4>
                            </div>
                            <div class="card-body pb-0 pt-0 pr-3 pl-3">
                                @foreach($latest_posts as $value)
                                <div class="row border-bottom py-2">
                                    <div class="col-md-4 p-1">
                                        <div class="blog__thumbnail">
                                            <a href="{{route('blog',['slug'=>$value->slug])}}" target="_blank">
                                                <img src="{{$value->featured_img}}" data-src="/photos/shares/allied-health-professional.jpg" class="img-thumbnail ls-is-cached lazyloaded">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-8 pl-2">
                                        <h6 class="font-weight-bold line-3 text-dark"><a href="{{route('blog',['slug'=>$value->slug])}}" class="blog_title">{{ucwords($value->title)}}</a></h6>
                                        <span class="mt-2 blog_content">{{strip_tags($value->content)}}</span>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="content__menu-categories card mt-3 p-3">
                            <h4 class="mb-3">Related search:</h4>
                            <ul class="group-item list-inline mb-0">
                                @foreach($search as $value)
                                <li class="list-item mb-1 text-truncate"><a href="{{route('v_keyword',['slug'=>$value['keyword_slug']])}}" class="text-primary">{{ucwords($value->keyword)}}</a></li>
                                @endforeach

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('js')
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f228731cea988ad"></script>
@endsection