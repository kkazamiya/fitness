<div class="container-fluid px-0 modal-cid" id="modal_out">
    <div class="container modal-cid-content">
        <span class="close" id="close-modal">×</span>
        <h4 class="size-large text-center">{{$result_modal['title']}}</h4>
        <p class="text-center text__primary">{{$result_modal['domain']}}</p>
        <a href="{{route('result_out',$result_modal['_id'])}}" target="_blank" title="Get Coupon" class="btn btn-block btn-primary py-2">Go to store</a>
        <div class=" py-2">
            <div class="col-12">
                <p class="size-medium mb-0">Receive the latest coupons & deals</p>
            </div>
            <div class="col-12 text-left d-flex align-items-center py-4">
                @if (\Illuminate\Support\Facades\Session::has('success'))
                <p class="alert alert-success mb-0 text-center">Please check your email to confirm the subscription</p>
                @else
                @if (\Illuminate\Support\Facades\Session::has('error'))
                <p class="mb-0 text-danger text-center">* Sorry, the email you sent is incorrect. Please try again</p>
                @endif
                <form action="/saving-email" method="POST" class="w-100">
                    @csrf
                    <div class="input-group w-100">
                        <input style="border: 2px solid blue" id="input-email" name="email" type="email"
                        class="form-control"
                        placeholder="Enter your email address..."
                        aria-label="Enter your email address..." aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <button id="submit-email" class="btn btn-primary px-3" type="submit">Subscribe
                            </button>
                        </div>
                    </div>
                </form>
                @endif
            </div>
            <h3 class="text-dark size-large text-center">You may also like</h3>
            <div class="row">
                @foreach($keyword_top as $value)
                <div class="col-md-6 text-truncate link">
                    › <a class="small text-dark" href="{{route('v_keyword',['slug'=>$value->keyword_slug])}}" target="_blank" title="{{ucwords($value->keyword)}}">{{ucwords($value->keyword)}}</a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
