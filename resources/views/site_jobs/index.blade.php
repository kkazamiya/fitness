@extends('site_jobs.layouts.job')
@section('title', 'Best Workout Tips from Exercise Experts and Gym Great Equipments, Fitness Centres Available')
@section('description', "Fitnessol supports client's in-desire for a healthy body by offering high-quality fitness equipment, affordable fitness services. Let us take care of your health.
")
@section('image', '')
@section('navbar')
@include('site_jobs.parts.navbar_index')
@endsection
@section('main_content')
@include('site_jobs.parts.form')
<section id="box_welcome" class="pt-3 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5 class="text-center mt-5 mb-4">WELCOME TO FITNESSOL!</h5>
                <div class="col-md-8 offset-md-2">
                    <p class="text-center text-justify">Fitnessol is an online related-fitness aggregative site for everyone to explore more fitness. Our responsibility is to give the largest product and service selection for all your needs with the top of the services and types of equipment line.</p>
                </div>
            </div>
            <div class="clearfix d-block mb-4 w-100 "></div>
            <div class="col-md-3 px-3 pb-3">
                <div class="card border-radius-0 h-100">
                    <img src="assets/images/ERAA1.jpg" class="card-img-top">
                    <div class="card-body bg-light">
                      <h5 class="card-title text-uppercase text-center">Fitness Learning</h5>
                      <p class="card-text text-center mb-4 text-center">Numerous fitness courses, training, and personal training services are available. </p>
                  </div>
                  <div class="card-footer p-0 border-radius-0">
                    <a href="{{route('blog',['slug'=>'fitness-courses-you-should-not-miss-out'])}}" class="d-block text-center p-3 text-white">
                        <span class="text-uppercase">LEARN MORE</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md-3 px-3 pb-3">
            <div class="card border-radius-0 h-100">
                <img src="assets/images/800-x-533-ER-EDM-blog-images-9.png" class="card-img-top">
                <div class="card-body bg-light">
                  <h5 class="card-title text-uppercase text-center">Fitness Equipment</h5>
                  <p class="card-text text-center mb-4">We give resources & reviews on fitness equipment to help you have a smart choice. </p>
              </div>
              <div class="card-footer p-0 border-radius-0">
                <a href="{{route('blog',['slug'=>'high-quality-gym-equipment-you-should-own'])}}" class="d-block text-center p-3 text-white">
                    <span class="text-uppercase">LEARN MORE</span>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-3 px-3 pb-3">
        <div class="card border-radius-0 h-100">
            <img src="assets/images/allied-health-professional.jpg" class="card-img-top">
            <div class="card-body bg-light">
                <h5 class="card-title text-uppercase text-center">Fitness coupons</h5>
                <p class="card-text text-center mb-4">Let’s save on your fitness cost with coupons, discounts, and many helpful saving tips.</p>
            </div>
            <div class="card-footer p-0 border-radius-0">
                <a href="{{route('blog',['slug'=>'gym-equipment-coupons'])}}" class="d-block text-center p-3 text-white">
                    <span class="text-uppercase">LEARN MORE</span>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-3 px-3 pb-3">
        <div class="card border-radius-0 h-100">
            <img src="assets/images/ERAA1.jpg" class="card-img-top">
            <div class="card-body bg-light">
              <h5 class="card-title text-uppercase text-center">Fitness tips</h5>
              <p class="card-text text-center mb-4 text-center">Helpful fitness tips help you get the best effectiveness for their training.</p>
          </div>
          <div class="card-footer p-0 border-radius-0">
            <a href="{{route('blog',['slug'=>'tips-to-exercise-effectively'])}}" class="d-block text-center p-3 text-white">
                <span class="text-uppercase">LEARN MORE</span>
            </a>
        </div>
    </div>
</div>
</div>
</div>
</section>
<section id="box_equipment" class="pt-4 pb-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="box_equipment">
                    <h4 class="text-center mb-4">Explore Top Gym Service Categories</h4>
                    <div class="col-md-8 text-center mx-auto mb-4">
                        <p>There are plenty of the best fitness types of equipment on our site at every price point from fitness services, fitness centers, fitness equipment to workout tips, and more.</p>
                    </div>
                
                </div>
                
                <div class="box_items-gym row">
                    <div class="col-md-2 col-6 mb-3 pl-3 pr-3">
                        <div class="border-0 card shadow">
                            <a href="https://amzn.to/3iAhGiq">
                                <div class="card-media p-2 equipment">
                                    <img src="assets/images/equipment-cardio-cybex.jpg" class="card-img-top" alt="">
                                </div>
                                <div class="card-body border-top p-2 text-center e-title">
                                    <span class="d-block">Cardio Training</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2 col-6 mb-3 pl-3 pr-3">
                        <div class="border-0 card shadow">
                            <a href="https://amzn.to/3gVhQjR">
                                <div class="card-media p-2 equipment">
                                    <img src="assets/images/strength.jpg" class="card-img-top" alt="">
                                </div>
                                <div class="card-body border-top p-2 text-center e-title">
                                    <span class="d-block">Strength Training</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2 col-6 mb-3 pl-3 pr-3">
                        <div class="border-0 card shadow">
                            <a href="https://amzn.to/3kIg9Zl">
                                <div class="card-media p-2 equipment">
                                    <img src="assets/images/yoga.jpg" class="card-img-top" alt="">
                                </div>
                                <div class="card-body border-top p-2 text-center e-title">
                                    <span class="d-block">Yoga</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2 col-6 mb-3 pl-3 pr-3">
                        <div class="border-0 card shadow">
                            <a href="https://amzn.to/2Fl5dRs">
                                <div class="card-media p-2 equipment">
                                    <img src="assets/images/tech.jpg" class="card-img-top" alt="">
                                </div>
                                <div class="card-body border-top p-2 text-center e-title">
                                    <span class="d-block">Fitness Technology</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2 col-6 mb-3 pl-3 pr-3">
                        <div class="border-0 card shadow">
                            <a href="https://amzn.to/3axGrsZ">
                                <div class="card-media p-2 equipment">
                                    <img src="assets/images/clothing.jpg" class="card-img-top" alt="">
                                </div>
                                <div class="card-body border-top p-2 text-center e-title">
                                    <span class="d-block">Clothing</span>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2 col-6 mb-3 pl-3 pr-3">
                        <div class="border-0 card shadow">
                            <a href="https://amzn.to/3aiTL4e">
                                <div class="card-media p-2 equipment">
                                    <img src="assets/images/accessory.jpg" class="card-img-top" alt="">
                                </div>
                                <div class="card-body border-top p-2 text-center e-title">
                                    <span class="d-block">Accessories</span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="trendings_search" class="pt-4 pb-5">
    <div class="container align-content-center">
        <h4 class="text-uppercase mb-4 text-center">TRENDING SEARCHES</h4>
        <div class="row">

           @foreach($keywords as $value)
           <div class="col-md-3 px-3">
            <a href="{{route('v_keyword',['slug'=>$value['keyword_slug']])}}" title="{{ucwords($value->keyword)}}" class="d-block text-truncate">{{ucwords($value->keyword)}}</a>
        </div>
        @endforeach
    </div>
</section>
<section id="blogs" class="pt-4">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-uppercase text-center mb-4">LATEST BLOG POSTS</h4>
            </div>
            @foreach($posts as $index=>$post)
            <div class="col-md-3 px-3">
                <div class="card border-0 border-radius-0 mb-4 blog-home">
                    <div class="card-media blog-home-img">
                        <a href="{{route('blog',['slug'=>$post['slug']])}}">
                            <img src="{{$post['featured_img']}}"

                            alt="{!! ucwords($post['title']) !!}" class="card-img-top border-radius-0 ">
                        </a>
                    </div>
                    <div class="card-body pl-0 pr-0 pb-0">
                        <div class="d-flex blogs_title">
                            <div class="border-bottom align-items-center d-flex flex-2 flex-column justify-content-center mr-3 pl-2 pb-2">
                                <span class="d-block">{{$post->created_at->format('d')}}</span>
                                <span class="d-block">{{$post->created_at->format('M')}}</span>
                            </div>
                            <div class="border-bottom flex-8 pb-2 px-2 blog_title">
                                <a href="{{route('blog',['slug'=>$post['slug']])}}" class="text-uppercase card-title font-weight-bold">{!! ucwords($post['title']) !!}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>
<section id="find_one">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex p3 justify-content-center">
                    <div class="col-md-8 d-flex pl-0 justify-content-center">
                        <h6 class="text-uppercase text-white my-auto p-4 follow-us">Follow us</h6>
                        <form action="{{route('saveEmail')}}" method="POST" class="my-auto col-md-8">
                            @csrf
                            <div class="input-group my-auto">
                                <input type="text" class="form-control form-email" name="email" placeholder="Email Address">
                                <div class="input-group-append border-0">
                                    <button type="submit" class="input-group-text border-0 text-uppercase text-white button-footer">Submit</button>
                                </div>
                            </div>
                        </form>




                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

