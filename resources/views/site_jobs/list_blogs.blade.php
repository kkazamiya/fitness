@extends('site_jobs.layouts.job')
@section('title', 'New blogs')
@section('description', '')
@section('image', '')
@section('navbar')
@include('site_jobs.parts.navbar')
@endsection
@section('main_content')
<div class="devide"></div>
<section id="blogs" class="pt-3 pb-5">
    <div class="container">
        <div class="row">
            <div class="breadcrumbs col-md-12">
            <div class="content">
                <ol class="bg-transparent breadcrumb breadcrumb_list p-0 pb-3 pt-3 mb-0">
                    <li class="breadcrumb_item mr-2">
                        <a href="{{route('home')}}">Home&nbsp;<i class="fa fa-angle-right ml-2"></i></a>
                    </li>
                    <li class="breadcrumb_item">
                        <span class="current_item">
                            Blogs
                        </span>
                    </li>
                </ol>
            </div>
            <!-- @include('site_jobs.parts.form_mini')
            -->
        </div>
        <div class="col-md-12">
            <h4 class="text-uppercase text-center mb-4">BLOG POSTS</h4>
        </div>

        <div class="infinite-scroll row">
            @foreach($blogs as $value)
            <div class="col-md-4 pb-2">
                <div class="card border-0 border-radius-0 mb-4">
                    <div class="card-media blog-home-img">
                        <a href="#">
                            <img src="{{$value->featured_img}}" alt="" class="card-img-top border-radius-0" >
                        </a>
                    </div>
                    <div class="card-body px-0 pb-0">
                        <div class="d-flex blogs_title border-bottom">
                            <div class="align-items-center d-flex flex-2 flex-column justify-content-center pb-3">
                                <span class="d-block">{{$value->created_at->format('d')}}</span>
                                <span class="d-block">{{$value->created_at->format('M')}}</span>
                            </div>
                            <div class="flex-8 pl-3">
                                <a href="{{route('blog',['slug'=>$value['slug']])}}" class="text-uppercase card-title">{{$value->title}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            {{$blogs->links()}}
        </div>

    </div>
    <div class="text-center mt-3 alert" style="color: #00a19e;"><span>
        No more blogs.
    </span></div>
    <div class="mt-3 text-center mb-3">
        <a href="#" class="load_more badge-pill btn-outline-pjob bg-white">Load More
            Blogs&nbsp;<i class="fa fa-angle-double-down"></i></a>
        
    </div>
</section>
<section id="trendings_search">
    <div class="container">
        <div class="container">
            <h4 class="text-uppercase mb-4">TRENDING SEARCHES</h4>
            <div class="row">

               @foreach($related_search as $value)
               <div class="col-md-3">
                <ul class="pl-0 list-inline">

                    <li><a href="{{route('v_keyword',['slug'=>$value['keyword_slug']])}}" class="d-block keyword-blog" title="{{ucwords($value->keyword)}}">{{ucwords($value->keyword)}}</a></li>


                </ul>
            </div>
            @endforeach
        </div>
    </div>
</div>
</section>
<section id="find_one">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="d-flex p-3 justify-content-center">
                        <div class="align-items-center d-flex mr-3 text-white">NEED AN EXERCISE PROFESSIONAL TO HELP YOU TO EXERCISE RIGHT?</div>
                        <a href="#" class=" font-weight-bold text-uppercase">FIND ONE HERE</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('js')
<script>
    if (typeof $('.infinite-scroll') !== "undefined" && typeof $('ul.pagination') !== "undefined") {
        $('ul.pagination').hide();
        $('.alert').hide();
        $(function () {

            $('.load_more').click(function (e) {
                e.preventDefault();
                let next_link = $('ul.pagination').find('a[rel=next]').attr('href');
                $('ul.pagination').remove();
                $('.when-loading').show();
                if(next_link !== undefined){
                    $.get(next_link, function (data) {
                    $('.infinite-scroll').append(data);
                    $('.when-loading').hide();
                    $('ul.pagination').hide();
                });
                } else{
                  $('.alert').show();
                  $('.load_more').hide()
                }
                
                
            });
            
        });

    }
</script>
@endsection