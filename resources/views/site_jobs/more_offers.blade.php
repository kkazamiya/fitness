@extends('site_jobs.layouts.job')
@section('title')
Best Deals and Coupons Updated Every Day - {{$domain_info->domain}}
@endsection
@section('description', 'Search for the latest '.$domain_info->domain.'. Verified employers. Fast and easy way find a job of 1.000.000+ postings in big cities in USA.')

@section('navbar')
@include('site_jobs.parts.navbar')
@endsection
@section('main_content')

<style>
        .btn_close {
            right: 20px;
            z-index: 2;
            color: #d5d5d5;
        }

        .result-search > .twitter-typeahead {
            width: 90%;
        }

        @media (max-width: 767.99px) {
            .result-search > .twitter-typeahead {
                width: 85%;
            }
            .modal-cid-content {
                width: 90%!important;
            }
        }

        .search-inline.tt-hint, .search-inline.tt-input {
            height: 31px;
            max-width: 100% !important;
        }

        .tt-menu.tt-open {
            z-index: 9999 !important;
        }

        .badge-intitle {
            font-size: 11px;
            color: #009900;
            border: 1px solid #009900;
            border-radius: 3px;
            padding-right: 3px;
            padding-left: 3px;
            font-weight: 400;
        }

        .btn-group-sm > .btn, .btn-sm {
            font-size: .775rem;
            font-weight: 600;
        }

        .discount {
            font-size: 1.3rem !important;
            line-height: 1.3;
            white-space: nowrap;
            font-weight: 700;
            display: block;
            text-overflow: ellipsis;
            overflow-x: hidden;
        }

        .card-header {
            padding: .2rem;
        }

        .card-body {
            padding: .5rem;
        }
        .sticky-top{
            font-size: 12px;
        }
        .modal-cid {
    position: fixed;
    z-index: 1500;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    overflow: auto;
    background-color: #000;
    background-color: rgba(0,0,0,.4);
    }
    .modal-cid-content {
    background-color: #fefefe;
    margin: 5% auto;
    padding: 20px;
    border: 1px solid #888;
    width: 40%;
    }
    .blog-desc{
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
    margin-bottom: 10px;
    }      
    .fade-line {
  position: relative;
  height: 4.6em; /* exactly three lines */
}
.fade-line:after {
    content: "";
    text-align: right;
    position: absolute;
    bottom: 0;
    right: 0;
    width: 120px;
    height: 1.5em;
    background: linear-gradient(to right, rgba(255, 255, 255, 0), rgba(255, 255, 255, 1) 50%);
}
.more-text{
    position: absolute;
    cursor: pointer;
    color: #3490dc;
    left: auto;
    right: 10px;
    top: 110px;
    z-index: 1000;
}
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-stick bg-white">
                <div class="sticky-top py-2 px-0">
                    <ul class="list-inline mb-0 w-100">
                        <li class="list-inline-item bg-success p-1">
                            <img src="{{url('/assets')}}/images/hourglass-icon.svg" width="20px"/>
                        </li>
                        <li class="list-inline-item"><a class="text-muted text-xs-center x-small font-weight-bold"
                                                        href="{{Request::url().'?t=lastest'}}">Latest</a>
                        </li>
                        <li class="list-inline-item"><a class="text-muted text-xs-center x-small"
                                                        href="{{Request::url().'?t=today'}}">Today</a>
                        </li>
                        <li class="list-inline-item"><a class="text-muted text-xs-center x-small"
                                                        href="{{Request::url().'?t=3days'}}">3
                                days</a></li>
                        <li class="list-inline-item"><a class="text-muted text-xs-center x-small"
                                                        href="{{Request::url().'?t=7days'}}">7
                                days</a></li>
                    </ul>
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item bg-danger p-1">
                            <img src="{{url('/assets')}}/images/coupon-icon.svg" width="20px"/>
                        </li>
                        <li class="list-inline-item"><a class="text-muted text-xs-center x-small font-weight-bold"
                                                        href="{{Request::url().'?c=all'}}">Sorted by</a>
                        </li>
                        <li class="list-inline-item"><a class="text-muted text-xs-center x-small"
                                                        href="{{Request::url().'?c=az'}}">A-Z</a>
                        </li>
                        <li class="list-inline-item"><a class="text-muted text-xs-center x-small"
                                                        href="{{Request::url().'?c=za'}}">Z-A</a>
                        </li>
                        <li class="list-inline-item"><a class="text-muted text-xs-center x-small"
                                                        href="{{Request::url().'?c=relevance'}}">Most Relevance</a>
                        </li>
                    </ul>
                    <ul class="list-inline mb-0">
                        <li class="list-inline-item bg-primary p-1 ac"><img
                                src="{{url('/assets')}}/images/filter-icon.svg"
                                width="20px"/></li>
                        <li class="list-inline-item"><a class="text-muted text-xs-center x-small font-weight-bold"
                                                        href="{{Request::url().'?f=all'}}">All Language</a>
                        </li>
                        <li class="list-inline-item"><a class="text-muted text-xs-center x-small"
                                                        href="{{Request::url().'?f=english'}}">English</a></li>
                        <li class="list-inline-item"><a class="text-muted text-xs-center x-small"
                                                        href="{{Request::url().'?f=other'}}">Others</a></li>
                        
                    </ul>
                    <div class="my-2 text-center">

                    </div>
                </div>
            </div>
            <div class="col-md-9">
                
                <div class="row mt-2">
                    @isset($all_data)
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-12">
                                    @isset($search_key)
                                        <h1 class="text-primary" style="font-size: 1.3rem">Best Deals and Coupons Updated Every Day - {{$search_key}}</h1>
                                        <p>{{$domain_info->desc}}</p>
                                        <p>Visit site: <a class="text-primary" href="/v-result/{{$domain_info->id}}?ref=domain" target="_blank" rel="nofollow">{{$domain_info->domain}}</a></p>
                                    @endisset
                                    
                                </div>
                            </div>
                            <?php
                            $a_add = ['best', 'top', 'hot', 'new', 'tip', 'trend', 'great'];
                            $show_coupons = false;
                            ?>
                            @foreach($all_data as $index => $per_data)
                                @if($per_data && $per_data['snippet'] != '')
                                    <div class="col-12 px-0 grid__brick" 
                                         data-rank="{{$index}}"
                                         data-title="{{$per_data['title']}}" id="div-{{$per_data['_id']}}">
                                        <div class="btn_close position-absolute">
                                            <a data-id="{{$per_data['_id']}}" class="text-muted" href="#"><i
                                                    class="fa fa-times"></i></a>
                                        </div>
                                        <div class="coupon mb-4">
                                            <div class="row">
                                                <div class="col-9 pr-0 deal-desc">
                                                    <h2 class="title-result text-truncate w-100"
                                                        itemprop="name">
                                                        {{ucfirst($per_data['title'])}}
                                                    </h2>
                                                    <p class="my-2">
                                                        <span class="badge-intitle">{{$a_add[rand(0, count($a_add)-1)]}}</span>
                                                        <span class="mb-0 link-job text-primary" id="{{$per_data['_id']}}">{{$per_data['domain']}}</span>

                                                    </p>
                                                    <span class="snippet text-snippet mb-0 w-100 fade-line"
                                                       id="snippet-{{$per_data['_id']}}" style="font-size: 14px">{!!$per_data['snippet']!!}
                                                    </span>
                                                </div>
                                                <div class="col-9">
                                                    <p style="color: #007bff; font-size: 70%" class="mb-0">
                                                        <span class="mr-3">{{rand(50, 500)}} People Used</span><br/>
                                                        
                                                    </p>
                                                </div>
                                                <div class="col-md-3 col-6 ml-auto pl-4">
                                                    <a href="{{Request::url().'?cid='.$per_data['_id']}}"
                                                        rel="nofollow"
                                                       class="w-100 btn btn-sm btn btn-primary pt-1 pb-1 text-truncate">Get link
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                            {{$all_data->links()}}
                            <div class="col-12 my-3">
                            </div>
                            @isset($keyword)
                                @if(count($all_data) < 5)
                                    <p>
                                        <button class="btn btn-outline-info btn-block" id="load-more-result"
                                                data-keyword="{{$keyword['_id']}}">Refresh Load
                                        </button>
                                    </p>
                                @endif
                            @endisset

                        </div>
                    @endisset
                    <div class="col-md-4 pl-md-0">
                        <div class="row">
                            @if(isset($keyword['also_ask']) && count($keyword['also_ask']) > 0)
                                <div class="col-12 mb-3">
                                    <h3 class="text-primary">People Also Ask</h3>
                                    <div id="accordion">
                                        @foreach($keyword['also_ask'] as $index => $ask)
                                            <div class="card">
                                                <div class="card-header" id="heading-{{$ask['_id']}}">
                                                    <h5 class="mb-0">
                                                        <button class="btn btn-link" data-toggle="collapse"
                                                                data-target="#collapse-{{$ask['_id']}}"
                                                                @if($index == 0) aria-expanded="true"
                                                                @else aria-expanded="false" @endif
                                                                aria-controls="collapse-{{$ask['_id']}}">
                                                            {{$ask['question']}}
                                                        </button>
                                                    </h5>
                                                </div>

                                                <div id="collapse-{{$ask['_id']}}"
                                                     @if($index == 0) class="collapse show"
                                                     @else class="collapse" @endif
                                                     aria-labelledby="heading-{{$ask['_id']}}"
                                                     data-parent="#accordion">
                                                    <div class="card-body snippet">{!! $ask['aswser'] !!}. <a
                                                            style="font-size: 13px" href="#" class="read-more-faq"
                                                            data-href="{{route('post_faq',['id_kw'=>$keyword['_id'],'id_q'=>$ask['_id']])}}">Read
                                                            more ››</a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                            @if(isset($related_kw_top))
                                <div class="col-12 mb-3">
                                    <h3 class="text-primary">Related Searches</h3>
                                    <div class="p-brands">
                                        @foreach($related_kw_top as $related_kw)
                                            <p class="text-truncate link mb-2">
                                                › <a href="/{{$related_kw['keyword_slug']}}" target="_blank"
                                                     title="{{$related_kw['keyword']}}">{{ucfirst($related_kw['keyword'])}}</a>
                                            </p>
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                            <!--  -->
                           <div class="col-12">
                                <h3 class="text-primary">Popular Searching</h3>
                                <div class="p-brands">
                                    @foreach($keyword_top as $popular_kw)
                                        <p class="text-truncate link mb-2">› <a
                                                href="/{{$popular_kw['keyword_slug']}}" target="_blank"
                                                title="{{$popular_kw['keyword']}}">{{ucfirst($popular_kw['keyword'])}}</a>
                                        </p>
                                    @endforeach
                                </div>
                            </div>
                            @if($posts)
                                <div class="col-12">
                                    <h3 class="text-primary">Saving Tips</h3>
                                    @foreach($posts as $index => $blog)
                                        <div class="blog-box p-0 mb-2 border w-100">
                                            <div class="d-flex justify-content-center">
                                                <img data-original="{{$blog['featured_img']}}"
                                                     src="/files/5d64e1fea22d142284003812/img_loading.svg"
                                                     alt="{{$blog['inform_title']}}" class="card-img-top lazyload">
                                            </div>
                                            <div class="p-2">
                                                <h4 class="blog-title">
                                                    <a href="/blog/{{$blog['slug']}}">{{$blog['title']}}</a>
                                                </h4>
                                                <div class="blog-desc">{{strip_tags($blog['content'])}}</div>
                                                <a href="/blog/{{$blog['slug']}}"
                                                   class="w-100 btn btn-sm btn btn-primary pt-1 pb-1 btn-info text-truncate">Read More</a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section id="find_one">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="d-flex p3 justify-content-center">
                    <div class="col-md-8 d-flex pl-0 justify-content-center">
                        <h6 class="text-uppercase text-white my-auto p-4 follow-us">Follow us</h6>
                        <form action="{{route('saveEmail')}}" method="POST" class="my-auto col-md-8">
                            @csrf
                            <div class="input-group my-auto">
                                <input type="text" class="form-control form-email" name="email" placeholder="Email Address">
                                <div class="input-group-append border-0">
                                    <button type="submit" class="input-group-text border-0 text-uppercase text-white button-footer">Submit</button>
                                </div>
                            </div>
                        </form>




                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@if($html_modal)
        {!! $html_modal !!}
    @endif
@endsection
@section('js')
    <script defer>
        function progress(timeleft, timetotal, $element) {
            $element.show();
            let progressBarWidth = timeleft * $element.width() / timetotal;
            $element.find('div').animate({width: progressBarWidth}, timeleft === timetotal ? 0 : 1000, 'linear').html("›› We are collecting the latest data for you. Please wait for " + timeleft + " seconds");
            if (timeleft < timetotal) {
                setTimeout(function () {
                    progress(timeleft + 1, timetotal, $element);
                }, 1000);
            }
        };

        function getParameterByName(name, url) {

            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        }

        $(document).ready(async function () {

            $(".view-job").click(function (e) {
            if ($(this).hasClass('view-job')) {
                e.preventDefault();
            }
            let host_link = $(this).data("link"), id_link = $(this).data("id");
            let a_href = '<a class="text-primary" href="/v-result/' + id_link + '" target="_blank" rel="nofollow">' + host_link + '</a>';
            console.log(a_href);
            $('#' + id_link).html(a_href).show();
            $(this).removeClass('text-white')
            .removeClass('view-job')
            .attr('href', '/v-result/' + id_link)
            .attr('target', '_blank');
        });
            
            $(".click-show").click(function () {
                let id_show = $(this).data("id-link") + "-hide";
                let id_hide = $(this).data("id-link") + "-showing";
                $("#" + id_hide).remove();
                $("#" + id_show).show();
            });
            $('header').removeClass('sticky-top');
            $('.text-snippet').each(function () {
                if ($(this).text().length > 260) {
                    console.log($(this).text().length);
                    let id = $(this).attr('id');
                    $(this).after('<span class="more-text" style="cursor: pointer;color: #3490dc;" data-snippet="' + id + '">More ›</span>')
                }
            });
            $('.btn_close a').click(function (e) {
                e.preventDefault();
                let id_hide = $(this).data('id');
                console.log(id_hide);
                $('#div-' + id_hide).hide('slow');
            });
            $('.more-text').on('click', function () {
                let id = $(this).data('snippet');
                if ($('#' + id).hasClass('text-snippet')) {
                    $(this).text('‹ Less');
                    $(this).css('position', 'unset');
                    $('#' + id).removeClass('text-snippet');
                    $('#' + id).removeClass('fade-line');
                } else {
                    $(this).text('More ›');
                    $('#' + id).addClass('text-snippet');
                    $('#' + id).addClass('fade-line');
                    $(this).css('position', 'absolute');
                }
            });
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $('.read-more-faq').click(function (e) {
                e.preventDefault();
                progress(1, 8, $('#progressBar'));
                let post_url = $(this).data("href");
                $.post({
                    type: "POST",
                    url: post_url
                }).done(function (data) {
                    if (data.code === 1) { 
                        window.location.href = data.msg;
                    }
                });
            });
            $(".kw_related").click(function (e) {
                e.preventDefault();
                progress(1, 8, $('#progressBar'));
                let post_url = $(this).attr("href");
                $.post({
                    type: "POST",
                    url: post_url
                }).done(function (data) {
                    if (data.code === 1) {
                        window.location.href = data.msg;
                    }
                });
            });
            if (typeof $('#modal_out') !== "undefined") {
                var cid = getParameterByName('cid');
                $('#close-modal').click(function () {
                    $('#modal_out').hide();
                });
                setTimeout(function () {
                    $('html, body').animate({
                        scrollTop: $("#" + cid).offset().top + 100
                    }, 'slow');
                }, 1000)
            }
        });
    </script>
@endsection

