<section id="content_top">
        <div class="container">
            <div class="row">
                <div class="col-md-12" >
                    <h3 class="text-center pt-5 mt-2 pb-4 text-white lead-text">Accompany Us To Master A Healthier Life</h3>
                    <div class="col-md-6 offset-md-3 mt-4 mb-5 pr-0">
                        <form action="{{route('search')}}" method="GET">
                             @csrf
                        <div class="input-group">
                           
                            <input type="text" class="form-control  type-search" placeholder="Search for workout ideas, sports gear, fitness equipment and more" name="q" autocomplete="off" >
                            <div class="input-group-append">
                              <span class="input-group-text bg-white">
                                <button type="submit" class="btn fa fa-search text-danger"></button> 
                              </span>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="box-slider_logo mt-4 pb-5">
                        <div class="owl-carousel owl-theme d-flex owl-loaded owl-drag" id="apps-slider">
                            
                            
                            
                        <div class="owl-stage-outer">
                            <div class="owl-stage" style="transform: translate3d(-1129px, 0px, 0px); transition: all 0s ease 0s; width: 2569px; height: 100%">
                                @foreach($result as $value)
                                <div class="owl-item" style="width: 82.727px; margin-right: 20px; height: 100%;padding-top: 0.75rem;">
                                    @foreach($value as $store)
                                    <a href="{{route('stores',['slug'=>$store->slug_store])}}" style="max-width: 100%;max-height: 100%">
                                    <div class="d-flex position-relative align-items-center item-apps pl-2 pr-2 mb-2 bg-white">
                                
                                    <img src="{{$store->store_image}}" alt="" style="max-width: 100%;max-height: 100%">
                                
                                    </div>
                                    </a>
                            @endforeach
                            </div>
                                @endforeach
                            </div></div><div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"><i class="fa fa-angle-left fa-3x text-white"></i></button><button type="button" role="presentation" class="owl-next"><i class="fa fa-angle-right fa-3x text-white"></i></button></div><div class="owl-dots disabled"></div></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-content__foot">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="p-4">
                            <p class="mb-0 text-white text-center">We aim to be the first stop for people on their journey of improving their bodies with training. We don't train them, we bring them anything they need to have the best effectiveness in fitness training. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>