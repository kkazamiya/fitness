<div class="search pt-3 ml-auto">
 	<form action="{{route('search')}}" method="GET">
 		@csrf
 		<div class="input-group">

 			<input type="text" class="form-control  type-search" placeholder="Search for workout ideas, sports gear, fitness equipment and more" name="q" autocomplete="off" >
 			<div class="input-group-append">
 				<span class="input-group-text bg-white">
 					<button type="submit" class="btn fa fa-search text-danger"></button> 
 				</span>
 			</div>
 		</div>
 	</form>
</div>