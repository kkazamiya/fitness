<section id="header" class="bg-white w-100 sticky">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <nav class="navbar navbar-expand-lg navbar-light h-100">
                    <div class="box__logo mr-5">
                        <a href="{{route('home')}}"><img src="{{url('/assets')}}/images/fitnessol-logo.png" alt=""></a>
                    </div>
                    <div class="button-nav">
                        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    </div>
                    
                   
                    <div class="mr-0 collapse" id="navbarNav">
                        
                        <div class="p-4 nav-icon ml-auto"> 
                            <ul class="navbar-nav  ml-auto">
                               <?php
                               if (isset($seo_config['entity_socials'])) {
                                $entity_socials = explode('||', trim($seo_config['entity_socials']));
                                ?>
                                <ul class="d-flex justify-content-between list-inline social-links">
                                    @foreach($entity_socials as $social)
                                    <?php $social = explode('|', $social);?>
                                    <li><a href="{{$social[1]}}" class=" p-2"><i class="fa fa-2x {{$social[0]}}"></i></a></li>
                                    @endforeach
                                </ul>
                                <?php }?> 
                            </ul>
                        </div>
                    </div>


                
            </nav>
        </div>
    </div>
</div>
</div>
</div>
</section>
