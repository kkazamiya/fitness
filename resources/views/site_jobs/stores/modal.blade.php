@if($result_modal)
    <style>
        /* The Modal (background) */
        .modal-cid {
            position: fixed; /* Stay in place */
            z-index: 1500; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0, 0, 0); /* Fallback color */
            background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        .modal-cid-content {
            background-color: #fefefe;
            margin: 5% auto;
            padding: 20px;
            border: 1px solid #888;
            width: 40%;
        }

        @media (max-width: 767.99px) {
            .modal-cid-content {
                width: 90%;
            }
        }

        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: black;
            text-decoration: none;
            cursor: pointer;
        }
    </style>
    <div class="modal-cid" id="modal_out">
        <div class="modal-cid-content">
            <span class="close" id="close-modal">&times;</span>
            <div class="row align-items-center">
                <div class="col-4">
                    <img class="lazyload img-fluid"
                         data-original="{{img_sync($store_info['store_image'])}}"
                         src="{{asset("/files/5d64e1fea22d142284003812/fitnessol-loading.svg")}}"
                         title="{{$store_info['store_name']}} Coupons"
                         alt="{{$store_info['store_name']}} Coupons and Promo Code">
                </div>
                <div class="col-8">
                    <p class="modal-title" id="modal_title">{{str_title($result_modal['coupon_name'])}}</p>
                </div>
            </div>
            
            @if($result_modal['code_type'] == 'code')
                <p class="text-center">Copy the code and go to <a class="text-success" id="store_domain"
                                                                  href="@if($store_info['store_aff'] != '') {{$store_info['store_aff']}} @else {{$store_info['store_domain']}} @endif"
                                                                  rel="nofollow"
                                                                  target="_blank">{{$store_info['store_domain']}}</a>
                </p> 
                <div class="input-group mb-3 cp-code" id="modal_show_code">
                    <input type="text" id="modal_input" class="form-control" value="{{$result_modal['coupon_code']}}"
                           readonly="">
                    <div class="input-group-append">
                        <button class="btn btn-outline-success btn-copy px-2"
                                data-clipboard-target="#modal_input"
                                id="copy_code" type="button" data-placement="bottom" title="">
                            Tap To Copy
                        </button>
                    </div>
                </div>
            @else
                <div class="input-group mb-12 cp-code" id="modal_show_deal">
                    <a class="btn btn-block btn-success"
                       href="@if($store_info['store_aff'] != '') {{$store_info['store_aff']}} @else {{$store_info['store_domain']}} @endif"
                       id="go_deal" rel="nofollow" target="_blank">Get
                        Deal</a>
                </div>
            @endif
            <p class="text-center"><a class="text-success" id="link-store"
                                      href="{{route('stores',['store_slug'=>$store_info['slug_store']])}}">More {{$store_info['store_name']}}
                    Coupons
                    &gt;&gt;</a></p>
            <div class="row my-2">
               
            </div>
            @isset($random_stores)
                <h3 class="text-dark size-large text-center">You may also like</h3>
                <div class="row">
                    @foreach($random_stores as $random_store)
                        <div class="col-md-6 text-truncate link">
                            › <a class="small text-dark"
                                 href="{{route('stores',['store_slug'=>$random_store['slug_store']])}}" target="_blank"
                                 title="Go store {{$random_store['store_name']}}">{{ucfirst($random_store['store_name'])}}</a>
                        </div>
                    @endforeach
                </div>
            @endisset
            @if(isset($related_kw_top))
                <h3 class="text-dark size-large text-center">You may also like</h3>
                <div class="row">
                    @foreach($related_kw_top as $related_kw)
                        <div class="col-md-6 text-truncate link">
                            › <a class="small text-dark" href="/{{$related_kw['keyword_slug']}}" target="_blank"
                                 title="{{$related_kw['keyword']}}">{{ucfirst($related_kw['keyword'])}}</a>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
    </div>
@endif
