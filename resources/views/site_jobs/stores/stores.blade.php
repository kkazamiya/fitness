@extends('site_jobs.layouts.job')
@section('title', seo_optimize($seo_config,'title_stores',$store_info))
@section('navbar')
@include('site_jobs.parts.navbar')
@endsection
<?php
if (isset($all_coupons[0]['coupon_name'])) {
    $desc = seo_optimize($seo_config, 'excerpt_stores', $store_info, $all_coupons[0]['coupon_name']);
} else {
    $desc = seo_optimize($seo_config, 'excerpt_stores', $store_info, 'Enjoy');
}
$afs = 2;
$adsense = 0;
$max_adsense = 4;
$visibility = 0;
$space_show = 3;
$id_ngang = ['9931065148', '3481206861', '5908253177', '2735294924'];
if ($store_info['store_aff'] != '') {
    $link_aff = $store_info['store_aff'];
} else {
    $link_aff = $store_info['store_domain'];
}
?> 
@section('description', $desc)
@section('image', img_sync($store_info['store_image']))
@section('schema')
    <script type="application/ld+json">
    {"@context":"http://schema.org",
    "@type":"Store",
    "name":"{{$store_info['store_name']}}",
    "logo":"{{$store_info['store_image']}}",
    "url":"{{$store_info['store_domain']}}",
    "description":"{{$desc}}",
    "sameAs":"{{$store_info['store_domain']}}",
    "image":"{{img_sync($store_info['store_image'])}}",
    "aggregateRating":
        {"@type":"AggregateRating","ratingValue":"{{$rate_star["avg_rate"]}}
        ","worstRating":1,"bestRating":5,"ratingCount":{{$rate_star["count_rate"]}}}
    }

    </script>
@endsection
@section('main_content')
    <?php echo '<script>var search_kw = "' . $store_info['store_name'] . '";</script>';?>
    <style>
        .badge {
            padding: .1em .2em;
        }

        .badge-success {
            background: transparent;
            border: 1px solid #1261a0;
            color: #1261a0;
        }

        .discount {
            font-size: 1.3rem !important;
            line-height: 1.3;
            white-space: nowrap;
            font-weight: 700;
            display: block;
            text-overflow: ellipsis;
            overflow-x: hidden;
        }
        .blog-desc{
            font-size: 13px;
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
        }
    </style>
    <div class="container-fluid py-3">
        <div class="owl-carousel owl-theme">
            @foreach($brand_top as $key => $brand)
                <div class="item col-md mb-2 px-1">
                    <a href="/store/{{$brand->slug_store}}"
                       title="{{$brand['store_name']}} Coupons and Promo Code">
                        <div class="square d-flex align-items-center border border-gray">
                            <img class="img-thumbnail border-0 align-middle align-v w-100 lazyload"
                                 data-original="{{img_sync($brand['store_image'])}}"
                                 src="/files/5d64e1fea22d142284003812/img_loading.svg"
                                 alt="{{$brand['store_name']}} Coupons and Promo Code">
                        </div>
                    </a>
                    <p class="p-0 text-center">
                        <small><span class="text-danger">{{rand(10,50)}}</span> Coupons</small>
                    </p>
                </div>
            @endforeach
        </div>
    </div>
    <div class="container mt-3">
        <main>
            <div class="row">
                <div class="col-md-3 bi px-0">
                    <div class="shadow-sm bp bg-white">
                        <div itemscope="" itemtype="http://schema.org/Store">
                            <meta itemprop="name" content="{{ucwords($store_info['store_name'])}}">
                            <meta itemprop="image" content="{{img_sync($store_info['store_image'])}}">
                            <div class="logo">
                                <img class="lazyload" width="80%"
                                     data-original="{{img_sync($store_info['store_image'])}}"
                                     src="{{asset("/files/5d64e1fea22d142284003812/img_loading.svg")}}"
                                     title="{{$store_info['store_name']}} Coupons"
                                     alt="{{$store_info['store_name']}} Coupons and Promo Code">
                                <noscript>
                                    <img width="80%" src="{{img_sync($store_info['store_image'])}}"
                                         title="{{$store_info['store_name']}} Coupons"
                                         alt="{{$store_info['store_name']}} Coupons and Promo Code">
                                </noscript>
                            </div>
                            <p class="text-center store-domain mb-0">
                                <i class="fa fa-external-link"></i>
                                <a href="{{$link_aff}}"
                                   title="{{$store_info['store_name']}} Coupons"
                                   rel="nofollow noopener">{{$store_info['store_domain']}}</a>
                            </p>
                            <div class="div-about-merchant">
                                <table border="0" cellpadding="0" cellspacing="0" class="about-merchant-stats my-2">
                                    <tbody>
                                    <tr>
                                        <td>Total Offers</td>
                                        <td class="cp-r">{{$count_coupon}}</td>
                                    </tr>
                                    <tr>
                                        <td>Coupon Codes</td>
                                        <td class="cp-r">{{$about_merchant['codes']}}</td>
                                    </tr>
                                    <tr>
                                        <td>Deals Coupons</td>
                                        <td class="cp-r">{{$about_merchant['deals']}}</td>
                                    </tr>
                                    @if($seo_config['top_off'] > 0)
                                        <tr>
                                            <td>Best Discount</td>
                                            <td class="cp-r">{{$seo_config['top_off']}}% OFF</td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <div class="vote pb-3">
                                <div class="rate-yo jq-ry-container js-btn-rate"
                                     data-rating="{{$rate_star["avg_rate"]}}"
                                     data-star-width="23px"
                                     readonly="readonly" style="width: 175px;" data-toggle="modal"
                                     data-target="#modal-brand-vote"
                                     data-cookie-key="{{$store_info['slug_store']}}-vote"
                                     data-website="{{$store_info['slug_store']}}">
                                    <div class="jq-ry-group-wrapper">
                                        <div class="jq-ry-normal-group jq-ry-group">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23px" height="23px"
                                                 viewBox="0 0 24 24" fill="#CCCCCC">
                                                <path
                                                    d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"></path>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23px" height="23px"
                                                 viewBox="0 0 24 24" fill="#CCCCCC" style="margin-left: 5px;">
                                                <path
                                                    d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"></path>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23px" height="23px"
                                                 viewBox="0 0 24 24" fill="#CCCCCC" style="margin-left: 5px;">
                                                <path
                                                    d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"></path>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23px" height="23px"
                                                 viewBox="0 0 24 24" fill="#CCCCCC" style="margin-left: 5px;">
                                                <path
                                                    d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"></path>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23px" height="23px"
                                                 viewBox="0 0 24 24" fill="#CCCCCC" style="margin-left: 5px;">
                                                <path
                                                    d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"></path>
                                            </svg>
                                        </div>
                                        <div class="jq-ry-rated-group jq-ry-group"
                                             style="width: {{$rate_star["avg_rate"] * 20}}%;">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23px" height="23px"
                                                 viewBox="0 0 24 24" fill="#F5A12A">
                                                <path
                                                    d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"></path>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23px" height="23px"
                                                 viewBox="0 0 24 24" fill="#F5A12A" style="margin-left: 5px;">
                                                <path
                                                    d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"></path>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23px" height="23px"
                                                 viewBox="0 0 24 24" fill="#F5A12A" style="margin-left: 5px;">
                                                <path
                                                    d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"></path>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23px" height="23px"
                                                 viewBox="0 0 24 24" fill="#F5A12A" style="margin-left: 5px;">
                                                <path
                                                    d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"></path>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23px" height="23px"
                                                 viewBox="0 0 24 24" fill="#F5A12A" style="margin-left: 5px;">
                                                <path
                                                    d="M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z"></path>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center"> 
                                    <span itemprop="ratingValue" class="mb-1">{{$rate_star["avg_rate"]}}</span>
                                    <span>/</span>
                                    <span itemprop="ratingCount" class="mb-1">{{$rate_star["count_rate"]}}</span>
                                    <span> votes </span>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="shadow-sm lp text-center bg-white">
                        <div class="w-75">
                            <ins class="adsbygoogle"
                                 style="display:block"
                                 data-ad-client="ca-pub-3943650398586158"
                                 data-ad-slot="5892489070"
                                 data-ad-format="auto"
                                 data-full-width-responsive="true"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({
                                    google_ad_client: "ca-pub-3943650398586158",
                                    enable_page_level_ads: true,
                                    overlays: {bottom: true}
                                });
                            </script>
                        </div>
                    </div>
                    <div class="shadow-sm lp bg-white">
                        
                    </div>
                    <div class="shadow-sm lp bg-white">
                        <p class="p-3 mb-0 font-weight-bold" style="font-size: 15px;">How to
                            use {{$store_info['store_name']}} coupons</p>
                        <div class="border-top links" style="font-size:14px;">
                            <span class="badge badge-success">1</span> - Click to the <strong>Show Code</strong> or
                            <strong>Get Deal</strong> to copy the code.
                            <br/><br/>
                            <span class="badge badge-success">2</span> - Go to <a
                                href="{{$link_aff}}"
                                title="{{$store_info['store_name']}} Coupons"
                                rel="nofollow noopener">{{$store_info['store_domain']}}</a> Website and remember to
                            paste the coupon code copied to the discount/ coupon box at check
                            out page
                            <br/><br/>
                            <span class="badge badge-success">3</span> - Wait for the discount and Enjoy your order
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="shadow-sm lp bg-white">
                        <div class="w-100">
                            <div class="box-afs" id="afscontainer1"></div>
                        </div>
                    </div>
                    @if($rand_blogs)
                        <div class="shadow-sm lp bg-white">
                            <p class="p-3 mb-0 font-weight-bold border-bottom" style="font-size: 15px;">Saving Tips</p>
                            @foreach($rand_blogs as $index => $blog)
                                <div class="blog-box p-0 mb-2 border-0 w-100">
                                    <div class="d-flex justify-content-center">
                                        <img data-original="{{$blog['featured_img']}}"
                                             src="/files/5d64e1fea22d142284003812/img_loading.svg"
                                             alt="{{$blog['title']}}" class="card-img-top lazyload">
                                    </div>
                                    <div class="p-2">
                                        <h5 class="blog-title">
                                            <a href="/blog/{{$blog['slug']}}">{{$blog['title']}}</a>
                                        </h5>
                                        <p class="blog-desc">{{str_replace('&nbsp;','',strip_tags($blog['content']))}}</p>
                                        <a href="/blog/{{$blog['slug']}}"
                                           class="btn btn-block btn-outline-info btn-sm btn-read-more">Read More</a>
                                    </div>
                                </div>
                               
                            @endforeach
                        </div>
                    @endif
                    <div class="shadow-sm lp bg-white">
                        <p class="p-3 mb-0 font-weight-bold" style="font-size: 15px;">Tips to try
                            when {{$store_info['store_name']}} Coupon Code didn't work</p>
                        <div class="border-top links" style="font-size:14px;">
                            <span class="text-primary"><i class="fa fa-user"></i></span> Remove the
                            <strong>Space</strong> after the code: Some coupons only work with exact code.<br/><br/>
                            <span class="text-primary"><i
                                    class="fa fa-user"></i></span> Check the products you're seeking and make
                            sure the code you take fit with the products you chose. Some coupon codes will only be valid
                            for certain products/ item<br/><br/>
                            <span class="text-primary"><i
                                    class="fa fa-user"></i></span><span> Some of the coupon codes are provided by our users and it may only be used for special products or events. You can contact us to have the best support for this</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="shadow-sm lp bg-white">
                        <p class="p-3 mb-0 font-weight-bold" style="font-size: 15px;">Why I should find
                            <strong>{{$store_info['store_name']}}</strong> coupon codes on Fitnessol.com</p>
                        <div class="border-top links" style="font-size:14px;">
                            <span class="text-primary"><i class="fa fa-check-circle"></i></span> All the coupon codes on
                            this website are checked with an automated system combined with our dedicated team<br/><br/>
                            <span class="text-primary"><i class="fa fa-check-circle"></i></span> Latest Coupons,
                            discounts and vouchers will be updated automatically and regularly <br/><br/>
                            <span class="text-primary"><i class="fa fa-check-circle"></i></span> Friendly interface for
                            coupon seekers <br/><br/>
                            <span class="text-primary"><i class="fa fa-check-circle"></i></span> Fast, simple and
                            effecitve <br/><br/>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <section class="col-md-9">
                    <h1>{{ucwords($store_info['store_name'])}} Coupons</h1>
                    
                    @php
                        if((count($all_coupons) - 1) < 4){
                            $show = count($all_coupons);
                        } else{
                            $show = 4;
                        }
                    @endphp
                    @foreach($all_coupons as $item)
                        <?php
                        if (isset($item['aff_link']) && $item['aff_link'] != '') $coupon_aff = $item['aff_link'];
                        ?>
                        <div class="shadow-sm rounded coupon mb-2"
                             id="{{$item['coupon_slug']}}" @if(isset($coupon_aff)) data-aff="{{$coupon_aff}}"
                             @else data-aff="{{$link_aff}}" @endif>
                            <div class="row d-flex align-items-center">
                                <div class="col-9 deal-desc">
                                    <h2 class="title">
                                        <a class="a_code" href="{{Request::url().'?cid='.$item['coupon_slug']}}"
                                           @if(isset($coupon_aff)) data-aff="{{$coupon_aff}}"
                                           @else data-aff="{{$link_aff}}" @endif
                                           rel="nofollow">{{str_title($item['coupon_name'])}}</a>
                                    </h2>
                                    @if($item['code_status'] == 2)
                                        <p class="mb-2 text-truncate"><span class="badge badge-success">Verified</span>
                                            <a class="text-primary a_code"
                                               @if(isset($coupon_aff)) data-aff="{{$coupon_aff}}"
                                               @else data-aff="{{$link_aff}}" @endif
                                               href="{{Request::url().'?cid='.$item['coupon_slug']}}">{{Request::url()."?cid=".$item['coupon_slug']}}</a>
                                        </p>
                                    @else
                                        <p class="mb-2 text-truncate"><span class="badge badge-success">
                                                @if($item['code_type'] == 'code')
                                                    Code
                                                @else
                                                    Deal
                                                @endif
                                            </span> <a
                                                class="text-primary a_code"
                                                @if(isset($coupon_aff)) data-aff="{{$coupon_aff}}"
                                                @else data-aff="{{$link_aff}}" @endif
                                                href="{{Request::url().'?cid='.$item['coupon_slug']}}">{{Request::url()."?cid=".$item['coupon_slug']}}</a>
                                        </p>
                                    @endif
                                    <p class="subtitle text-snippet mb-1">{{$item['coupon_excerpt']}}</p>
                                    <p class="subtitle text-snippet mb-2"><i class="fa fa-user-circle"></i>
                                        {{rand(50, 200)}} People Used
                                        @isset($item['expired_date'])
                                            @if($item['expired_date'] != '')
                                                <span class="expire text-muted"
                                                      data-expired="{{$item['expired_date']}}">Expire
                                                        Date:
                                                            @php
                                                                try{
                                                                echo Carbon\Carbon::parse(intval($item['expired_date']))->format('Y-m-d');
                                                                } catch (Exception $e) {
                                                                echo date('d-m-Y', $item['expired_date']);
                                                                }
                                                            @endphp
                                                    </span>
                                            @endif
                                        @endisset
                                    </p>
                                </div>
                                <div class="col-3 pl-0 d-flex justify-content-center align-items-center">
                                    @if($item['off_sale'] != 0)
                                        @if($item['off_type'] == 'percent')
                                            <div class="discount">{{$item['off_sale']}}% Off</div>
                                        @else
                                            <div class="discount">${{$item['off_sale']}} Off</div>
                                        @endif
                                    @endif
                                </div>
                                <div class="col-9"></div>
                                <div class="col-md-3 d-flex justify-content-end pr-md-4">
                                    @if($item['code_type'] == 'code' && $item['coupon_code'] != "")
                                        <div class="show-code">
                                            <a id="a-{{$item['coupon_slug']}}"
                                               class="btn h-btn get-deal-btn a_code"
                                               @if(isset($coupon_aff)) data-aff="{{$coupon_aff}}"
                                               @else data-aff="{{$link_aff}}" @endif
                                               href="{{Request::url().'?cid='.$item['coupon_slug']}}"
                                               rel="nofollow">
                                                <p id="code-{{$item['coupon_slug']}}">{{$item['coupon_code']}}</p>
                                                <span>Show Code</span>
                                            </a>
                                        </div>
                                    @else
                                        <div class="get-deal">
                                            <a id="a-{{$item['coupon_slug']}}"
                                               @if(isset($coupon_aff)) data-aff="{{$coupon_aff}}"
                                               @else data-aff="{{$link_aff}}" @endif
                                               href="{{Request::url().'?cid='.$item['coupon_slug']}}"
                                               rel="nofollow">
                                                Get Deal
                                            </a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <?php
                        $visibility++;
                        if ($afs <= 3) {
                            if ($visibility == 1 || $visibility % $space_show === 0) {
                                echo '<div class="shadow-sm mb-2"><div class="box-afs" id="afscontainer' . $afs . '"></div></div>';
                                $afs++;
                            }
                        } else {
                        if ($adsense < $max_adsense && ($visibility % $space_show) === 0) {
                        ?>
                        

                        <?php
                        $adsense++;
                        }
                        }
                        ?>
                        @if($visibility == $show && isset($random_stores))
                            <div class="my-2">
                                
                            </div>
                            <div class="my-2">
                                <div class="row">
                                    <div class="col-6">
                                        <h3>New Stores</h3>
                                        <div class="row new-store">
                                            @foreach($random_stores as $random_store)
                                                <div class="col-6 links">
                                                    <p>
                                                        <a href="/store/{{$random_store['slug_store']}}">
                                                            {{$random_store['store_name']}}
                                                        </a>
                                                    </p>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <h3>Relevant Keywords</h3>
                                        <div class="row new-store">
                                            @foreach($random_keywords as $random_keyword)
                                                <div class="col-6 links text-truncate">
                                                    <p>
                                                        <a href="/{{$random_keyword['slug']}}">
                                                            {{$random_keyword['keyword']}}
                                                        </a>
                                                    </p>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                    @if(count($expired_coupons) > 0 )
                        <h3>Expired Coupons</h3>
                        @foreach($expired_coupons as $expired_coupon)
                            <?php
                            $visibility++;
                            if ($afs <= 3) {
                                if ($visibility % $space_show === 0) {
                                    echo '<div class="shadow-sm mb-2"><div class="box-afs" id="afscontainer' . $afs . '"></div></div>';
                                    $afs++;
                                }
                            } else {
                            if ($adsense < $max_adsense && $visibility % $space_show === 0) {
                            ?>
                            
                            <?php
                            $adsense++;
                            }
                            }
                            ?>
                            <div class="shadow-sm rounded coupon mb-2"
                                 id="{{$expired_coupon['coupon_slug']}}" data-aff="{{$link_aff}}">
                                <div class="row d-flex align-items-center">
                                    <div class="col-9 deal-desc">
                                        <h2 class="title">
                                            <a class="a_code" data-aff="{{$link_aff}}"
                                               href="{{Request::url().'?r=e&cid='.$expired_coupon['coupon_slug']}}"
                                               rel="nofollow">{{str_title($expired_coupon['coupon_name'])}}</a>
                                        </h2>
                                        @if($expired_coupon['code_status'] == 2)
                                            <p class="mb-2 text-truncate"><span
                                                    class="badge badge-success">Verified</span>
                                                <a
                                                    class="text-primary a_code" data-aff="{{$link_aff}}"
                                                    href="{{Request::url().'?r=e&cid='.$expired_coupon['coupon_slug']}}">{{Request::url()."?cid=".$expired_coupon['coupon_slug']}}</a>
                                            </p>
                                        @else
                                            <p class="mb-2 text-truncate"><span class="badge badge-success">
                                                @if($expired_coupon['code_type'] == 'code')
                                                        Code
                                                    @else
                                                        Deal
                                                    @endif
                                            </span> <a
                                                    class="text-primary a_code" data-aff="{{$link_aff}}"
                                                    href="{{Request::url().'?r=e&cid='.$expired_coupon['coupon_slug']}}">{{Request::url()."?cid=".$expired_coupon['coupon_slug']}}</a>
                                            </p>
                                        @endif
                                        <p class="subtitle text-snippet mb-1">{{$expired_coupon['coupon_excerpt']}}</p>
                                        <p class="subtitle text-snippet mb-2"><i class="fa fa-user-circle"></i>
                                            {{rand(50, 200)}} People Used @isset($expired_coupon['expired_date'])
                                                @if($expired_coupon['expired_date'] != '')
                                                    <span class="expire text-muted"
                                                          data-expired="{{$expired_coupon['expired_date']}}">Expire
                                                        Date:
                                                            @php
                                                                try{
                                                                echo Carbon\Carbon::parse(intval($expired_coupon['expired_date']))->format('Y-m-d');
                                                                } catch (Exception $e) {
                                                                echo date('d-m-Y', $expired_coupon['expired_date']);
                                                                }
                                                            @endphp
                                                    </span>
                                                @endif
                                            @endisset
                                        </p>
                                    </div>
                                    <div class="col-3 pl-0 d-flex justify-content-center align-items-center">
                                        @if($expired_coupon['off_sale'] != 0)
                                            @if($expired_coupon['off_type'] == 'percent')
                                                <div class="discount">{{$expired_coupon['off_sale']}}% Off</div>
                                            @else
                                                <div class="discount">${{$expired_coupon['off_sale']}} Off</div>
                                            @endif
                                        @endif
                                    </div>
                                    <div class="col-9"></div>
                                    <div class="col-md-3 d-flex justify-content-end pr-md-4">
                                        @if($expired_coupon['code_type'] == 'code' && $expired_coupon['coupon_code'] != "")
                                            <div class="show-code">
                                                <a id="a-{{$expired_coupon['coupon_slug']}}"
                                                   class="btn h-btn get-deal-btn a_code" data-aff="{{$link_aff}}"
                                                   href="{{Request::url().'?r=e&cid='.$expired_coupon['coupon_slug']}}"
                                                   rel="nofollow">
                                                    <p id="code-{{$expired_coupon['coupon_slug']}}">{{$expired_coupon['coupon_code']}}</p>
                                                    <span>Show Code</span>
                                                </a>
                                            </div>
                                        @else
                                            <div class="get-deal">
                                                <a id="a-{{$expired_coupon['coupon_slug']}}" class="a_code"
                                                   data-aff="{{$link_aff}}"
                                                   href="{{Request::url().'?r=e&cid='.$expired_coupon['coupon_slug']}}"
                                                   rel="nofollow">
                                                    Get Deal
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if($random_coupons != [])
                        <h4 class="mt-3">Buyers seeking
                            <strong>{{$store_info['store_name']}}</strong> find these coupons as well</h4>
                        @foreach($random_coupons as $random_coupon)
                            <?php
                            $visibility++;
                            if ($random_coupon['store_info']['store_aff'] != '') {
                                $link_aff = $random_coupon['store_info']['store_aff'];
                            } else {
                                $link_aff = $random_coupon['store_info']['store_domain'];
                            }
                            if ($afs <= 3) {
                                if ($visibility % $space_show === 0) {
                                    echo '<div class="shadow-sm rounded coupon mb-2"><div class="box-afs" id="afscontainer' . $afs . '"></div></div>';
                                    $afs++;
                                }
                            } else {
                            if ($adsense < $max_adsense && $visibility % $space_show === 0) {
                            ?>
                            
                            <?php
                            $adsense++;
                            }
                            }
                            ?>
                            <div class="shadow-sm rounded coupon mb-2"
                                 id="{{$random_coupon['coupon_slug']}}">
                                <div class="row d-flex align-items-center">
                                    <div class="col-9 deal-desc">
                                        <h2 class="title">
                                            <a class="a_code" data-aff="{{$link_aff}}"
                                               href="{{route('stores',['store_slug'=>$random_coupon['store_info']['slug_store']]).'?cid='.$random_coupon['coupon_slug']}}"
                                               rel="nofollow">{{str_title($random_coupon['coupon_name'])}}</a>
                                        </h2>
                                        @if($random_coupon['code_status'] == 2)
                                            <p class="mb-2 text-truncate"><span
                                                    class="badge badge-success">Verified</span>
                                                <a data-aff="{{$link_aff}}"
                                                   class="text-primary a_code"
                                                   href="{{route('stores',['store_slug'=>$random_coupon['store_info']['slug_store']]).'?cid='.$random_coupon['coupon_slug']}}">{{Request::url()."?cid=".$random_coupon['coupon_slug']}}</a>
                                            </p>
                                        @else
                                            <p class="mb-2 text-truncate"><span class="badge badge-success">
                                                @if($random_coupon['code_type'] == 'code')
                                                        Code
                                                    @else
                                                        Deal
                                                    @endif
                                            </span> <a data-aff="{{$link_aff}}"
                                                       class="text-primary a_code"
                                                       href="{{route('stores',['store_slug'=>$random_coupon['store_info']['slug_store']]).'?cid='.$random_coupon['coupon_slug']}}">{{Request::url()."?cid=".$random_coupon['coupon_slug']}}</a>
                                            </p>
                                        @endif
                                        <p class="subtitle text-snippet mb-1">{{$random_coupon['coupon_excerpt']}}</p>
                                        <p class="subtitle text-snippet mb-2"><i class="fa fa-user-circle"></i>
                                            {{rand(50, 200)}} People Used @isset($random_coupon['expired_date'])
                                                @if($random_coupon['expired_date'] != '')
                                                    <span class="expire text-muted"
                                                          data-expired="{{$random_coupon['expired_date']}}">Expire
                                                        Date:
                                                            @php
                                                                try{
                                                                echo Carbon\Carbon::parse(intval($random_coupon['expired_date']))->format('Y-m-d');
                                                                } catch (Exception $e) {
                                                                echo date('d-m-Y', $random_coupon['expired_date']);
                                                                }
                                                            @endphp
                                                    </span>
                                                @endif
                                            @endisset
                                        </p>
                                    </div>
                                    <div class="col-3 pl-0 d-flex justify-content-center align-items-center">
                                        @if($random_coupon['off_sale'] != 0)
                                            @if($random_coupon['off_type'] == 'percent')
                                                <div class="discount">{{$random_coupon['off_sale']}}% Off</div>
                                            @else
                                                <div class="discount">${{$random_coupon['off_sale']}} Off</div>
                                            @endif
                                        @endif
                                    </div>
                                    <div class="col-9"></div>
                                    <div class="col-md-3 d-flex justify-content-end pr-md-4">
                                        @if($random_coupon['code_type'] == 'code' && $random_coupon['coupon_code'] != "")
                                            <div class="show-code">
                                                <a id="a-{{$random_coupon['coupon_slug']}}"
                                                   class="btn h-btn get-deal-btn a_code" data-aff="{{$link_aff}}"
                                                   href="{{route('stores',['store_slug'=>$random_coupon['store_info']['slug_store']]).'?cid='.$random_coupon['coupon_slug']}}"
                                                   rel="nofollow">
                                                    <p id="code-{{$random_coupon['coupon_slug']}}">{{$random_coupon['coupon_code']}}</p>
                                                    <span>Show Code</span>
                                                </a>
                                            </div>
                                        @else
                                            <div class="get-deal">
                                                <a id="a-{{$random_coupon['coupon_slug']}}"
                                                   class="a_code" data-aff="{{$link_aff}}"
                                                   href="{{route('stores',['store_slug'=>$random_coupon['store_info']['slug_store']]).'?cid='.$random_coupon['coupon_slug']}}"
                                                   data-cpid="{{$random_coupon['coupon_slug']}}"
                                                   rel="nofollow">
                                                    Get Deal
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                    @if(isset($store_info['rich_content']) && $store_info['rich_content'] != '')
                        
                        <div class="rich-content">
                            <div class="rich-content-p p-3 bg-light">
                                {!! imgALT_Nofollow($store_info['rich_content'], $store_info['store_name']) !!}
                            </div>
                        </div>
                    @endisset
                </section>
            </div>
        </main>
        <div class="modal fade" id="modal-brand-vote" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-dialog-brand-vote shadow-sm rounded" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body text-center px-2">
                        <p class="title mb-3">How was your overall experience?</p>
                        <div class="stars mb-1">
                            <img data-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/1.svg"
                                 data-grey-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/-1.svg"
                                 data-rate-str="Poor" class="js-selected-vote"
                                 src="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/1.svg"
                                 alt="coupon vote">
                            <img data-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/2.svg"
                                 data-grey-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/-2.svg"
                                 data-rate-str="Fair" class="js-selected-vote"
                                 src="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/2.svg"
                                 alt="coupon vote">
                            <img data-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/3.svg"
                                 data-grey-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/-3.svg"
                                 data-rate-str="Good" class="js-selected-vote"
                                 src="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/3.svg"
                                 alt="coupon vote">
                            <img data-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/4.svg"
                                 data-grey-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/-4.svg"
                                 data-rate-str="Very Good" class=""
                                 src="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/-4.svg"
                                 alt="coupon vote">
                            <img data-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/5.svg"
                                 data-grey-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/-5.svg"
                                 data-rate-str="Great" class=""
                                 src="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/-5.svg"
                                 alt="coupon vote">
                        </div>
                        <p class="tip mb-1">Good</p>
                        <form class="js-brand-vote-form mb-2" style="display: none;">
                            <div class="form-group">
                            <textarea class="form-control comment" required="required" rows="3"
                                      placeholder="Write your comment of no less than 20 characters here, so that we could do better."
                                      onkeyup="brandVoteTextareaCounter()"></textarea>
                                <p class="text-count"><i>0</i>/200</p>
                                <p class="text-verify">Input no less than 20 characters</p>
                            </div>
                        </form>
                        <button class="btn btn-outline-primary js-brand-vote-submit mb-1">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if($html_modal)
        {!! $html_modal !!}
    @endif
@endsection
@section('js')
    <script defer type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    
    <script>
        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        }

        $(document).ready(function () {
            $(".a_code").click(function () {
                let ex_link = $(this).attr('href');
                let aff_link = $(this).data('aff');
                window.open(ex_link, '_blank');
                window.open(aff_link, '_self');
                return false;
            });
            if (typeof $('#modal_out') !== "undefined") {
                var cid = getParameterByName('cid');
                $('#close-modal').click(function () {
                    $('#modal_out').hide();
                });
                setTimeout(function () {
                    $('html, body').animate({
                        scrollTop: $("#" + cid).offset().top + 100
                    }, 'slow');
                }, 1000)
            }
            let carousel = $('.owl-carousel').owlCarousel({
                loop: true,
                autoplay: true,
                margin: 10,
                responsiveClass: true,
                animateIn: true,
                animateOut: true,
                lazyLoad: true,
                lazyLoadEager: 2,
                responsive: {
                    0: {
                        items: 2,
                        nav: false
                    },
                    600: {
                        items: 6,
                        nav: false
                    },
                    1000: {
                        items: 15,
                        nav: false,
                        loop: true,
                        slideBy: 2
                    }
                }
            });
            /*carousel.on('changed.owl.carousel', function (event) {
                $("img.lazyload").lazyload();
            });*/
            $("img.lazyload").lazyload();
        });
    </script>
    <div class="modal fade" id="modal-key-coupon" tabindex="-1" role="dialog" aria-labelledby=""
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-key-coupon shadow-sm rounded coupon" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row align-items-center">
                        <div class="col-4">
                            <img class="lazyload thumbnail" id="modal_logo" width="100%"
                                 src="/files/5d64e1fea22d142284003812/img_loading.svg"
                                 alt="Coupon Stores">
                        </div>
                        <div class="col-8">
                            <p class="modal-title" id="modal_title"></p>
                        </div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-center">Copy the code and go to <a class="text-success" id="store_domain"
                                                                      href="#"
                                                                      rel="nofollow" target="_blank"></a></p>
                    <div class="input-group mb-3 cp-code" id="modal_show_code">
                        <input type="text" id="modal_input" class="form-control" value="" readonly="">
                        <div class="input-group-append">
                            <button class="btn btn-outline-success btn-copy"
                                    data-clipboard-target="#modal_input"
                                    id="copy_code" type="button" data-placement="bottom" title="">
                                Tap To Copy
                            </button>
                        </div>
                    </div>
                    <div class="input-group mb-12 cp-code" id="modal_show_deal" style="display: none">
                        <a class="btn btn-block btn-success" id="go_deal" rel="nofollow" target="_blank">Get
                            Deal</a>
                    </div>
                    <p class="text-center"><a class="text-success" id="link-store" href="#">More Green Roads
                            World Coupons
                            &gt;&gt;</a></p>
                </div>
            </div>
        </div>
    </div>
@endsection
