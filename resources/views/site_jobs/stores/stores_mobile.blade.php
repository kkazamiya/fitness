@extends('site_jobs.layouts.job')
@section('title', seo_optimize($seo_config,'title_stores',$store_info))
@section('navbar')
@include('site_jobs.parts.navbar')
@endsection
<?php
if (isset($all_coupons[0]['coupon_name'])) {
    $desc = seo_optimize($seo_config, 'excerpt_stores', $store_info, $all_coupons[0]['coupon_name']);
} else {
    $desc = seo_optimize($seo_config, 'excerpt_stores', $store_info, 'Enjoy');
}
if ($store_info['store_aff'] != '') {
    $link_aff = $store_info['store_aff'];
} else {
    $link_aff = $store_info['store_domain'];
}
?>
<style type="text/css" media="screen">
    .full-text {
    position: relative;
    max-height: 22px;
    overflow: hidden;
}
    .blog-desc{
            font-size: 13px;
            overflow: hidden;
            text-overflow: ellipsis;
            display: -webkit-box;
            -webkit-line-clamp: 3;
            -webkit-box-orient: vertical;
        }
    
</style>
@section('description', $desc)
@section('image', img_sync($store_info['store_image']))
@section('schema')
    <script type="application/ld+json">
    {"@context":"http://schema.org",
    "@type":"Store",
    "name":"{{$store_info['store_name']}}",
    "logo":"{{$store_info['store_image']}}",
    "url":"{{$store_info['store_domain']}}",
    "description":"{{$desc}}",
    "sameAs":"{{$store_info['store_domain']}}",
    "image":"{{img_sync($store_info['store_image'])}}",
    "aggregateRating":
        {"@type":"AggregateRating","ratingValue":"{{$rate_star["avg_rate"]}}
        ","worstRating":1,"bestRating":5,"ratingCount":{{$rate_star["count_rate"]}}}
    }

    </script>
  
@endsection
@section('main_content')
    <div class="container mt-3">
        <main>
            <div class="row">
                <div class="col-12 text-center">
                    <img class="lazyload w-50"
                         data-original="{{img_sync($store_info['store_image'])}}"
                         src="{{asset("/files/5d64e1fea22d142284003812/img_loading.svg")}}"
                         title="{{$store_info['store_name']}} Coupons"
                         alt="{{$store_info['store_name']}} Coupons and Promo Code">
                    <noscript>
                        <img class="w-50" src="{{img_sync($store_info['store_image'])}}"
                             title="{{$store_info['store_name']}} Coupons"
                             alt="{{$store_info['store_name']}} Coupons and Promo Code">
                    </noscript>
                </div>
                <div class="col-12 text-center">
                    <h1 class="mt-0">
                        <small>{{ucwords($store_info['store_name'])}} Coupons</small>
                    </h1>
                </div>
            </div>
            <div class="row">
                <section class="col-12 mt-3">
                    <?php
                    $show = 2;
                    if (count($all_coupons) === 0) {
                        $show = 0;
                    }
                    echo '<script>var search_kw = "' . $store_info['store_name'] . '";</script>';
                    ?>
                    @foreach($all_coupons as $index => $item)
                        <?php
                        if (isset($item['aff_link']) && $item['aff_link'] != '') $coupon_aff = $item['aff_link'];
                        ?>
                        <div class="shadow-sm rounded coupon mb-4" id="{{$item['coupon_slug']}}"
                             @if(isset($coupon_aff)) data-aff="{{$coupon_aff}}"
                             @else data-aff="{{$link_aff}}" @endif>
                            <p class="small mb-1">
                                @if($item['code_status'] == 2)
                                    <span class="text-success pr-3">
                                                <i class="fa fa-check-circle"></i> Verified
                                            </span>
                                @endif
                                @if(isset($item['expired_date']) && $item['expired_date'] != '')
                                    <span class="expire text-muted" data-expired="{{$item['expired_date']}}">
                                                Expire Date:
                                                @php
                                                    try{
                                                    echo Carbon\Carbon::parse(intval($item['expired_date']))->format('Y-m-d');
                                                    } catch (Exception $e) {
                                                    echo date('d-m-Y', $item['expired_date']);
                                                    }
                                                @endphp
                                            </span>
                                @endif
                            </p>
                            <div class="deal-result d-flex justify-content-center py-2">
                                <div class="text-center">
                                    @if($item['off_sale'] != 0)
                                        @if($item['off_type'] == 'percent')
                                            <div class="discount text-success">
                                                {{$item['off_sale']}}<small>%</small> <small> Off</small>
                                            </div>
                                        @else
                                            <div class="discount text-success">
                                                <small>$</small>{{$item['off_sale']}} <small> Off</small>
                                            </div>
                                        @endif
                                    @else
                                        <div class="discount text-success">Sale <small> Off</small></div>
                                    @endisset
                                </div>
                            </div>
                            <h2 style="font-size: 20px;font-weight: 700;">
                                <a href="{{Request::url().'?cid='.$item['coupon_slug']}}"
                                   class="a_code" @if(isset($coupon_aff)) data-aff="{{$coupon_aff}}"
                                   @else data-aff="{{$link_aff}}" @endif
                                   rel="nofollow">{{str_title($item['coupon_name'])}}</a>
                            </h2>
                            <p class="text-muted text-snippet">
                                {{$item['coupon_excerpt']}}
                            </p>
                            <div class="row d-flex justify-content-center">
                                <div class="col-6 px-0">
                                    @if($item['code_type'] == 'code' && $item['coupon_code'] != "")
                                        <div class="show-code">
                                            <a id="a-{{$item['coupon_slug']}}"
                                               class="btn h-btn get-deal-btn a_code"
                                               @if(isset($coupon_aff)) data-aff="{{$coupon_aff}}"
                                               @else data-aff="{{$link_aff}}" @endif
                                               href="{{Request::url().'?cid='.$item['coupon_slug']}}"
                                               rel="nofollow">
                                                <p id="code-{{$item['coupon_slug']}}">{{$item['coupon_code'] }}</p>
                                                <span>Show Code</span>
                                            </a>
                                        </div>
                                    @else
                                        <div class="get-deal">
                                            <a id="a-{{$item['coupon_slug']}}"
                                               href="{{Request::url().'?cid='.$item['coupon_slug']}}"
                                               class="a_code" @if(isset($coupon_aff)) data-aff="{{$coupon_aff}}"
                                               @else data-aff="{{$link_aff}}" @endif
                                               rel="nofollow">
                                                Get Deal
                                            </a>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="shadow-sm rounded coupon mb-4">
                        
                    </div>
                    @if(count($expired_coupons) > 0 )
                        <h3>Expired Coupons</h3>
                        <div class="full-text" id="full-text" style="max-height: 1000px">
                            @foreach($expired_coupons as $expired_coupon)
                                <div class="shadow-sm rounded coupon mb-4"
                                     id="{{$expired_coupon['coupon_slug']}}">
                                    <p class="small mb-1">@if($expired_coupon['code_status'] == 2)
                                            <span class="text-success pr-3">
                                                        <i class="fa fa-check-circle"></i> Verified
                                                    </span>
                                        @endif
                                        @isset($expired_coupon['expired_date'])
                                            @if($expired_coupon['expired_date'] != '')
                                                <span class="expire text-muted"
                                                      data-expired="{{$expired_coupon['expired_date']}}">Expire
                                                        Date:
                                                            @php
                                                                try{
                                                                echo Carbon\Carbon::parse(intval($expired_coupon['expired_date']))->format('Y-m-d');
                                                                } catch (Exception $e) {
                                                                echo date('d-m-Y', $expired_coupon['expired_date']);
                                                                }
                                                            @endphp
                                                        </span>
                                            @endif
                                        @endisset</p>
                                    <div class="deal-result d-flex justify-content-center py-2">
                                        <div class="text-center">
                                            @if($expired_coupon['off_sale'] != 0)
                                                @if($expired_coupon['off_type'] == 'percent')
                                                    <div class="discount text-success">
                                                        {{$expired_coupon['off_sale']}}<small>%</small>
                                                        <small> Off</small>
                                                    </div>
                                                @else
                                                    <div class="discount text-success">
                                                        <small>$</small>{{$expired_coupon['off_sale']}}
                                                        <small> Off</small>
                                                    </div>
                                                @endif
                                            @else
                                                <div class="discount text-success">Sale<small> Off</small>
                                                </div>
                                            @endisset
                                        </div>
                                    </div>
                                    <h2 style="font-size: 20px;font-weight: 700;">
                                        <a href="{{Request::url().'?r=e&cid='.$expired_coupon['coupon_slug']}}"
                                           class="a_code" data-aff="{{$link_aff}}"
                                           rel="nofollow">{{str_title($expired_coupon['coupon_name'])}}</a>
                                    </h2>
                                    <p class="text-muted text-snippet">
                                        {{$expired_coupon['coupon_excerpt']}}
                                    </p>
                                    <div class="row d-flex justify-content-center">
                                        <div class="col-6 px-0">
                                            @if($expired_coupon['code_type'] == 'code' && $expired_coupon['coupon_code'] != "")
                                                <div class="show-code">
                                                    <a id="a-{{$expired_coupon['coupon_slug']}}"
                                                       class="btn h-btn get-deal-btn a_code" data-aff="{{$link_aff}}"
                                                       href="{{Request::url().'?r=e&cid='.$expired_coupon['coupon_slug']}}"
                                                       rel="nofollow">
                                                        <p id="code-{{$expired_coupon['coupon_slug']}}">{{$expired_coupon['coupon_code'] }}</p>
                                                        <span>Show Code</span>
                                                    </a>
                                                </div>
                                            @else
                                                <div class="get-deal">
                                                    <a id="a-{{$expired_coupon['coupon_slug']}}"
                                                       href="{{Request::url().'?r=e&cid='.$expired_coupon['coupon_slug']}}"
                                                       class="a_code" data-aff="{{$link_aff}}"
                                                       rel="nofollow">
                                                        Get Deal
                                                    </a>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <p class="text-center mt-3"><a class="btn btn-outline-info px-3 py-1" href="#"
                                                       id="read-more-expired">Show More Coupons</a></p>
                    @endif
                    @isset($random_stores)
                        <div class="row p-1">
                            <div class="col-6">
                                <h3>New Stores</h3>
                                @foreach($random_stores as $random_store)
                                    <p class="mb-1 text-truncate">
                                        <a href="/store/{{$random_store['slug_store']}}" class="text-primary">
                                            {{ucwords($random_store['store_name'])}}
                                        </a>
                                    </p>
                                @endforeach
                            </div>
                            <div class="col-6">
                                <h3>Keywords</h3>
                                @foreach($random_keywords as $random_keyword)
                                    <p class="mb-1 text-truncate">
                                        <a href="/{{$random_keyword['keyword_slug']}}" class="text-primary">
                                            {{ucwords($random_keyword['keyword'])}}
                                        </a>
                                    </p>
                                @endforeach
                            </div>
                        </div>
                    @endisset
                    @isset($random_coupons)
                        <h4 class="mt-3">Buyers seeking
                            <strong>{{$store_info['store_name']}}</strong> find these coupons as well</h4>
                        @foreach($random_coupons as $random_coupon)
                            <?php
                            if ($random_coupon['store_info']['store_aff'] != '') {
                                $link_aff = $random_coupon['store_info']['store_aff'];
                            } else {
                                $link_aff = $random_coupon['store_info']['store_domain'];
                            }
                            ?>
                            <div class="shadow-sm rounded coupon mb-4" id="{{$random_coupon['coupon_slug']}}">
                                <p class="small mb-1">@if($random_coupon['code_status'] == 2)
                                        <span class="text-success pr-3">
                                                        <i class="fa fa-check-circle"></i> Verified
                                                    </span>
                                    @endif
                                    @isset($random_coupon['expired_date'])
                                        @if($random_coupon['expired_date'] != '')
                                            <span class="expire text-muted"
                                                  data-expired="{{$random_coupon['expired_date']}}">Expire
                                                        Date:
                                                            @php
                                                                try{
                                                                echo Carbon\Carbon::parse(intval($random_coupon['expired_date']))->format('Y-m-d');
                                                                } catch (Exception $e) {
                                                                echo date('d-m-Y', $random_coupon['expired_date']);
                                                                }
                                                            @endphp
                                                        </span>
                                        @endif
                                    @endisset</p>
                                <div class="deal-result d-flex justify-content-center py-2">
                                    <div class="text-center">
                                        @if($random_coupon['off_sale'] != 0)
                                            @if($random_coupon['off_type'] == 'percent')
                                                <div class="discount text-success">
                                                    {{$random_coupon['off_sale']}}<small>%</small><small>
                                                        Off</small>
                                                </div>
                                            @else
                                                <div class="discount text-success">
                                                    <small>$</small>{{$random_coupon['off_sale']}}
                                                    <small> Off</small>
                                                </div>
                                            @endif
                                        @else
                                            <div class="discount text-success">Sale<small> Off</small></div>
                                        @endisset
                                    </div>
                                </div>
                                <h2 style="font-size: 20px;font-weight: 700;">
                                    <a href="{{route('stores',['store_slug'=>$random_coupon['store_info']['slug_store']]).'?cid='.$random_coupon['coupon_slug']}}"
                                       class="a_code" data-aff="{{$link_aff}}"
                                       rel="nofollow">{{str_title($random_coupon['coupon_name'])}}</a>
                                </h2>
                                <p class="text-muted text-snippet">
                                    {{$random_coupon['coupon_excerpt']}}
                                </p>
                                <div class="row d-flex justify-content-center">
                                    <div class="col-6 px-0">
                                        @if($random_coupon['code_type'] == 'code' && $random_coupon['coupon_code'] != "")
                                            <div class="show-code">
                                                <a id="a-{{$random_coupon['coupon_slug']}}"
                                                   class="btn h-btn get-deal-btn a_code" data-aff="{{$link_aff}}"
                                                   href="{{route('stores',['store_slug'=>$random_coupon['store_info']['slug_store']]).'?cid='.$random_coupon['coupon_slug']}}"
                                                   rel="nofollow">
                                                    <p id="code-{{$random_coupon['coupon_slug']}}">{{$random_coupon['coupon_code'] }}</p>
                                                    <span>Show Code</span>
                                                </a>
                                            </div>
                                        @else
                                            <div class="get-deal">
                                                <a id="a-{{$random_coupon['coupon_slug']}}"
                                                   href="{{route('stores',['store_slug'=>$random_coupon['store_info']['slug_store']]).'?cid='.$random_coupon['coupon_slug']}}"
                                                   class="a_code" data-aff="{{$link_aff}}"
                                                   rel="nofollow">
                                                    Get Deal
                                                </a>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endisset
                    @isset($store_info['rich_content'])
                        <div class="rich-content">
                            <div>
                                <div class="rich-content-p p-3 bg-light">
                                    {!! imgALT_Nofollow($store_info['rich_content'], $store_info['store_name']) !!}
                                </div>
                            </div>
                        </div>
                    @endisset
                </section>
                <div class="col-md-3 bi">
                    <div class="shadow-sm lp">
                        <p class="p-3 mb-0 font-weight-bold" style="font-size: 15px;">How to
                            use {{$store_info['store_name']}} coupons</p>
                        <div class="border-top links" style="font-size:14px;">
                            <span class="badge badge-success">1</span> - Click to the <strong>Show Code</strong> or
                            <strong>Get Deal</strong> to copy the code.
                            <br/><br/>
                            <span class="badge badge-success">2</span> - Go to <a
                                href="{{$link_aff}}"
                                title="{{$store_info['store_name']}} Coupons"
                                rel="nofollow noopener">{{$store_info['store_domain']}}</a> Website and remember to
                            paste the coupon code copied to the discount/ coupon box at check
                            out page
                            <br/><br/>
                            <span class="badge badge-success">3</span> - Wait for the discount and Enjoy your order
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="shadow-sm lp">
                        <p class="p-3 mb-0 font-weight-bold" style="font-size: 15px;">Tips to try
                            when {{$store_info['store_name']}} Coupon Code didn't work</p>
                        <div class="border-top links" style="font-size:14px;">
                            <span class="text-primary"><i class="fa fa-user"></i></span> Remove the
                            <strong>Space</strong> after the code: Some coupons only work with exact code.<br/><br/>
                            <span class="text-primary"><i
                                    class="fa fa-user"></i></span> Check the products you're seeking and make
                            sure the code you take fit with the products you chose. Some coupon codes will only be valid
                            for certain products/ item<br/><br/>
                            <span class="text-primary"><i
                                    class="fa fa-user"></i></span><span> Some of the coupon codes are provided by our users and it may only be used for special products or events. You can contact us to have the best support for this</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="shadow-sm lp">
                        <p class="p-3 mb-0 font-weight-bold" style="font-size: 15px;">Why I should find
                            <strong>{{$store_info['store_name']}}</strong> coupon codes on fitnessol.com</p>
                        <div class="border-top links" style="font-size:14px;">
                            <span class="text-danger"><i class="fa fa-check-circle"></i></span> All the coupon codes on
                            this website are checked with an automated system combined with our dedicated team<br/><br/>
                            <span class="text-danger"><i class="fa fa-check-circle"></i></span> Latest Coupons,
                            discounts and vouchers will be updated automatically and regularly <br/><br/>
                            <span class="text-danger"><i class="fa fa-check-circle"></i></span> Friendly interface for
                            coupon seekers <br/><br/>
                            <span class="text-danger"><i class="fa fa-check-circle"></i></span> fat, simple and
                            effecitve <br/><br/>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="shadow-sm lp">
                        <p class="p-3 mb-0 font-weight-bold" style="font-size:20.0pt">Saving Tips</p>

                        @foreach($rand_blogs as $index => $blog)
                                <div class="blog-box p-0 mb-2 border-0 w-100">
                                    <div class="d-flex justify-content-center">
                                        <img data-original="{{$blog['featured_img']}}"
                                             src="/files/5d64e1fea22d142284003812/img_loading.svg"
                                             alt="{{$blog['title']}}" class="card-img-top lazyload">
                                    </div>
                                    <div class="p-2">
                                        <h5 class="blog-title">
                                            <a href="/blog/{{$blog['slug']}}">{{ucwords($blog['title'])}}</a>
                                        </h5>
                                        <p class="blog-desc">{{str_replace('&nbsp;','',strip_tags($blog['content']))}}</p>
                                        <a href="/blog/{{$blog['slug']}}"
                                           class="btn btn-block btn-outline-info btn-sm btn-read-more">Read More</a>
                                    </div>
                                </div>
                               
                            @endforeach
                    </div>
                </div>
            </div>
        </main>
        <div class="modal fade" id="modal-brand-vote" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
            <div class="modal-dialog modal-dialog-brand-vote shadow-sm rounded" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body text-center px-2">
                        <p class="title mb-3">How was your overall experience?</p>
                        <div class="stars mb-1">
                            <img data-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/1.svg"
                                 data-grey-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/-1.svg"
                                 data-rate-str="Poor" class="js-selected-vote"
                                 src="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/1.svg"
                                 alt="Voted Coupons">
                            <img data-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/2.svg"
                                 data-grey-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/-2.svg"
                                 data-rate-str="Fair" class="js-selected-vote"
                                 src="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/2.svg"
                                 alt="Voted Coupons">
                            <img data-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/3.svg"
                                 data-grey-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/-3.svg"
                                 data-rate-str="Good" class="js-selected-vote"
                                 src="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/3.svg"
                                 alt="Voted Coupons">
                            <img data-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/4.svg"
                                 data-grey-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/-4.svg"
                                 data-rate-str="Very Good" class=""
                                 src="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/-4.svg"
                                 alt="Voted Coupons">
                            <img data-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/5.svg"
                                 data-grey-img="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/-5.svg"
                                 data-rate-str="Great" class=""
                                 src="https://s3.amazonaws.com/www.couponscdn.com/project-brand/image/vote/-5.svg"
                                 alt="Voted Coupons">
                        </div>
                        <p class="tip mb-1">Good</p>
                        <form class="js-brand-vote-form mb-2" style="display: none;">
                            <div class="form-group">
                            <textarea class="form-control comment" required="required" rows="3"
                                      placeholder="Write your comment of no less than 20 characters here, so that we could do better."
                                      onkeyup="brandVoteTextareaCounter()"></textarea>
                                <p class="text-count"><i>0</i>/200</p>
                                <p class="text-verify">Input no less than 20 characters</p>
                            </div>
                        </form>
                        <button class="btn btn-outline-success js-brand-vote-submit mb-1">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid text-center">
        <div class="owl-carousel owl-theme mt-3">
            @foreach($brand_top as $key => $brand)
                <div class="col mb-3 px-2">
                    <a href="/store/{{$brand->slug_store}}"
                       title="{{$brand['store_name']}} Coupons and Promo Code">
                        <div class="square d-flex align-items-center border border-gray">
                            <img class="img-thumbnail border-0 align-middle w-100 lazyload"
                                 data-original="{{img_sync($brand['store_image'])}}"
                                 src="/files/5d64e1fea22d142284003812/img_loading.svg"
                                 alt="{{$brand['store_name']}} Coupons and Promo Code">
                        </div>
                    </a>
                    <p class="p-0 text-center">
                        <small><span class="text-danger">{{rand(10,50)}}</span> Coupons</small>
                    </p>
                </div>
            @endforeach
        </div>
    </div>
    @if($html_modal)
        {!! $html_modal !!}
    @endif
@endsection
@section('js')
    <script defer type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <script>
        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        }
        $("#read-more-expired").click(function () {
            let t = 22;
            console.log($(this).outerHeight());
            return (
                ($ps = $("#full-text").find(".coupon")),
                $ps.each(function () {
                    t += $(this).outerHeight();
                }),
                $("#full-text")
                    .css({ height: $("#full-text").height(), "max-height": 9999 })
                    .animate({ height: t }),
                $(this).hide(),
                !1
            );
        });
        
        $(document).ready(function () {
            $(".a_code").click(function () {
                let ex_link = $(this).attr('href');
                let aff_link = $(this).data('aff');
                window.open(ex_link, '_blank');
                window.open(aff_link, '_self');
                return false;
            });
            if (typeof $('#modal_out') !== "undefined") {
                var cid = getParameterByName('cid');
                $('#close-modal').click(function () {
                    $('#modal_out').hide();
                });
                setTimeout(function () {
                    $('html, body').animate({
                        scrollTop: $("#div-" + cid).offset().top + 100
                    }, 'slow');
                }, 1000)
            }
            let carousel = $('.owl-carousel').owlCarousel({
                loop: true,
                autoplay: true,
                margin: 5,
                responsiveClass: true,
                animateIn: true,
                animateOut: true,
                lazyLoad: true,
                lazyLoadEager: 2,
                responsive: {
                    0: {
                        items: 2,
                        nav: false
                    },
                    600: {
                        items: 6,
                        nav: false
                    },
                    1000: {
                        items: 15,
                        nav: false,
                        loop: true,
                        slideBy: 2
                    }
                }
            });
            carousel.on('changed.owl.carousel', function (event) {
                $("img.lazyload").lazyload();
            });
        });
    </script>
    <div class="modal fade" id="modal-key-coupon" tabindex="-1" role="dialog" aria-labelledby=""
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-key-coupon shadow-sm rounded coupon" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="row align-items-center">
                        <div class="col-4">
                            <img class="thumbnail" id="modal_logo" width="100%"
                                 src="data:image/webp;base64,UklGRrYHAABXRUJQVlA4IKoHAAAQIwCdASqMAFEAPm00lEckIyIhJ7sauIANiWMDf8/72DaF9p0Xjt4xMK3cDsT34S+Kr0d7ScreJH1r/kP6x+0vyL+4rwf4AXpv/IbyeAD84/ov+a/rvrs/M/6T0A+tHlX+t3eteLewB+av+D6fX/R/kfOz9N/9j3C/5b/X/+R/eO1J+0ns7/sYY94CKQjDFeQSRdRVHH/rh+9Cnpw1hKc0vW7Vwe+94WXLN3vsMiIPwkzJieuArZSeQVMcDy2mzXKhXewzhislHA92STOXEcZnOHChdezGYQHhAgcRA5NAfYC45GYRYJ5tFc/hZeZ3hpEU+S+8SjjWZJYXCNWosK6moTw2nnOq6dJLKcjjMjzhFPCd2+F06dPeFik9kvbpmejAAP78qAO4UEJkwimkDxLy6pgY9y0j9zu9S7RWbUAEHBQjt4Ra6gUL/Khc6N+81qT8AuNnqAHC+G5CqP9NSr4ItLujVkQg966LHAUgCVLZxbL9u49BUYrOUTD/NlGoArGIimpQGN1J186QnECsnKMY3eNlyGbZkptme4Wg+faPL7waDYp1k6LpdX/kKX8NfinZuD2mJX2qXvkDIdmb5Xu3rD6heGux4Pg3AiQmUJcdE6OeMg4hdWW4puM5qivD/CcMdQbloX5jf9z68MLe7QBnMzVlR7+PyPoU3Ttfr5Ys3hzVIy8bYzuGSse1JBNt58DwTr6CojjcCVeMuL+RI/rS/yorB3VcH4a8gTY5OG5gkG+fP5yboxbTjMslCOOXLpkAHse1uiMNKh4pNbzxmqINpEle4rODLpL/GBH4OXStBi6VOPc2hxOo2FLLV0wBjUnIlVKMxOV0kRQKGiWzrWUobL+IlXvIrHxGy+uuNq/IYt/WUwK3/pV2fyQuXkYOHLF8CJ6gNLBlYjgmr67X+Tsu0RrPZ2s/uB/UlnIq0OPjPMZL1oG3Ds6sbtwB3Z4BCYCxAoV6jjqvHuX53PucuNOxiSRp2W5gyPtRi6MnNOL08YSg6tEvom8j/hjlkBvfw0GdaSxrr+fZtvKyq/lfxEk4CJLC72xhWEpDVLDfXn2m8E604Z341FMMLlf/5e/HPKk062I95/QL+1krVfFE0NVARea0FSgtIxkkhiuVi5GGNLP3dBPm+NsnJUwx8gxr5iXa5Doc4LxaRGtOX9z8ivarOuce/GFdOk9KMicgVdWGpYJsz04Zi16VcK3+qXOMLnJ6ImiNHI1EZ6BJq30spY7lMbAIap3IqWqTLq2Ou7ctSGW/033/1hCZb7nxsReqYysUqRqcGrcTW7v9WueDp8/1G2nDr2XazTGom3cH5u+IXK0Pf7KcYzSKXhJjxApWMVgHH/BVKDqTElyXNXcOZP2k7WuO/LvSid78JwgjWd7ZG0zIQ9Devk/RjYMad+x8+XhPXBDl2aRKYKjp+m0oS1NtugxyjudJfSht574B1ya18+wPFV5dleeVW5dF4oPz1Xcv2Gy9KvbVO03jqo8icesVLjEw1bI8Ke4wm8HyFIf7kWCSR2KB+chrILRmTe6kuLK/9gES/nOe3+ZhsDTWslsjE9yGv459lxkXP3/vjXrH7GYLymKP/dXCUopWQrPlIrbesK86swt7IRmySQ11GHMFe6Q3W0Xi5nYIv6gm6lckr78qIYUZgMvsKVRnkfUasWC6TYAYqew4dUTzXBEAvk2HMkH8++yFJJrvaybWD53e638zEdNDoZs+bTfqhNAr+NbutqVef8EQpssWiiTEBzlzqOdSQwCcnAYhwCmccjxqCMUu2TJuhd6EOsAF42x5nYBjfFfknXBKXT0/lf+l6W7iTq/CfTUOn20U8HYif8eWhLvU0Qcw/CL9hT7UD4Sgr4DB61k22QVNpPJadHIeCpjQPWo2ulDDxMsYAQRgYuj3lM6pCdlqletWj12rrkaCP5GRW1Ol1jNqxoG5EWKrPfo9HDtbBfRhlg4wIfH2cqkTil4NOKxcZtUljztU0ZM0kZbFUbRQ+PlR2SRgjy7M2GUuWW3jH9PfYXU0P2DzzfguRGhQVuLUNuqTea32QSchFApAoHB4SXZkXzslCvVf8s/fsHpgzaiDk0eZCF6qDfBwWUPjUYH3q+QdSQd4uXvT5Bp6Mpp1A8sWLPKO3TaUaOF/jwI1KKl7u5xfimXWj4vJp3d535muFun/K8CvzMOy65esOa8TuXSvFQR1mM9GQQ+iPaVRF3KrJ/bzIZf6t0kWEmNl9wzrQ+4uS3bb8mE2i07uLU/eV4Q+EoR9UHlDBHAAB3rJMdVjhFJyOazQ/FhDOA4Cz7LzaBzukSvLly4emLafOhihNy6WB6NE32sPciST6yeCZvm8n2jAGlvuGLSYlfXSmY2233dQGXUgifEuLGrnZCSfJtyH59lfadekpzSXR5vIy1R1RcslTFgW1SkO2WXPZ2uFVXMUYuHOADixqdLYPWAHcZXtsVzarQ0ZGyU3zXP0wvZ2FZM2J5XHowBAMV6G6n+LtN4cKsmc/mk3Jtww72/VztJlY2c914hzpj5YnUpxrbE97+1gll5OhNd3IbV4CPmx4LH/dLoBmBMsixIDvm+3hbC2mg6HVHixtlBKUtEDp7pqQB7K9zlo+ZJaarNNgDdiioAAAAA="
                                 alt="Go to Coupon Stores">
                        </div>
                        <div class="col-8">
                            <p class="modal-title" id="modal_title"></p>
                        </div>
                    </div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-center">Copy the code and go to <a class="text-success" id="store_domain"
                                                                      href="#"
                                                                      rel="nofollow" target="_blank"></a></p>
                    <div class="input-group mb-3 cp-code" id="modal_show_code">
                        <input type="text" id="modal_input" class="form-control" value="" readonly="">
                        <div class="input-group-append">
                            <button class="btn btn-outline-success btn-copy"
                                    data-clipboard-target="#modal_input"
                                    id="copy_code" type="button" data-placement="bottom" title="">
                                Tap To Copy
                            </button>
                        </div>
                    </div>
                    <div class="input-group mb-12 cp-code" id="modal_show_deal" style="display: none">
                        <a class="btn btn-block btn-success" id="go_deal" rel="nofollow" target="_blank">Get
                            Deal</a>
                    </div>
                    <p class="text-center"><a class="text-success" id="link-store" href="#">More Green Roads
                            World Coupons
                            &gt;&gt;</a></p>
                </div>
            </div>
        </div>
    </div>
@endsection
