<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@_index')->name('home');

Route::get('/search', 'Keyword\KeywordViewController@_searchKeyword')->name('search');
Route::get('/autocomplete', 'HomeController@_autocomplete')->name('search_autocomplete');
Route::post('/related-keyword', 'Keyword\KeywordViewController@_relatedKeyword');
// add route post_faq
Route::post('/faq/{id_kw}/{id_q}', 'Keyword\KeywordViewController@_postFAQs')->name('post_faq');

Route::get('/more-info/{domain?}', 'Keyword\KeywordViewController@_moreOffers');

Route::get('/v-result/{result_id?}', 'Keyword\KeywordViewController@_outLink')->name('result_out');

Route::get('/blog/{slug?}', 'Posts\PostViewController@_blog')->name('blog');
Route::post('/saving-email', 'HomeController@_savingEmail')->name('saveEmail');

Route::get('/sitemap-job/xml/{file_name}', 'SiteMapController@_sitemapDynamic');

Route::get('/store/{slug}','Store\StoreViewController@stores')->name('stores');

Route::get('/clear-cache-ar', function () {
    $exitCode = \Illuminate\Support\Facades\Artisan::call('config:clear');
    $exitCode = \Illuminate\Support\Facades\Artisan::call('cache:clear');
    return 'DONE'; //Return anything
});
Auth::routes();
Route::group(['prefix' => 'c-panel', 'as' => 'cpanel.'], function () {
    Route::get('/', 'DashboardController@index')->name('home-dashboard');
    Route::get('/clear-cache', function () {
        $exitCode = \Illuminate\Support\Facades\Artisan::call('cache:clear');
        return view('cpanel.cache_clear');
    });

    Route::resource('tags', 'Tags\CmsTagsController');
    Route::post('tags/{id}', 'Tags\CmsTagsController@update');
    Route::post('tags-table', 'Tags\CmsTagsController@index')->name('tags.table');

    Route::resource('tags_kw', 'Tags\TagsKWController');
    Route::post('tags_kw/{id}', 'Tags\TagsKWController@update');
    Route::post('tags_kw-effect', 'Tags\TagsKWController@_checkEffect');
    Route::post('tags_kw-table', 'Tags\TagsKWController@index')->name('tags_kw.table');

    Route::resource('emails', 'EmailController');
    Route::get('email-send', 'EmailController@_sendEmail')->name('email.get-send');
    Route::post('email-send', 'EmailController@_sendEmail')->name('email.post-send');
    Route::post('emails-table', 'EmailController@show_svs')->name('emails.show_table');

    Route::resource('categories', 'CategoriesController');
    Route::post('ajax-categories', 'CategoriesController@show_svs')->name('svs_categories');

    Route::resource('config-site', 'CmsConfigController');
 
    Route::resource('keywords', 'Keyword\CmsKeysController');
    Route::post('keywords/{id}', 'Keyword\CmsKeysController@update');
    Route::get('add-kw-excel', 'Keyword\CmsKeysController@_vAddExcel');
    Route::post('add-kw-excel', 'Keyword\CmsKeysController@_addExcel')->name('add_kw_excel');
    Route::post('add-kw-txt', 'Keyword\CmsKeysController@_addTXT');

    Route::get('keyword-remove', 'Keyword\CmsKeysController@_removeKw')->name('get-remove-keyword');
    Route::post('keyword-remove', 'Keyword\CmsKeysController@_removeKw')->name('post-remove-keyword');
    Route::post('ajax-keyword-black', 'BlacklistController@show_svs_kw')->name('svs_keyword_black');

    Route::post('ajax-keywords', 'Keyword\CmsKeysController@show_svs')->name('svs_keywords');
    Route::get('add-connect', 'Keyword\CmsKeysController@_vAddCUT');
    Route::post('add-connect', 'Keyword\CmsKeysController@_pAddCUT')->name('add_connect');
    Route::get('add-connect-store', 'Keyword\CmsKeysController@_connectToStore');
    Route::post('add-connect-store', 'Keyword\CmsKeysController@_pConnectToStore')->name('add_connect_store');

    Route::post('/daterange/kw_crawled', 'Keyword\CmsKeysController@kw_crawled')->name('daterange.kw_crawled');

    Route::resource('posts', 'Posts\CmsPostController');
    Route::post('posts/{id}', 'Posts\CmsPostController@update');
    Route::post('ajax-posts', 'Posts\CmsPostController@show_svs')->name('svs_posts');

    

    
    Route::get('add-excel', 'Store\CmsStoreController@add')->name('stores.add_excel');
    Route::post('add-excel/stores', 'Store\CmsStoreController@_addTXT')->name('stores.add_excel_post');
    Route::get('export-excel', 'Store\CmsStoreController@export')->name('stores.export_excel');
    Route::post('export-excel', 'Store\CmsStoreController@_pexportAllURL')->name('stores.p_export');
    Route::post('store-remove/{id}', 'Store\CmsStoreController@destroy')->name('stores.destroy');
    Route::resource('stores', 'Store\CmsStoreController');
    Route::post('stores/{id}', 'Store\CmsStoreController@update');
    Route::post('ajax-stores', 'Store\CmsStoreController@svs_store')->name('all_stores');
    // route update stt
    Route::post('/update-store-stt','Store\CmsStoreController@_updateStt');


    Route::resource('syncs', 'Store\SyncsController');
    Route::post('syncs/{id}', 'Store\SyncsController@update');
    Route::post('all-sync','Store\SyncsController@stores_svs')->name('all_sync');

    Route::post('/sync-one','Store\SyncsController@_SyncOne');
    Route::post('/sync-start','Store\SyncsController@_startSync');
    Route::get('/sync-start','Store\SyncsController@_gstartSync');

    Route::resource('blacklist', 'BlacklistController');
    Route::post('ajax-blacklist', 'BlacklistController@show_svs')->name('svs_blacklist');

    Route::get('clear-cache/{cache_option?}/{slug_cache?}', 'CacheController@_clearCache');

    Route::get('run-bot', 'ScraperController@_index')->name('run_bot_keywords');

    Route::post('other-get-info', 'ScraperController@_getOtherInfo');
    Route::post('track-control', 'ScraperController@_trackControl');
    Route::post('track-bing', 'ScraperController@_bingPost')->name('bing_scraper');
    Route::post('track-google', 'ScraperController@_googleScraper')->name('google_scraper');
    Route::post('track-amz', 'ScraperController@_amzScraper')->name('amz_scraper');

    Route::get('bot-multi', 'ScraperController@_vBotMulti')->name('bot_multi');
    Route::post('get-keyword', 'ScraperController@_getKeyword');
    Route::post('add-2-sitemap', 'ScraperController@_add2Sitemap');

    Route::get('file-manager', '\UniSharp\LaravelFilemanager\Controllers\LfmController@show');
    Route::post('file-manager/upload', '\UniSharp\LaravelFilemanager\Controllers\UploadController@upload');

});

Route::get('/{slug?}', 'Keyword\KeywordViewController@_viewKeyword')->name('v_keyword');

/*Migration DB
php artisan migrate --path=/database/migrations/2019_08_28_152107_create_categories_collection.php
php artisan migrate --path=/database/migrations/2019_08_28_150242_create_spam_keyword_collection.php
php artisan migrate --path=/database/migrations/2019_08_28_152854_create_results_collection.php
php artisan migrate --path=/database/migrations/2019_08_28_151618_create_keyword_collection.php
php artisan migrate --path=/database/migrations/2019_09_13_131221_create_coupons_collection.php
php artisan migrate --path=/database/migrations/2019_09_13_135230_create_keys_related.php
db.categories.getIndexes();
*/
